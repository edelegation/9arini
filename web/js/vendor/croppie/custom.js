function croppieUpload() {
    var $uploadCrop;
    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result,
                    enableExif: true,
                    enableOrientation: false
                }).then(function () {
                    console.log('jQuery bind complete');
                });

            }


            reader.readAsDataURL(input.files[0]);
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 200,
            height: 200
        },
        enableExif: true,
        enableOrientation: false,
        enforceBoundary: true,
        boundary: {
            width: 300,
            height: 300
        }
    });;

    $('#listing_image_file, #user_image_file').on('change', function () {
        readFile(this);
    });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $('#avatar').attr('src', resp);
            $('#listing_image_crop_img').val(resp);
            $('#user_image_crop_img').val(resp);
            $('.upload-demo').removeClass('ready');

        });
    });
}

croppieUpload();