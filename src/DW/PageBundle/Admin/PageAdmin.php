<?php

namespace DW\PageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PageAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';
    protected $baseRoutePattern = 'page';

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Page')
            ->add(
                'title',
                'text',
                array(
                    'label' => 'Title',
                    'required' => true
                )
            )
            ->add(
                'description',
                'ckeditor',
                array(
                    'label' => 'Description',
                    'required' => true,
                    'config' => array(
                        'filebrowser_image_browse_url' => array(
                            'route' => 'elfinder',
                            'route_parameters' => array('instance' => 'ckeditor'),
                        ),
                    )
                )
            )
            ->add(
                'metaTitle',
                'text',
                array(
                    'label' => 'Meta Title',
                    'required' => true
                )
            )
            ->add(
                'metaDescription',
                'textarea',
                array(
                    'label' => 'Meta Description',
                    'required' => true
                )
            )
            ->add(
                'slug',
                'text',
                array(
                    'disabled' => true
                )
            )
            ->add(
                'published',
                null,
                array(
                    'label' => 'admin.page.published.label'
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.page.created_at.label'
                )
            )
            ->add(
                'updatedAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.page.updated_at.label'
                )
            )
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'title',
                null,
                array('label' => 'admin.page.title.label')
            )
            ->add(
                'description',
                null,
                array('label' => 'admin.page.description.label')
            )
            ->add(
                'published',
                null,
                array('label' => 'admin.page.published.label')
            )
            ->add(
                'createdAt',
                'doctrine_orm_callback',
                array(
                    'label' => 'admin.page.created_at.label',
                    'callback' => function ($queryBuilder, $alias, $field, $value) {
                        /** @var \DateTime $date */
                        $date = $value['value'];
                        if (!$date) {
                            return false;
                        }

                        $queryBuilder
                            ->andWhere("DATE_FORMAT($alias.createdAt,'%Y-%m-%d') = :createdAt")
                            ->setParameter('createdAt', $date->format('Y-m-d'));

                        return true;
                    },
                    'field_type' => 'sonata_type_date_picker',
                    'field_options' => array('format' => 'dd/MM/yyyy'),
                ),
                null
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add(
                'title',
                null,
                array('label' => 'admin.page.title.label')
            )
            ->add(
                'description',
                'html',
                array(
                    'label' => 'admin.page.description.label',
                    'truncate' => array(
                        'length' => 100,
                        'preserve' => true
                    )
                )
            )
            ->add(
                'published',
                null,
                array(
                    'editable' => true,
                    'label' => 'admin.page.published.label'
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'label' => 'admin.page.created_at.label',
                    'format' => 'd/m/Y'
                )
            );

        $listMapper->add(
            '_action',
            'actions',
            array(
                'actions' => array(
//                    'create' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            )
        );
    }

    public function getExportFields()
    {
        return array(
            'Id' => 'id',
            'Title' => 'title',
            'Description' => 'description',
            'Published' => 'published',
            'Created At' => 'createdAt'
        );
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $dataSourceIt = $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
        $dataSourceIt->setDateTimeFormat('d M Y'); //change this to suit your needs
        return $dataSourceIt;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions["delete"]);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
//        $collection->remove('create');
//        $collection->remove('delete');
    }


}
