<?php

namespace DW\PageBundle\Repository;

use DW\PageBundle\Entity\PageTranslation;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class PageRepository extends EntityRepository
{
    /**
     * @param $slug
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlug($slug)
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->where('p.slug = :slug')
            ->andWhere('p.published = :published')
            ->setParameter('slug', $slug)
            ->setParameter('published', true);
        try {
            $query = $queryBuilder->getQuery();

            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @return array|null
     */
    public function findAllPublished()
    {
        $queryBuilder = $this->createQueryBuilder('p')
            ->addSelect("t")
            ->leftJoin('p.translations', 't')
            ->andWhere('p.published = :published')
            ->setParameter('published', true)
            ->orderBy('p.id', 'ASC')
            ->addOrderBy('t.locale', 'ASC');
        try {
            $query = $queryBuilder->getQuery();

            return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return null;
        }
    }

}
