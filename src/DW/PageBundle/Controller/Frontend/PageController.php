<?php

namespace DW\PageBundle\Controller\Frontend;

use DW\PageBundle\Repository\PageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Page frontend controller.
 *
 * @Route("/page")
 */
class PageController extends Controller
{
    /**
     * show page depending upon the slug available.
     *
     * @Route("/{slug}", name="dw_page_show")
     *
     * @Method("GET")
     *
     * @param  Request $request
     * @param  string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var PageRepository $page */
        $page = $em->getRepository('DWPageBundle:Page')->findOneBySlug($slug);
        if (!$page) {
            throw new NotFoundHttpException(sprintf('%s page not found.', $slug));
        }

        $this->get('dw.helper.global')->log($request->getHttpHost());

        return $this->render(
            '@DWPage/Frontend/Page/show.html.twig',
            array(
                'page' => $page
            )
        );

    }
}
