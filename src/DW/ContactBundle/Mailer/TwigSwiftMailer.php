<?php

namespace DW\ContactBundle\Mailer;

use DW\ContactBundle\Entity\Contact;

class TwigSwiftMailer implements MailerInterface
{
    protected $mailer;
    protected $twig;
    protected $parameters;
    protected $templates;
    protected $fromName;
    protected $fromEmail;
    protected $contactEmail;

    /**
     * @param \Swift_Mailer     $mailer
     * @param \Twig_Environment $twig
     * @param array             $parameters
     * @param array             $templates
     */
    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $twig,
        array $parameters,
        array $templates
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;

        /** parameters */
        $this->parameters = $parameters['parameters'];
        $this->fromName = $parameters['parameters']['dw_contact_from_name'];
        $this->fromEmail = $parameters['parameters']['dw_contact_from_email'];
        $this->contactEmail = $parameters['parameters']['dw_contact_contact_email'];
        $this->templates = $templates;
    }

    /**
     * @param Contact $contact
     */
    public function sendContactMessage(Contact $contact)
    {
        $template = $this->templates['templates']['contact_message'];

        $context = array(
            'contact' => $contact,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $this->contactEmail);
    }


    /**
     * @param string $templateName
     * @param array $context
     * @param string $fromEmail
     * @param string $fromName
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $fromName, $toEmail)
    {
        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($templateName);
        $context = $this->twig->mergeGlobals($context);

        $subject = $template->renderBlock('subject', $context);
        $context["message"] = $template->renderBlock('message', $context);

        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom([$fromEmail => $fromName])
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message
                ->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);

        $this->mailer->getTransport()->stop();
    }

}
