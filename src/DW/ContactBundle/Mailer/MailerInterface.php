<?php

namespace DW\ContactBundle\Mailer;

use DW\ContactBundle\Entity\Contact;

interface MailerInterface
{
    /**
     * email is sent to admin
     *
     * @param Contact $contact
     *
     * @return void
     */
    public function sendContactMessage(Contact $contact);
}
