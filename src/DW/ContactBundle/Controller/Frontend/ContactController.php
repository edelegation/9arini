<?php

namespace DW\ContactBundle\Controller\Frontend;

use DW\ContactBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Booking controller.
 *
 * @Route("/contact")
 */
class ContactController extends Controller
{
    /**
     * Creates a new Contact entity.
     *
     * @Route("/new", name="dw_contact_new")
     *
     * @Method({"GET", "POST"})
     *
     * @param  Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->createCreateForm($contact);

        $submitted = $this->get('dw_contact.form.handler.contact')->process($form);
        if ($submitted !== false) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('contact.new.success', array(), 'dw_contact')
            );

            return $this->redirect($this->generateUrl('dw_contact_new'));
        }

        return $this->render(
            'DWContactBundle:Contact:index.html.twig',
            array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * Creates a form to create a contact entity.
     *
     * @param Contact $contact The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Contact $contact)
    {
        $form = $this->get('form.factory')->createNamed(
            'contact_new',
            'contact_new',
            $contact,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_contact_new')
            )
        );

        return $form;
    }
}
