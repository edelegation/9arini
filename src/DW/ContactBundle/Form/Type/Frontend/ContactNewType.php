<?php

namespace DW\ContactBundle\Form\Type\Frontend;

use DW\ContactBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactNewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                null
            )
            ->add(
                'lastName',
                null
            )
            ->add(
                'email',
                null
            )
            ->add(
                'phone',
                null
            )
            ->add(
                'subject',
                null
            )
            ->add(
                'message',
                null
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\ContactBundle\Entity\Contact',
                'translation_domain' => 'dw_contact',
                'cascade_validation' => true,
                'validation_groups' => array('DWContact'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'contact_new';
    }
}
