<?php

namespace DW\ContactBundle\Entity;

use DW\ContactBundle\Model\BaseContact;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Contact
 *
 * @ORM\Entity(repositoryClass="DW\ContactBundle\Repository\ContactRepository")
 *
 * @ORM\Table(name="contact")
 */
class Contact extends BaseContact
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer
     */
    private $id;

    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return (string)$this->getSubject();
    }
}
