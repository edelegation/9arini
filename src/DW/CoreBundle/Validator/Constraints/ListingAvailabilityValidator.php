<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ListingAvailabilityValidator extends ConstraintValidator
{

    private $minPrice;

    /**
     * @param int $minPrice
     */
    public function __construct($minPrice)
    {
        $this->minPrice = $minPrice;
    }

    /**
     * @param mixed      $listingAvailability
     * @param Constraint $constraint
     */
    public function validate($listingAvailability, Constraint $constraint)
    {
        /** @var $listingAvailability \DW\CoreBundle\Document\ListingAvailability */
        //Price

        if ($listingAvailability->getPrice() < $this->minPrice) {
            $this->context->buildViolation($constraint::$messageMinPrice)
                ->atPath('price')
                ->setParameter('{{ min_price }}', $this->minPrice)
                ->setTranslationDomain('dw_listing')
                ->addViolation();
        }
    }

}
