<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ListingDiscount extends Constraint
{
    public static $messageMinDiscount = "listing_discount.discount.min {{ min_discount }}";
    public static $messageMaxDiscount = "listing_discount.discount.max {{ max_discount }}";

    public function validatedBy()
    {
        return 'listing_discount';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
