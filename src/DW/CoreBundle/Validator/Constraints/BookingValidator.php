<?php

namespace DW\CoreBundle\Validator\Constraints;

use DW\CoreBundle\Entity\Booking as BookingEntity;
use DW\CoreBundle\Form\Type\Frontend\BookingNewType;
use DW\CoreBundle\Model\Manager\BookingManager;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BookingValidator extends ConstraintValidator
{
    private $bookingManager;
    private $minStartDelay;
    private $minStartTimeDelay;

    /**
     * @param BookingManager $bookingManager
     * @param int            $minStartDelay
     * @param int            $minStartTimeDelay
     */
    public function __construct(BookingManager $bookingManager, $minStartDelay, $minStartTimeDelay)
    {
        $this->bookingManager = $bookingManager;
        $this->minStartDelay = $minStartDelay;
        $this->minStartTimeDelay = $minStartTimeDelay;
    }

    /**
     * @param BookingEntity|mixed $booking
     * @param Booking|Constraint  $constraint
     */
    public function validate($booking, Constraint $constraint)
    {
        if ($booking->getStart() && $booking->getEnd()) {

            $violations = $this->getViolations($booking, $constraint);

            if (count($violations)) {
                foreach ($violations as $violation) {
                    $message = $violation['message'];
                    $atPath = isset($violation['atPath']) ? $violation['atPath'] : 'date_range';
                    $domain = isset($violation['domain']) ? $violation['domain'] : 'dw_booking';
                    $parameter = isset($violation['parameter']) ? $violation['parameter'] : array();
                    reset($parameter);

                    if ($parameter) {
                        $this->context->buildViolation($message)
                            ->atPath($atPath)
                            ->setParameter(
                                '{{ ' . key($parameter) . ' }}',
                                $parameter[key($parameter)]
                            )
                            ->setTranslationDomain($domain)
                            ->addViolation();
                    } else {
                        $this->context->buildViolation($message)
                            ->atPath($atPath)
                            ->setTranslationDomain($domain)
                            ->addViolation();
                    }
                }
            }
        }
    }

    /**
     * @param BookingEntity      $booking
     * @param Booking|Constraint $constraint
     * @return array
     */
    private function getViolations($booking, $constraint)
    {
        $violations = array();

//        if ($booking->getUser() == $booking->getListing()->getUser()) {
//            $violations[] = array(
//                'message' => $constraint::$messageSelfBooking,
//            );
//        }


        $errors = $this->bookingManager->checkBookingAvailabilityAndSetAmounts($booking);
        //Availability error
        if (in_array('unavailable', $errors)) {
            $violations[] = array(
                'message' => $constraint::$messageUnavailable,
            );
        }

        //Duration error
        if (in_array('duration_invalid', $errors)) {
            $violations[] = array(
                'message' => $constraint::$messageDurationInvalid,
            );
        }

        //Date Time errors
        if (in_array('date_range.invalid.min_start', $errors)) {
            $now = new \DateTime();
            if ($this->minStartDelay > 0) {
                $now->add(new \DateInterval('P' . $this->minStartDelay . 'D'));
            }
            $violations[] = array(
                'message' => 'date_range.invalid.min_start {{ min_start_day }}',
                'parameter' => array('min_start_day' => $now->format('d/m/Y')),
                'domain' => 'dw'
            );
        }

        if (in_array('date_range.invalid.max_end', $errors)) {
            $violations[] = array(
                'message' => 'date_range.invalid.max_end {{ date_max }}',
                'parameter' => array('date_max' => $booking->getEnd()->format('d/m/Y')),
                'domain' => 'dw'
            );
        }

        if (in_array('date_range.invalid.end_before_start', $errors)) {
            $violations[] = array(
                'message' => 'date_range.invalid.end_before_start',
            );
        }

        if (in_array('time_range.invalid.end_before_start', $errors)) {
            $violations[] = array(
                'message' => 'time_range.invalid.end_before_start',
            );
        }

        if (in_array('time_range.invalid.single_time', $errors)) {
            $violations[] = array(
                'message' => 'time_range.invalid.single_time',
            );
        }

        if (in_array('time_range.invalid.duration', $errors)) {
            $violations[] = array(
                'message' => 'time_range.invalid.duration',
            );
        }

        if (in_array('time_range.invalid.required', $errors)) {
            $violations[] = array(
                'message' => 'time_range.invalid.required',
            );
        }

        if (in_array('time_range.invalid.min_start', $errors)) {
            $now = new \DateTime();
            if ($this->minStartTimeDelay > 0) {
                $now->add(new \DateInterval('PT' . $this->minStartTimeDelay . 'H'));
            }
            $violations[] = array(
                'message' => 'time_range.invalid.min_start {{ min_start_time }}',
                'parameter' => array('min_start_time' => $now->format('d/m/Y H:i')),
                'domain' => 'dw'
            );
        }

        //Amount error
        if (in_array('amount_invalid', $errors)) {
            $violations[] = array(
                'message' => $constraint::$messageAmountInvalid,
                'parameter' => array(
                    'min_price' => $this->bookingManager->minPrice
                ),
                'domain' => 'dw'
            );
        }

        //Voucher error
        if (in_array('code_voucher_invalid', $errors)) {
            $violations[] = array(
                'message' => BookingNewType::$voucherError,
                'atPath' => 'codeVoucher',
            );
        }

        if (in_array('amount_voucher_invalid', $errors)) {
            $violations[] = array(
                'message' => $constraint::$messageAmountInvalid,
                'parameter' => array(
                    'min_price' => $this->bookingManager->minPrice
                ),
                'atPath' => 'codeVoucher',
                'domain' => 'dw'
            );
        }

        //Delivery error
        if (in_array('delivery_max_invalid', $errors)) {
            $violations[] = array(
                'message' => BookingNewType::$messageDeliveryMaxInvalid,
                'atPath' => 'deliveryAddress',
            );
        }

        if (in_array('delivery_invalid', $errors)) {
            $violations[] = array(
                'message' => BookingNewType::$messageDeliveryInvalid,
                'atPath' => 'deliveryAddress',
            );
        }

        return $violations;
    }
}
