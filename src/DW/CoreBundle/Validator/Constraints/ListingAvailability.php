<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ListingAvailability extends Constraint
{
    public static $messageMinPrice = "listing_price.min {{ min_price }}";


    public function validatedBy()
    {
        return 'listing_availability';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
