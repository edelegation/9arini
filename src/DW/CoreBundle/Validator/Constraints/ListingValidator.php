<?php

namespace DW\CoreBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ListingValidator extends ConstraintValidator
{
    private $emr;
    private $minPrice;
    private $country;
    private $requestForm;
    /**
     * @var Request
     */
    private $request;
    private $requestStack;

    /**
     * @param EntityManager $emr
     * @param RequestStack $requestStack
     * @param $minPrice
     * @param $country
     * @internal param $minCategories
     */
    public function __construct(EntityManager $emr, RequestStack $requestStack, $minPrice, $country)
    {
        $this->emr = $emr;
        $this->minPrice = $minPrice;
        $this->country = $country;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getMasterRequest();
    }

    /**
     * @param mixed      $listing
     * @param Constraint $constraint
     */
    public function validate($listing, Constraint $constraint)
    {
        /** @var $listing \DW\CoreBundle\Entity\Listing */
        /** @var $constraint \DW\CoreBundle\Validator\Constraints\Listing */

        //Price
        if ($listing->getPrice() < $this->minPrice) {
            $this->context->buildViolation($constraint::$messageMinPrice)
                ->atPath('price')
                ->setTranslationDomain('dw_listing')
                ->setParameter('{{ min_price }}', $this->minPrice )
                ->addViolation();
        }

        //Duration
        if ($listing->getMinDuration() && $listing->getMaxDuration() &&
            $listing->getMinDuration() > $listing->getMaxDuration()
        ) {
            $this->context->buildViolation($constraint::$messageDuration)
                ->atPath('min_duration')
                ->setTranslationDomain('dw_listing')
                ->addViolation();
        }


        //dump($this->request->request->all()); die;
        //dump($this->request->request->all()); die;

        //Location
        if ($this->country && ($listing->getLocation()->getCoordinate()->getCountry()->getCode() !=  $this->country)) {
            $this->context->buildViolation($constraint::$messageCountryInvalid)
                ->atPath('location.coordinate')
                ->setTranslationDomain('dw_listing')
                ->addViolation();
        }

        //Status
//        $oldListing = $this->emr->getUnitOfWork()->getOriginalEntityData($listing);
//
//        if (is_array($oldListing) and !empty($oldListing)) {
//            $oldStatus = $oldListing['status'];
//            if ($oldStatus == $listing::STATUS_INVALIDATED && $listing->getStatus() != $listing::STATUS_INVALIDATED) {
//                $listing->setStatus($oldStatus);
//                $this->context->buildViolation($constraint::$messageStatusInvalidated)
//                    ->atPath('status')
//                    ->setTranslationDomain('dw_listing')
//                    ->addViolation();
//            }
//        }
    }

}
