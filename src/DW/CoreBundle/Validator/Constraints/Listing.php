<?php

namespace DW\CoreBundle\Validator\Constraints;

use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use JMS\TranslationBundle\Model\Message;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Listing extends Constraint implements TranslationContainerInterface
{
    //public static $messageStatusInvalidated = "listing_status.invalidated";
    public static $messageMinPrice = "listing_price.min {{ min_price }}";
    public static $messageDuration = "listing_duration.overlap";
    public static $messageCountryInvalid = "listing_location_country.invalid";

    public function validatedBy()
    {
        return 'listing';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        $messages = array();
        $messages[] = new Message(self::$messageMinPrice, 'dw_listing');
        $messages[] = new Message(self::$messageDuration, 'dw_listing');
        /*$messages[] = new Message(self::$messageCountryInvalid, 'dw_listing');*/

        return $messages;
    }
}
