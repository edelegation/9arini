<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;


class TimeRangesOverlap extends Constraint
{
    public static $messageOverlap = "time_ranges.overlap";
    public static $messageMin = "time_ranges.min {{ limit }}";
    public static $messageMax = "time_ranges.max {{ limit }}";
    public $min;
    public $max;

    public function __construct($options = null)
    {
        if (null !== $options && !is_array($options)) {
            $options = array(
                'min' => $options,
                'max' => $options,
            );
        }
        parent::__construct($options);

        if (null === $this->min && null === $this->max) {
            throw new MissingOptionsException(
                sprintf('Either option "min" or "max" must be given for constraint %s', __CLASS__), array('min', 'max')
            );
        }
    }

    public function validatedBy()
    {
        return 'time_ranges_overlap';
    }
}
