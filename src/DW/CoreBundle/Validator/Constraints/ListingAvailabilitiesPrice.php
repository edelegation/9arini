<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class ListingAvailabilitiesPrice extends Constraint
{
    public static $messageMinPrice = "listing_price.min {{ min_price }}";


    public function validatedBy()
    {
        return 'listing_availabilities_price';
    }
}
