<?php

namespace DW\CoreBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Booking extends Constraint
{
    public static $messageUnavailable = 'booking.new.error.unavailable';
    public static $messageDurationInvalid = 'booking.new.error.duration_invalid';
    public static $messageAmountInvalid = 'booking.new.error.amount_invalid {{ min_price }}';

//    public static $messageSelfBooking = 'booking.new.self_booking.error';

    public function validatedBy()
    {
        return 'booking';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
