<?php


namespace DW\CoreBundle\Model;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Validator\Constraints as DWAssert;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BaseBookingPayinRefund
 *
 *
 * @ORM\MappedSuperclass()
 *
 */
abstract class BaseBookingPayinRefund
{
    /* Status */
    const STATUS_PAYED = 1;
    const STATUS_TO_DO = 0;

    public static $statusValues = array(
        self::STATUS_TO_DO => 'entity.booking.bank_wire.status.to_do',
        self::STATUS_PAYED => 'entity.booking.bank_wire.status.payed',
    );

    /**
     * @ORM\Column(name="status", type="smallint")
     *
     * @var integer
     */
    protected $status = self::STATUS_TO_DO;

    /**
     * @ORM\Column(name="validated", type="boolean")
     *
     * @var boolean
     */
    protected $validated = false;

    /**
     *
     * @ORM\Column(name="amount", type="decimal", precision=8, scale=0)
     *
     * @var integer
     */
    protected $amount;

    /**
     * @ORM\Column(name="payed_at", type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $payedAt;

    public function __construct()
    {

    }


    /**
     * Set status
     *
     * @param  integer $status
     * @return BaseBooking
     */
    public function setStatus($status)
    {
        if (!in_array($status, array_keys(self::$statusValues))) {
            throw new \InvalidArgumentException(
                sprintf('Invalid value for booking_payin_refund.status : %s.', $status)
            );
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get Status Text
     *
     * @return string
     */
    public function getStatusText()
    {
        return self::$statusValues[$this->getStatus()];
    }

    /**
     * @return bool
     */
    public function isValidated()
    {
        return $this->validated;
    }

    /**
     * @param bool $validated
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;
    }


    /**
     * Set amount
     *
     * @param int $amount
     * @return Booking
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }


    /**
     * Get amount decimal
     *
     * @return float
     */
    public function getAmountDecimal()
    {
        return $this->amount;
    }

    /**
     * @return \DateTime
     */
    public function getPayedAt()
    {
        return $this->payedAt;
    }

    /**
     * @param \DateTime $payedAt
     */
    public function setPayedAt($payedAt)
    {
        $this->payedAt = $payedAt;
    }
}
