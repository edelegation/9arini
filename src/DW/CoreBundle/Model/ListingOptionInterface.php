<?php

namespace DW\CoreBundle\Model;


use DW\CoreBundle\Entity\Listing;

interface ListingOptionInterface
{
    /**
     * @param Listing $listing
     * @return mixed
     */
    public function setListing(Listing $listing);

    /**
     * @return Listing
     */
    public function getListing();

    public function getTranslations();

    public function getName();

}