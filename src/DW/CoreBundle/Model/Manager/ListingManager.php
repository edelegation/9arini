<?php

namespace DW\CoreBundle\Model\Manager;

use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Entity\ListingCategory;
use DW\CoreBundle\Entity\ListingListingCategory;
use DW\CoreBundle\Entity\ListingListingCharacteristic;
use DW\CoreBundle\Entity\ListingTranslation;
use DW\CoreBundle\Mailer\TwigSwiftMailer;
use DW\CoreBundle\Model\ListingCategoryFieldValueInterface;
use DW\CoreBundle\Model\ListingCategoryListingCategoryFieldInterface;
use DW\CoreBundle\Model\ListingOptionInterface;
use DW\CoreBundle\Repository\ListingCharacteristicRepository;
use DW\CoreBundle\Repository\ListingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ListingManager extends BaseManager
{
    protected $em;
    protected $securityTokenStorage;
    protected $newListingIsPublished;
    public $maxPerPage;
    protected $mailer;

    /**
     * @param EntityManager   $em
     * @param TokenStorage    $securityTokenStorage
     * @param int             $newListingIsPublished
     * @param int             $maxPerPage
     * @param TwigSwiftMailer $mailer
     */
    public function __construct(
        EntityManager $em,
        TokenStorage $securityTokenStorage,
        $newListingIsPublished,
        $maxPerPage,
        TwigSwiftMailer $mailer
    ) {
        $this->em = $em;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->newListingIsPublished = $newListingIsPublished;
        $this->maxPerPage = $maxPerPage;
        $this->mailer = $mailer;
    }

    /**
     * @param  Listing $listing
     * @return Listing
     */
    public function save(Listing $listing)
    {
        $listingPublished = false; $validate = true;
        if (!$listing->getId()) {
            // New Listing
            if ($this->newListingIsPublished) {
                $listing->setStatus(Listing::STATUS_PUBLISHED);
                $listingPublished = true;
            } else {
                $listing->setStatus(Listing::STATUS_TO_VALIDATE);
            }
        } else {
            // Edit Listing
            if ( in_array($listing->getStatus(), [
                    Listing::STATUS_PUBLISHED,
                    Listing::STATUS_TO_VALIDATE,
                    Listing::STATUS_INVALIDATED ]
            )) {
                if ( $this->isListingUpdated($listing) ) {
                    $listing->setStatus(Listing::STATUS_TO_VALIDATE);
                } else {
                    $validate = false;
                }
            } else {
                $validate = false;
            }
        }

        $this->persistAndFlush($listing);
        $listing->generateSlug();


        $this->em->flush();
        $this->em->refresh($listing);

        if ($listingPublished) {
            $this->mailer->sendListingActivatedMessageToOfferer($listing);
        }else {
            if($validate) {
                $this->mailer->sendValidateListingToOfferer($listing);
                $this->mailer->sendValidateListingToModerator($listing);
            }
        }

        return $listing;
    }

    /**
     * In case of new characteristics are created, we need to associate them to listing
     *
     * @param Listing $listing
     *
     * @return Listing
     */
    public function refreshListingListingCharacteristics(Listing $listing)
    {
        /** @var ListingCharacteristicRepository $listingCharacteristicRepository */
        $listingCharacteristicRepository = $this->em->getRepository('DWCoreBundle:ListingCharacteristic');

        //Get all characteristics
        $listingCharacteristics = new ArrayCollection(
            $listingCharacteristicRepository->findAllTranslated()
        );

        //Remove characteristics already associated to listing
        $listingListingCharacteristics = $listing->getListingListingCharacteristics();
        foreach ($listingListingCharacteristics as $listingListingCharacteristic) {
            $listingCharacteristics->removeElement($listingListingCharacteristic->getListingCharacteristic());
        }

        //Associate new characteristics not already associated to listing
        foreach ($listingCharacteristics as $listingCharacteristic) {
            $listingListingCharacteristic = new ListingListingCharacteristic();
            $listingListingCharacteristic->setListing($listing);
            $listingListingCharacteristic->setListingCharacteristic($listingCharacteristic);
            $listingListingCharacteristic->setListingCharacteristicValue();
            $listing->addListingListingCharacteristic($listingListingCharacteristic);
        }

        return $listing;
    }

    /**
     * Create categories and field values while listing deposit.
     *
     * @param  Listing $listing
     * @param  array   $categories Id(s) of ListingCategory(s) selected*
     * @return Listing
     */
    public function addCategories(Listing $listing, array $categories)
    {
        $this->removeCategories($listing);

        //Find all categories entity selected
        $listingCategories = $this->em->getRepository('DWCoreBundle:ListingCategory')->findBy([
            'id' => $categories
        ]);

        foreach ($listingCategories as $category) {

            //Create the corresponding ListingListingCategory
            $listingListingCategory = new ListingListingCategory();
            $listingListingCategory->setListing($listing);
            $listingListingCategory->setCategory($category);

            $listing->addListingListingCategory($listingListingCategory);
        }

        if(!is_null($listing->getId())){
            $this->persistAndFlush($listing);
            $this->em->flush();
            $this->em->refresh($listing);
        }

        return $listing;
    }

    public function removeCategories(Listing $listing)
    {
        foreach ($listing->getListingListingCategories() as $listingCategory){
            $listing->removeListingListingCategory($listingCategory);

            $this->persistAndFlush($listing);
            $this->em->flush();
            $this->em->refresh($listing);
        }

    }

    /**
     * @param int    $ownerId
     * @param int[]  $status
     * @param int    $page
     *
     * @return Paginator
     */
    public function findByOwner($ownerId, $status, $page)
    {
        $queryBuilder = $this->getRepository()->getFindByOwnerQuery($ownerId, $status);

        $queryBuilder
            ->addOrderBy('l.createdAt', 'desc');

        //Pagination
        $queryBuilder
            ->setFirstResult(($page - 1) * $this->maxPerPage)
            ->setMaxResults($this->maxPerPage);

        //Query
        $query = $queryBuilder->getQuery();

        return new Paginator($query);
    }

    /**
     * Send Update Calendar mail for all published listing
     *
     * @return integer Count of alerts sent
     */
    public function alertUpdateCalendars()
    {
        $result = 0;
        $listings = $this->getRepository()->findAllPublished();

        foreach ($listings as $listing) {
            if ($this->alertUpdateCalendar($listing)) {
                $result++;
            }
        }

        return $result;
    }

    /**
     * Send Alert Update Calendar
     *
     * @param Listing $listing
     *
     * @return boolean
     */
    public function alertUpdateCalendar(Listing $listing)
    {
        $this->mailer->sendUpdateYourCalendarMessageToOfferer($listing);

        return true;
    }


    /**
     * Duplicate Listing
     *
     * @param  Listing $listing
     * @return Listing
     */
    public function duplicate(Listing $listing)
    {
        $listingCloned = clone $listing;
        if (!$this->newListingIsPublished) {
            $listing->setStatus(Listing::STATUS_TO_VALIDATE);
        }

        $listingCloned->generateSlug();
        $this->persistAndFlush($listingCloned);


        //Options
        /** @var ListingOptionInterface $option */
        if ($listingCloned->getOptions()) {
            foreach ($listingCloned->getOptions() as $option) {
                $this->persistAndFlush($option);
            }
        }

        $this->em->flush();
        $this->em->refresh($listingCloned);

        return $listingCloned;
    }

    /**
     *
     * @return ListingRepository
     */
    public function getRepository()
    {
        return $this->em->getRepository('DWCoreBundle:Listing');
    }

    /**
     * @param Listing $listing
     * @return bool
     */
    private function isListingUpdated(Listing $listing)
    {
        $uow = $this->em->getUnitOfWork();
        $originalListing = $uow->getOriginalEntityData($listing);
        $updated = false;

        // Check Title, Description And Rules
        if ( $originalListing['title'] != $listing->getTitle()
            || $originalListing['description'] != $listing->getDescription()
            || $originalListing['rules'] != $listing->getRules()
        ) {
            $updated = true;
        }
        return $updated;
    }

}
