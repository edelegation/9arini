<?php

namespace DW\CoreBundle\Model\Manager;

abstract class BaseManager
{

    protected function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }
}
