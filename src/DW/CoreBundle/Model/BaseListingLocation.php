<?php


namespace DW\CoreBundle\Model;

use DW\CoreBundle\Entity\City;
use DW\CoreBundle\Entity\ListingLocation;
use Doctrine\ORM\Mapping as ORM;
use DW\CoreBundle\Entity\Region;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingLocation
 *
 * @ORM\MappedSuperclass
 */
abstract class BaseListingLocation
{
    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\Region", inversedBy="location", cascade={"persist"})
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @var string
     */
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\City", inversedBy="location", cascade={"persist"})
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @var string
     */
    protected $city;

    /**
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="assert.not_blank")
     * @var string
     */
    protected $address;

    /**
     * @var string
     */
    protected $country;


    /**
     * Set region
     *
     * @param Region $region
     *
     * @return ListingLocation
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param City $city
     *
     * @return ListingLocation
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return ListingLocation
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCompleteAddress()
    {
        return $this->getAddress() . ", " . $this->getCity() . ", " . $this->getRegion();
    }
}
