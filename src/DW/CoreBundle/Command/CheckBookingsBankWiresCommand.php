<?php

namespace DW\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Validate bookings commands
 * For example every two hours :
 */

//Cron: 0 */2  * * *  user   php app/console dw:bookings:checkBankWires

class CheckBookingsBankWiresCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:bookings:checkBankWires')
            ->setDescription('Check Bookings Bank Wires. Set status to done if bank wire has been transferred')
            ->setHelp("Usage php app/console dw:bookings:checkBankWires");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $result = $container->get('dw.booking_bank_wire.manager')->checkBookingsBankWires();

        $output->writeln($result . " booking(s) bank wires checked");
        $this->getContainer()->get('monolog.logger.crons')->info($result . " booking(s) bank wires checked");
    }

}
