<?php

namespace DW\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Bookings imminent alert
 * Every hour
 */

//Cron: 0 */1 * * *  user   php app/console dw:bookings:alertImminent

class AlertImminentBookingsCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:bookings:alertImminent')
            ->setDescription('Alert imminent Bookings.')
            ->addOption(
                'delay',
                null,
                InputOption::VALUE_OPTIONAL,
                'Booking imminent alert delay in minutes. To use only on no prod env'
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_NONE,
                'Extra precaution to ensure to use on test mode'
            )
            ->setHelp("Usage php app/console dw:bookings:alertImminent");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $delay = $this->getContainer()->getParameter('dw.booking.alert_imminent_delay');
        if ($input->getOption('test') && $input->hasOption('delay')) {
            $delay = $input->getOption('delay');
        }

        $container = $this->getContainer();
        $bookingManager = $container->get('dw.booking.manager');

        $result = $bookingManager->alertImminentBookings($delay);
        $output->writeln($result . " booking(s) imminent alerted");
        $this->getContainer()->get('monolog.logger.crons')->info($result . " booking(s) imminent alerted");
    }

}
