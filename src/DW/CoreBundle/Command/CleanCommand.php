<?php

namespace DW\CoreBundle\Command;

use DirectoryIterator;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CleanCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:clean:images')
            ->setDescription('Clean Images')
            ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        /** @var EntityManager $em */
        $em = $container->get('doctrine.orm.entity_manager');
        $userImages = $em->getRepository('DWUserBundle:UserImage')->findAll();
        $i =0 ;
        foreach ($userImages as $image) {

            if( @filesize($image->getAbsolutePath(true)) === 0) {
                $image->removeUpload(true);
                $image->setCropImg(null);

                $em->persist($image);
                $em->flush();
		$i++;
            };
        }

        $output->writeln($i ." files deleted");

    }

    private function file_is_valid_image($path) {
        $size = @getimagesize($path);
        return !empty($size);
    }

}
