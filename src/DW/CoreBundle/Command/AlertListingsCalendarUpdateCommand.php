<?php

namespace DW\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Calendar update alert commands
 * Every Month on 27  :
 */

//Cron: 0 0 27 * *  user   php app/console dw:listings:alertUpdateCalendars

class AlertListingsCalendarUpdateCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:listings:alertUpdateCalendars')
            ->setDescription('Alert listings calendars update.')
            ->setHelp("Usage php app/console dw:listings:alertUpdateCalendars");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $listingManager = $container->get('dw.listing.manager');

        $result = $listingManager->alertUpdateCalendars();

        $output->writeln($result . " listing(s) calendar update alerted");
        $this->getContainer()->get('monolog.logger.crons')->info($result . " listing(s) calendar update alerted");
    }

}
