<?php

namespace DW\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Bookings expiring alert
 * Every  15 minutes :
 */

//Cron: */15 * * * *  user   php app/console dw:bookings:alertExpiring

class AlertExpiringBookingsCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:bookings:alertExpiring')
            ->setDescription('Alert Expiring Bookings.')
            ->addOption(
                'delay',
                null,
                InputOption::VALUE_OPTIONAL,
                'Booking expiration delay in minutes. To use only on no prod env'
            )
            ->addOption(
                'alert_delay',
                null,
                InputOption::VALUE_OPTIONAL,
                'Booking expiring alert delay in minutes. To use only on no prod env'
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_NONE,
                'Extra precaution to ensure to use on test mode'
            )
            ->setHelp("Usage php app/console dw:bookings:alertExpiring");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $delay = $this->getContainer()->getParameter('dw.booking.expiration_delay');
        $alertDelay = $this->getContainer()->getParameter('dw.booking.alert_expiration_delay');

        if ($input->getOption('test') && $input->hasOption('delay') && $input->hasOption('alert_delay')) {
            $delay = $input->getOption('delay');
            $alertDelay = $input->getOption('alert_delay');
        }

        $container = $this->getContainer();
        $bookingManager = $container->get('dw.booking.manager');

        $result = $bookingManager->alertExpiringBookings($delay, $alertDelay);

        $output->writeln($result . " booking(s) expiring alerted");
        $this->getContainer()->get('monolog.logger.crons')->info($result . " booking(s) expiring alerted");
    }

}
