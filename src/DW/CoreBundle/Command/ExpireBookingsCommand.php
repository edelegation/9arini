<?php

namespace DW\CoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/*
 * Expire bookings commands
 * For example every hour :
 */

//Cron: 0 */1  * * *  user   php app/console dw:bookings:expire

class ExpireBookingsCommand extends ContainerAwareCommand
{

    public function configure()
    {
        $this
            ->setName('dw:bookings:expire')
            ->setDescription('Expire Bookings.')
            ->addOption(
                'delay',
                null,
                InputOption::VALUE_OPTIONAL,
                'Booking expiration delay in minutes. To use only on no prod env'
            )
            ->addOption(
                'test',
                null,
                InputOption::VALUE_NONE,
                'Extra precaution to ensure to use on test mode'
            )
            ->setHelp("Usage php app/console dw:bookings:expire");
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $delay = $this->getContainer()->getParameter('dw.booking.expiration_delay');
        $delayExpirationAccepted = $this->getContainer()->getParameter('dw.booking.expiration_delay_accepted');
        if ($input->getOption('test') && $input->hasOption('delay')) {
            $delay = $input->getOption('delay');
        }

        $container = $this->getContainer();
        $bookingManager = $container->get('dw.booking.manager');

        $result = $bookingManager->expireBookings($delay);
        $result2 = $bookingManager->expireBookingsAccepted($delayExpirationAccepted);

        $output->writeln($result . " booking(s) expired");
        $output->writeln($result2 . " booking(s) accepted expired");
        $this->getContainer()->get('monolog.logger.crons')->info($result . " booking(s) expired");
        $this->getContainer()->get('monolog.logger.crons')->info($result2 . " booking(s) accepted expired");

    }

}
