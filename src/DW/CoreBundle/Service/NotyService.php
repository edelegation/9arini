<?php

namespace DW\CoreBundle\Service;

use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Entity\Booking;
use DW\ReviewBundle\Model\ReviewManager;
use DW\UserBundle\Entity\User;

class NotyService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ReviewManager
     */
    private $reviewManager;

    public function __construct(
        EntityManager $em,
        ReviewManager $reviewManager
    )
    {
        $this->em = $em;
        $this->reviewManager = $reviewManager;
    }

    public function getNotifications(User $user)
    {
        // for asker
        $acceptedBookings = $this->getRepositoryBooking()->findByAsker(
            $user->getId(),
            [Booking::STATUS_ACCEPTED]
        );

        //for offerer
        $newBookings = $this->getRepositoryBooking()->findByOfferer(
            $user->getId(),
            [Booking::STATUS_NEW,Booking::STATUS_PAYED]
        );

        // Reviews
        $unreviewedBookingsAsker = $this->reviewManager->getUnreviewedBookings('asker', $user);
        $unreviewedBookingsOfferer = $this->reviewManager->getUnreviewedBookings('offerer', $user);

        $askerArray =  [
            'booking' => count($acceptedBookings),
            'reviews' => count($unreviewedBookingsAsker)
        ];
        $arrayOfferer = [
            'booking' => count($newBookings),
            'reviews' => count($unreviewedBookingsOfferer)
        ];

        $askerArray = array_filter($askerArray, function($a) { return ($a !== 0); });
        $arrayOfferer = array_filter($arrayOfferer, function($a) { return ($a !== 0); });

        return ['asker' => $askerArray, 'offerer' => $arrayOfferer];
    }

    private function getRepositoryBooking()
    {
        return $this->em->getRepository('DWCoreBundle:Booking');
    }
}