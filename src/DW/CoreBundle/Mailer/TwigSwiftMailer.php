<?php

namespace DW\CoreBundle\Mailer;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\Listing;
use DW\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\Translator;

class TwigSwiftMailer implements MailerInterface
{
    const TRANS_DOMAIN = 'dw_mail';

    protected $mailer;
    protected $router;
    protected $twig;
    protected $requestStack;
    protected $translator;
//    protected $parameters;
    protected $timeUnit;
    protected $timeUnitIsDay;
    /** @var  array locales */
    protected $templates;
    protected $fromEmail;
    protected $fromName;
    protected $paymentFromName;
    protected $paymentFromEmail;
    protected $adminEmail;
    protected $moderatorEmail;

    /**
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param \Twig_Environment     $twig
     * @param RequestStack          $requestStack
     * @param Translator            $translator
     * @param array                 $parameters
     * @param array                 $templates
     */
    public function __construct(
        \Swift_Mailer $mailer,
        UrlGeneratorInterface $router,
        \Twig_Environment $twig,
        RequestStack $requestStack,
        Translator $translator,
        array $parameters,
        array $templates
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->translator = $translator;

        /** parameters */
//        $this->parameters = $parameters['parameters'];
        $parameters = $parameters['parameters'];

        $this->fromName = $parameters['dw_from_name'];
        $this->fromEmail = $parameters['dw_from_email'];

        $this->paymentFromName = $parameters['dw_payment_from_name'];
        $this->paymentFromEmail = $parameters['dw_payment_from_email'];

        $this->adminEmail = $parameters['dw_admin_email'];
        $this->moderatorEmail = $parameters['dw_moderator_email'];

        $this->timeUnit = $parameters['dw_time_unit'];
        $this->timeUnitIsDay = ($this->timeUnit % 1440 == 0) ? true : false;

        $this->templates = $templates;
    }

    /**
     * @param Listing $listing
     */
    public function sendListingActivatedMessageToOfferer(Listing $listing)
    {
        $user = $listing->getUser();
        $template = $this->templates['templates']['listing_activated_offerer'];

        $listingCalendarEditUrl = $this->router->generate(
            'dw_dashboard_listing_edit_availabilities_status',
            array(
                'listing_id' => $listing->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'listing' => $listing,
            'listing_calendar_edit_url' => $listingCalendarEditUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingRequestMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_request_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingAcceptedMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_accepted_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );
        $profilePaymentInfoUrl = $this->router->generate(
            'dw_user_dashboard_profile_edit_payment',
            array(),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
            'profile_payment_info_url' => $profilePaymentInfoUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param Booking $booking
     */
    public function sendBookingRefusedMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_refused_offerer'];

        $listingCalendarEditUrl = $this->router->generate(
            'dw_dashboard_listing_edit_availabilities_status',
            array(
                'listing_id' => $listing->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'booking' => $booking,
            'listing_calendar_edit_url' => $listingCalendarEditUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingExpirationAlertMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_request_expiration_alert_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingRequestExpiredMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_request_expired_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * Remind offerer to rate asker
     *
     * @param Booking $booking
     */
    public function sendReminderToRateAskerMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['reminder_to_rate_asker_offerer'];

        $offererToAskerReviewUrl = $this->router->generate(
            'dw_dashboard_review_new',
            array(
                'booking_id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'booking' => $booking,
            'offerer_to_asker_review_url' => $offererToAskerReviewUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingCanceledByAskerMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_canceled_by_asker_offerer'];

        $offererCancellationAmount = $booking->getBankWire() ? $booking->getBankWire()->getAmountDecimal() : 0;

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'offerer_cancellation_amount' => $offererCancellationAmount,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingImminentMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['booking_imminent_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendWireTransferMessageToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $template = $this->templates['templates']['booking_bank_wire_transfer_offerer'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_offerer',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $paymentOffererUrl = $this->router->generate(
            'dw_dashboard_booking_bank_wire_offerer',
            array(),
            true
        );

        $context = array(
            'user' => $user,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
            'offerer_payments_list' => $paymentOffererUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Listing $listing
     */
    public function sendUpdateYourCalendarMessageToOfferer(Listing $listing)
    {
        $user = $listing->getUser();
        $template = $this->templates['templates']['update_your_calendar_offerer'];

        $listingCalendarEditUrl = $this->router->generate(
            'dw_dashboard_listing_edit_availabilities_status',
            array(
                'listing_id' => $listing->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'listing_calendar_edit_url' => $listingCalendarEditUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param Booking $booking
     */
    public function sendPaymentErrorMessageToOfferer(Booking $booking)
    {
//        $listing = $booking->getListing();
//        $user = $listing->getUser();
//        $asker = $booking->getUser();
//        $bookingRequestUrl = $this->router->generate(
//            'dw_dashboard_booking_show_offerer',
//            array('id' => $booking->getId()),
//            true
//        );
//
//        $context = array(
//            'user' => $user,
//            'asker' => $asker,
//            'booking_request_url' => $bookingRequestUrl,
//        );
//
//        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param Booking $booking
     */
    public function sendBookingRequestMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['booking_request_asker'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_asker',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingAcceptedMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['booking_accepted_asker'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_asker',
            array(
                'id' => $booking->getId()
            ),
            true
        );
        $paymentAskerUrl = $this->router->generate(
            'dw_dashboard_booking_payin_asker',
            array(),
            true
        );

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
            'payments_asker_list' => $paymentAskerUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param Booking $booking
     */
    public function sendBookingRefusedMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $template = $this->templates['templates']['booking_refused_asker'];

        $similarListingUrl = $this->router->generate(
            'dw_home',
            array(),
            true
        );

        $context = array(
            'user' => $user,
            'booking' => $booking,
            'similar_booking_listings_url' => $similarListingUrl //'#'
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingRequestExpiredMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $template = $this->templates['templates']['booking_request_expired_asker'];

        $similarListingUrl = $this->router->generate(
            'dw_home',
            array(),
            true
        );

        $context = array(
            'user' => $user,
            'booking' => $booking,
            'similar_booking_listings_url' => $similarListingUrl //'#'
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingImminentMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['booking_imminent_asker'];

        $bookingRequestUrl = $this->router->generate(
            'dw_dashboard_booking_show_asker',
            array(
                'id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'listing' => $listing,
            'booking' => $booking,
            'booking_request_url' => $bookingRequestUrl,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendReminderToRateOffererMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['reminder_to_rate_offerer_asker'];

        $askerToOffererReviewUrl = $this->router->generate(
            'dw_dashboard_review_new',
            array(
                'booking_id' => $booking->getId()
            ),
            true
        );

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'booking' => $booking,
            'asker_to_offerer_review_url' => $askerToOffererReviewUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param Booking $booking
     */
    public function sendBookingCanceledByAskerMessageToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['booking_canceled_by_asker_asker'];

        $profilePaymentInfoUrl = $this->router->generate(
            'dw_user_dashboard_profile_edit_payment',
            array(),
            true
        );

        $askerCancellationAmount = $booking->getPayinRefund() ? $booking->getPayinRefund()->getAmountDecimal() : 0;

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'listing' => $listing,
            'booking' => $booking,
            'profile_payment_info_url' => $profilePaymentInfoUrl,
            'asker_cancellation_amount' => $askerCancellationAmount,
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    public function sendMessageToAdminPayment(Booking $booking)
    {
        $template = $this->templates['templates']['admin_message'];

        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $asker = $booking->getUser();

        $context = array(
            'offerer' => $offerer,
            'asker' => $asker,
            'booking' => $booking,
            'listing' => $listing
        );

        $this->sendMessage($template, $context, $this->paymentFromEmail, $this->paymentFromName, $this->adminEmail);
    }

    /**
     * @param Booking $booking
     */
//    public function sendPaymentErrorMessageToAsker(Booking $booking)
//    {
//        $user = $booking->getUser();
//        $listing = $booking->getListing();
//        $offerer = $listing->getUser();
//        $template = $this->templates['templates']['payment_error_asker'];
//
//        $bookingRequestUrl = $this->router->generate(
//            'dw_dashboard_booking_show_offerer',
//            array('id' => $booking->getId()),
//            true
//        );
//
//        $context = array(
//            'user' => $user,
//            'offerer' => $offerer,
//            'listing' => $listing,
//            'booking' => $booking,
//            'booking_request_url' => $bookingRequestUrl
//        );
//
//        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
//    }


    /**
     * @param string $templateName
     * @param array $context
     * @param string $fromEmail
     * @param string $fromName
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $fromName, $toEmail)
    {
        $context['trans_domain'] = self::TRANS_DOMAIN;

        if (isset($context['listing'])) {
            /** @var Listing $listing */
            $listing = $context['listing'];

                $slug = $listing->getSlug();
                $title = $listing->getTitle();

            $context['listing_public_url'] = $this->router->generate(
                'dw_listing_show',
                array(
                    'slug' => $slug
                ),
                true
            );

            $context['listing_title'] = $title;
        }

        if (isset($context['booking'])) {
            $context['booking_time_range_title'] = $context['booking_time_range'] = '';
            if (!$this->timeUnitIsDay) {
                /** @var Booking $booking */
                $booking = $context['booking'];
                $context['booking_time_range_title'] = $this->translator->trans(
                    'booking.time_range.title',
                    array(),
                    'dw_mail'
                );
                $context['booking_time_range'] .= $booking->getStartTime()->format('H:i') . " - " .
                    $booking->getEndTime()->format('H:i');
            }
        }

        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($templateName);
        $context = $this->twig->mergeGlobals($context);

        $subject = $template->renderBlock('subject', $context);
        $context["message"] = $template->renderBlock('message', $context);

        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

//        echo 'subject:' . $subject . 'endsubject';
//        echo 'htmlBody:' . $htmlBody . 'endhtmlBody';
//        echo 'textBody:' . $textBody . 'endtextBody';

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom([$fromEmail => $fromName])
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message
                ->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);

        $this->mailer->getTransport()->stop();
    }


    /**
     * Payment Success to Offerer
     *
     * @param Booking $booking
     */
    public function sendPaymentSuccessToOfferer(Booking $booking)
    {
        $listing = $booking->getListing();
        $user = $listing->getUser();
        $asker = $booking->getUser();
        $template = $this->templates['templates']['payment_success_offerer'];

        $context = array(
            'user' => $user,
            'asker' => $asker,
            'booking' => $booking
        );

        $this->sendMessage($template, $context, $this->paymentFromEmail, $this->paymentFromName, $user->getEmail());
    }

    /**
     * Payment Success to Asker
     *
     * @param Booking $booking
     */
    public function sendPaymentSuccessToAsker(Booking $booking)
    {
        $user = $booking->getUser();
        $listing = $booking->getListing();
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['payment_success_asker'];

        $context = array(
            'user' => $user,
            'offerer' => $offerer,
            'booking' => $booking
        );

        $this->sendMessage($template, $context, $this->paymentFromEmail, $this->paymentFromName, $user->getEmail());
    }

    public function sendValidateListingToModerator(Listing $listing)
    {
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['validate_listing_moderator'];

        $context = array(
            'offerer' => $offerer,
            'listing' => $listing
        );
        //dump($this->fromName);die;
        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $this->moderatorEmail);
    }

    public function sendValidateListingToOfferer(Listing $listing)
    {
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['validate_listing_offerer'];

        $context = array(
            'offerer' => $offerer,
            'listing' => $listing
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $offerer->getEmail());
    }

    public function sendInvalidateListingMessageToOfferer(Listing $listing)
    {
        $offerer = $listing->getUser();
        $template = $this->templates['templates']['invalidated_listing'];

        $context = array(
            'offerer' => $offerer,
            'listing' => $listing
        );
        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $offerer->getEmail());
    }


}
