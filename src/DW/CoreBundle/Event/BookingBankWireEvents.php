<?php

namespace DW\CoreBundle\Event;


class BookingBankWireEvents
{
    /**
     * The BOOKING_BANK_WIRE_CHECK event occurs when a booking bank wire is checked.
     *
     * This event allows you to set the booking bank wire status.
     * The event listener method receives a DW\CoreBundle\Event\BookingBankWireEvent instance.
     */
    const BOOKING_BANK_WIRE_CHECK = 'dw.booking_bank_wire.check';
}