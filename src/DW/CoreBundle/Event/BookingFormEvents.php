<?php

namespace DW\CoreBundle\Event;


class BookingFormEvents
{
    /**
     * The BOOKING_NEW_FORM_BUILD event is thrown each time a new booking form is build
     *
     * This event allows you to add form fields and validation on them.
     *
     * The event listener receives a \DW\CoreBundle\Event\BookingFormBuilderEvent instance.
     */
    const BOOKING_NEW_FORM_BUILD = 'dw.booking_new.form.build';
}