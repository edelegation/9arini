<?php
namespace DW\CoreBundle\Utils;

class PHP
{

    public static function ksort_recursive(&$array, $sortFlags = SORT_STRING)
    {
        if (!is_array($array)) {
            return false;
        }
        ksort($array, $sortFlags);
        foreach ($array as &$arr) {
            self::ksort_recursive($arr, $sortFlags);
        }

        return true;
    }
}