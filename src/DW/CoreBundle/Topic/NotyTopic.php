<?php

namespace DW\CoreBundle\Topic;

use DW\CoreBundle\Service\NotyService;
use DW\UserBundle\Model\UserManager;
use Gos\Bundle\WebSocketBundle\Client\ClientManipulator;
use Gos\Bundle\WebSocketBundle\Topic\TopicInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Wamp\Topic;
use Gos\Bundle\WebSocketBundle\Router\WampRequest;
use Symfony\Component\Security\Core\User\UserInterface;

class NotyTopic implements TopicInterface
{
    /**
     * @var ClientManipulator
     */
    private $clientManipulator;

    /** @var array list of subs */
    private $clients;

    /** @var string current client */
    private $currentClient;

    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var NotyService
     */
    private $notyService;

    /**
     * @param ClientManipulator $clientManipulator
     * @param UserManager $manager
     * @param NotyService $notyService
     */
    public function __construct(
        ClientManipulator $clientManipulator,
        UserManager $manager,
        NotyService $notyService
    )
    {
        $this->clientManipulator = $clientManipulator;
        $this->manager = $manager;
        $this->notyService = $notyService;
    }

    /**
     * This will receive any Subscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        /** @var ConnectionInterface $client **/
        foreach($topic as $i => $client){
            $clients[$i] = $this->getClient($client);
        }
        $this->clients = array_unique($clients);

        $this->currentClient = $this->getClient($connection);

        if ($topic->getId() === 'user/notifications')
        {
            if(count($this->clients) > 0 && !in_array(NULL, $this->clients))
            {
                foreach ($this->clients as $key => $client)
                {
                    $user = $this->getUser($client);
                    $events[$key] = $this->notyService->getNotifications($user);

                }

                $topic->broadcast([
                    'msg' => $events,
                    //'clients' => $this->clients,
                    //'current_client' => $this->currentClient
                ]);
            }

        }
        //dump($this->clients);
        //this will broadcast the message to ALL subscribers of this topic.
        //$topic->broadcast(['msg' => $connection->resourceId . " has joined " . $topic->getId()]);
        //$this->clients[] = $this->clientStorage->getClient($connection->resourceId)->getUsername();
        //$this->currentUser = $this->getCurrentUserSubs($connection);
        //$topic->broadcast(['clients' => $this->clients, 'current_user' => $this->currentUser]);
    }

    /**
     * This will receive any UnSubscription requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @return void
     */
    public function onUnSubscribe(ConnectionInterface $connection, Topic $topic, WampRequest $request)
    {
        //this will broadcast the message to ALL subscribers of this topic.
        //$topic->broadcast(['msg' => $connection->resourceId . " has left " . $topic->getId()]);
    }


    /**
     * This will receive any Publish requests for this topic.
     *
     * @param ConnectionInterface $connection
     * @param Topic $topic
     * @param WampRequest $request
     * @param $event
     * @param array $exclude
     * @param array $eligible
     * @return mixed|void
     */
    public function onPublish(ConnectionInterface $connection, Topic $topic, WampRequest $request, $event, array $exclude, array $eligible)
    {
        /*if ($topic->getId() === 'user/notifications')
        {
            if(count($this->clients) > 0 && $event == null)
            {
                foreach ($this->clients as $key => $client)
                {

                    $user = $this->getUser($client);
                    $events[$key] = $this->notyService->getNotifications($user);

                }

                $topic->broadcast([
                    'msg' => $events,
                    'clients' => $this->clients,
                    'current_client' => $this->currentClient
                ]);
            }

        }*/
    }


    public function getClient(ConnectionInterface $connection)
    {
        $client = $this->clientManipulator->getClient($connection);
        if($client instanceof UserInterface) {
            return $client->getUsername();
        }
    }

    public function getCurrentSubscriber()
    {
        return $this->currentClient;
    }

    public function getUser($email)
    {
        return $this->manager->findUserByEmail($email);
    }

    /**
     * Like RPC is will use to prefix the channel
     * @return string
     */
    public function getName()
    {
        return 'noty.topic';
    }
}