<?php

namespace DW\CoreBundle\DataFixtures\ORM;


use DW\CoreBundle\Entity\City;
use DW\CoreBundle\Entity\Region;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRegionData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //$data = json_decode('{"1":{"id":"1","coordinates":{"lat":"","lng":""},"name":{"fr":"Ariana","ar":""},"seo_name":{"fr":"ariana","ar":""},"areas":{"1":{"id":1,"name":{"fr":"Ariana Ville","ar":""},"seo_name":{"fr":"ariana_ville","ar":""}},"2":{"id":2,"name":{"fr":"Ettadhamen","ar":""},"seo_name":{"fr":"ettadhamen","ar":""}},"3":{"id":3,"name":{"fr":"Kalaât El Andalous","ar":""},"seo_name":{"fr":"kalaât_el_andalous","ar":""}},"4":{"id":4,"name":{"fr":"La Soukra","ar":""},"seo_name":{"fr":"la_soukra","ar":""}},"5":{"id":5,"name":{"fr":"Mnihla","ar":""},"seo_name":{"fr":"mnihla","ar":""}},"6":{"id":6,"name":{"fr":"Raoued","ar":""},"seo_name":{"fr":"raoued","ar":""}},"7":{"id":7,"name":{"fr":"Sidi Thabet","ar":""},"seo_name":{"fr":"sidi_thabet","ar":""}},"265":{"id":265,"name":{"fr":"Borj Louzir","ar":""},"seo_name":{"fr":"borj_louzir","ar":""}},"266":{"id":266,"name":{"fr":"Chotrana","ar":""},"seo_name":{"fr":"chotrana","ar":""}},"267":{"id":267,"name":{"fr":"Ennasr","ar":""},"seo_name":{"fr":"ennasr","ar":""}},"268":{"id":268,"name":{"fr":"Ghazela","ar":""},"seo_name":{"fr":"ghazela","ar":""}},"269":{"id":269,"name":{"fr":"Jardins D\'el Menzah","ar":""},"seo_name":{"fr":"jardins_del_menzah","ar":""}},"270":{"id":270,"name":{"fr":"Manar","ar":""},"seo_name":{"fr":"manar","ar":""}},"271":{"id":271,"name":{"fr":"Menzah","ar":""},"seo_name":{"fr":"menzah","ar":""}}},"areaOrder":[{"id":1,"order_id":0},{"id":2,"order_id":1},{"id":3,"order_id":2},{"id":4,"order_id":3},{"id":5,"order_id":4},{"id":6,"order_id":5},{"id":7,"order_id":6},{"id":265,"order_id":7},{"id":266,"order_id":8},{"id":267,"order_id":9},{"id":268,"order_id":10},{"id":269,"order_id":11},{"id":270,"order_id":12},{"id":271,"order_id":13}]},"2":{"id":"2","coordinates":{"lat":"","lng":""},"name":{"fr":"Béja","ar":""},"seo_name":{"fr":"béja","ar":""},"areas":{"8":{"id":8,"name":{"fr":"Amdoun","ar":""},"seo_name":{"fr":"amdoun","ar":""}},"9":{"id":9,"name":{"fr":"Béja Nord","ar":""},"seo_name":{"fr":"béja_nord","ar":""}},"10":{"id":10,"name":{"fr":"Béja Sud","ar":""},"seo_name":{"fr":"béja_sud","ar":""}},"11":{"id":11,"name":{"fr":"Goubellat","ar":""},"seo_name":{"fr":"goubellat","ar":""}},"12":{"id":12,"name":{"fr":"Medjez el-Bab","ar":""},"seo_name":{"fr":"medjez_el-bab","ar":""}},"13":{"id":13,"name":{"fr":"Nefza","ar":""},"seo_name":{"fr":"nefza","ar":""}},"14":{"id":14,"name":{"fr":"Téboursouk","ar":""},"seo_name":{"fr":"téboursouk","ar":""}},"15":{"id":15,"name":{"fr":"Testour","ar":""},"seo_name":{"fr":"testour","ar":""}},"16":{"id":16,"name":{"fr":"Thibar","ar":""},"seo_name":{"fr":"thibar","ar":""}}},"areaOrder":[{"id":8,"order_id":0},{"id":9,"order_id":1},{"id":10,"order_id":2},{"id":11,"order_id":3},{"id":12,"order_id":4},{"id":13,"order_id":5},{"id":14,"order_id":6},{"id":15,"order_id":7},{"id":16,"order_id":8}]},"3":{"id":"3","coordinates":{"lat":"","lng":""},"name":{"fr":"Ben Arous","ar":""},"seo_name":{"fr":"ben_arous","ar":""},"areas":{"17":{"id":17,"name":{"fr":"Ben Arous","ar":""},"seo_name":{"fr":"ben_arous","ar":""}},"18":{"id":18,"name":{"fr":"Boumhel","ar":""},"seo_name":{"fr":"boumhel","ar":""}},"19":{"id":19,"name":{"fr":"El Mourouj","ar":""},"seo_name":{"fr":"el_mourouj","ar":""}},"20":{"id":20,"name":{"fr":"Ezzahra","ar":""},"seo_name":{"fr":"ezzahra","ar":""}},"21":{"id":21,"name":{"fr":"Fouchana","ar":""},"seo_name":{"fr":"fouchana","ar":""}},"22":{"id":22,"name":{"fr":"Hammam Chott","ar":""},"seo_name":{"fr":"hammam_chott","ar":""}},"23":{"id":23,"name":{"fr":"Hammam Lif","ar":""},"seo_name":{"fr":"hammam_lif","ar":""}},"24":{"id":24,"name":{"fr":"Mohamedia","ar":""},"seo_name":{"fr":"mohamedia","ar":""}},"25":{"id":25,"name":{"fr":"Medina Jedida","ar":""},"seo_name":{"fr":"medina_jedida","ar":""}},"26":{"id":26,"name":{"fr":"Mégrine","ar":""},"seo_name":{"fr":"mégrine","ar":""}},"27":{"id":27,"name":{"fr":"Mornag","ar":""},"seo_name":{"fr":"mornag","ar":""}},"28":{"id":28,"name":{"fr":"Radès","ar":""},"seo_name":{"fr":"radès","ar":""}},"304":{"id":304,"name":{"fr":"Borj Cedria","ar":""},"seo_name":{"fr":"borj_cedria","ar":""}}},"areaOrder":[{"id":17,"order_id":0},{"id":304,"order_id":1},{"id":18,"order_id":2},{"id":19,"order_id":3},{"id":20,"order_id":4},{"id":21,"order_id":5},{"id":22,"order_id":6},{"id":23,"order_id":7},{"id":24,"order_id":8},{"id":25,"order_id":9},{"id":26,"order_id":10},{"id":27,"order_id":11},{"id":28,"order_id":12}]},"4":{"id":"4","coordinates":{"lat":"","lng":""},"name":{"fr":"Bizerte","ar":""},"seo_name":{"fr":"bizerte","ar":""},"areas":{"29":{"id":29,"name":{"fr":"Bizerte Nord","ar":""},"seo_name":{"fr":"bizerte_nord","ar":""}},"30":{"id":30,"name":{"fr":"Bizerte Sud","ar":""},"seo_name":{"fr":"bizerte_sud","ar":""}},"31":{"id":31,"name":{"fr":"Djoumime","ar":""},"seo_name":{"fr":"djoumime","ar":""}},"32":{"id":32,"name":{"fr":"El Alia","ar":""},"seo_name":{"fr":"el_alia","ar":""}},"33":{"id":33,"name":{"fr":"Ghar El Melh","ar":""},"seo_name":{"fr":"ghar_el_melh","ar":""}},"34":{"id":34,"name":{"fr":"Ghezala","ar":""},"seo_name":{"fr":"ghezala","ar":""}},"35":{"id":35,"name":{"fr":"Mateur","ar":""},"seo_name":{"fr":"mateur","ar":""}},"36":{"id":36,"name":{"fr":"Menzel Bourguiba","ar":""},"seo_name":{"fr":"menzel_bourguiba","ar":""}},"37":{"id":37,"name":{"fr":"Menzel Jemil","ar":""},"seo_name":{"fr":"menzel_jemil","ar":""}},"38":{"id":38,"name":{"fr":"Ras Jebel","ar":""},"seo_name":{"fr":"ras_jebel","ar":""}},"39":{"id":39,"name":{"fr":"Sejenane","ar":""},"seo_name":{"fr":"sejenane","ar":""}},"40":{"id":40,"name":{"fr":"Tinja","ar":""},"seo_name":{"fr":"tinja","ar":""}},"41":{"id":41,"name":{"fr":"Utique","ar":""},"seo_name":{"fr":"utique","ar":""}},"42":{"id":42,"name":{"fr":"Zarzouna","ar":""},"seo_name":{"fr":"zarzouna","ar":""}}},"areaOrder":[{"id":29,"order_id":0},{"id":30,"order_id":1},{"id":31,"order_id":2},{"id":32,"order_id":3},{"id":33,"order_id":4},{"id":34,"order_id":5},{"id":35,"order_id":6},{"id":36,"order_id":7},{"id":37,"order_id":8},{"id":38,"order_id":9},{"id":39,"order_id":10},{"id":40,"order_id":11},{"id":41,"order_id":12},{"id":42,"order_id":13}]},"5":{"id":"5","coordinates":{"lat":"","lng":""},"name":{"fr":"Gabès","ar":""},"seo_name":{"fr":"gabès","ar":""},"areas":{"43":{"id":43,"name":{"fr":"Gabès Médina","ar":""},"seo_name":{"fr":"gabès_médina","ar":""}},"44":{"id":44,"name":{"fr":"Gabès Ouest","ar":""},"seo_name":{"fr":"gabès_ouest","ar":""}},"45":{"id":45,"name":{"fr":"Gabès Sud","ar":""},"seo_name":{"fr":"gabès_sud","ar":""}},"46":{"id":46,"name":{"fr":"Ghanouch","ar":""},"seo_name":{"fr":"ghanouch","ar":""}},"47":{"id":47,"name":{"fr":"El Hamma","ar":""},"seo_name":{"fr":"el_hamma","ar":""}},"48":{"id":48,"name":{"fr":"Matmata","ar":""},"seo_name":{"fr":"matmata","ar":""}},"49":{"id":49,"name":{"fr":"Mareth","ar":""},"seo_name":{"fr":"mareth","ar":""}},"50":{"id":50,"name":{"fr":"Menzel El Habib","ar":""},"seo_name":{"fr":"menzel_el_habib","ar":""}},"51":{"id":51,"name":{"fr":"Métouia","ar":""},"seo_name":{"fr":"métouia","ar":""}},"52":{"id":52,"name":{"fr":"Nouvelle Matmata","ar":""},"seo_name":{"fr":"nouvelle_matmata","ar":""}}},"areaOrder":[{"id":43,"order_id":0},{"id":44,"order_id":1},{"id":45,"order_id":2},{"id":46,"order_id":3},{"id":47,"order_id":4},{"id":48,"order_id":5},{"id":49,"order_id":6},{"id":50,"order_id":7},{"id":51,"order_id":8},{"id":52,"order_id":9}]},"6":{"id":"6","coordinates":{"lat":"","lng":""},"name":{"fr":"Gafsa","ar":""},"seo_name":{"fr":"gafsa","ar":""},"areas":{"53":{"id":53,"name":{"fr":"Belkhir","ar":""},"seo_name":{"fr":"belkhir","ar":""}},"54":{"id":54,"name":{"fr":"El Guettar","ar":""},"seo_name":{"fr":"el_guettar","ar":""}},"55":{"id":55,"name":{"fr":"El Ksar","ar":""},"seo_name":{"fr":"el_ksar","ar":""}},"56":{"id":56,"name":{"fr":"Gafsa Nord","ar":""},"seo_name":{"fr":"gafsa_nord","ar":""}},"57":{"id":57,"name":{"fr":"Gafsa Sud","ar":""},"seo_name":{"fr":"gafsa_sud","ar":""}},"58":{"id":58,"name":{"fr":"Mdhila","ar":""},"seo_name":{"fr":"mdhila","ar":""}},"59":{"id":59,"name":{"fr":"Métlaoui","ar":""},"seo_name":{"fr":"métlaoui","ar":""}},"60":{"id":60,"name":{"fr":"Oum El Araies","ar":""},"seo_name":{"fr":"oum_el_araies","ar":""}},"61":{"id":61,"name":{"fr":"Redeyef","ar":""},"seo_name":{"fr":"redeyef","ar":""}},"62":{"id":62,"name":{"fr":"Sidi Aïch","ar":""},"seo_name":{"fr":"sidi_aïch","ar":""}},"63":{"id":63,"name":{"fr":"Sened","ar":""},"seo_name":{"fr":"sened","ar":""}}},"areaOrder":[{"id":53,"order_id":0},{"id":54,"order_id":1},{"id":55,"order_id":2},{"id":56,"order_id":3},{"id":57,"order_id":4},{"id":58,"order_id":5},{"id":59,"order_id":6},{"id":60,"order_id":7},{"id":61,"order_id":8},{"id":62,"order_id":9},{"id":63,"order_id":10}]},"7":{"id":"7","coordinates":{"lat":"","lng":""},"name":{"fr":"Jendouba","ar":""},"seo_name":{"fr":"jendouba","ar":""},"areas":{"64":{"id":64,"name":{"fr":"Ain Draham","ar":""},"seo_name":{"fr":"ain_draham","ar":""}},"65":{"id":65,"name":{"fr":"Balta-Bou Aouane","ar":""},"seo_name":{"fr":"balta-bou_aouane","ar":""}},"66":{"id":66,"name":{"fr":"Bou Salem","ar":""},"seo_name":{"fr":"bou_salem","ar":""}},"67":{"id":67,"name":{"fr":"Fernana","ar":""},"seo_name":{"fr":"fernana","ar":""}},"68":{"id":68,"name":{"fr":"Ghardimaou","ar":""},"seo_name":{"fr":"ghardimaou","ar":""}},"69":{"id":69,"name":{"fr":"Jendouba","ar":""},"seo_name":{"fr":"jendouba","ar":""}},"70":{"id":70,"name":{"fr":"Jendouba Nord","ar":""},"seo_name":{"fr":"jendouba_nord","ar":""}},"71":{"id":71,"name":{"fr":"Oued Meliz","ar":""},"seo_name":{"fr":"oued_meliz","ar":""}},"72":{"id":72,"name":{"fr":"Tabarka","ar":""},"seo_name":{"fr":"tabarka","ar":""}}},"areaOrder":[{"id":64,"order_id":0},{"id":65,"order_id":1},{"id":66,"order_id":2},{"id":67,"order_id":3},{"id":68,"order_id":4},{"id":69,"order_id":5},{"id":70,"order_id":6},{"id":71,"order_id":7},{"id":72,"order_id":8}]},"8":{"id":"8","coordinates":{"lat":"","lng":""},"name":{"fr":"Kairouan","ar":""},"seo_name":{"fr":"kairouan","ar":""},"areas":{"73":{"id":73,"name":{"fr":"Bouhajla","ar":""},"seo_name":{"fr":"bouhajla","ar":""}},"74":{"id":74,"name":{"fr":"Chebika","ar":""},"seo_name":{"fr":"chebika","ar":""}},"75":{"id":75,"name":{"fr":"Echrarda","ar":""},"seo_name":{"fr":"echrarda","ar":""}},"76":{"id":76,"name":{"fr":"El Alâa","ar":""},"seo_name":{"fr":"el_alâa","ar":""}},"77":{"id":77,"name":{"fr":"El Ouslatia","ar":""},"seo_name":{"fr":"el_ouslatia","ar":""}},"78":{"id":78,"name":{"fr":"Haffouz","ar":""},"seo_name":{"fr":"haffouz","ar":""}},"79":{"id":79,"name":{"fr":"Hajeb El Ayoun","ar":""},"seo_name":{"fr":"hajeb_el_ayoun","ar":""}},"80":{"id":80,"name":{"fr":"Kairouan Nord","ar":""},"seo_name":{"fr":"kairouan_nord","ar":""}},"81":{"id":81,"name":{"fr":"Kairouan Sud","ar":""},"seo_name":{"fr":"kairouan_sud","ar":""}},"82":{"id":82,"name":{"fr":"Nasrallah","ar":""},"seo_name":{"fr":"nasrallah","ar":""}},"83":{"id":83,"name":{"fr":"Sbikha","ar":""},"seo_name":{"fr":"sbikha","ar":""}}},"areaOrder":[{"id":73,"order_id":0},{"id":74,"order_id":1},{"id":75,"order_id":2},{"id":76,"order_id":3},{"id":77,"order_id":4},{"id":78,"order_id":5},{"id":79,"order_id":6},{"id":80,"order_id":7},{"id":81,"order_id":8},{"id":82,"order_id":9},{"id":83,"order_id":10}]},"9":{"id":"9","coordinates":{"lat":"","lng":""},"name":{"fr":"Kasserine","ar":""},"seo_name":{"fr":"kasserine","ar":""},"areas":{"84":{"id":84,"name":{"fr":"Djedeliane","ar":""},"seo_name":{"fr":"djedeliane","ar":""}},"85":{"id":85,"name":{"fr":"El Ayoun","ar":""},"seo_name":{"fr":"el_ayoun","ar":""}},"86":{"id":86,"name":{"fr":"Ezzouhour","ar":""},"seo_name":{"fr":"ezzouhour","ar":""}},"87":{"id":87,"name":{"fr":"Fériana","ar":""},"seo_name":{"fr":"fériana","ar":""}},"88":{"id":88,"name":{"fr":"Foussana","ar":""},"seo_name":{"fr":"foussana","ar":""}},"89":{"id":89,"name":{"fr":"Hassi Ferid","ar":""},"seo_name":{"fr":"hassi_ferid","ar":""}},"90":{"id":90,"name":{"fr":"Hidra","ar":""},"seo_name":{"fr":"hidra","ar":""}},"91":{"id":91,"name":{"fr":"Kasserine Nord","ar":""},"seo_name":{"fr":"kasserine_nord","ar":""}},"92":{"id":92,"name":{"fr":"Kasserine Sud","ar":""},"seo_name":{"fr":"kasserine_sud","ar":""}},"93":{"id":93,"name":{"fr":"Majel Bel Abbès","ar":""},"seo_name":{"fr":"majel_bel_abbès","ar":""}},"94":{"id":94,"name":{"fr":"Sbeïtla","ar":""},"seo_name":{"fr":"sbeïtla","ar":""}},"95":{"id":95,"name":{"fr":"Sbiba","ar":""},"seo_name":{"fr":"sbiba","ar":""}},"96":{"id":96,"name":{"fr":"Thala","ar":""},"seo_name":{"fr":"thala","ar":""}}},"areaOrder":[{"id":84,"order_id":0},{"id":85,"order_id":1},{"id":86,"order_id":2},{"id":87,"order_id":3},{"id":88,"order_id":4},{"id":89,"order_id":5},{"id":90,"order_id":6},{"id":91,"order_id":7},{"id":92,"order_id":8},{"id":93,"order_id":9},{"id":94,"order_id":10},{"id":95,"order_id":11},{"id":96,"order_id":12}]},"10":{"id":"10","coordinates":{"lat":"","lng":""},"name":{"fr":"Kébili","ar":""},"seo_name":{"fr":"kébili","ar":""},"areas":{"97":{"id":97,"name":{"fr":"Douz Nord","ar":""},"seo_name":{"fr":"douz_nord","ar":""}},"98":{"id":98,"name":{"fr":"Douz Sud","ar":""},"seo_name":{"fr":"douz_sud","ar":""}},"99":{"id":99,"name":{"fr":"Faouar","ar":""},"seo_name":{"fr":"faouar","ar":""}},"100":{"id":100,"name":{"fr":"Kébili Nord","ar":""},"seo_name":{"fr":"kébili_nord","ar":""}},"101":{"id":101,"name":{"fr":"Kébili Sud","ar":""},"seo_name":{"fr":"kébili_sud","ar":""}},"102":{"id":102,"name":{"fr":"Souk Lahad","ar":""},"seo_name":{"fr":"souk_lahad","ar":""}}},"areaOrder":[{"id":97,"order_id":0},{"id":98,"order_id":1},{"id":99,"order_id":2},{"id":100,"order_id":3},{"id":101,"order_id":4},{"id":102,"order_id":5}]},"11":{"id":"11","coordinates":{"lat":"","lng":""},"name":{"fr":"Le Kef","ar":""},"seo_name":{"fr":"le_kef","ar":""},"areas":{"103":{"id":103,"name":{"fr":"Dahmani","ar":""},"seo_name":{"fr":"dahmani","ar":""}},"104":{"id":104,"name":{"fr":"Djerissa","ar":""},"seo_name":{"fr":"djerissa","ar":""}},"105":{"id":105,"name":{"fr":"El Ksour","ar":""},"seo_name":{"fr":"el_ksour","ar":""}},"106":{"id":106,"name":{"fr":"Es-Sers","ar":""},"seo_name":{"fr":"es-sers","ar":""}},"107":{"id":107,"name":{"fr":"Kalâat Khasbah","ar":""},"seo_name":{"fr":"kalâat_khasbah","ar":""}},"108":{"id":108,"name":{"fr":"Kalâat Snan","ar":""},"seo_name":{"fr":"kalâat_snan","ar":""}},"109":{"id":109,"name":{"fr":"Kef Est","ar":""},"seo_name":{"fr":"kef_est","ar":""}},"110":{"id":110,"name":{"fr":"Kef Ouest","ar":""},"seo_name":{"fr":"kef_ouest","ar":""}},"111":{"id":111,"name":{"fr":"Nebeur","ar":""},"seo_name":{"fr":"nebeur","ar":""}},"112":{"id":112,"name":{"fr":"Sakiet Sidi Youssef","ar":""},"seo_name":{"fr":"sakiet_sidi_youssef","ar":""}},"113":{"id":113,"name":{"fr":"Tajerouine","ar":""},"seo_name":{"fr":"tajerouine","ar":""}}},"areaOrder":[{"id":103,"order_id":0},{"id":104,"order_id":1},{"id":105,"order_id":2},{"id":106,"order_id":3},{"id":107,"order_id":4},{"id":108,"order_id":5},{"id":109,"order_id":6},{"id":110,"order_id":7},{"id":111,"order_id":8},{"id":112,"order_id":9},{"id":113,"order_id":10}]},"12":{"id":"12","coordinates":{"lat":"","lng":""},"name":{"fr":"Mahdia","ar":""},"seo_name":{"fr":"mahdia","ar":""},"areas":{"114":{"id":114,"name":{"fr":"Bou Merdès","ar":""},"seo_name":{"fr":"bou_merdès","ar":""}},"115":{"id":115,"name":{"fr":"Chebba","ar":""},"seo_name":{"fr":"chebba","ar":""}},"116":{"id":116,"name":{"fr":"Chorbane","ar":""},"seo_name":{"fr":"chorbane","ar":""}},"117":{"id":117,"name":{"fr":"El Jem","ar":""},"seo_name":{"fr":"el_jem","ar":""}},"118":{"id":118,"name":{"fr":"Essouassi","ar":""},"seo_name":{"fr":"essouassi","ar":""}},"119":{"id":119,"name":{"fr":"Hebira","ar":""},"seo_name":{"fr":"hebira","ar":""}},"120":{"id":120,"name":{"fr":"Ksour Essef","ar":""},"seo_name":{"fr":"ksour_essef","ar":""}},"121":{"id":121,"name":{"fr":"Mahdia","ar":""},"seo_name":{"fr":"mahdia","ar":""}},"122":{"id":122,"name":{"fr":"Melloulèche","ar":""},"seo_name":{"fr":"melloulèche","ar":""}},"123":{"id":123,"name":{"fr":"Ouled Chamekh","ar":""},"seo_name":{"fr":"ouled_chamekh","ar":""}},"124":{"id":124,"name":{"fr":"Sidi Alouane","ar":""},"seo_name":{"fr":"sidi_alouane","ar":""}}},"areaOrder":[{"id":114,"order_id":0},{"id":115,"order_id":1},{"id":116,"order_id":2},{"id":117,"order_id":3},{"id":118,"order_id":4},{"id":119,"order_id":5},{"id":120,"order_id":6},{"id":121,"order_id":7},{"id":122,"order_id":8},{"id":123,"order_id":9},{"id":124,"order_id":10}]},"13":{"id":"13","coordinates":{"lat":"","lng":""},"name":{"fr":"La Manouba","ar":""},"seo_name":{"fr":"la_manouba","ar":""},"areas":{"125":{"id":125,"name":{"fr":"Borj El Amri","ar":""},"seo_name":{"fr":"borj_el_amri","ar":""}},"126":{"id":126,"name":{"fr":"Djedeida","ar":""},"seo_name":{"fr":"djedeida","ar":""}},"127":{"id":127,"name":{"fr":"Douar Hicher","ar":""},"seo_name":{"fr":"douar_hicher","ar":""}},"128":{"id":128,"name":{"fr":"El Battan","ar":""},"seo_name":{"fr":"el_battan","ar":""}},"130":{"id":130,"name":{"fr":"Mornaguia","ar":""},"seo_name":{"fr":"mornaguia","ar":""}},"131":{"id":131,"name":{"fr":"Oued Ellil","ar":""},"seo_name":{"fr":"oued_ellil","ar":""}},"132":{"id":132,"name":{"fr":"Tebourba","ar":""},"seo_name":{"fr":"tebourba","ar":""}},"272":{"id":272,"name":{"fr":"Manouba Ville","ar":""},"seo_name":{"fr":"manouba_ville","ar":""}},"273":{"id":273,"name":{"fr":"Denden","ar":""},"seo_name":{"fr":"denden","ar":""}},"274":{"id":274,"name":{"fr":"Ksar Said","ar":""},"seo_name":{"fr":"ksar_said","ar":""}}},"areaOrder":[{"id":125,"order_id":0},{"id":126,"order_id":1},{"id":127,"order_id":2},{"id":128,"order_id":3},{"id":130,"order_id":4},{"id":131,"order_id":5},{"id":132,"order_id":6},{"id":272,"order_id":7},{"id":273,"order_id":8},{"id":274,"order_id":9}]},"14":{"id":"14","coordinates":{"lat":"","lng":""},"name":{"fr":"Médenine","ar":""},"seo_name":{"fr":"médenine","ar":""},"areas":{"133":{"id":133,"name":{"fr":"Ben Gardane","ar":""},"seo_name":{"fr":"ben_gardane","ar":""}},"134":{"id":134,"name":{"fr":"Beni Khedech","ar":""},"seo_name":{"fr":"beni_khedech","ar":""}},"135":{"id":135,"name":{"fr":"Djerba - Ajim","ar":""},"seo_name":{"fr":"djerba-ajim","ar":""}},"136":{"id":136,"name":{"fr":"Djerba-Houmt Souk","ar":""},"seo_name":{"fr":"djerba-houmt_souk","ar":""}},"137":{"id":137,"name":{"fr":"Djerba - Midoun","ar":""},"seo_name":{"fr":"djerba-midoun","ar":""}},"138":{"id":138,"name":{"fr":"Médenine Nord","ar":""},"seo_name":{"fr":"médenine_nord","ar":""}},"139":{"id":139,"name":{"fr":"Médenine Sud","ar":""},"seo_name":{"fr":"médenine_sud","ar":""}},"140":{"id":140,"name":{"fr":"Sidi Makhloulf","ar":""},"seo_name":{"fr":"sidi_makhloulf","ar":""}},"141":{"id":141,"name":{"fr":"Zarzis","ar":""},"seo_name":{"fr":"zarzis","ar":""}}},"areaOrder":[{"id":133,"order_id":0},{"id":134,"order_id":1},{"id":135,"order_id":2},{"id":136,"order_id":3},{"id":137,"order_id":4},{"id":138,"order_id":5},{"id":139,"order_id":6},{"id":140,"order_id":7},{"id":141,"order_id":8}]},"15":{"id":"15","coordinates":{"lat":"","lng":""},"name":{"fr":"Monastir","ar":""},"seo_name":{"fr":"monastir","ar":""},"areas":{"142":{"id":142,"name":{"fr":"Bekalta","ar":""},"seo_name":{"fr":"bekalta","ar":""}},"143":{"id":143,"name":{"fr":"Bembla","ar":""},"seo_name":{"fr":"bembla","ar":""}},"144":{"id":144,"name":{"fr":"Beni Hassen","ar":""},"seo_name":{"fr":"beni_hassen","ar":""}},"145":{"id":145,"name":{"fr":"Jemmal","ar":""},"seo_name":{"fr":"jemmal","ar":""}},"146":{"id":146,"name":{"fr":"Ksar Hellal","ar":""},"seo_name":{"fr":"ksar_hellal","ar":""}},"147":{"id":147,"name":{"fr":"Ksibet el-Médiouni","ar":""},"seo_name":{"fr":"ksibet_el-médiouni","ar":""}},"148":{"id":148,"name":{"fr":"Moknine","ar":""},"seo_name":{"fr":"moknine","ar":""}},"149":{"id":149,"name":{"fr":"Monastir","ar":""},"seo_name":{"fr":"monastir","ar":""}},"150":{"id":150,"name":{"fr":"Ouerdanine","ar":""},"seo_name":{"fr":"ouerdanine","ar":""}},"151":{"id":151,"name":{"fr":"Sahline","ar":""},"seo_name":{"fr":"sahline","ar":""}},"152":{"id":152,"name":{"fr":"Sayada-Lamta-Bou Hajar","ar":""},"seo_name":{"fr":"sayada-lamta-bou_hajar","ar":""}},"153":{"id":153,"name":{"fr":"Téboulba","ar":""},"seo_name":{"fr":"téboulba","ar":""}},"154":{"id":154,"name":{"fr":"Zéramdine","ar":""},"seo_name":{"fr":"zéramdine","ar":""}}},"areaOrder":[{"id":142,"order_id":0},{"id":143,"order_id":1},{"id":144,"order_id":2},{"id":145,"order_id":3},{"id":146,"order_id":4},{"id":147,"order_id":5},{"id":148,"order_id":6},{"id":149,"order_id":7},{"id":150,"order_id":8},{"id":151,"order_id":9},{"id":152,"order_id":10},{"id":153,"order_id":11},{"id":154,"order_id":12}]},"16":{"id":"16","coordinates":{"lat":"","lng":""},"name":{"fr":"Nabeul","ar":""},"seo_name":{"fr":"nabeul","ar":""},"areas":{"155":{"id":155,"name":{"fr":"Béni Khalled","ar":""},"seo_name":{"fr":"béni_khalled","ar":""}},"156":{"id":156,"name":{"fr":"Béni Khiar","ar":""},"seo_name":{"fr":"béni_khiar","ar":""}},"157":{"id":157,"name":{"fr":"Bou Argoub","ar":""},"seo_name":{"fr":"bou_argoub","ar":""}},"158":{"id":158,"name":{"fr":"Dar Châabane El Fehri","ar":""},"seo_name":{"fr":"dar_châabane_el_fehri","ar":""}},"159":{"id":159,"name":{"fr":"El Haouaria","ar":""},"seo_name":{"fr":"el_haouaria","ar":""}},"160":{"id":160,"name":{"fr":"El Mida","ar":""},"seo_name":{"fr":"el_mida","ar":""}},"161":{"id":161,"name":{"fr":"Grombalia","ar":""},"seo_name":{"fr":"grombalia","ar":""}},"162":{"id":162,"name":{"fr":"Hammam Ghezèze","ar":""},"seo_name":{"fr":"hammam_ghezèze","ar":""}},"163":{"id":163,"name":{"fr":"Hammamet","ar":""},"seo_name":{"fr":"hammamet","ar":""}},"164":{"id":164,"name":{"fr":"Kélibia","ar":""},"seo_name":{"fr":"kélibia","ar":""}},"165":{"id":165,"name":{"fr":"Korba","ar":""},"seo_name":{"fr":"korba","ar":""}},"166":{"id":166,"name":{"fr":"Menzel Bouzelfa","ar":""},"seo_name":{"fr":"menzel_bouzelfa","ar":""}},"167":{"id":167,"name":{"fr":"Menzel Temime","ar":""},"seo_name":{"fr":"menzel_temime","ar":""}},"168":{"id":168,"name":{"fr":"Nabeul","ar":""},"seo_name":{"fr":"nabeul","ar":""}},"169":{"id":169,"name":{"fr":"Soliman","ar":""},"seo_name":{"fr":"soliman","ar":""}},"170":{"id":170,"name":{"fr":"Takelsa","ar":""},"seo_name":{"fr":"takelsa","ar":""}},"275":{"id":275,"name":{"fr":"Hammamet Centre","ar":""},"seo_name":{"fr":"hammamet_centre","ar":""}},"276":{"id":276,"name":{"fr":"Hammamet Nord","ar":""},"seo_name":{"fr":"hammamet_nord","ar":""}},"277":{"id":277,"name":{"fr":"Hammamet Sud","ar":""},"seo_name":{"fr":"hammamet_sud","ar":""}},"278":{"id":278,"name":{"fr":"Mrezga","ar":""},"seo_name":{"fr":"mrezga","ar":""}}},"areaOrder":[{"id":155,"order_id":0},{"id":156,"order_id":1},{"id":157,"order_id":2},{"id":158,"order_id":3},{"id":159,"order_id":4},{"id":160,"order_id":5},{"id":161,"order_id":6},{"id":162,"order_id":7},{"id":163,"order_id":8},{"id":164,"order_id":9},{"id":165,"order_id":10},{"id":166,"order_id":11},{"id":167,"order_id":12},{"id":168,"order_id":13},{"id":169,"order_id":14},{"id":170,"order_id":15},{"id":275,"order_id":16},{"id":276,"order_id":17},{"id":277,"order_id":18},{"id":278,"order_id":19}]},"17":{"id":"17","coordinates":{"lat":"","lng":""},"name":{"fr":"Sfax","ar":""},"seo_name":{"fr":"sfax","ar":""},"areas":{"171":{"id":171,"name":{"fr":"Agareb","ar":""},"seo_name":{"fr":"agareb","ar":""}},"172":{"id":172,"name":{"fr":"Bir Ali Ben Khalifa","ar":""},"seo_name":{"fr":"bir_ali_ben_khalifa","ar":""}},"173":{"id":173,"name":{"fr":"Jebiniana","ar":""},"seo_name":{"fr":"jebiniana","ar":""}},"174":{"id":174,"name":{"fr":"El Amra","ar":""},"seo_name":{"fr":"el_amra","ar":""}},"175":{"id":175,"name":{"fr":"El Hencha","ar":""},"seo_name":{"fr":"el_hencha","ar":""}},"176":{"id":176,"name":{"fr":"Ghraiba","ar":""},"seo_name":{"fr":"ghraiba","ar":""}},"177":{"id":177,"name":{"fr":"Kerkennah","ar":""},"seo_name":{"fr":"kerkennah","ar":""}},"178":{"id":178,"name":{"fr":"Mahrès","ar":""},"seo_name":{"fr":"mahrès","ar":""}},"179":{"id":179,"name":{"fr":"Menzel Chaker","ar":""},"seo_name":{"fr":"menzel_chaker","ar":""}},"180":{"id":180,"name":{"fr":"Sakiet Eddaïer","ar":""},"seo_name":{"fr":"sakiet_eddaïer","ar":""}},"181":{"id":181,"name":{"fr":"Sakiet Ezzit","ar":""},"seo_name":{"fr":"sakiet_ezzit","ar":""}},"184":{"id":184,"name":{"fr":"Sfax Ville","ar":""},"seo_name":{"fr":"sfax_ville","ar":""}},"185":{"id":185,"name":{"fr":"Skhira","ar":""},"seo_name":{"fr":"skhira","ar":""}},"186":{"id":186,"name":{"fr":"Thyna","ar":""},"seo_name":{"fr":"thyna","ar":""}},"279":{"id":279,"name":{"fr":"Sfax Médina","ar":""},"seo_name":{"fr":"sfax_medina","ar":""}},"280":{"id":280,"name":{"fr":"Route el Ain","ar":""},"seo_name":{"fr":"route_el_ain","ar":""}},"281":{"id":281,"name":{"fr":"Route el Afrane","ar":""},"seo_name":{"fr":"route_el_afrane","ar":""}},"282":{"id":282,"name":{"fr":"Route Gremda","ar":""},"seo_name":{"fr":"route_gremda","ar":""}},"283":{"id":283,"name":{"fr":"Route Menzel Chaker","ar":""},"seo_name":{"fr":"route_menzel_chaker","ar":""}},"284":{"id":284,"name":{"fr":"Route Matar","ar":""},"seo_name":{"fr":"route_matar","ar":""}},"285":{"id":285,"name":{"fr":"Route Soukra","ar":""},"seo_name":{"fr":"route_soukra","ar":""}},"286":{"id":286,"name":{"fr":"Route Tunis","ar":""},"seo_name":{"fr":"route_tunis","ar":""}},"287":{"id":287,"name":{"fr":"Route Mehdia","ar":""},"seo_name":{"fr":"route_mehdia","ar":""}},"288":{"id":288,"name":{"fr":"Route Teniour","ar":""},"seo_name":{"fr":"route_teniour","ar":""}}},"areaOrder":[{"id":171,"order_id":0},{"id":172,"order_id":1},{"id":173,"order_id":2},{"id":174,"order_id":3},{"id":175,"order_id":4},{"id":176,"order_id":5},{"id":177,"order_id":6},{"id":178,"order_id":7},{"id":179,"order_id":8},{"id":180,"order_id":9},{"id":181,"order_id":10},{"id":184,"order_id":11},{"id":185,"order_id":12},{"id":186,"order_id":13},{"id":279,"order_id":14},{"id":280,"order_id":15},{"id":281,"order_id":16},{"id":282,"order_id":17},{"id":283,"order_id":18},{"id":284,"order_id":19},{"id":285,"order_id":20},{"id":286,"order_id":21},{"id":287,"order_id":22},{"id":288,"order_id":23}]},"18":{"id":"18","coordinates":{"lat":"","lng":""},"name":{"fr":"Sidi Bouzid","ar":""},"seo_name":{"fr":"sidi_bouzid","ar":""},"areas":{"187":{"id":187,"name":{"fr":"Bir El Hafey","ar":""},"seo_name":{"fr":"bir_el_hafey","ar":""}},"188":{"id":188,"name":{"fr":"Cebbala Ouled Asker","ar":""},"seo_name":{"fr":"cebbala_ouled_asker","ar":""}},"189":{"id":189,"name":{"fr":"Jilma","ar":""},"seo_name":{"fr":"jilma","ar":""}},"190":{"id":190,"name":{"fr":"Meknassy","ar":""},"seo_name":{"fr":"meknassy","ar":""}},"191":{"id":191,"name":{"fr":"Menzel Bouzaiane","ar":""},"seo_name":{"fr":"menzel_bouzaiane","ar":""}},"192":{"id":192,"name":{"fr":"Mezzouna","ar":""},"seo_name":{"fr":"mezzouna","ar":""}},"193":{"id":193,"name":{"fr":"Ouled Haffouz","ar":""},"seo_name":{"fr":"ouled_haffouz","ar":""}},"194":{"id":194,"name":{"fr":"Regueb","ar":""},"seo_name":{"fr":"regueb","ar":""}},"195":{"id":195,"name":{"fr":"Sidi Ali Ben Aoun","ar":""},"seo_name":{"fr":"sidi_ali_ben_aoun","ar":""}},"196":{"id":196,"name":{"fr":"Sidi Bouzid Est","ar":""},"seo_name":{"fr":"sidi_bouzid_est","ar":""}},"197":{"id":197,"name":{"fr":"Sidi Bouzid Ouest","ar":""},"seo_name":{"fr":"sidi_bouzid_ouest","ar":""}},"198":{"id":198,"name":{"fr":"Souk Jedid","ar":""},"seo_name":{"fr":"souk_jedid","ar":""}}},"areaOrder":[{"id":187,"order_id":0},{"id":188,"order_id":1},{"id":189,"order_id":2},{"id":190,"order_id":3},{"id":191,"order_id":4},{"id":192,"order_id":5},{"id":193,"order_id":6},{"id":194,"order_id":7},{"id":195,"order_id":8},{"id":196,"order_id":9},{"id":197,"order_id":10},{"id":198,"order_id":11}]},"19":{"id":"19","coordinates":{"lat":"","lng":""},"name":{"fr":"Siliana","ar":""},"seo_name":{"fr":"siliana","ar":""},"areas":{"199":{"id":199,"name":{"fr":"Bargou","ar":""},"seo_name":{"fr":"bargou","ar":""}},"200":{"id":200,"name":{"fr":"Bou Arada","ar":""},"seo_name":{"fr":"bou_arada","ar":""}},"201":{"id":201,"name":{"fr":"Sidi Bou Rouis","ar":""},"seo_name":{"fr":"sidi_bou_rouis","ar":""}},"202":{"id":202,"name":{"fr":"El Aroussa","ar":""},"seo_name":{"fr":"el_aroussa","ar":""}},"203":{"id":203,"name":{"fr":"El Krib","ar":""},"seo_name":{"fr":"el_krib","ar":""}},"204":{"id":204,"name":{"fr":"Rouhia","ar":""},"seo_name":{"fr":"rouhia","ar":""}},"205":{"id":205,"name":{"fr":"Gaâfour","ar":""},"seo_name":{"fr":"gaâfour","ar":""}},"206":{"id":206,"name":{"fr":"Kesra","ar":""},"seo_name":{"fr":"kesra","ar":""}},"207":{"id":207,"name":{"fr":"Makthar","ar":""},"seo_name":{"fr":"makthar","ar":""}},"208":{"id":208,"name":{"fr":"Siliana Nord","ar":""},"seo_name":{"fr":"siliana_nord","ar":""}},"209":{"id":209,"name":{"fr":"Siliana Sud","ar":""},"seo_name":{"fr":"siliana_sud","ar":""}}},"areaOrder":[{"id":199,"order_id":0},{"id":200,"order_id":1},{"id":201,"order_id":2},{"id":202,"order_id":3},{"id":203,"order_id":4},{"id":204,"order_id":5},{"id":205,"order_id":6},{"id":206,"order_id":7},{"id":207,"order_id":8},{"id":208,"order_id":9},{"id":209,"order_id":10}]},"20":{"id":"20","coordinates":{"lat":"","lng":""},"name":{"fr":"Sousse","ar":""},"seo_name":{"fr":"sousse","ar":""},"areas":{"210":{"id":210,"name":{"fr":"Akouda","ar":""},"seo_name":{"fr":"akouda","ar":""}},"211":{"id":211,"name":{"fr":"Bouficha","ar":""},"seo_name":{"fr":"bouficha","ar":""}},"212":{"id":212,"name":{"fr":"Enfidha","ar":""},"seo_name":{"fr":"enfidha","ar":""}},"213":{"id":213,"name":{"fr":"Hammam Sousse","ar":""},"seo_name":{"fr":"hammam_sousse","ar":""}},"214":{"id":214,"name":{"fr":"Hergla","ar":""},"seo_name":{"fr":"hergla","ar":""}},"215":{"id":215,"name":{"fr":"Kalaâ Kebira","ar":""},"seo_name":{"fr":"kalaâ_kebira","ar":""}},"216":{"id":216,"name":{"fr":"Kalaâ Sghira","ar":""},"seo_name":{"fr":"kalaâ_sghira","ar":""}},"217":{"id":217,"name":{"fr":"Kondar","ar":""},"seo_name":{"fr":"kondar","ar":""}},"218":{"id":218,"name":{"fr":"M\'saken","ar":""},"seo_name":{"fr":"msaken","ar":""}},"219":{"id":219,"name":{"fr":"Sidi Bou Ali","ar":""},"seo_name":{"fr":"sidi_bou_ali","ar":""}},"220":{"id":220,"name":{"fr":"Sidi El Héni","ar":""},"seo_name":{"fr":"sidi_el_héni","ar":""}},"221":{"id":221,"name":{"fr":"Sousse Jawhara","ar":""},"seo_name":{"fr":"sousse_jawhara","ar":""}},"222":{"id":222,"name":{"fr":"Sousse Médina","ar":""},"seo_name":{"fr":"sousse_médina","ar":""}},"223":{"id":223,"name":{"fr":"Sousse Riadh","ar":""},"seo_name":{"fr":"sousse_riadh","ar":""}},"224":{"id":224,"name":{"fr":"Sousse Sidi Abdelhamid","ar":""},"seo_name":{"fr":"sousse_sidi_abdelhamid","ar":""}},"225":{"id":225,"name":{"fr":"Zaouit-Ksibat Thrayett","ar":""},"seo_name":{"fr":"zaouit-ksibat_thrayett","ar":""}},"289":{"id":289,"name":{"fr":"Sahloul","ar":""},"seo_name":{"fr":"sahloul","ar":""}},"290":{"id":290,"name":{"fr":"Kantaoui","ar":""},"seo_name":{"fr":"kantaoui","ar":""}}},"areaOrder":[{"id":210,"order_id":0},{"id":211,"order_id":1},{"id":212,"order_id":2},{"id":213,"order_id":3},{"id":214,"order_id":4},{"id":215,"order_id":5},{"id":216,"order_id":6},{"id":217,"order_id":7},{"id":218,"order_id":8},{"id":219,"order_id":9},{"id":220,"order_id":10},{"id":221,"order_id":11},{"id":222,"order_id":12},{"id":223,"order_id":13},{"id":224,"order_id":14},{"id":225,"order_id":15},{"id":289,"order_id":16},{"id":290,"order_id":17}]},"21":{"id":"21","coordinates":{"lat":"","lng":""},"name":{"fr":"Tataouine","ar":""},"seo_name":{"fr":"tataouine","ar":""},"areas":{"226":{"id":226,"name":{"fr":"Bir Lahmar","ar":""},"seo_name":{"fr":"bir_lahmar","ar":""}},"227":{"id":227,"name":{"fr":"Dehiba","ar":""},"seo_name":{"fr":"dehiba","ar":""}},"228":{"id":228,"name":{"fr":"Ghomrassen","ar":""},"seo_name":{"fr":"ghomrassen","ar":""}},"229":{"id":229,"name":{"fr":"Remada","ar":""},"seo_name":{"fr":"remada","ar":""}},"230":{"id":230,"name":{"fr":"Smâr","ar":""},"seo_name":{"fr":"smâr","ar":""}},"231":{"id":231,"name":{"fr":"Tataouine Nord","ar":""},"seo_name":{"fr":"tataouine_nord","ar":""}},"232":{"id":232,"name":{"fr":"Tataouine Sud","ar":""},"seo_name":{"fr":"tataouine_sud","ar":""}}},"areaOrder":[{"id":226,"order_id":0},{"id":227,"order_id":1},{"id":228,"order_id":2},{"id":229,"order_id":3},{"id":230,"order_id":4},{"id":231,"order_id":5},{"id":232,"order_id":6}]},"22":{"id":"22","coordinates":{"lat":"","lng":""},"name":{"fr":"Tozeur","ar":""},"seo_name":{"fr":"tozeur","ar":""},"areas":{"233":{"id":233,"name":{"fr":"Degache","ar":""},"seo_name":{"fr":"degache","ar":""}},"234":{"id":234,"name":{"fr":"Hazoua","ar":""},"seo_name":{"fr":"hazoua","ar":""}},"235":{"id":235,"name":{"fr":"Nefta","ar":""},"seo_name":{"fr":"nefta","ar":""}},"236":{"id":236,"name":{"fr":"Tameghza","ar":""},"seo_name":{"fr":"tameghza","ar":""}},"237":{"id":237,"name":{"fr":"Tozeur","ar":""},"seo_name":{"fr":"tozeur","ar":""}}},"areaOrder":[{"id":233,"order_id":0},{"id":234,"order_id":1},{"id":235,"order_id":2},{"id":236,"order_id":3},{"id":237,"order_id":4}]},"23":{"id":"23","coordinates":{"lat":"","lng":""},"name":{"fr":"Tunis","ar":""},"seo_name":{"fr":"tunis","ar":""},"areas":{"240":{"id":240,"name":{"fr":"Carthage","ar":""},"seo_name":{"fr":"carthage","ar":""}},"241":{"id":241,"name":{"fr":"Cité El Khadra","ar":""},"seo_name":{"fr":"cité_el_khadra","ar":""}},"242":{"id":242,"name":{"fr":"Djebel Jelloud","ar":""},"seo_name":{"fr":"djebel_jelloud","ar":""}},"243":{"id":243,"name":{"fr":"El Kabaria","ar":""},"seo_name":{"fr":"el_kabaria","ar":""}},"245":{"id":245,"name":{"fr":"El Omrane","ar":""},"seo_name":{"fr":"el_omrane","ar":""}},"246":{"id":246,"name":{"fr":"El Omrane supérieur","ar":""},"seo_name":{"fr":"el_omrane_supérieur","ar":""}},"247":{"id":247,"name":{"fr":"El Ouardia","ar":""},"seo_name":{"fr":"el_ouardia","ar":""}},"248":{"id":248,"name":{"fr":"Ettahrir","ar":""},"seo_name":{"fr":"ettahrir","ar":""}},"249":{"id":249,"name":{"fr":"Ezzouhour","ar":""},"seo_name":{"fr":"ezzouhour","ar":""}},"250":{"id":250,"name":{"fr":"Hraïria","ar":""},"seo_name":{"fr":"hraïria","ar":""}},"251":{"id":251,"name":{"fr":"La Goulette","ar":""},"seo_name":{"fr":"la_goulette","ar":""}},"252":{"id":252,"name":{"fr":"La Marsa","ar":""},"seo_name":{"fr":"la_marsa","ar":""}},"253":{"id":253,"name":{"fr":"Le Bardo","ar":""},"seo_name":{"fr":"le_bardo","ar":""}},"254":{"id":254,"name":{"fr":"Le Kram","ar":""},"seo_name":{"fr":"le_kram","ar":""}},"255":{"id":255,"name":{"fr":"Médina","ar":""},"seo_name":{"fr":"médina","ar":""}},"256":{"id":256,"name":{"fr":"Séjoumi","ar":""},"seo_name":{"fr":"séjoumi","ar":""}},"257":{"id":257,"name":{"fr":"Sidi El Béchir","ar":""},"seo_name":{"fr":"sidi_el_béchir","ar":""}},"258":{"id":258,"name":{"fr":"Sidi Hassine","ar":""},"seo_name":{"fr":"sidi_hassine","ar":""}},"291":{"id":291,"name":{"fr":"Agba","ar":""},"seo_name":{"fr":"agba","ar":""}},"292":{"id":292,"name":{"fr":"Aïn Zaghouan","ar":""},"seo_name":{"fr":"ain_zaghouan","ar":""}},"293":{"id":293,"name":{"fr":"Centre Urbain Nord","ar":""},"seo_name":{"fr":"centre_urbain_nord","ar":""}},"294":{"id":294,"name":{"fr":"Centre Ville - Lafayette","ar":""},"seo_name":{"fr":"centre_ville_-_lafayette","ar":""}},"295":{"id":295,"name":{"fr":"Cité Olympique","ar":""},"seo_name":{"fr":"cite_olympique","ar":""}},"296":{"id":296,"name":{"fr":"Gammarth","ar":""},"seo_name":{"fr":"gammarth","ar":""}},"297":{"id":297,"name":{"fr":"Jardin De Carthage","ar":""},"seo_name":{"fr":"jardin_de_carthage","ar":""}},"298":{"id":298,"name":{"fr":"L\'aouina","ar":""},"seo_name":{"fr":"laouina","ar":""}},"299":{"id":299,"name":{"fr":"Les Berges Du Lac","ar":""},"seo_name":{"fr":"les_berges_du_lac","ar":""}},"300":{"id":300,"name":{"fr":"Mutuelleville","ar":""},"seo_name":{"fr":"mutuelleville","ar":""}},"301":{"id":301,"name":{"fr":"Sidi Bou Said","ar":""},"seo_name":{"fr":"sidi_bou_said","ar":""}},"302":{"id":302,"name":{"fr":"Sidi Daoud","ar":""},"seo_name":{"fr":"sidi_daoud","ar":""}}},"areaOrder":[{"id":240,"order_id":0},{"id":241,"order_id":1},{"id":242,"order_id":2},{"id":243,"order_id":3},{"id":245,"order_id":4},{"id":246,"order_id":5},{"id":247,"order_id":6},{"id":248,"order_id":7},{"id":249,"order_id":8},{"id":250,"order_id":9},{"id":251,"order_id":10},{"id":252,"order_id":11},{"id":253,"order_id":12},{"id":254,"order_id":13},{"id":255,"order_id":14},{"id":256,"order_id":15},{"id":257,"order_id":16},{"id":258,"order_id":17},{"id":291,"order_id":18},{"id":292,"order_id":19},{"id":293,"order_id":20},{"id":294,"order_id":21},{"id":295,"order_id":22},{"id":296,"order_id":23},{"id":297,"order_id":24},{"id":298,"order_id":25},{"id":299,"order_id":26},{"id":300,"order_id":27},{"id":301,"order_id":28},{"id":302,"order_id":29}]},"24":{"id":"24","coordinates":{"lat":"","lng":""},"name":{"fr":"Zaghouan","ar":""},"seo_name":{"fr":"zaghouan","ar":""},"areas":{"259":{"id":259,"name":{"fr":"Bir Mchergua","ar":""},"seo_name":{"fr":"bir_mchergua","ar":""}},"260":{"id":260,"name":{"fr":"El Fahs","ar":""},"seo_name":{"fr":"el_fahs","ar":""}},"261":{"id":261,"name":{"fr":"En-Nadhour","ar":""},"seo_name":{"fr":"en-nadhour","ar":""}},"262":{"id":262,"name":{"fr":"Ez-Zeriba","ar":""},"seo_name":{"fr":"ez-zeriba","ar":""}},"263":{"id":263,"name":{"fr":"Saouaf","ar":""},"seo_name":{"fr":"saouaf","ar":""}},"264":{"id":264,"name":{"fr":"Zaghouan","ar":""},"seo_name":{"fr":"zaghouan","ar":""}}},"areaOrder":[{"id":259,"order_id":0},{"id":260,"order_id":1},{"id":261,"order_id":2},{"id":262,"order_id":3},{"id":263,"order_id":4},{"id":264,"order_id":5}]}}');
        $data = json_decode('{
          "1": {
            "id": "1",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Ariana",
              "ar": ""
            },
            "seo_name": {
              "fr": "ariana",
              "ar": ""
            },
            "areas": {
              "1": {
                "id": 1,
                "name": {
                  "fr": "Ariana Ville",
                  "ar": ""
                },
                "code": 2080,
                "seo_name": {
                  "fr": "ariana_ville",
                  "ar": ""
                }
              },
              "2": {
                "id": 2,
                "name": {
                  "fr": "Ettadhamen",
                  "ar": ""
                },
                "code": 2041,
                "seo_name": {
                  "fr": "ettadhamen",
                  "ar": ""
                }
              },
              "3": {
                "id": 3,
                "name": {
                  "fr": "Kalaât El Andalous",
                  "ar": ""
                },
                "code": 2022,
                "seo_name": {
                  "fr": "kalaât_el_andalous",
                  "ar": ""
                }
              },
              "4": {
                "id": 4,
                "name": {
                  "fr": "La Soukra",
                  "ar": ""
                },
                "code": 2036,
                "seo_name": {
                  "fr": "la_soukra",
                  "ar": ""
                }
              },
              "5": {
                "id": 5,
                "name": {
                  "fr": "Mnihla",
                  "ar": ""
                },
                "code": 2094,
                "seo_name": {
                  "fr": "mnihla",
                  "ar": ""
                }
              },
              "6": {
                "id": 6,
                "name": {
                  "fr": "Raoued",
                  "ar": ""
                },
                "code": 2056,
                "seo_name": {
                  "fr": "raoued",
                  "ar": ""
                }
              },
              "7": {
                "id": 7,
                "name": {
                  "fr": "Sidi Thabet",
                  "ar": ""
                },
                "code": 2020,
                "seo_name": {
                  "fr": "sidi_thabet",
                  "ar": ""
                }
              },
              "265": {
                "id": 265,
                "name": {
                  "fr": "Borj Louzir",
                  "ar": ""
                },
                "code": 2073,
                "seo_name": {
                  "fr": "borj_louzir",
                  "ar": ""
                }
              },
              "266": {
                "id": 266,
                "name": {
                  "fr": "Chotrana",
                  "ar": ""
                },
                "code": 2036,
                "seo_name": {
                  "fr": "chotrana",
                  "ar": ""
                }
              },
              "267": {
                "id": 267,
                "name": {
                  "fr": "Ennasr",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ennasr",
                  "ar": ""
                }
              },
              "268": {
                "id": 268,
                "name": {
                  "fr": "Ghazela",
                  "ar": ""
                },
                "code": 2083,
                "seo_name": {
                  "fr": "ghazela",
                  "ar": ""
                }
              },
              "269": {
                "id": 269,
                "name": {
                  "fr": "Jardins D\'el Menzah",
                  "ar": ""
                },
                "code": 2004,
                "seo_name": {
                  "fr": "jardins_del_menzah",
                  "ar": ""
                }
              },
              "270": {
                "id": 270,
                "name": {
                  "fr": "Manar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "manar",
                  "ar": ""
                }
              },
              "271": {
                "id": 271,
                "name": {
                  "fr": "Menzah",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzah",
                  "ar": ""
                }
              }
            }
          },
          "2": {
            "id": "2",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Béja",
              "ar": ""
            },
            "seo_name": {
              "fr": "béja",
              "ar": ""
            },
            "areas": {
              "8": {
                "id": 8,
                "name": {
                  "fr": "Amdoun",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "amdoun",
                  "ar": ""
                }
              },
              "9": {
                "id": 9,
                "name": {
                  "fr": "Béja Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "béja_nord",
                  "ar": ""
                }
              },
              "10": {
                "id": 10,
                "name": {
                  "fr": "Béja Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "béja_sud",
                  "ar": ""
                }
              },
              "11": {
                "id": 11,
                "name": {
                  "fr": "Goubellat",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "goubellat",
                  "ar": ""
                }
              },
              "12": {
                "id": 12,
                "name": {
                  "fr": "Medjez el-Bab",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "medjez_el-bab",
                  "ar": ""
                }
              },
              "13": {
                "id": 13,
                "name": {
                  "fr": "Nefza",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nefza",
                  "ar": ""
                }
              },
              "14": {
                "id": 14,
                "name": {
                  "fr": "Téboursouk",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "téboursouk",
                  "ar": ""
                }
              },
              "15": {
                "id": 15,
                "name": {
                  "fr": "Testour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "testour",
                  "ar": ""
                }
              },
              "16": {
                "id": 16,
                "name": {
                  "fr": "Thibar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "thibar",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 8,
                "order_id": 0
              },
              {
                "id": 9,
                "order_id": 1
              },
              {
                "id": 10,
                "order_id": 2
              },
              {
                "id": 11,
                "order_id": 3
              },
              {
                "id": 12,
                "order_id": 4
              },
              {
                "id": 13,
                "order_id": 5
              },
              {
                "id": 14,
                "order_id": 6
              },
              {
                "id": 15,
                "order_id": 7
              },
              {
                "id": 16,
                "order_id": 8
              }
            ]
          },
          "3": {
            "id": "3",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Ben Arous",
              "ar": ""
            },
            "seo_name": {
              "fr": "ben_arous",
              "ar": ""
            },
            "areas": {
              "17": {
                "id": 17,
                "name": {
                  "fr": "Ben Arous",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ben_arous",
                  "ar": ""
                }
              },
              "18": {
                "id": 18,
                "name": {
                  "fr": "Boumhel",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "boumhel",
                  "ar": ""
                }
              },
              "19": {
                "id": 19,
                "name": {
                  "fr": "El Mourouj",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_mourouj",
                  "ar": ""
                }
              },
              "20": {
                "id": 20,
                "name": {
                  "fr": "Ezzahra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ezzahra",
                  "ar": ""
                }
              },
              "21": {
                "id": 21,
                "name": {
                  "fr": "Fouchana",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "fouchana",
                  "ar": ""
                }
              },
              "22": {
                "id": 22,
                "name": {
                  "fr": "Hammam Chott",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammam_chott",
                  "ar": ""
                }
              },
              "23": {
                "id": 23,
                "name": {
                  "fr": "Hammam Lif",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammam_lif",
                  "ar": ""
                }
              },
              "24": {
                "id": 24,
                "name": {
                  "fr": "Mohamedia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mohamedia",
                  "ar": ""
                }
              },
              "25": {
                "id": 25,
                "name": {
                  "fr": "Medina Jedida",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "medina_jedida",
                  "ar": ""
                }
              },
              "26": {
                "id": 26,
                "name": {
                  "fr": "Mégrine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mégrine",
                  "ar": ""
                }
              },
              "27": {
                "id": 27,
                "name": {
                  "fr": "Mornag",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mornag",
                  "ar": ""
                }
              },
              "28": {
                "id": 28,
                "name": {
                  "fr": "Radès",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "radès",
                  "ar": ""
                }
              },
              "304": {
                "id": 304,
                "name": {
                  "fr": "Borj Cedria",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "borj_cedria",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 17,
                "order_id": 0
              },
              {
                "id": 304,
                "order_id": 1
              },
              {
                "id": 18,
                "order_id": 2
              },
              {
                "id": 19,
                "order_id": 3
              },
              {
                "id": 20,
                "order_id": 4
              },
              {
                "id": 21,
                "order_id": 5
              },
              {
                "id": 22,
                "order_id": 6
              },
              {
                "id": 23,
                "order_id": 7
              },
              {
                "id": 24,
                "order_id": 8
              },
              {
                "id": 25,
                "order_id": 9
              },
              {
                "id": 26,
                "order_id": 10
              },
              {
                "id": 27,
                "order_id": 11
              },
              {
                "id": 28,
                "order_id": 12
              }
            ]
          },
          "4": {
            "id": "4",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Bizerte",
              "ar": ""
            },
            "seo_name": {
              "fr": "bizerte",
              "ar": ""
            },
            "areas": {
              "29": {
                "id": 29,
                "name": {
                  "fr": "Bizerte Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bizerte_nord",
                  "ar": ""
                }
              },
              "30": {
                "id": 30,
                "name": {
                  "fr": "Bizerte Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bizerte_sud",
                  "ar": ""
                }
              },
              "31": {
                "id": 31,
                "name": {
                  "fr": "Djoumime",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djoumime",
                  "ar": ""
                }
              },
              "32": {
                "id": 32,
                "name": {
                  "fr": "El Alia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_alia",
                  "ar": ""
                }
              },
              "33": {
                "id": 33,
                "name": {
                  "fr": "Ghar El Melh",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghar_el_melh",
                  "ar": ""
                }
              },
              "34": {
                "id": 34,
                "name": {
                  "fr": "Ghezala",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghezala",
                  "ar": ""
                }
              },
              "35": {
                "id": 35,
                "name": {
                  "fr": "Mateur",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mateur",
                  "ar": ""
                }
              },
              "36": {
                "id": 36,
                "name": {
                  "fr": "Menzel Bourguiba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_bourguiba",
                  "ar": ""
                }
              },
              "37": {
                "id": 37,
                "name": {
                  "fr": "Menzel Jemil",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_jemil",
                  "ar": ""
                }
              },
              "38": {
                "id": 38,
                "name": {
                  "fr": "Ras Jebel",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ras_jebel",
                  "ar": ""
                }
              },
              "39": {
                "id": 39,
                "name": {
                  "fr": "Sejenane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sejenane",
                  "ar": ""
                }
              },
              "40": {
                "id": 40,
                "name": {
                  "fr": "Tinja",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tinja",
                  "ar": ""
                }
              },
              "41": {
                "id": 41,
                "name": {
                  "fr": "Utique",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "utique",
                  "ar": ""
                }
              },
              "42": {
                "id": 42,
                "name": {
                  "fr": "Zarzouna",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "zarzouna",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 29,
                "order_id": 0
              },
              {
                "id": 30,
                "order_id": 1
              },
              {
                "id": 31,
                "order_id": 2
              },
              {
                "id": 32,
                "order_id": 3
              },
              {
                "id": 33,
                "order_id": 4
              },
              {
                "id": 34,
                "order_id": 5
              },
              {
                "id": 35,
                "order_id": 6
              },
              {
                "id": 36,
                "order_id": 7
              },
              {
                "id": 37,
                "order_id": 8
              },
              {
                "id": 38,
                "order_id": 9
              },
              {
                "id": 39,
                "order_id": 10
              },
              {
                "id": 40,
                "order_id": 11
              },
              {
                "id": 41,
                "order_id": 12
              },
              {
                "id": 42,
                "order_id": 13
              }
            ]
          },
          "5": {
            "id": "5",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Gabès",
              "ar": ""
            },
            "seo_name": {
              "fr": "gabès",
              "ar": ""
            },
            "areas": {
              "43": {
                "id": 43,
                "name": {
                  "fr": "Gabès Médina",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gabès_médina",
                  "ar": ""
                }
              },
              "44": {
                "id": 44,
                "name": {
                  "fr": "Gabès Ouest",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gabès_ouest",
                  "ar": ""
                }
              },
              "45": {
                "id": 45,
                "name": {
                  "fr": "Gabès Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gabès_sud",
                  "ar": ""
                }
              },
              "46": {
                "id": 46,
                "name": {
                  "fr": "Ghanouch",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghanouch",
                  "ar": ""
                }
              },
              "47": {
                "id": 47,
                "name": {
                  "fr": "El Hamma",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_hamma",
                  "ar": ""
                }
              },
              "48": {
                "id": 48,
                "name": {
                  "fr": "Matmata",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "matmata",
                  "ar": ""
                }
              },
              "49": {
                "id": 49,
                "name": {
                  "fr": "Mareth",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mareth",
                  "ar": ""
                }
              },
              "50": {
                "id": 50,
                "name": {
                  "fr": "Menzel El Habib",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_el_habib",
                  "ar": ""
                }
              },
              "51": {
                "id": 51,
                "name": {
                  "fr": "Métouia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "métouia",
                  "ar": ""
                }
              },
              "52": {
                "id": 52,
                "name": {
                  "fr": "Nouvelle Matmata",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nouvelle_matmata",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 43,
                "order_id": 0
              },
              {
                "id": 44,
                "order_id": 1
              },
              {
                "id": 45,
                "order_id": 2
              },
              {
                "id": 46,
                "order_id": 3
              },
              {
                "id": 47,
                "order_id": 4
              },
              {
                "id": 48,
                "order_id": 5
              },
              {
                "id": 49,
                "order_id": 6
              },
              {
                "id": 50,
                "order_id": 7
              },
              {
                "id": 51,
                "order_id": 8
              },
              {
                "id": 52,
                "order_id": 9
              }
            ]
          },
          "6": {
            "id": "6",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Gafsa",
              "ar": ""
            },
            "seo_name": {
              "fr": "gafsa",
              "ar": ""
            },
            "areas": {
              "53": {
                "id": 53,
                "name": {
                  "fr": "Belkhir",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "belkhir",
                  "ar": ""
                }
              },
              "54": {
                "id": 54,
                "name": {
                  "fr": "El Guettar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_guettar",
                  "ar": ""
                }
              },
              "55": {
                "id": 55,
                "name": {
                  "fr": "El Ksar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_ksar",
                  "ar": ""
                }
              },
              "56": {
                "id": 56,
                "name": {
                  "fr": "Gafsa Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gafsa_nord",
                  "ar": ""
                }
              },
              "57": {
                "id": 57,
                "name": {
                  "fr": "Gafsa Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gafsa_sud",
                  "ar": ""
                }
              },
              "58": {
                "id": 58,
                "name": {
                  "fr": "Mdhila",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mdhila",
                  "ar": ""
                }
              },
              "59": {
                "id": 59,
                "name": {
                  "fr": "Métlaoui",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "métlaoui",
                  "ar": ""
                }
              },
              "60": {
                "id": 60,
                "name": {
                  "fr": "Oum El Araies",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "oum_el_araies",
                  "ar": ""
                }
              },
              "61": {
                "id": 61,
                "name": {
                  "fr": "Redeyef",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "redeyef",
                  "ar": ""
                }
              },
              "62": {
                "id": 62,
                "name": {
                  "fr": "Sidi Aïch",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_aïch",
                  "ar": ""
                }
              },
              "63": {
                "id": 63,
                "name": {
                  "fr": "Sened",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sened",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 53,
                "order_id": 0
              },
              {
                "id": 54,
                "order_id": 1
              },
              {
                "id": 55,
                "order_id": 2
              },
              {
                "id": 56,
                "order_id": 3
              },
              {
                "id": 57,
                "order_id": 4
              },
              {
                "id": 58,
                "order_id": 5
              },
              {
                "id": 59,
                "order_id": 6
              },
              {
                "id": 60,
                "order_id": 7
              },
              {
                "id": 61,
                "order_id": 8
              },
              {
                "id": 62,
                "order_id": 9
              },
              {
                "id": 63,
                "order_id": 10
              }
            ]
          },
          "7": {
            "id": "7",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Jendouba",
              "ar": ""
            },
            "seo_name": {
              "fr": "jendouba",
              "ar": ""
            },
            "areas": {
              "64": {
                "id": 64,
                "name": {
                  "fr": "Ain Draham",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ain_draham",
                  "ar": ""
                }
              },
              "65": {
                "id": 65,
                "name": {
                  "fr": "Balta-Bou Aouane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "balta-bou_aouane",
                  "ar": ""
                }
              },
              "66": {
                "id": 66,
                "name": {
                  "fr": "Bou Salem",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bou_salem",
                  "ar": ""
                }
              },
              "67": {
                "id": 67,
                "name": {
                  "fr": "Fernana",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "fernana",
                  "ar": ""
                }
              },
              "68": {
                "id": 68,
                "name": {
                  "fr": "Ghardimaou",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghardimaou",
                  "ar": ""
                }
              },
              "69": {
                "id": 69,
                "name": {
                  "fr": "Jendouba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jendouba",
                  "ar": ""
                }
              },
              "70": {
                "id": 70,
                "name": {
                  "fr": "Jendouba Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jendouba_nord",
                  "ar": ""
                }
              },
              "71": {
                "id": 71,
                "name": {
                  "fr": "Oued Meliz",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "oued_meliz",
                  "ar": ""
                }
              },
              "72": {
                "id": 72,
                "name": {
                  "fr": "Tabarka",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tabarka",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 64,
                "order_id": 0
              },
              {
                "id": 65,
                "order_id": 1
              },
              {
                "id": 66,
                "order_id": 2
              },
              {
                "id": 67,
                "order_id": 3
              },
              {
                "id": 68,
                "order_id": 4
              },
              {
                "id": 69,
                "order_id": 5
              },
              {
                "id": 70,
                "order_id": 6
              },
              {
                "id": 71,
                "order_id": 7
              },
              {
                "id": 72,
                "order_id": 8
              }
            ]
          },
          "8": {
            "id": "8",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Kairouan",
              "ar": ""
            },
            "seo_name": {
              "fr": "kairouan",
              "ar": ""
            },
            "areas": {
              "73": {
                "id": 73,
                "name": {
                  "fr": "Bouhajla",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bouhajla",
                  "ar": ""
                }
              },
              "74": {
                "id": 74,
                "name": {
                  "fr": "Chebika",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "chebika",
                  "ar": ""
                }
              },
              "75": {
                "id": 75,
                "name": {
                  "fr": "Echrarda",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "echrarda",
                  "ar": ""
                }
              },
              "76": {
                "id": 76,
                "name": {
                  "fr": "El Alâa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_alâa",
                  "ar": ""
                }
              },
              "77": {
                "id": 77,
                "name": {
                  "fr": "El Ouslatia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_ouslatia",
                  "ar": ""
                }
              },
              "78": {
                "id": 78,
                "name": {
                  "fr": "Haffouz",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "haffouz",
                  "ar": ""
                }
              },
              "79": {
                "id": 79,
                "name": {
                  "fr": "Hajeb El Ayoun",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hajeb_el_ayoun",
                  "ar": ""
                }
              },
              "80": {
                "id": 80,
                "name": {
                  "fr": "Kairouan Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kairouan_nord",
                  "ar": ""
                }
              },
              "81": {
                "id": 81,
                "name": {
                  "fr": "Kairouan Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kairouan_sud",
                  "ar": ""
                }
              },
              "82": {
                "id": 82,
                "name": {
                  "fr": "Nasrallah",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nasrallah",
                  "ar": ""
                }
              },
              "83": {
                "id": 83,
                "name": {
                  "fr": "Sbikha",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sbikha",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 73,
                "order_id": 0
              },
              {
                "id": 74,
                "order_id": 1
              },
              {
                "id": 75,
                "order_id": 2
              },
              {
                "id": 76,
                "order_id": 3
              },
              {
                "id": 77,
                "order_id": 4
              },
              {
                "id": 78,
                "order_id": 5
              },
              {
                "id": 79,
                "order_id": 6
              },
              {
                "id": 80,
                "order_id": 7
              },
              {
                "id": 81,
                "order_id": 8
              },
              {
                "id": 82,
                "order_id": 9
              },
              {
                "id": 83,
                "order_id": 10
              }
            ]
          },
          "9": {
            "id": "9",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Kasserine",
              "ar": ""
            },
            "seo_name": {
              "fr": "kasserine",
              "ar": ""
            },
            "areas": {
              "84": {
                "id": 84,
                "name": {
                  "fr": "Djedeliane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djedeliane",
                  "ar": ""
                }
              },
              "85": {
                "id": 85,
                "name": {
                  "fr": "El Ayoun",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_ayoun",
                  "ar": ""
                }
              },
              "86": {
                "id": 86,
                "name": {
                  "fr": "Ezzouhour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ezzouhour",
                  "ar": ""
                }
              },
              "87": {
                "id": 87,
                "name": {
                  "fr": "Fériana",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "fériana",
                  "ar": ""
                }
              },
              "88": {
                "id": 88,
                "name": {
                  "fr": "Foussana",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "foussana",
                  "ar": ""
                }
              },
              "89": {
                "id": 89,
                "name": {
                  "fr": "Hassi Ferid",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hassi_ferid",
                  "ar": ""
                }
              },
              "90": {
                "id": 90,
                "name": {
                  "fr": "Hidra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hidra",
                  "ar": ""
                }
              },
              "91": {
                "id": 91,
                "name": {
                  "fr": "Kasserine Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kasserine_nord",
                  "ar": ""
                }
              },
              "92": {
                "id": 92,
                "name": {
                  "fr": "Kasserine Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kasserine_sud",
                  "ar": ""
                }
              },
              "93": {
                "id": 93,
                "name": {
                  "fr": "Majel Bel Abbès",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "majel_bel_abbès",
                  "ar": ""
                }
              },
              "94": {
                "id": 94,
                "name": {
                  "fr": "Sbeïtla",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sbeïtla",
                  "ar": ""
                }
              },
              "95": {
                "id": 95,
                "name": {
                  "fr": "Sbiba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sbiba",
                  "ar": ""
                }
              },
              "96": {
                "id": 96,
                "name": {
                  "fr": "Thala",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "thala",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 84,
                "order_id": 0
              },
              {
                "id": 85,
                "order_id": 1
              },
              {
                "id": 86,
                "order_id": 2
              },
              {
                "id": 87,
                "order_id": 3
              },
              {
                "id": 88,
                "order_id": 4
              },
              {
                "id": 89,
                "order_id": 5
              },
              {
                "id": 90,
                "order_id": 6
              },
              {
                "id": 91,
                "order_id": 7
              },
              {
                "id": 92,
                "order_id": 8
              },
              {
                "id": 93,
                "order_id": 9
              },
              {
                "id": 94,
                "order_id": 10
              },
              {
                "id": 95,
                "order_id": 11
              },
              {
                "id": 96,
                "order_id": 12
              }
            ]
          },
          "10": {
            "id": "10",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Kébili",
              "ar": ""
            },
            "seo_name": {
              "fr": "kébili",
              "ar": ""
            },
            "areas": {
              "97": {
                "id": 97,
                "name": {
                  "fr": "Douz Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "douz_nord",
                  "ar": ""
                }
              },
              "98": {
                "id": 98,
                "name": {
                  "fr": "Douz Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "douz_sud",
                  "ar": ""
                }
              },
              "99": {
                "id": 99,
                "name": {
                  "fr": "Faouar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "faouar",
                  "ar": ""
                }
              },
              "100": {
                "id": 100,
                "name": {
                  "fr": "Kébili Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kébili_nord",
                  "ar": ""
                }
              },
              "101": {
                "id": 101,
                "name": {
                  "fr": "Kébili Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kébili_sud",
                  "ar": ""
                }
              },
              "102": {
                "id": 102,
                "name": {
                  "fr": "Souk Lahad",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "souk_lahad",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 97,
                "order_id": 0
              },
              {
                "id": 98,
                "order_id": 1
              },
              {
                "id": 99,
                "order_id": 2
              },
              {
                "id": 100,
                "order_id": 3
              },
              {
                "id": 101,
                "order_id": 4
              },
              {
                "id": 102,
                "order_id": 5
              }
            ]
          },
          "11": {
            "id": "11",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Le Kef",
              "ar": ""
            },
            "seo_name": {
              "fr": "le_kef",
              "ar": ""
            },
            "areas": {
              "103": {
                "id": 103,
                "name": {
                  "fr": "Dahmani",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "dahmani",
                  "ar": ""
                }
              },
              "104": {
                "id": 104,
                "name": {
                  "fr": "Djerissa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djerissa",
                  "ar": ""
                }
              },
              "105": {
                "id": 105,
                "name": {
                  "fr": "El Ksour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_ksour",
                  "ar": ""
                }
              },
              "106": {
                "id": 106,
                "name": {
                  "fr": "Es-Sers",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "es-sers",
                  "ar": ""
                }
              },
              "107": {
                "id": 107,
                "name": {
                  "fr": "Kalâat Khasbah",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kalâat_khasbah",
                  "ar": ""
                }
              },
              "108": {
                "id": 108,
                "name": {
                  "fr": "Kalâat Snan",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kalâat_snan",
                  "ar": ""
                }
              },
              "109": {
                "id": 109,
                "name": {
                  "fr": "Kef Est",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kef_est",
                  "ar": ""
                }
              },
              "110": {
                "id": 110,
                "name": {
                  "fr": "Kef Ouest",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kef_ouest",
                  "ar": ""
                }
              },
              "111": {
                "id": 111,
                "name": {
                  "fr": "Nebeur",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nebeur",
                  "ar": ""
                }
              },
              "112": {
                "id": 112,
                "name": {
                  "fr": "Sakiet Sidi Youssef",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sakiet_sidi_youssef",
                  "ar": ""
                }
              },
              "113": {
                "id": 113,
                "name": {
                  "fr": "Tajerouine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tajerouine",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 103,
                "order_id": 0
              },
              {
                "id": 104,
                "order_id": 1
              },
              {
                "id": 105,
                "order_id": 2
              },
              {
                "id": 106,
                "order_id": 3
              },
              {
                "id": 107,
                "order_id": 4
              },
              {
                "id": 108,
                "order_id": 5
              },
              {
                "id": 109,
                "order_id": 6
              },
              {
                "id": 110,
                "order_id": 7
              },
              {
                "id": 111,
                "order_id": 8
              },
              {
                "id": 112,
                "order_id": 9
              },
              {
                "id": 113,
                "order_id": 10
              }
            ]
          },
          "12": {
            "id": "12",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Mahdia",
              "ar": ""
            },
            "seo_name": {
              "fr": "mahdia",
              "ar": ""
            },
            "areas": {
              "114": {
                "id": 114,
                "name": {
                  "fr": "Bou Merdès",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bou_merdès",
                  "ar": ""
                }
              },
              "115": {
                "id": 115,
                "name": {
                  "fr": "Chebba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "chebba",
                  "ar": ""
                }
              },
              "116": {
                "id": 116,
                "name": {
                  "fr": "Chorbane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "chorbane",
                  "ar": ""
                }
              },
              "117": {
                "id": 117,
                "name": {
                  "fr": "El Jem",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_jem",
                  "ar": ""
                }
              },
              "118": {
                "id": 118,
                "name": {
                  "fr": "Essouassi",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "essouassi",
                  "ar": ""
                }
              },
              "119": {
                "id": 119,
                "name": {
                  "fr": "Hebira",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hebira",
                  "ar": ""
                }
              },
              "120": {
                "id": 120,
                "name": {
                  "fr": "Ksour Essef",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ksour_essef",
                  "ar": ""
                }
              },
              "121": {
                "id": 121,
                "name": {
                  "fr": "Mahdia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mahdia",
                  "ar": ""
                }
              },
              "122": {
                "id": 122,
                "name": {
                  "fr": "Melloulèche",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "melloulèche",
                  "ar": ""
                }
              },
              "123": {
                "id": 123,
                "name": {
                  "fr": "Ouled Chamekh",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ouled_chamekh",
                  "ar": ""
                }
              },
              "124": {
                "id": 124,
                "name": {
                  "fr": "Sidi Alouane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_alouane",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 114,
                "order_id": 0
              },
              {
                "id": 115,
                "order_id": 1
              },
              {
                "id": 116,
                "order_id": 2
              },
              {
                "id": 117,
                "order_id": 3
              },
              {
                "id": 118,
                "order_id": 4
              },
              {
                "id": 119,
                "order_id": 5
              },
              {
                "id": 120,
                "order_id": 6
              },
              {
                "id": 121,
                "order_id": 7
              },
              {
                "id": 122,
                "order_id": 8
              },
              {
                "id": 123,
                "order_id": 9
              },
              {
                "id": 124,
                "order_id": 10
              }
            ]
          },
          "13": {
            "id": "13",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "La Manouba",
              "ar": ""
            },
            "seo_name": {
              "fr": "la_manouba",
              "ar": ""
            },
            "areas": {
              "125": {
                "id": 125,
                "name": {
                  "fr": "Borj El Amri",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "borj_el_amri",
                  "ar": ""
                }
              },
              "126": {
                "id": 126,
                "name": {
                  "fr": "Djedeida",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djedeida",
                  "ar": ""
                }
              },
              "127": {
                "id": 127,
                "name": {
                  "fr": "Douar Hicher",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "douar_hicher",
                  "ar": ""
                }
              },
              "128": {
                "id": 128,
                "name": {
                  "fr": "El Battan",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_battan",
                  "ar": ""
                }
              },
              "130": {
                "id": 130,
                "name": {
                  "fr": "Mornaguia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mornaguia",
                  "ar": ""
                }
              },
              "131": {
                "id": 131,
                "name": {
                  "fr": "Oued Ellil",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "oued_ellil",
                  "ar": ""
                }
              },
              "132": {
                "id": 132,
                "name": {
                  "fr": "Tebourba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tebourba",
                  "ar": ""
                }
              },
              "272": {
                "id": 272,
                "name": {
                  "fr": "Manouba Ville",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "manouba_ville",
                  "ar": ""
                }
              },
              "273": {
                "id": 273,
                "name": {
                  "fr": "Denden",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "denden",
                  "ar": ""
                }
              },
              "274": {
                "id": 274,
                "name": {
                  "fr": "Ksar Said",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ksar_said",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 125,
                "order_id": 0
              },
              {
                "id": 126,
                "order_id": 1
              },
              {
                "id": 127,
                "order_id": 2
              },
              {
                "id": 128,
                "order_id": 3
              },
              {
                "id": 130,
                "order_id": 4
              },
              {
                "id": 131,
                "order_id": 5
              },
              {
                "id": 132,
                "order_id": 6
              },
              {
                "id": 272,
                "order_id": 7
              },
              {
                "id": 273,
                "order_id": 8
              },
              {
                "id": 274,
                "order_id": 9
              }
            ]
          },
          "14": {
            "id": "14",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Médenine",
              "ar": ""
            },
            "seo_name": {
              "fr": "médenine",
              "ar": ""
            },
            "areas": {
              "133": {
                "id": 133,
                "name": {
                  "fr": "Ben Gardane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ben_gardane",
                  "ar": ""
                }
              },
              "134": {
                "id": 134,
                "name": {
                  "fr": "Beni Khedech",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "beni_khedech",
                  "ar": ""
                }
              },
              "135": {
                "id": 135,
                "name": {
                  "fr": "Djerba - Ajim",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djerba-ajim",
                  "ar": ""
                }
              },
              "136": {
                "id": 136,
                "name": {
                  "fr": "Djerba-Houmt Souk",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djerba-houmt_souk",
                  "ar": ""
                }
              },
              "137": {
                "id": 137,
                "name": {
                  "fr": "Djerba - Midoun",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djerba-midoun",
                  "ar": ""
                }
              },
              "138": {
                "id": 138,
                "name": {
                  "fr": "Médenine Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "médenine_nord",
                  "ar": ""
                }
              },
              "139": {
                "id": 139,
                "name": {
                  "fr": "Médenine Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "médenine_sud",
                  "ar": ""
                }
              },
              "140": {
                "id": 140,
                "name": {
                  "fr": "Sidi Makhloulf",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_makhloulf",
                  "ar": ""
                }
              },
              "141": {
                "id": 141,
                "name": {
                  "fr": "Zarzis",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "zarzis",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 133,
                "order_id": 0
              },
              {
                "id": 134,
                "order_id": 1
              },
              {
                "id": 135,
                "order_id": 2
              },
              {
                "id": 136,
                "order_id": 3
              },
              {
                "id": 137,
                "order_id": 4
              },
              {
                "id": 138,
                "order_id": 5
              },
              {
                "id": 139,
                "order_id": 6
              },
              {
                "id": 140,
                "order_id": 7
              },
              {
                "id": 141,
                "order_id": 8
              }
            ]
          },
          "15": {
            "id": "15",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Monastir",
              "ar": ""
            },
            "seo_name": {
              "fr": "monastir",
              "ar": ""
            },
            "areas": {
              "142": {
                "id": 142,
                "name": {
                  "fr": "Bekalta",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bekalta",
                  "ar": ""
                }
              },
              "143": {
                "id": 143,
                "name": {
                  "fr": "Bembla",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bembla",
                  "ar": ""
                }
              },
              "144": {
                "id": 144,
                "name": {
                  "fr": "Beni Hassen",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "beni_hassen",
                  "ar": ""
                }
              },
              "145": {
                "id": 145,
                "name": {
                  "fr": "Jemmal",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jemmal",
                  "ar": ""
                }
              },
              "146": {
                "id": 146,
                "name": {
                  "fr": "Ksar Hellal",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ksar_hellal",
                  "ar": ""
                }
              },
              "147": {
                "id": 147,
                "name": {
                  "fr": "Ksibet el-Médiouni",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ksibet_el-médiouni",
                  "ar": ""
                }
              },
              "148": {
                "id": 148,
                "name": {
                  "fr": "Moknine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "moknine",
                  "ar": ""
                }
              },
              "149": {
                "id": 149,
                "name": {
                  "fr": "Monastir",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "monastir",
                  "ar": ""
                }
              },
              "150": {
                "id": 150,
                "name": {
                  "fr": "Ouerdanine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ouerdanine",
                  "ar": ""
                }
              },
              "151": {
                "id": 151,
                "name": {
                  "fr": "Sahline",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sahline",
                  "ar": ""
                }
              },
              "152": {
                "id": 152,
                "name": {
                  "fr": "Sayada-Lamta-Bou Hajar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sayada-lamta-bou_hajar",
                  "ar": ""
                }
              },
              "153": {
                "id": 153,
                "name": {
                  "fr": "Téboulba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "téboulba",
                  "ar": ""
                }
              },
              "154": {
                "id": 154,
                "name": {
                  "fr": "Zéramdine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "zéramdine",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 142,
                "order_id": 0
              },
              {
                "id": 143,
                "order_id": 1
              },
              {
                "id": 144,
                "order_id": 2
              },
              {
                "id": 145,
                "order_id": 3
              },
              {
                "id": 146,
                "order_id": 4
              },
              {
                "id": 147,
                "order_id": 5
              },
              {
                "id": 148,
                "order_id": 6
              },
              {
                "id": 149,
                "order_id": 7
              },
              {
                "id": 150,
                "order_id": 8
              },
              {
                "id": 151,
                "order_id": 9
              },
              {
                "id": 152,
                "order_id": 10
              },
              {
                "id": 153,
                "order_id": 11
              },
              {
                "id": 154,
                "order_id": 12
              }
            ]
          },
          "16": {
            "id": "16",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Nabeul",
              "ar": ""
            },
            "seo_name": {
              "fr": "nabeul",
              "ar": ""
            },
            "areas": {
              "155": {
                "id": 155,
                "name": {
                  "fr": "Béni Khalled",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "béni_khalled",
                  "ar": ""
                }
              },
              "156": {
                "id": 156,
                "name": {
                  "fr": "Béni Khiar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "béni_khiar",
                  "ar": ""
                }
              },
              "157": {
                "id": 157,
                "name": {
                  "fr": "Bou Argoub",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bou_argoub",
                  "ar": ""
                }
              },
              "158": {
                "id": 158,
                "name": {
                  "fr": "Dar Châabane El Fehri",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "dar_châabane_el_fehri",
                  "ar": ""
                }
              },
              "159": {
                "id": 159,
                "name": {
                  "fr": "El Haouaria",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_haouaria",
                  "ar": ""
                }
              },
              "160": {
                "id": 160,
                "name": {
                  "fr": "El Mida",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_mida",
                  "ar": ""
                }
              },
              "161": {
                "id": 161,
                "name": {
                  "fr": "Grombalia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "grombalia",
                  "ar": ""
                }
              },
              "162": {
                "id": 162,
                "name": {
                  "fr": "Hammam Ghezèze",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammam_ghezèze",
                  "ar": ""
                }
              },
              "163": {
                "id": 163,
                "name": {
                  "fr": "Hammamet",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammamet",
                  "ar": ""
                }
              },
              "164": {
                "id": 164,
                "name": {
                  "fr": "Kélibia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kélibia",
                  "ar": ""
                }
              },
              "165": {
                "id": 165,
                "name": {
                  "fr": "Korba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "korba",
                  "ar": ""
                }
              },
              "166": {
                "id": 166,
                "name": {
                  "fr": "Menzel Bouzelfa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_bouzelfa",
                  "ar": ""
                }
              },
              "167": {
                "id": 167,
                "name": {
                  "fr": "Menzel Temime",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_temime",
                  "ar": ""
                }
              },
              "168": {
                "id": 168,
                "name": {
                  "fr": "Nabeul",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nabeul",
                  "ar": ""
                }
              },
              "169": {
                "id": 169,
                "name": {
                  "fr": "Soliman",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "soliman",
                  "ar": ""
                }
              },
              "170": {
                "id": 170,
                "name": {
                  "fr": "Takelsa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "takelsa",
                  "ar": ""
                }
              },
              "275": {
                "id": 275,
                "name": {
                  "fr": "Hammamet Centre",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammamet_centre",
                  "ar": ""
                }
              },
              "276": {
                "id": 276,
                "name": {
                  "fr": "Hammamet Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammamet_nord",
                  "ar": ""
                }
              },
              "277": {
                "id": 277,
                "name": {
                  "fr": "Hammamet Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammamet_sud",
                  "ar": ""
                }
              },
              "278": {
                "id": 278,
                "name": {
                  "fr": "Mrezga",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mrezga",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 155,
                "order_id": 0
              },
              {
                "id": 156,
                "order_id": 1
              },
              {
                "id": 157,
                "order_id": 2
              },
              {
                "id": 158,
                "order_id": 3
              },
              {
                "id": 159,
                "order_id": 4
              },
              {
                "id": 160,
                "order_id": 5
              },
              {
                "id": 161,
                "order_id": 6
              },
              {
                "id": 162,
                "order_id": 7
              },
              {
                "id": 163,
                "order_id": 8
              },
              {
                "id": 164,
                "order_id": 9
              },
              {
                "id": 165,
                "order_id": 10
              },
              {
                "id": 166,
                "order_id": 11
              },
              {
                "id": 167,
                "order_id": 12
              },
              {
                "id": 168,
                "order_id": 13
              },
              {
                "id": 169,
                "order_id": 14
              },
              {
                "id": 170,
                "order_id": 15
              },
              {
                "id": 275,
                "order_id": 16
              },
              {
                "id": 276,
                "order_id": 17
              },
              {
                "id": 277,
                "order_id": 18
              },
              {
                "id": 278,
                "order_id": 19
              }
            ]
          },
          "17": {
            "id": "17",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Sfax",
              "ar": ""
            },
            "seo_name": {
              "fr": "sfax",
              "ar": ""
            },
            "areas": {
              "171": {
                "id": 171,
                "name": {
                  "fr": "Agareb",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "agareb",
                  "ar": ""
                }
              },
              "172": {
                "id": 172,
                "name": {
                  "fr": "Bir Ali Ben Khalifa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bir_ali_ben_khalifa",
                  "ar": ""
                }
              },
              "173": {
                "id": 173,
                "name": {
                  "fr": "Jebiniana",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jebiniana",
                  "ar": ""
                }
              },
              "174": {
                "id": 174,
                "name": {
                  "fr": "El Amra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_amra",
                  "ar": ""
                }
              },
              "175": {
                "id": 175,
                "name": {
                  "fr": "El Hencha",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_hencha",
                  "ar": ""
                }
              },
              "176": {
                "id": 176,
                "name": {
                  "fr": "Ghraiba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghraiba",
                  "ar": ""
                }
              },
              "177": {
                "id": 177,
                "name": {
                  "fr": "Kerkennah",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kerkennah",
                  "ar": ""
                }
              },
              "178": {
                "id": 178,
                "name": {
                  "fr": "Mahrès",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mahrès",
                  "ar": ""
                }
              },
              "179": {
                "id": 179,
                "name": {
                  "fr": "Menzel Chaker",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_chaker",
                  "ar": ""
                }
              },
              "180": {
                "id": 180,
                "name": {
                  "fr": "Sakiet Eddaïer",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sakiet_eddaïer",
                  "ar": ""
                }
              },
              "181": {
                "id": 181,
                "name": {
                  "fr": "Sakiet Ezzit",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sakiet_ezzit",
                  "ar": ""
                }
              },
              "184": {
                "id": 184,
                "name": {
                  "fr": "Sfax Ville",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sfax_ville",
                  "ar": ""
                }
              },
              "185": {
                "id": 185,
                "name": {
                  "fr": "Skhira",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "skhira",
                  "ar": ""
                }
              },
              "186": {
                "id": 186,
                "name": {
                  "fr": "Thyna",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "thyna",
                  "ar": ""
                }
              },
              "279": {
                "id": 279,
                "name": {
                  "fr": "Sfax Médina",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sfax_medina",
                  "ar": ""
                }
              },
              "280": {
                "id": 280,
                "name": {
                  "fr": "Route el Ain",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_el_ain",
                  "ar": ""
                }
              },
              "281": {
                "id": 281,
                "name": {
                  "fr": "Route el Afrane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_el_afrane",
                  "ar": ""
                }
              },
              "282": {
                "id": 282,
                "name": {
                  "fr": "Route Gremda",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_gremda",
                  "ar": ""
                }
              },
              "283": {
                "id": 283,
                "name": {
                  "fr": "Route Menzel Chaker",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_menzel_chaker",
                  "ar": ""
                }
              },
              "284": {
                "id": 284,
                "name": {
                  "fr": "Route Matar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_matar",
                  "ar": ""
                }
              },
              "285": {
                "id": 285,
                "name": {
                  "fr": "Route Soukra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_soukra",
                  "ar": ""
                }
              },
              "286": {
                "id": 286,
                "name": {
                  "fr": "Route Tunis",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_tunis",
                  "ar": ""
                }
              },
              "287": {
                "id": 287,
                "name": {
                  "fr": "Route Mehdia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_mehdia",
                  "ar": ""
                }
              },
              "288": {
                "id": 288,
                "name": {
                  "fr": "Route Teniour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "route_teniour",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 171,
                "order_id": 0
              },
              {
                "id": 172,
                "order_id": 1
              },
              {
                "id": 173,
                "order_id": 2
              },
              {
                "id": 174,
                "order_id": 3
              },
              {
                "id": 175,
                "order_id": 4
              },
              {
                "id": 176,
                "order_id": 5
              },
              {
                "id": 177,
                "order_id": 6
              },
              {
                "id": 178,
                "order_id": 7
              },
              {
                "id": 179,
                "order_id": 8
              },
              {
                "id": 180,
                "order_id": 9
              },
              {
                "id": 181,
                "order_id": 10
              },
              {
                "id": 184,
                "order_id": 11
              },
              {
                "id": 185,
                "order_id": 12
              },
              {
                "id": 186,
                "order_id": 13
              },
              {
                "id": 279,
                "order_id": 14
              },
              {
                "id": 280,
                "order_id": 15
              },
              {
                "id": 281,
                "order_id": 16
              },
              {
                "id": 282,
                "order_id": 17
              },
              {
                "id": 283,
                "order_id": 18
              },
              {
                "id": 284,
                "order_id": 19
              },
              {
                "id": 285,
                "order_id": 20
              },
              {
                "id": 286,
                "order_id": 21
              },
              {
                "id": 287,
                "order_id": 22
              },
              {
                "id": 288,
                "order_id": 23
              }
            ]
          },
          "18": {
            "id": "18",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Sidi Bouzid",
              "ar": ""
            },
            "seo_name": {
              "fr": "sidi_bouzid",
              "ar": ""
            },
            "areas": {
              "187": {
                "id": 187,
                "name": {
                  "fr": "Bir El Hafey",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bir_el_hafey",
                  "ar": ""
                }
              },
              "188": {
                "id": 188,
                "name": {
                  "fr": "Cebbala Ouled Asker",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "cebbala_ouled_asker",
                  "ar": ""
                }
              },
              "189": {
                "id": 189,
                "name": {
                  "fr": "Jilma",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jilma",
                  "ar": ""
                }
              },
              "190": {
                "id": 190,
                "name": {
                  "fr": "Meknassy",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "meknassy",
                  "ar": ""
                }
              },
              "191": {
                "id": 191,
                "name": {
                  "fr": "Menzel Bouzaiane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "menzel_bouzaiane",
                  "ar": ""
                }
              },
              "192": {
                "id": 192,
                "name": {
                  "fr": "Mezzouna",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mezzouna",
                  "ar": ""
                }
              },
              "193": {
                "id": 193,
                "name": {
                  "fr": "Ouled Haffouz",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ouled_haffouz",
                  "ar": ""
                }
              },
              "194": {
                "id": 194,
                "name": {
                  "fr": "Regueb",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "regueb",
                  "ar": ""
                }
              },
              "195": {
                "id": 195,
                "name": {
                  "fr": "Sidi Ali Ben Aoun",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_ali_ben_aoun",
                  "ar": ""
                }
              },
              "196": {
                "id": 196,
                "name": {
                  "fr": "Sidi Bouzid Est",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_bouzid_est",
                  "ar": ""
                }
              },
              "197": {
                "id": 197,
                "name": {
                  "fr": "Sidi Bouzid Ouest",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_bouzid_ouest",
                  "ar": ""
                }
              },
              "198": {
                "id": 198,
                "name": {
                  "fr": "Souk Jedid",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "souk_jedid",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 187,
                "order_id": 0
              },
              {
                "id": 188,
                "order_id": 1
              },
              {
                "id": 189,
                "order_id": 2
              },
              {
                "id": 190,
                "order_id": 3
              },
              {
                "id": 191,
                "order_id": 4
              },
              {
                "id": 192,
                "order_id": 5
              },
              {
                "id": 193,
                "order_id": 6
              },
              {
                "id": 194,
                "order_id": 7
              },
              {
                "id": 195,
                "order_id": 8
              },
              {
                "id": 196,
                "order_id": 9
              },
              {
                "id": 197,
                "order_id": 10
              },
              {
                "id": 198,
                "order_id": 11
              }
            ]
          },
          "19": {
            "id": "19",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Siliana",
              "ar": ""
            },
            "seo_name": {
              "fr": "siliana",
              "ar": ""
            },
            "areas": {
              "199": {
                "id": 199,
                "name": {
                  "fr": "Bargou",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bargou",
                  "ar": ""
                }
              },
              "200": {
                "id": 200,
                "name": {
                  "fr": "Bou Arada",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bou_arada",
                  "ar": ""
                }
              },
              "201": {
                "id": 201,
                "name": {
                  "fr": "Sidi Bou Rouis",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_bou_rouis",
                  "ar": ""
                }
              },
              "202": {
                "id": 202,
                "name": {
                  "fr": "El Aroussa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_aroussa",
                  "ar": ""
                }
              },
              "203": {
                "id": 203,
                "name": {
                  "fr": "El Krib",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_krib",
                  "ar": ""
                }
              },
              "204": {
                "id": 204,
                "name": {
                  "fr": "Rouhia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "rouhia",
                  "ar": ""
                }
              },
              "205": {
                "id": 205,
                "name": {
                  "fr": "Gaâfour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gaâfour",
                  "ar": ""
                }
              },
              "206": {
                "id": 206,
                "name": {
                  "fr": "Kesra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kesra",
                  "ar": ""
                }
              },
              "207": {
                "id": 207,
                "name": {
                  "fr": "Makthar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "makthar",
                  "ar": ""
                }
              },
              "208": {
                "id": 208,
                "name": {
                  "fr": "Siliana Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "siliana_nord",
                  "ar": ""
                }
              },
              "209": {
                "id": 209,
                "name": {
                  "fr": "Siliana Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "siliana_sud",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 199,
                "order_id": 0
              },
              {
                "id": 200,
                "order_id": 1
              },
              {
                "id": 201,
                "order_id": 2
              },
              {
                "id": 202,
                "order_id": 3
              },
              {
                "id": 203,
                "order_id": 4
              },
              {
                "id": 204,
                "order_id": 5
              },
              {
                "id": 205,
                "order_id": 6
              },
              {
                "id": 206,
                "order_id": 7
              },
              {
                "id": 207,
                "order_id": 8
              },
              {
                "id": 208,
                "order_id": 9
              },
              {
                "id": 209,
                "order_id": 10
              }
            ]
          },
          "20": {
            "id": "20",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Sousse",
              "ar": ""
            },
            "seo_name": {
              "fr": "sousse",
              "ar": ""
            },
            "areas": {
              "210": {
                "id": 210,
                "name": {
                  "fr": "Akouda",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "akouda",
                  "ar": ""
                }
              },
              "211": {
                "id": 211,
                "name": {
                  "fr": "Bouficha",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bouficha",
                  "ar": ""
                }
              },
              "212": {
                "id": 212,
                "name": {
                  "fr": "Enfidha",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "enfidha",
                  "ar": ""
                }
              },
              "213": {
                "id": 213,
                "name": {
                  "fr": "Hammam Sousse",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hammam_sousse",
                  "ar": ""
                }
              },
              "214": {
                "id": 214,
                "name": {
                  "fr": "Hergla",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hergla",
                  "ar": ""
                }
              },
              "215": {
                "id": 215,
                "name": {
                  "fr": "Kalaâ Kebira",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kalaâ_kebira",
                  "ar": ""
                }
              },
              "216": {
                "id": 216,
                "name": {
                  "fr": "Kalaâ Sghira",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kalaâ_sghira",
                  "ar": ""
                }
              },
              "217": {
                "id": 217,
                "name": {
                  "fr": "Kondar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kondar",
                  "ar": ""
                }
              },
              "218": {
                "id": 218,
                "name": {
                  "fr": "M\'saken",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "msaken",
                  "ar": ""
                }
              },
              "219": {
                "id": 219,
                "name": {
                  "fr": "Sidi Bou Ali",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_bou_ali",
                  "ar": ""
                }
              },
              "220": {
                "id": 220,
                "name": {
                  "fr": "Sidi El Héni",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_el_héni",
                  "ar": ""
                }
              },
              "221": {
                "id": 221,
                "name": {
                  "fr": "Sousse Jawhara",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sousse_jawhara",
                  "ar": ""
                }
              },
              "222": {
                "id": 222,
                "name": {
                  "fr": "Sousse Médina",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sousse_médina",
                  "ar": ""
                }
              },
              "223": {
                "id": 223,
                "name": {
                  "fr": "Sousse Riadh",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sousse_riadh",
                  "ar": ""
                }
              },
              "224": {
                "id": 224,
                "name": {
                  "fr": "Sousse Sidi Abdelhamid",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sousse_sidi_abdelhamid",
                  "ar": ""
                }
              },
              "225": {
                "id": 225,
                "name": {
                  "fr": "Zaouit-Ksibat Thrayett",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "zaouit-ksibat_thrayett",
                  "ar": ""
                }
              },
              "289": {
                "id": 289,
                "name": {
                  "fr": "Sahloul",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sahloul",
                  "ar": ""
                }
              },
              "290": {
                "id": 290,
                "name": {
                  "fr": "Kantaoui",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "kantaoui",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 210,
                "order_id": 0
              },
              {
                "id": 211,
                "order_id": 1
              },
              {
                "id": 212,
                "order_id": 2
              },
              {
                "id": 213,
                "order_id": 3
              },
              {
                "id": 214,
                "order_id": 4
              },
              {
                "id": 215,
                "order_id": 5
              },
              {
                "id": 216,
                "order_id": 6
              },
              {
                "id": 217,
                "order_id": 7
              },
              {
                "id": 218,
                "order_id": 8
              },
              {
                "id": 219,
                "order_id": 9
              },
              {
                "id": 220,
                "order_id": 10
              },
              {
                "id": 221,
                "order_id": 11
              },
              {
                "id": 222,
                "order_id": 12
              },
              {
                "id": 223,
                "order_id": 13
              },
              {
                "id": 224,
                "order_id": 14
              },
              {
                "id": 225,
                "order_id": 15
              },
              {
                "id": 289,
                "order_id": 16
              },
              {
                "id": 290,
                "order_id": 17
              }
            ]
          },
          "21": {
            "id": "21",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Tataouine",
              "ar": ""
            },
            "seo_name": {
              "fr": "tataouine",
              "ar": ""
            },
            "areas": {
              "226": {
                "id": 226,
                "name": {
                  "fr": "Bir Lahmar",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bir_lahmar",
                  "ar": ""
                }
              },
              "227": {
                "id": 227,
                "name": {
                  "fr": "Dehiba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "dehiba",
                  "ar": ""
                }
              },
              "228": {
                "id": 228,
                "name": {
                  "fr": "Ghomrassen",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ghomrassen",
                  "ar": ""
                }
              },
              "229": {
                "id": 229,
                "name": {
                  "fr": "Remada",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "remada",
                  "ar": ""
                }
              },
              "230": {
                "id": 230,
                "name": {
                  "fr": "Smâr",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "smâr",
                  "ar": ""
                }
              },
              "231": {
                "id": 231,
                "name": {
                  "fr": "Tataouine Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tataouine_nord",
                  "ar": ""
                }
              },
              "232": {
                "id": 232,
                "name": {
                  "fr": "Tataouine Sud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tataouine_sud",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 226,
                "order_id": 0
              },
              {
                "id": 227,
                "order_id": 1
              },
              {
                "id": 228,
                "order_id": 2
              },
              {
                "id": 229,
                "order_id": 3
              },
              {
                "id": 230,
                "order_id": 4
              },
              {
                "id": 231,
                "order_id": 5
              },
              {
                "id": 232,
                "order_id": 6
              }
            ]
          },
          "22": {
            "id": "22",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Tozeur",
              "ar": ""
            },
            "seo_name": {
              "fr": "tozeur",
              "ar": ""
            },
            "areas": {
              "233": {
                "id": 233,
                "name": {
                  "fr": "Degache",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "degache",
                  "ar": ""
                }
              },
              "234": {
                "id": 234,
                "name": {
                  "fr": "Hazoua",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hazoua",
                  "ar": ""
                }
              },
              "235": {
                "id": 235,
                "name": {
                  "fr": "Nefta",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "nefta",
                  "ar": ""
                }
              },
              "236": {
                "id": 236,
                "name": {
                  "fr": "Tameghza",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tameghza",
                  "ar": ""
                }
              },
              "237": {
                "id": 237,
                "name": {
                  "fr": "Tozeur",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "tozeur",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 233,
                "order_id": 0
              },
              {
                "id": 234,
                "order_id": 1
              },
              {
                "id": 235,
                "order_id": 2
              },
              {
                "id": 236,
                "order_id": 3
              },
              {
                "id": 237,
                "order_id": 4
              }
            ]
          },
          "23": {
            "id": "23",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Tunis",
              "ar": ""
            },
            "seo_name": {
              "fr": "tunis",
              "ar": ""
            },
            "areas": {
              "240": {
                "id": 240,
                "name": {
                  "fr": "Carthage",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "carthage",
                  "ar": ""
                }
              },
              "241": {
                "id": 241,
                "name": {
                  "fr": "Cité El Khadra",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "cité_el_khadra",
                  "ar": ""
                }
              },
              "242": {
                "id": 242,
                "name": {
                  "fr": "Djebel Jelloud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "djebel_jelloud",
                  "ar": ""
                }
              },
              "243": {
                "id": 243,
                "name": {
                  "fr": "El Kabaria",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_kabaria",
                  "ar": ""
                }
              },
              "245": {
                "id": 245,
                "name": {
                  "fr": "El Omrane",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_omrane",
                  "ar": ""
                }
              },
              "246": {
                "id": 246,
                "name": {
                  "fr": "El Omrane supérieur",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_omrane_supérieur",
                  "ar": ""
                }
              },
              "247": {
                "id": 247,
                "name": {
                  "fr": "El Ouardia",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_ouardia",
                  "ar": ""
                }
              },
              "248": {
                "id": 248,
                "name": {
                  "fr": "Ettahrir",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ettahrir",
                  "ar": ""
                }
              },
              "249": {
                "id": 249,
                "name": {
                  "fr": "Ezzouhour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ezzouhour",
                  "ar": ""
                }
              },
              "250": {
                "id": 250,
                "name": {
                  "fr": "Hraïria",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "hraïria",
                  "ar": ""
                }
              },
              "251": {
                "id": 251,
                "name": {
                  "fr": "La Goulette",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "la_goulette",
                  "ar": ""
                }
              },
              "252": {
                "id": 252,
                "name": {
                  "fr": "La Marsa",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "la_marsa",
                  "ar": ""
                }
              },
              "253": {
                "id": 253,
                "name": {
                  "fr": "Le Bardo",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "le_bardo",
                  "ar": ""
                }
              },
              "254": {
                "id": 254,
                "name": {
                  "fr": "Le Kram",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "le_kram",
                  "ar": ""
                }
              },
              "255": {
                "id": 255,
                "name": {
                  "fr": "Médina",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "médina",
                  "ar": ""
                }
              },
              "256": {
                "id": 256,
                "name": {
                  "fr": "Séjoumi",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "séjoumi",
                  "ar": ""
                }
              },
              "257": {
                "id": 257,
                "name": {
                  "fr": "Sidi El Béchir",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_el_béchir",
                  "ar": ""
                }
              },
              "258": {
                "id": 258,
                "name": {
                  "fr": "Sidi Hassine",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_hassine",
                  "ar": ""
                }
              },
              "291": {
                "id": 291,
                "name": {
                  "fr": "Agba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "agba",
                  "ar": ""
                }
              },
              "292": {
                "id": 292,
                "name": {
                  "fr": "Aïn Zaghouan",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ain_zaghouan",
                  "ar": ""
                }
              },
              "293": {
                "id": 293,
                "name": {
                  "fr": "Centre Urbain Nord",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "centre_urbain_nord",
                  "ar": ""
                }
              },
              "294": {
                "id": 294,
                "name": {
                  "fr": "Centre Ville - Lafayette",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "centre_ville_-_lafayette",
                  "ar": ""
                }
              },
              "295": {
                "id": 295,
                "name": {
                  "fr": "Cité Olympique",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "cite_olympique",
                  "ar": ""
                }
              },
              "296": {
                "id": 296,
                "name": {
                  "fr": "Gammarth",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "gammarth",
                  "ar": ""
                }
              },
              "297": {
                "id": 297,
                "name": {
                  "fr": "Jardin De Carthage",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "jardin_de_carthage",
                  "ar": ""
                }
              },
              "298": {
                "id": 298,
                "name": {
                  "fr": "L\'aouina",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "laouina",
                  "ar": ""
                }
              },
              "299": {
                "id": 299,
                "name": {
                  "fr": "Les Berges Du Lac",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "les_berges_du_lac",
                  "ar": ""
                }
              },
              "300": {
                "id": 300,
                "name": {
                  "fr": "Mutuelleville",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "mutuelleville",
                  "ar": ""
                }
              },
              "301": {
                "id": 301,
                "name": {
                  "fr": "Sidi Bou Said",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_bou_said",
                  "ar": ""
                }
              },
              "302": {
                "id": 302,
                "name": {
                  "fr": "Sidi Daoud",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "sidi_daoud",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 240,
                "order_id": 0
              },
              {
                "id": 241,
                "order_id": 1
              },
              {
                "id": 242,
                "order_id": 2
              },
              {
                "id": 243,
                "order_id": 3
              },
              {
                "id": 245,
                "order_id": 4
              },
              {
                "id": 246,
                "order_id": 5
              },
              {
                "id": 247,
                "order_id": 6
              },
              {
                "id": 248,
                "order_id": 7
              },
              {
                "id": 249,
                "order_id": 8
              },
              {
                "id": 250,
                "order_id": 9
              },
              {
                "id": 251,
                "order_id": 10
              },
              {
                "id": 252,
                "order_id": 11
              },
              {
                "id": 253,
                "order_id": 12
              },
              {
                "id": 254,
                "order_id": 13
              },
              {
                "id": 255,
                "order_id": 14
              },
              {
                "id": 256,
                "order_id": 15
              },
              {
                "id": 257,
                "order_id": 16
              },
              {
                "id": 258,
                "order_id": 17
              },
              {
                "id": 291,
                "order_id": 18
              },
              {
                "id": 292,
                "order_id": 19
              },
              {
                "id": 293,
                "order_id": 20
              },
              {
                "id": 294,
                "order_id": 21
              },
              {
                "id": 295,
                "order_id": 22
              },
              {
                "id": 296,
                "order_id": 23
              },
              {
                "id": 297,
                "order_id": 24
              },
              {
                "id": 298,
                "order_id": 25
              },
              {
                "id": 299,
                "order_id": 26
              },
              {
                "id": 300,
                "order_id": 27
              },
              {
                "id": 301,
                "order_id": 28
              },
              {
                "id": 302,
                "order_id": 29
              }
            ]
          },
          "24": {
            "id": "24",
            "coordinates": {
              "lat": "",
              "lng": ""
            },
            "name": {
              "fr": "Zaghouan",
              "ar": ""
            },
            "seo_name": {
              "fr": "zaghouan",
              "ar": ""
            },
            "areas": {
              "259": {
                "id": 259,
                "name": {
                  "fr": "Bir Mchergua",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "bir_mchergua",
                  "ar": ""
                }
              },
              "260": {
                "id": 260,
                "name": {
                  "fr": "El Fahs",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "el_fahs",
                  "ar": ""
                }
              },
              "261": {
                "id": 261,
                "name": {
                  "fr": "En-Nadhour",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "en-nadhour",
                  "ar": ""
                }
              },
              "262": {
                "id": 262,
                "name": {
                  "fr": "Ez-Zeriba",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "ez-zeriba",
                  "ar": ""
                }
              },
              "263": {
                "id": 263,
                "name": {
                  "fr": "Saouaf",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "saouaf",
                  "ar": ""
                }
              },
              "264": {
                "id": 264,
                "name": {
                  "fr": "Zaghouan",
                  "ar": ""
                },
                "seo_name": {
                  "fr": "zaghouan",
                  "ar": ""
                }
              }
            },
            "areaOrder": [
              {
                "id": 259,
                "order_id": 0
              },
              {
                "id": 260,
                "order_id": 1
              },
              {
                "id": 261,
                "order_id": 2
              },
              {
                "id": 262,
                "order_id": 3
              },
              {
                "id": 263,
                "order_id": 4
              },
              {
                "id": 264,
                "order_id": 5
              }
            ]
          }
        }');

        foreach ($data as $key => $item) {
            $region = new Region();
            $region->setName($item->name->fr);
            $region->setCode('0000');
            $manager->persist($region);
            $this->addReference('region' . $key, $region);

            foreach ($item->areas as $key2 => $area) {
                $city = new City();
                $city->setName($area->name->fr);
                $city->setCode('0000');
                $city->setRegion($region);

                $manager->persist($city);
                $this->addReference('ville' . $key2, $city);
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }

}
