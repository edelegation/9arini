<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\PageBundle\Entity\Page;
use DW\PageBundle\Entity\PageTranslation;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //Page Who we are
        $page = new Page();
        $page->setPublished(true);

        $page->setMetaTitle('Qui sommes nous?');

        $page->setTitle('Qui sommes nous?');

        $page->setMetaDescription('en cours');

        $page->setDescription(
            '<p>Nous sommes DW bien s&ucirc;r !</p>
            <h3>Qu&#39;est-ce que c&#39;est?</h3>
            <p>DW est un projet open source d&eacute;di&eacute; &agrave; la r&eacute;alisation d&#39;une solution puissante (et gratuite) pour les places de march&eacute; collaboratives (ou pas &agrave; vrai dire).</p>
            <h3>Qui finance tout &ccedil;a ?</h3>
            <p>La <a href="http://www.sofracs.com" title="création de marketplace">SOFRACS</a>. C&rsquo;est une agence web bas&eacute;e &agrave; Paris sp&eacute;cialis&eacute;e dans la r&eacute;alisation de places de march&eacute; collaboratives de location, de services, et de vente. Au fil des ans, ils ont d&eacute;cid&eacute; de partager leurs connaissances en finan&ccedil;ant le d&eacute;veloppement de DW.</p>
            <h3>Qu&rsquo;utilisez-vous sur DW ?</h3>
            <p>DW utilise Symfony 2.</p>
            <h3>O&ugrave; puis-je l&rsquo;obtenir?</h3>
            <p>Ici : <a target="_blank" href="https://github.com/Cocolabs-SAS/dw">https://github.com/Cocolabs-SAS/dw</a></p>
            <h3>Avez-vous une mascotte ?</h3>
            <p>Voici une vid&eacute;o de &quot;Cocotte&quot;&nbsp;: <a href="http://9arini.tn/">http://9arini.tn/</a></p>'
        );

        //Page How it Works
        $page1 = new Page();
        $page1->setPublished(true);

        $page1->setMetaTitle('Comment ca marche?');

        $page1->setTitle('Comment ca marche?');

        $page1->setMetaDescription('en cours');

        $page1->setDescription('en cours');

        //Page The team
        $page2 = new Page();
        $page2->setPublished(true);

        $page2->setMetaTitle('L\'équipe');

        $page2->setTitle('L\'équipe');

        $page2->setMetaDescription('en cours');

        $page2->setDescription('en cours');

        //Page FAQ
        $page3 = new Page();
        $page3->setPublished(true);

        $page3->setMetaTitle('FAQ');

        $page3->setTitle('FAQ');

        $page3->setMetaDescription('en cours');

        $page3->setDescription('en cours');


        //Page Legal notices
        $page4 = new Page();
        $page4->setPublished(true);

        $page4->setMetaTitle('Mentions légales');

        $page4->setTitle('Mentions légales');

        $page4->setMetaDescription('en cours');

        $page4->setDescription('en cours');


        $page5 = new Page();
        $page5->setPublished(true);

        $page5->setMetaTitle('Conditions générales d\'utilisation');

        $page5->setTitle('Conditions générales d\'utilisation');

        $page5->setMetaDescription('en cours');

        $page5->setDescription('en cours');

        $manager->persist($page);
        $manager->persist($page1);
        $manager->persist($page2);
        $manager->persist($page3);
        $manager->persist($page4);
        $manager->persist($page5);

        $manager->flush();

        $page->generateSlug();
        $page1->generateSlug();
        $page2->generateSlug();
        $page3->generateSlug();
        $page4->generateSlug();
        $page5->generateSlug();

        $manager->persist($page);
        $manager->persist($page1);
        $manager->persist($page2);
        $manager->persist($page3);
        $manager->persist($page4);
        $manager->persist($page5);

        $manager->flush();

        $this->addReference('who-we-are', $page);
        $this->addReference('how-it-works', $page1);
        $this->addReference('the-team', $page2);
        $this->addReference('faq', $page3);
        $this->addReference('legal-notice', $page4);
        $this->addReference('terms-of-use', $page5);

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }

}
