<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\CoreBundle\Entity\ListingCharacteristic;
use DW\CoreBundle\Entity\ListingCharacteristicGroup;
use DW\CoreBundle\Entity\ListingCharacteristicType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadListingCharacteristicData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $listingCharacteristic = new ListingCharacteristic();
        $listingCharacteristic->setPosition(1);
        $listingCharacteristic->setName('Caractéristique_1');
        $listingCharacteristic->setDescription('Description de la Caractéristique_1');
        /** @var ListingCharacteristicType $listingCharacteristicType */
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_yes_no'));
        $listingCharacteristic->setListingCharacteristicType($listingCharacteristicType);
        /** @var ListingCharacteristicGroup $listingCharacteristicGroup */
        $listingCharacteristicGroup = $manager->merge($this->getReference('group_1'));
        $listingCharacteristic->setListingCharacteristicGroup($listingCharacteristicGroup);

        $manager->persist($listingCharacteristic);
        $manager->flush();
        $this->addReference('characteristic_1', $listingCharacteristic);


        $listingCharacteristic = new ListingCharacteristic();
        $listingCharacteristic->setPosition(2);
        $listingCharacteristic->setName('Caractéristique_2');
        $listingCharacteristic->setDescription('Description de la Caractéristique_2');
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_quantity'));
        $listingCharacteristic->setListingCharacteristicGroup($listingCharacteristicGroup);
        $listingCharacteristic->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristic);
        $manager->flush();
        $this->addReference('characteristic_2', $listingCharacteristic);

        $listingCharacteristic = new ListingCharacteristic();
        $listingCharacteristic->setPosition(3);
        $listingCharacteristic->setName('Caractéristique_3');
        $listingCharacteristic->setDescription('Description de la Caractéristique_3');
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_custom_1'));
        $listingCharacteristic->setListingCharacteristicType($listingCharacteristicType);
        $listingCharacteristicGroup = $manager->merge($this->getReference('group_2'));
        $listingCharacteristic->setListingCharacteristicGroup($listingCharacteristicGroup);
        $manager->persist($listingCharacteristic);
        $manager->flush();
        $this->addReference('characteristic_3', $listingCharacteristic);


        $listingCharacteristic = new ListingCharacteristic();
        $listingCharacteristic->setPosition(4);
        $listingCharacteristic->setName('Caractéristique_4');
        $listingCharacteristic->setDescription('Description de la Caractéristique_4');
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_custom_1'));
        $listingCharacteristic->setListingCharacteristicType($listingCharacteristicType);
        $listingCharacteristicGroup = $manager->merge($this->getReference('group_2'));
        $listingCharacteristic->setListingCharacteristicGroup($listingCharacteristicGroup);
        $manager->persist($listingCharacteristic);
        $manager->flush();
        $this->addReference('characteristic_4', $listingCharacteristic);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }

}
