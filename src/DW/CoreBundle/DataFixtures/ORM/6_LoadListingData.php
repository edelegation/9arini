<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Entity\ListingCategory;
use DW\CoreBundle\Entity\ListingCharacteristic;
use DW\CoreBundle\Entity\ListingCharacteristicValue;
use DW\CoreBundle\Entity\ListingListingCategory;
use DW\CoreBundle\Entity\ListingListingCharacteristic;
use DW\CoreBundle\Entity\ListingLocation;
use DW\GeoBundle\Entity\Area;
use DW\GeoBundle\Entity\City;
use DW\GeoBundle\Entity\Coordinate;
use DW\GeoBundle\Entity\Country;
use DW\GeoBundle\Entity\Department;
use DW\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadListingData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //GeoGraphical entities
        $country = new Country();
        $country->setCode("TN");
        $country->setName('Tunisie');

        $area = new Area();
        $area->setCountry($country);
        $area->setName('Tunis');

        $department = new Department();
        $department->setCountry($country);
        $department->setArea($area);
        $department->setName('nodepartment');

        $city = new City();
        $city->setCountry($country);
        $city->setArea($area);
        $city->setDepartment($department);
        $city->setName('Tunis');

        $manager->persist($country);
        $manager->persist($area);
        $manager->persist($department);
        $manager->persist($city);

        //Coordinate entity
        $coordinate = new Coordinate();
        $coordinate->setCountry($country);
        $coordinate->setArea($area);
        $coordinate->setDepartment($department);
        $coordinate->setCity($city);
        $coordinate->setZip("1001");
        $coordinate->setRoute("Ave de france");
        $coordinate->setStreetNumber("9");
        $coordinate->setAddress("9 Rue de la Lune, 1001 Tunis, Tunisie");
        $coordinate->setLat(36.838569);
        $coordinate->setLng(10.170534);
        $manager->persist($coordinate);

        //Listing Location
        $location = new ListingLocation();
        $location->setCountry("TN");
        $location->setRegion($manager->merge($this->getReference('region1')));
        $location->setCity($manager->merge($this->getReference('ville1')));
        $location->setAddress("Ave de france");
        $location->setCoordinate($coordinate);
        $manager->persist($location);

        //Listing
        $listing = new Listing();
        $listing->setLocation($location);
        $listing->setTitle('Cours de Math');

        $listing->setDescription('Description cours de Math');

        $listing->setStatus(Listing::STATUS_PUBLISHED);
        $listing->setPrice(50);
        $listing->setCertified(1);

        /** @var User $user */
        $user = $manager->merge($this->getReference('offerer'));
        $listing->setUser($user);

        /** @var ListingCategory $category */
        $category = $manager->merge($this->getReference('soutien_scolaire'));
        $listingCategory = new ListingListingCategory();
        $listingCategory->setListing($listing);
        $listingCategory->setCategory($category);
        $listing->addListingListingCategory($listingCategory);

        $category = $manager->merge($this->getReference('math'));
        $listingCategory = new ListingListingCategory();
        $listingCategory->setListing($listing);
        $listingCategory->setCategory($category);
        $listing->addListingListingCategory($listingCategory);

        $category = $manager->merge($this->getReference('college'));
        $listingCategory = new ListingListingCategory();
        $listingCategory->setListing($listing);
        $listingCategory->setCategory($category);
        $listing->addListingListingCategory($listingCategory);

        /** @var ListingCharacteristic $characteristic */
        $characteristic = $manager->merge($this->getReference('characteristic_1'));
        $listingListingCharacteristic = new ListingListingCharacteristic();
        $listingListingCharacteristic->setListing($listing);
        $listingListingCharacteristic->setListingCharacteristic($characteristic);
        /** @var ListingCharacteristicValue $value */
        $value = $manager->merge($this->getReference('characteristic_value_yes'));
        $listingListingCharacteristic->setListingCharacteristicValue($value);
        $listing->addListingListingCharacteristic($listingListingCharacteristic);


        $characteristic = $manager->merge($this->getReference('characteristic_2'));
        $listingListingCharacteristic = new ListingListingCharacteristic();
        $listingListingCharacteristic->setListing($listing);
        $listingListingCharacteristic->setListingCharacteristic($characteristic);
        $value = $manager->merge($this->getReference('characteristic_value_2'));
        $listingListingCharacteristic->setListingCharacteristicValue($value);
        $listing->addListingListingCharacteristic($listingListingCharacteristic);


        $characteristic = $manager->merge($this->getReference('characteristic_3'));
        $listingListingCharacteristic = new ListingListingCharacteristic();
        $listingListingCharacteristic->setListing($listing);
        $listingListingCharacteristic->setListingCharacteristic($characteristic);
        $value = $manager->merge($this->getReference('characteristic_value_custom_1'));
        $listingListingCharacteristic->setListingCharacteristicValue($value);
        $listing->addListingListingCharacteristic($listingListingCharacteristic);


        $characteristic = $manager->merge($this->getReference('characteristic_4'));
        $listingListingCharacteristic = new ListingListingCharacteristic();
        $listingListingCharacteristic->setListing($listing);
        $listingListingCharacteristic->setListingCharacteristic($characteristic);
        $value = $manager->merge($this->getReference('characteristic_value_1'));
        $listingListingCharacteristic->setListingCharacteristicValue($value);
        $listing->addListingListingCharacteristic($listingListingCharacteristic);

        $listing->generateSlug();

        $manager->persist($listing);
        $manager->flush();

        $this->addReference('listing-one', $listing);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9;
    }

}
