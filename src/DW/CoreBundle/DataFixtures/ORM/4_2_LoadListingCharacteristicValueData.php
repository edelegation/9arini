<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\CoreBundle\Entity\ListingCharacteristicType;
use DW\CoreBundle\Entity\ListingCharacteristicValue;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadListingCharacteristicValueData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('Oui');
        $listingCharacteristicValue->setPosition(1);
        /** @var ListingCharacteristicType $listingCharacteristicType */
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_yes_no'));
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_yes', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('Non');
        $listingCharacteristicValue->setPosition(2);
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_no', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('1');
        $listingCharacteristicValue->setPosition(1);
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_quantity'));
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_1', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('2');
        $listingCharacteristicValue->setPosition(2);
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_2', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('3');
        $listingCharacteristicValue->setPosition(2);
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_3', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('Valeur personnalisée 1');
        $listingCharacteristicValue->setPosition(1);
        $listingCharacteristicType = $manager->merge($this->getReference('characteristic_type_custom_1'));
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_custom_1', $listingCharacteristicValue);

        $listingCharacteristicValue = new ListingCharacteristicValue();
        $listingCharacteristicValue->setName('Valeur personnalisée 2');
        $listingCharacteristicValue->setPosition(2);
        $listingCharacteristicValue->setListingCharacteristicType($listingCharacteristicType);
        $manager->persist($listingCharacteristicValue);
        $manager->flush();
        $this->addReference('characteristic_value_custom_2', $listingCharacteristicValue);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

}
