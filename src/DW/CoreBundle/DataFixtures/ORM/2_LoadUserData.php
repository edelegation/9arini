<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\UserBundle\Entity\User;
use DW\UserBundle\Event\UserEvent;
use DW\UserBundle\Event\UserEvents;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var  ContainerInterface container */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('dw_user.user_manager');

        /** @var  User $user */
        $user = $userManager->createUser();
        $user->setUsername('offerer@9arini.tn');
        $user->setEmail('offerer@9arini.tn');
        $user->setPlainPassword('12345678');
        $user->setLastName('OffererName');
        $user->setFirstName('OffererFirstName');
        $user->setCountryOfResidence('FR');
        $user->setBirthday(new \DateTime('1973-05-29'));
        $user->setEnabled(true);
        $user->setAnnualIncome(1000);
        $user->setEmailVerified(true);
        $user->setPhoneVerified(true);
        $user->setMotherTongue("fr");

        $event = new UserEvent($user);
        $this->container->get('event_dispatcher')->dispatch(UserEvents::USER_REGISTER, $event);
        $user = $event->getUser();

        $userManager->updateUser($user);
        $this->addReference('offerer', $user);

        $user = $userManager->createUser();
        $user->setUsername('asker@9arini.tn');
        $user->setEmail('asker@9arini.tn');
        $user->setPlainPassword('12345678');
        $user->setLastName('AskerName');
        $user->setFirstName('AskerFirstName');
        $user->setCountryOfResidence('FR');
        $user->setBirthday(new \DateTime('1975-08-27'));
        $user->setEnabled(true);
        $user->setAnnualIncome(1000);
        $user->setMotherTongue("fr");

        $event = new UserEvent($user);
        $this->container->get('event_dispatcher')->dispatch(UserEvents::USER_REGISTER, $event);
        $user = $event->getUser();

        $userManager->updateUser($user);
        $this->addReference('asker', $user);

        $user = $userManager->createUser();
        $user->setUsername('disableuser@9arini.tn');
        $user->setEmail('disableuser@9arini.tn');
        $user->setPlainPassword('12345678');
        $user->setLastName('DisableUserLastName');
        $user->setFirstName('DisableUserFirstName');
        $user->setCountryOfResidence('FR');
        $user->setBirthday(new \DateTime('1978-08-27'));
        $user->setEnabled(false);
        $user->setAnnualIncome(1000);
        $user->setMotherTongue("fr");

        $event = new UserEvent($user);
        $this->container->get('event_dispatcher')->dispatch(UserEvents::USER_REGISTER, $event);
        $user = $event->getUser();

        $userManager->updateUser($user);
        $this->addReference('disable-user', $user);

        $user = $userManager->createUser();
        $user->setLastName('super-admin');
        $user->setFirstName('super-admin');
        $user->setUsername('super-admin@9arini.tn');
        $user->setEmail('super-admin@9arini.tn');
        $user->setPlainPassword('super-admin');
        $user->setCountryOfResidence('FR');
        $user->setBirthday(new \DateTime('1978-07-01'));
        $user->setEnabled(true);
        $user->addRole('ROLE_SUPER_ADMIN');

        $event = new UserEvent($user);
        $this->container->get('event_dispatcher')->dispatch(UserEvents::USER_REGISTER, $event);
        $user = $event->getUser();

        $userManager->updateUser($user);
        $this->addReference('super-admin', $user);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }

}
