<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\MessageBundle\Entity\Message;
use DW\MessageBundle\Entity\MessageMetadata;
use DW\MessageBundle\Entity\Thread;
use DW\MessageBundle\Entity\ThreadMetadata;
use DW\PageBundle\Entity\PageTranslation;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadThreadData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
       /* //Add new thread
        for ($i = 1; $i <= 50; $i++) {
            $thread = new Thread();
            $user = $manager->getRepository("DWUserBundle:User")->findOneByEmail("offerer@dw.rocks");
            $thread->setCreatedBy($user);
            $thread->setCreatedAt(new \DateTime("2017-10-10"));
            $thread->setIsDeleted(false);
            $thread->setSubject("message " . $i);
            $thread->addParticipant($user);
            $thread->setListing($manager->getRepository('DWCoreBundle:Listing')->findAll()[0]);
            $manager->persist($thread);
            $manager->flush();


            //Add new message
            $message = new Message();
            $message->setCreatedAt(new \DateTime('2017-01-01'));
            $message->setBody("Voici le message .... " . $i);
            $message->setSender($user);
            $message->setThread($thread);
            $manager->persist($message);
            $manager->flush();

            //Add new thread metadata
            $threadMetadata = new ThreadMetadata();
            $threadMetadata->setParticipant($user);
            $threadMetadata->setThread($thread);
            $threadMetadata->setLastMessageDate(new \DateTime('2017-11-01'));
            $manager->persist($threadMetadata);
            $manager->flush();

            //Add new message metadata
            $messageMetadata = new MessageMetadata();
            $messageMetadata->setIsRead(false);
            $messageMetadata->setMessage($message);
            $messageMetadata->setParticipant($user);
            $manager->persist($messageMetadata);
            $manager->flush();
        }*/

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }

}
