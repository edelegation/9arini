<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\CoreBundle\Entity\Booking;
use DW\MessageBundle\Entity\Message;
use DW\MessageBundle\Entity\MessageMetadata;
use DW\MessageBundle\Entity\Thread;
use DW\MessageBundle\Entity\ThreadMetadata;
use DW\PageBundle\Entity\PageTranslation;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DW\ReviewBundle\Entity\Review;
use DW\ReviewBundle\Repository\ReviewRepository;

class LoadReviewData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        /*//Add new thread
        for ($i = 1; $i <= 50; $i++) {
            $user = $manager->getRepository("DWUserBundle:User")->findOneByEmail("offerer@dw.rocks");
            $review = new Review();
            $booking = new Booking();
            $booking->setThread($manager->getRepository("DWMessageBundle:Thread")->find($i));
            $booking->setMessage("booking ".$i);
            $booking->setCreatedAt(new \DateTime("2017-10-10"));
            $booking->setStart(new \DateTime("2017-10-10"));
            $booking->setStartTime(new \DateTime("2017-10-10"));
            $booking->setEnd(new \DateTime("2017-10-15"));
            $booking->setEndTime(new \DateTime("2017-10-15"));
            $booking->setListing($manager->getRepository("DWCoreBundle:Listing")->findAll()[0]);
            $booking->setAcceptedBookingAt(new \DateTime("2017-10-10"));
            $booking->setAmount(100);
            $booking->setAmountFeeAsAsker(100);
            $booking->setAmountFeeAsOfferer(100);
            $booking->setAmountTotal(120);
            $booking->setCancellationPolicy(1);
            $booking->setUser($user);
            $manager->persist($booking);
            $manager->flush();


            $review->setCreatedAt(new \DateTime("2017-10-10"));
            $review->setComment("comment " . $i);
            $review->setReviewBy($user);
            $review->setReviewTo($user);
            $review->setRating(2);
            $review->setBooking($booking);
            $manager->persist($review);
            $manager->flush();
        }*/

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12;
    }

}
