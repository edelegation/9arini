<?php

namespace DW\CoreBundle\DataFixtures\ORM;

use DW\CoreBundle\Entity\ListingCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadListingCategoryData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $category = new ListingCategory();
        $category->setName('Type de prestation');

        $category2 = new ListingCategory();
        $category2->setName('Cours');

        $category3 = new ListingCategory();
        $category3->setName('Système éducatif');

        $category4 = new ListingCategory();
        $category4->setName('Loisirs');

        $category5 = new ListingCategory();
        $category5->setName('Niveaux');

        $manager->persist($category);
        $manager->persist($category2);
        $manager->persist($category3);
        $manager->persist($category4);
        $manager->persist($category5);
        $manager->flush();

        $subCategory1 = new ListingCategory();
        $subCategory1->setName('Soutien Scolaire');
        $subCategory1->setParent($category);
        $this->addReference('soutien_scolaire', $subCategory1);

        $subCategory2 = new ListingCategory();
        $subCategory2->setName('Formation & Loisir');
        $subCategory2->setParent($category);

        $manager->persist($subCategory1);
        $manager->persist($subCategory2);
        $manager->flush();

        $subCategory1 = new ListingCategory();
        $subCategory1->setName('Math');
        $subCategory1->setCssClass('im im-icon-Aim');
        $subCategory1->setParent($category2);
        $this->addReference('math', $subCategory1);

        $subCategory2 = new ListingCategory();
        $subCategory2->setName('Physique');
        $subCategory1->setCssClass('im im-icon-Archery-2');
        $subCategory2->setParent($category2);

        $manager->persist($subCategory1);
        $manager->persist($subCategory2);
        $manager->flush();

        $subCategory1 = new ListingCategory();
        $subCategory1->setName('Dance');
        $subCategory1->setCssClass('im im-icon-Atom');
        $subCategory1->setParent($category4);

        $subCategory2 = new ListingCategory();
        $subCategory2->setName('Musique');
        $subCategory1->setCssClass('im im-icon-Astronaut');
        $subCategory2->setParent($category4);

        $manager->persist($subCategory1);
        $manager->persist($subCategory2);
        $manager->flush();


        $subCategory1 = new ListingCategory();
        $subCategory1->setName('Canadien');
        $subCategory1->setParent($category3);

        $subCategory2 = new ListingCategory();
        $subCategory2->setName('Mission');
        $subCategory2->setParent($category3);

        $manager->persist($subCategory1);
        $manager->persist($subCategory2);
        $manager->flush();

        $subSubCategory1 = new ListingCategory();
        $subSubCategory1->setName('Primaire');
        $subSubCategory1->setParent($subCategory1);

        $subSubCategory2 = new ListingCategory();
        $subSubCategory2->setName('Collège');
        $subSubCategory2->setParent($subCategory1);
        $this->addReference('college', $subSubCategory2);

        $subSubCategory3 = new ListingCategory();
        $subSubCategory3->setName('Secondaire');
        $subSubCategory3->setParent($subCategory1);

        $manager->persist($subSubCategory1);
        $manager->persist($subSubCategory2);
        $manager->persist($subSubCategory3);
        $manager->flush();

        $subSubCategory1 = new ListingCategory();
        $subSubCategory1->setName('Primaire');
        $subSubCategory1->setParent($subCategory2);

        $subSubCategory2 = new ListingCategory();
        $subSubCategory2->setName('Collège');
        $subSubCategory2->setParent($subCategory2);

        $subSubCategory3 = new ListingCategory();
        $subSubCategory3->setName('Secondaire');
        $subSubCategory3->setParent($subCategory2);

        $manager->persist($subSubCategory1);
        $manager->persist($subSubCategory2);
        $manager->persist($subSubCategory3);
        $manager->flush();


        $subCategory1 = new ListingCategory();
        $subCategory1->setName('Débutant');
        $subCategory1->setParent($category5);

        $subCategory2 = new ListingCategory();
        $subCategory2->setName('Intermédiare');
        $subCategory2->setParent($category5);

        $subCategory3 = new ListingCategory();
        $subCategory3->setName('Avancé');
        $subCategory3->setParent($category5);

        $manager->persist($subCategory1);
        $manager->persist($subCategory2);
        $manager->persist($subCategory3);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }

}
