<?php


namespace DW\CoreBundle\Entity;

use DW\CoreBundle\Model\BaseListingDiscount;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingDiscount
 *
 * @ORM\Entity(repositoryClass="DW\CoreBundle\Repository\ListingDiscountRepository")
 * @UniqueEntity(
 *     fields={"listing", "fromQuantity"},
 *     errorPath="fromQuantity",
 *     message="assert.unique"
 * )
 * @ORM\Table(name="listing_discount",
 *  indexes={
 *    @ORM\Index(name="discount_idx", columns={"discount"}),
 *    @ORM\Index(name="from_quantity_idx", columns={"from_quantity"})
 *  },
 *  uniqueConstraints={@ORM\UniqueConstraint(name="discount_unique", columns={"listing_id", "from_quantity"})}
 * )
 */
class ListingDiscount extends BaseListingDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Listing", inversedBy="discounts")
     * @ORM\JoinColumn(name="listing_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $listing;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set listing
     *
     * @param  \DW\CoreBundle\Entity\Listing $listing
     * @return ListingDiscount
     */
    public function setListing(Listing $listing = null)
    {
        $this->listing = $listing;

        return $this;
    }

    /**
     * Get listing
     *
     * @return \DW\CoreBundle\Entity\Listing
     */
    public function getListing()
    {
        return $this->listing;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
