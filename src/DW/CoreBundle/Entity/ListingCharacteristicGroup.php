<?php

namespace DW\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingCharacteristicGroup
 *
 * @ORM\Entity()
 * @UniqueEntity("position", message="assert.unique")
 * @ORM\Table(name="listing_characteristic_group", indexes={
 *    @ORM\Index(name="position_lcg_idx", columns={"position"})
 *  })
 *
 */
class ListingCharacteristicGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(type="string", length=255, name="name", nullable=false)
     */
    protected $name;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(name="position", type="smallint", nullable=false, unique=true)
     */
    private $position;

    /**
     *
     * @ORM\OneToMany(targetEntity="ListingCharacteristic", mappedBy="listingCharacteristicGroup", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "asc"})
     */
    private $listingCharacteristics;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listingCharacteristics = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets name.
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param  boolean $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Add listingCharacteristics
     *
     * @param  \DW\CoreBundle\Entity\ListingCharacteristic $listingCharacteristics
     * @return ListingCharacteristicType
     */
    public function addListingCharacteristic(ListingCharacteristic $listingCharacteristics)
    {
        $this->listingCharacteristics[] = $listingCharacteristics;

        return $this;
    }

    /**
     * Remove listingCharacteristics
     *
     * @param \DW\CoreBundle\Entity\ListingCharacteristic $listingCharacteristics
     */
    public function removeListingCharacteristic(ListingCharacteristic $listingCharacteristics)
    {
        $this->listingCharacteristics->removeElement($listingCharacteristics);
    }

    /**
     * Get listingCharacteristics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListingCharacteristics()
    {
        return $this->listingCharacteristics;
    }


    public function __toString()
    {
        return $this->getName();
    }
}
