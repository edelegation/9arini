<?php


namespace DW\CoreBundle\Entity;

use DW\CoreBundle\Model\BaseListingLocation;
use DW\GeoBundle\Entity\Coordinate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingLocation
 *
 * @ORM\Table(name="listing_location")
 *
 * @ORM\Entity
 */
class ListingLocation extends BaseListingLocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="DW\CoreBundle\Entity\Listing", mappedBy="location")
     * @ORM\JoinColumn(name="listing_id", referencedColumnName="id", onDelete="CASCADE")
     **/
    private $listing;

    /**
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\ManyToOne(targetEntity="DW\GeoBundle\Entity\Coordinate", inversedBy="listingLocations", cascade={"persist"})
     * @ORM\JoinColumn(name="coordinate_id", referencedColumnName="id", nullable=false)
     *
     * @var Coordinate
     */
    private $coordinate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coordinate
     *
     * @param  \DW\GeoBundle\Entity\Coordinate $coordinate
     * @return ListingLocation
     */
    public function setCoordinate(Coordinate $coordinate)
    {
        $this->coordinate = $coordinate;

        return $this;
    }

    /**
     * Get coordinate
     *
     * @return \DW\GeoBundle\Entity\Coordinate
     */
    public function getCoordinate()
    {
        return $this->coordinate;
    }

    /**
     * Set listing
     *
     * @param  \DW\CoreBundle\Entity\Listing $listing
     * @return ListingLocation
     */
    public function setListing(Listing $listing = null)
    {
        $this->listing = $listing;

        return $this;
    }

    /**
     * Get listing
     *
     * @return \DW\CoreBundle\Entity\Listing
     */
    public function getListing()
    {
        return $this->listing;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;

            $coordinate = $this->getCoordinate();
            $this->setCoordinate(clone $coordinate);
        }
    }
}
