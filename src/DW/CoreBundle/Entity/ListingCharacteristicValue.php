<?php

namespace DW\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingCharacteristicValue
 *
 * @ORM\Entity(repositoryClass="DW\CoreBundle\Repository\ListingCharacteristicValueRepository")
 *
 * @ORM\Table(name="listing_characteristic_value",indexes={
 *    @ORM\Index(name="position_lcv_idx", columns={"position"})
 *  })
 *
 */
class ListingCharacteristicValue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var string $name
     * @Assert\NotBlank(message="assert.not_blank")
     * @ORM\Column(type="string", length=255, name="name", nullable=false)
     *
     */
    protected $name;

    /**
     * @var int
     *
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(name="position", type="smallint", nullable=false)
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\ListingCharacteristicType", inversedBy="listingCharacteristicValues")
     * @ORM\JoinColumn(name="listing_characteristic_type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $listingCharacteristicType;

    /**
     *
     * @ORM\OneToMany(targetEntity="ListingListingCharacteristic", mappedBy="listingCharacteristicValue", cascade={"persist", "remove"})
     *
     */
    private $listingListingCharacteristics;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets name.
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param  boolean $position
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Get characteristic type
     *
     * @return ListingCharacteristicType
     */
    public function getListingCharacteristicType()
    {
        return $this->listingCharacteristicType;
    }

    /**
     * Set characteristic type
     *
     * @param $listingCharacteristicType
     * @return ListingCharacteristicType
     */
    public function setListingCharacteristicType(ListingCharacteristicType $listingCharacteristicType)
    {
        $this->listingCharacteristicType = $listingCharacteristicType;

        return $this;
    }

    /**
     * Add listingListingCharacteristics
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristics
     * @return ListingCharacteristicValue
     */
    public function addListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristics)
    {
        $this->listingListingCharacteristics[] = $listingListingCharacteristics;

        return $this;
    }

    /**
     * Remove listingListingCharacteristics
     *
     * @param \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristics
     */
    public function removeListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristics)
    {
        $this->listingListingCharacteristics->removeElement($listingListingCharacteristics);
    }

    /**
     * Get listingListingCharacteristics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListingListingCharacteristics()
    {
        return $this->listingListingCharacteristics;
    }
}
