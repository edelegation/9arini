<?php

namespace DW\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 *
 * @ORM\Table(name="region")
 * @ORM\Entity
 */
class Region
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\City", mappedBy="region")
     * @var City
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\ListingLocation", mappedBy="region")
     * @var ListingLocation
     */
    private $location;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Region
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new ArrayCollection();
        $this->cities = new ArrayCollection();
    }

    /**
     * Add location
     *
     * @param ListingLocation $location
     *
     * @return Region
     */
    public function addLocation(ListingLocation $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param ListingLocation $location
     */
    public function removeLocation(ListingLocation $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return Collection
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add city
     *
     * @param City $city
     *
     * @return Region
     */
    public function addCity(City $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param City $city
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    public function __toString()
    {
        return $this->name;
    }
}
