<?php

namespace DW\CoreBundle\Entity;

use DW\CoreBundle\Model\ListingCategoryListingCategoryFieldInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingCategory
 *
 * @Gedmo\Tree(type="nested")
 *
 * @ORM\Entity(repositoryClass="DW\CoreBundle\Repository\ListingCategoryRepository")
 *
 * @ORM\Table(name="listing_category",indexes={
 *    @ORM\Index(name="name_idx", columns={"name"})
 *  })
 *
 */
class ListingCategory
{
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="ListingCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="ListingCategory", mappedBy="parent")
     * @ORM\OrderBy({"id" = "ASC"})
     */
    private $children;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\ListingListingCategory", mappedBy="category", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $listingListingCategories;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Model\ListingCategoryListingCategoryFieldInterface", mappedBy="category", cascade={"persist", "remove"})
     */
    private $fields;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    protected $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    protected $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    protected $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    protected $root;

    /**
     * @var string
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(name="css_class", type="string", length=255, nullable=true)
     */
    protected $cssClass = 'default_class';

    /**
     * @var boolean
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(name="display", type="boolean")
     */
    protected $display = false;


    public function __construct()
    {
        $this->listingListingCategories = new ArrayCollection();
        $this->fields = new ArrayCollection();
    }

    public function getSluggableFields()
    {
        return ['name'];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;

    }

    /**
     * Set parent
     *
     * @param  \DW\CoreBundle\Entity\ListingCategory $parent
     * @return ListingCategory
     */
    public function setParent(ListingCategory $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \DW\CoreBundle\Entity\ListingCategory
     */
    public function getParent()
    {
        return $this->parent;
    }


    /**
     * Add children
     *
     * @param  \DW\CoreBundle\Entity\ListingCategory $children
     * @return ListingCategory
     */
    public function addChild(ListingCategory $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \DW\CoreBundle\Entity\ListingCategory $children
     */
    public function removeChild(ListingCategory $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add field
     *
     * @param  ListingCategoryListingCategoryFieldInterface $field
     * @return ListingCategory
     */
    public function addField($field)
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove listings
     *
     * @param  ListingCategoryListingCategoryFieldInterface $field
     */
    public function removeField($field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|ListingCategoryListingCategoryFieldInterface[]
     */
    public function getFields()
    {
        return $this->fields;
    }


    /**
     * Add category
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCategory $listingListingCategory
     * @return ListingCategory
     */
    public function addListingListingCategory(ListingListingCategory $listingListingCategory)
    {
        if (!$this->listingListingCategories->contains($listingListingCategory)) {
            $this->listingListingCategories[] = $listingListingCategory;
        }

        return $this;
    }

    /**
     * Remove category
     *
     * @param \DW\CoreBundle\Entity\ListingListingCategory $listingListingCategory
     */
    public function removeListingListingCategory(ListingListingCategory $listingListingCategory)
    {
        $this->listingListingCategories->removeElement($listingListingCategory);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListingListingCategories()
    {
        return $this->listingListingCategories;
    }

    /**
     * @param ArrayCollection $listingListingCategories
     * @return ArrayCollection
     */
    public function setListingListingCategories(ArrayCollection $listingListingCategories)
    {
        $this->listingListingCategories = $listingListingCategories;
    }


    /**
     * Set lft
     *
     * @param  integer $lft
     * @return ListingCategory
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param  integer $lvl
     * @return ListingCategory
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param  integer $rgt
     * @return ListingCategory
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param  integer $root
     * @return ListingCategory
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Return if is leaf
     *
     * @return boolean
     */
    public function isLeaf()
    {
        return $this->getRgt() == $this->getLft() + 1;
    }

    /**
     * Set name
     *
     * @param  string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = ucfirst($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * @param string $cssClass
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    /**
     * Set display
     *
     * @param boolean $display
     *
     * @return ListingCategory
     */
    public function setDisplay($display)
    {
        $this->display = $display;

        return $this;
    }

    /**
     * Get display
     *
     * @return boolean
     */
    public function getDisplay()
    {
        return $this->display;
    }
}
