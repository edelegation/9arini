<?php

namespace DW\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListingCharacteristic
 *
 * @ORM\Entity(repositoryClass="DW\CoreBundle\Repository\ListingCharacteristicRepository")
 * @UniqueEntity("position", message="assert.unique")
 *
 * @ORM\Table(name="listing_characteristic",indexes={
 *    @ORM\Index(name="position_idx", columns={"position"})
 *  })
 *
 */
class ListingCharacteristic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\ListingCharacteristicType", inversedBy="listingCharacteristics")
     * @ORM\JoinColumn(name="listing_characteristic_type_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $listingCharacteristicType;

    /**
     *
     * @ORM\OneToMany(targetEntity="ListingListingCharacteristic", mappedBy="listingCharacteristic", cascade={"persist", "remove"})
     */
    private $listingListingCharacteristics;

    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\ListingCharacteristicGroup", inversedBy="listingCharacteristics", fetch="EAGER")
     * @ORM\JoinColumn(name="listing_characteristic_group_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $listingCharacteristicGroup;

    /**
     * @ORM\Column(name="position", type="smallint", nullable=false)
     */
    protected $position;

    /**
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\Column(type="string", length=255, name="name", nullable=false)
     *
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     *
     * @var string
     */
    protected $description;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listingListingCharacteristics = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getListingCharacteristicTypeValues()
    {
        return $this->getListingCharacteristicType()->getListingCharacteristicValues();
    }

    /**
     * Add characteristics
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     * @return ListingCharacteristic
     */
    public function addListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristic)
    {
        $this->listingListingCharacteristics[] = $listingListingCharacteristic;

        return $this;
    }

    /**
     * Remove characteristics
     *
     * @param \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     */
    public function removeListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristic)
    {
        $this->listingListingCharacteristics->removeElement($listingListingCharacteristic);
    }

    /**
     * Get characteristics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListingListingCharacteristics()
    {
        return $this->listingListingCharacteristics;
    }

    /**
     * Set listingCharacteristicType
     *
     * @param  \DW\CoreBundle\Entity\ListingCharacteristicType $listingCharacteristicType
     * @return ListingCharacteristic
     */
    public function setListingCharacteristicType(ListingCharacteristicType $listingCharacteristicType)
    {
        $this->listingCharacteristicType = $listingCharacteristicType;

        return $this;
    }

    /**
     * Get listingCharacteristicType
     *
     * @return \DW\CoreBundle\Entity\ListingCharacteristicType
     */
    public function getListingCharacteristicType()
    {
        return $this->listingCharacteristicType;
    }

    /**
     * Set ListingCharacteristicGroup
     *
     * @param  \DW\CoreBundle\Entity\ListingCharacteristicGroup $listingCharacteristicGroup
     * @return ListingCharacteristic
     */
    public function setListingCharacteristicGroup(ListingCharacteristicGroup $listingCharacteristicGroup)
    {
        $this->listingCharacteristicGroup = $listingCharacteristicGroup;

        return $this;
    }

    /**
     * Get ListingCharacteristicGroup
     *
     * @return \DW\CoreBundle\Entity\ListingCharacteristicGroup
     */
    public function getListingCharacteristicGroup()
    {
        return $this->listingCharacteristicGroup;
    }

    /**
     * Set position
     *
     * @param  boolean $position
     * @return ListingCharacteristic
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets name.
     *
     * @param $name
     * @return ListingCharacteristic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return ListingCharacteristic
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}
