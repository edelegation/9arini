<?php

namespace DW\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DW\CoreBundle\Entity\ListingLocation;

/**
 * City
 *
 * @ORM\Table(name="city")
 * @ORM\Entity
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="DW\CoreBundle\Entity\Region", inversedBy="city", cascade={"persist"})
     *
     * @var string
     */
    protected $region;

    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\ListingLocation", mappedBy="city")
     * @var ListingLocation
     */
    private $location;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return City
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->location = new ArrayCollection();
    }

    /**
     * Add location
     *
     * @param ListingLocation $location
     *
     * @return City
     */
    public function addLocation(ListingLocation $location)
    {
        $this->location[] = $location;

        return $this;
    }

    /**
     * Remove location
     *
     * @param ListingLocation $location
     */
    public function removeLocation(ListingLocation $location)
    {
        $this->location->removeElement($location);
    }

    /**
     * Get location
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set region
     *
     * @param \DW\CoreBundle\Entity\Region $region
     *
     * @return City
     */
    public function setRegion(\DW\CoreBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \DW\CoreBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    public function __toString()
    {
       return $this->name;
    }
}
