<?php

namespace DW\CoreBundle\Entity;

use DW\CoreBundle\Model\BaseListing;
use DW\CoreBundle\Model\ListingOptionInterface;
use DW\MessageBundle\Entity\Thread;
use DW\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Listing
 *
 * @ORM\Entity(repositoryClass="DW\CoreBundle\Repository\ListingRepository")
 *
 * @ORM\Table(name="listing",indexes={
 *    @ORM\Index(name="status_l_idx", columns={"status"}),
 *    @ORM\Index(name="price_idx", columns={"price"}),
 *    @ORM\Index(name="type_idx", columns={"type"}),
 *    @ORM\Index(name="min_duration_idx", columns={"min_duration"}),
 *    @ORM\Index(name="max_duration_idx", columns={"max_duration"}),
 *    @ORM\Index(name="average_rating_idx", columns={"average_rating"}),
 *    @ORM\Index(name="admin_notation_idx", columns={"admin_notation"}),
 *    @ORM\Index(name="slug_idx", columns={"slug"}),
 *  })
 */
class Listing extends BaseListing
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Sluggable\Sluggable;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="DW\CoreBundle\Model\CustomIdGenerator")
     *
     * @var integer
     */
    private $id;

    /**
     * @Assert\NotBlank(message="assert.not_blank")
     *
     * @ORM\ManyToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="listings", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="ListingLocation", inversedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="location_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @var ListingLocation
     **/
    private $location;

    /**
     * @ORM\OneToMany(targetEntity="ListingListingCategory", mappedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true)//, fetch="EAGER"
     *
     */
    private $listingListingCategories;

    /**
     * @ORM\OneToMany(targetEntity="ListingListingCharacteristic", mappedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true) //, fetch="EAGER"
     *
     */
    private $listingListingCharacteristics;

    /**
     *
     * @ORM\OneToMany(targetEntity="ListingDiscount", mappedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"fromQuantity" = "asc"})
     */
    private $discounts;


    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity="DW\MessageBundle\Entity\Thread", mappedBy="listing", cascade={"remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    private $threads;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Model\ListingOptionInterface", mappedBy="listing", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $options;

    /**
     * @Assert\NotBlank(message="assert.not_blank")
     * @Assert\NotNull(message="assert.not_blank")
     * @Assert\Length(
     *      min = "3",
     *      max = "150",
     *      minMessage = "assert.min_length {{ limit }}",
     *      maxMessage = "assert.max_length {{ limit }}"
     * )
     *
     * @ORM\Column(name="title", type="string", length=150, nullable=false)
     *
     * @var string
     */
    protected $title;

    /**
     * @Assert\NotBlank(message="assert.not_blank")
     * @Assert\NotNull(message="assert.not_blank")
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     *
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="rules", type="text", length=65535, nullable=true)
     *
     * @var string
     */
    protected $rules;

    /**
     * @ORM\Column(name="reason_invalid", type="text", length=65535, nullable=true)
     *
     * @var string
     */
    protected $reasonInvalid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display", type="boolean")
     */
    protected $display = false;


    public function __construct()
    {
        $this->listingListingCharacteristics = new ArrayCollection();
        $this->listingListingCategories = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->threads = new ArrayCollection();
        $this->options = new ArrayCollection();
    }

    public function getSluggableFields()
    {
        return ['title','id'];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add characteristics
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     * @return Listing
     */
    public function addListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristic)
    {
        $this->listingListingCharacteristics[] = $listingListingCharacteristic;

        return $this;
    }


    /**
     * Remove characteristics
     *
     * @param \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     */
    public function removeListingListingCharacteristic(ListingListingCharacteristic $listingListingCharacteristic)
    {
        $this->listingListingCharacteristics->removeElement($listingListingCharacteristic);
        $listingListingCharacteristic->setListing(null);
    }

    /**
     * Get characteristics
     *
     * @return \Doctrine\Common\Collections\Collection|ListingListingCharacteristic[]
     */
    public function getListingListingCharacteristics()
    {
        return $this->listingListingCharacteristics;
    }

    /**
     * Get characteristics ordered by Group and Characteristic
     *
     * @return ArrayCollection
     */
    public function getListingListingCharacteristicsOrderedByGroup()
    {
        $iterator = $this->listingListingCharacteristics->getIterator();
        $iterator->uasort(
            function ($a, $b) {
                /**
                 * @var ListingListingCharacteristic $a
                 * @var ListingListingCharacteristic $b
                 */
                $groupPosA = $a->getListingCharacteristic()->getListingCharacteristicGroup()->getPosition();
                $groupPosB = $b->getListingCharacteristic()->getListingCharacteristicGroup()->getPosition();

                $characteristicPosA = $a->getListingCharacteristic()->getPosition();
                $characteristicPosB = $b->getListingCharacteristic()->getPosition();
                if ($groupPosA == $groupPosB) {
                    if ($characteristicPosA == $characteristicPosB) {
                        return 0;
                    }

                    return ($characteristicPosA < $characteristicPosB) ? -1 : 1;
                }

                return ($groupPosA < $groupPosB) ? -1 : 1;
            }
        );

        return new ArrayCollection(iterator_to_array($iterator));
    }

    /**
     * Add characteristics
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     * @return Listing
     */
    public function addListingListingCharacteristicsOrderedByGroup(
        ListingListingCharacteristic $listingListingCharacteristic
    ) {
        return $this->addListingListingCharacteristic($listingListingCharacteristic);
    }


    /**
     * Remove characteristics
     *
     * @param \DW\CoreBundle\Entity\ListingListingCharacteristic $listingListingCharacteristic
     */
    public function removeListingListingCharacteristicsOrderedByGroup(
        ListingListingCharacteristic $listingListingCharacteristic
    ) {
        $this->removeListingListingCharacteristic($listingListingCharacteristic);
    }


    /**
     * Add category
     *
     * @param  \DW\CoreBundle\Entity\ListingListingCategory $listingListingCategory
     * @return Listing
     */
    public function addListingListingCategory(ListingListingCategory $listingListingCategory)
    {
        $listingListingCategory->setListing($this);
        $this->listingListingCategories[] = $listingListingCategory;

        return $this;
    }


    /**
     * Remove category
     *
     * @param \DW\CoreBundle\Entity\ListingListingCategory $listingListingCategory
     */
    public function removeListingListingCategory(ListingListingCategory $listingListingCategory)
    {
//        foreach ($listingListingCategory->getValues() as $value) {
//            $listingListingCategory->removeValue($value);
//        }

        $this->listingListingCategories->removeElement($listingListingCategory);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection|ListingListingCategory[]
     */
    public function getListingListingCategories()
    {
        return $this->listingListingCategories;
    }


    /**
     * Set user
     *
     * @param  \DW\UserBundle\Entity\User $user
     * @return Listing
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DW\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set location
     *
     * @param  \DW\CoreBundle\Entity\ListingLocation $location
     * @return Listing
     */
    public function setLocation(ListingLocation $location = null)
    {
        $this->location = $location;
        //Needed to persist listing_id on listing_location table when inserting a new listing embedding a listing location form
        $this->location->setListing($this);

        return $this;
    }

    /**
     * Get location
     *
     * @return \DW\CoreBundle\Entity\Listing
     *
     * Location
     */
    public function getLocation()
    {
        return $this->location;
    }


    /**
     * Add discount
     *
     * @param  \DW\CoreBundle\Entity\ListingDiscount $discount
     * @return Listing
     */
    public function addDiscount(ListingDiscount $discount)
    {
        $discount->setListing($this);
        $this->discounts[] = $discount;

        return $this;
    }

    /**
     * Remove discount
     *
     * @param \DW\CoreBundle\Entity\ListingDiscount $discount
     */
    public function removeDiscount(ListingDiscount $discount)
    {
        $this->discounts->removeElement($discount);
    }

    /**
     * Get discounts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @param ArrayCollection|ListingDiscount[] $discounts
     */
    public function setDiscounts(ArrayCollection $discounts)
    {
        foreach ($discounts as $discount) {
            $discount->setListing($this);
        }

        $this->discounts = $discounts;
    }

    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * @param ArrayCollection|Booking[] $bookings
     */
    public function setBookings(ArrayCollection $bookings)
    {
        foreach ($bookings as $booking) {
            $booking->setListing($this);
        }

        $this->bookings = $bookings;
    }

    /**
     * Add booking
     *
     * @param \DW\CoreBundle\Entity\Booking $booking
     *
     * @return Listing
     */
    public function addBooking(Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \DW\CoreBundle\Entity\Booking $booking
     */
    public function removeBooking(Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * @return mixed
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * @param ArrayCollection|Thread[] $threads
     */
    public function setThreads(ArrayCollection $threads)
    {
        foreach ($threads as $thread) {
            $thread->setListing($this);
        }

        $this->threads = $threads;
    }

    /**
     * Add thread
     *
     * @param \DW\MessageBundle\Entity\Thread $thread
     *
     * @return Listing
     */
    public function addThread(Thread $thread)
    {
        $this->threads[] = $thread;

        return $this;
    }

    /**
     * Remove thread
     *
     * @param \DW\MessageBundle\Entity\Thread $thread
     */
    public function removeThread(Thread $thread)
    {
        $this->threads->removeElement($thread);
    }

    /**
     * Add ListingOption
     *
     * @param  ListingOptionInterface $option
     * @return Listing
     */
    public function addOption($option)
    {
        $option->setListing($this);
        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove ListingOption
     *
     * @param ListingOptionInterface $option
     */
    public function removeOption($option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get ListingOptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param ArrayCollection $options
     * @return $this
     */
    public function setOptions(ArrayCollection $options)
    {
        foreach ($options as $option) {
            $option->setListing($this);
        }

        $this->options = $options;
    }


    /**
     * @param bool $strict
     *
     * @return array
     */
    public function getCompletionInformations($strict = true)
    {
        $characteristic = 0;
        foreach ($this->getListingListingCharacteristics() as $characteristics) {
            if ($characteristics->getListingCharacteristicValue()) {
                $characteristic = 1;
            }
        }

        return array(
            "title" => $this->getTitle() ? 1 : 0,
            "description" => (
                ($strict && $this->getDescription()) ||
                (!$strict && strlen($this->getDescription()) > 250)
            ) ? 1 : 0,
            "price" => $this->getPrice() ? 1 : 0,
            "characteristic" => $characteristic,
        );
    }

    /**
     * Set title
     *
     * @param  string $title
     * @return Listing
     */
    public function setTitle($title)
    {
        $this->title = ucfirst($title);

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return Listing
     */
    public function setDescription($description)
    {
        $this->description = $description;

//        if (in_array("phone", $typeFilter)) {
//            $pattern = "(0[0-9])?([-. ]?[0-9]{2}){4}";
//            //$pattern = "(([-. _|,;\!\$\?\!\#:|@]?){1,}[0-9]{2}){4,}";
//            //$pattern = "(([-. _|,;\/\!\$\?\!\#:|@]?){1,}[0-9]{2}){4,}";
//            //$pattern = "^0[1-68]([-. ]?[0-9]{2}){4}$";
//            //$sep = "((-|\.| |_|,|;|/){0,}[0-9]{2}){4,}";
//            $content = preg_replace("#$pattern#", $replaceBy, $content);
//
//            $pattern = "\+[0-9]{1}([-. ]?[0-9]){10}";
//            $content = preg_replace("#$pattern#", $replaceBy, $content);
//        }
//        if (in_array("email", $typeFilter)) {
//            $pattern = "[a-zA-Z0-9_.+-]+(@)[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+";
//            $content = preg_replace("#$pattern#", $replaceBy, $content);
//        }
//        if (in_array("address", $typeFilter)) {
//            //$pattern = "([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}";
//            $pattern = "([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+(com|fr|co|org|net|biz|tv|info)";
//            $content = preg_replace("#$pattern#", $replaceBy, $content);
//        }

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param  string $rules
     * @return Listing
     */
    public function setRules($rules)
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * Get rules
     *
     * @return string
     */
    public function getRules()
    {
        return $this->rules;
    }

    public function __toString()
    {
        return (string)$this->getTitle();
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;

            //Location
            $location = $this->getLocation();
            $this->setLocation(clone $location);

            //Characteristics
            $characteristics = $this->getListingListingCharacteristics();
            $this->listingListingCharacteristics = new ArrayCollection();
            foreach ($characteristics as $characteristic) {
                $characteristic = clone $characteristic;
                $characteristic->setListing($this);
                $this->addListingListingCharacteristic($characteristic);
            }

            //Categories
            $categories = $this->getListingListingCategories();
            $this->listingListingCategories = new ArrayCollection();
            foreach ($categories as $category) {
                $category = clone $category;
                $category->setListing($this);
                $this->addListingListingCategory($category);
            }

            //Discounts
            $discounts = $this->getDiscounts();
            $this->discounts = new ArrayCollection();
            foreach ($discounts as $discount) {
                $this->addDiscount(clone $discount);
            }

            //Options
            $options = $this->getOptions();
            if ($options) {
                $this->options = new ArrayCollection();
                foreach ($options as $option) {
                    $this->addOption(clone $option);
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param bool $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return string
     */
    public function getReasonInvalid()
    {
        return $this->reasonInvalid;
    }

    /**
     * @param string $reasonInvalid
     */
    public function setReasonInvalid($reasonInvalid)
    {
        $this->reasonInvalid = $reasonInvalid;
    }
}
