<?php

namespace DW\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PriceType extends AbstractType
{
    protected $pricePrecision;

    /**
     * @param int       $pricePrecision
     */
    public function __construct($pricePrecision)
    {
        $this->pricePrecision = $pricePrecision;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'translation_domain' => 'dw_listing',
                'scale' => $this->pricePrecision,
                'attr' => array(
                    'class' => 'numbers-only'
                ),
                'include_vat' => null //if true then incl tax is displayed else excl tax is displayed
            )
        );
    }


    /**
     * Pass the include_vat to the view
     *
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (array_key_exists('include_vat', $options) && $options["include_vat"] !== null) {
            // set an "include_vat" variable that will be available when rendering this field
            $view->vars['include_vat'] = $options["include_vat"];
        }
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'money';
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'price';
    }
}
