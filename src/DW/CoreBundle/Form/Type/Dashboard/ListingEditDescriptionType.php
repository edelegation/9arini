<?php

namespace DW\CoreBundle\Form\Type\Dashboard;

use DW\CoreBundle\Form\Type\LanguageFilteredType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingEditDescriptionType extends ListingEditType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'title',
                'text',
                array(
                    'label' => 'listing.form.title'
                )
            )
            ->add(
                'description',
                'textarea',
                array(
                    'label' => 'listing.form.description'
                )
            )
            ->add(
                'rules',
                'textarea',
                array(
                    'label' => 'listing.form.rules.en'
                )
            )
            ->add(
                'slug',
                'hidden'
            )
        ;

        //Status field already added
        //$builder->remove('status');
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_edit_description';
    }
}
