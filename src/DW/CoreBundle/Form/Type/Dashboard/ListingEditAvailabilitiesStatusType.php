<?php

namespace DW\CoreBundle\Form\Type\Dashboard;

use DW\CoreBundle\Document\ListingAvailability;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class ListingEditAvailabilitiesStatusType extends ListingEditAvailabilitiesType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add(
                'status',
                'choice',
                array(
                    'label' => 'listing.form.status',
                    'mapped' => false,
                    'choices' => array_flip(ListingAvailability::$visibleValues),
                    'choices_as_values' => true,
                    'block_name' => 'status'
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            array(
                'translation_domain' => 'dw_listing',
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_edit_availabilities_status';
    }
}
