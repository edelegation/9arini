<?php

namespace DW\CoreBundle\Form\Type\Dashboard;

use DW\CoreBundle\Entity\Booking;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookingEditType extends AbstractType
{
    public static $tacError = 'listing.form.tac.error';

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Booking $booking */
        //$booking = $builder->getData();
        $builder
            ->add(
                "tac",
                "checkbox",
                array(
                    'label' => 'booking.form.tac',
                    'mapped' => false,
                    'constraints' => new IsTrue(
                        array(
                            "message" => self::$tacError
                        )
                    ),
                )
            )
            ->add(
                'message',
                'textarea',
                array(
                    'mapped' => false,
                    'label' => 'booking.form.message',
                    'required' => true,
                    'constraints' => new NotBlank(),
                )
            );

    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\Booking',
                'csrf_token_id' => 'booking_edit',
                'translation_domain' => 'dw_booking',
                'cascade_validation' => true,
//                'validation_groups' => array('edit', 'default'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'booking_edit';
    }
}
