<?php

namespace DW\CoreBundle\Form\Type\Dashboard;

use DW\CoreBundle\Form\Type\ListingDiscountType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingEditDiscountType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'discounts',
                'collection',
                array(
                    'allow_delete' => true,
                    'allow_add' => true,
                    'type' => new ListingDiscountType(),
                    'by_reference' => false,
                    'prototype' => true,
                    /** @Ignore */
                    'label' => false,
                    'cascade_validation' => true,//Important to have error on collection item field!
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\Listing',
                'translation_domain' => 'dw_listing',
                'cascade_validation' => true,//To have error on collection item field
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_edit_discounts';
    }

}
