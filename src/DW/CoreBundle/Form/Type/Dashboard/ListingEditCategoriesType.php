<?php

namespace DW\CoreBundle\Form\Type\Dashboard;

use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Helper\EditCategoryHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingEditCategoriesType extends AbstractType
{
    private $request;
    private $entityManager;
    private $parentsCategoriesIDs;
    private $helper;

    /**
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     * @param EditCategoryHelper $helper
     * @param $parentsCategoriesIDs
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        EditCategoryHelper $helper,
        $parentsCategoriesIDs
    )
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->entityManager = $entityManager;
        $this->parentsCategoriesIDs = $parentsCategoriesIDs;
        $this->helper = $helper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Listing $listing */
        $listing = $builder->getData();
        $this->helper->extractCategories($listing);

        $listingCategoryRepository = $this->entityManager->getRepository("DWCoreBundle:ListingCategory");
        $categories = $listingCategoryRepository->findBy(['parent' => null]);

        $cats = [];
        foreach ($categories as $category){
            if (in_array($category->getId(), $this->parentsCategoriesIDs)) {
                $key = (string) array_search($category->getId(), $this->parentsCategoriesIDs);
                $cats[$key] = $category;
            }
        }

        $levels = array('level' => $cats['level']->getChildren()->toArray());
        foreach($cats['system']->getChildren()->toArray() as $system) {
            if(count($system->getChildren()->toArray())) {
                $levels[$system->getName()] = $system->getChildren()->toArray();
            } else {
                $levels[] = $system;
            }
        }

        $builder
            ->add(
                'type',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => $cats['type']->getChildren()->toArray(),
                    'data' => $this->helper->getType(),
                    'multiple' => false,
                    'choice_attr' => function ($choice, $currentChoiceKey) {
                        if ($choice->getId() == $this->parentsCategoriesIDs['tutoring_subject_type']) {
                            return array('class' => 'tutoring');
                        }
                        return array('class' => 'training');
                    },
                    'mapped' => false
                )
            )
            ->add(
                'subject',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => array(
                        'tutoring_subject' => $cats['tutoring_subject']->getChildren()->toArray(),
                        'training_subject' => $cats['training_subject']->getChildren()->toArray()
                    ),
                    'empty_data' => null,
                    'data' => $this->helper->getSubject(),
                    'multiple' => false,
                    'mapped' => false
                )
            )
            ->add(
                'level',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => $levels,
                    'empty_value' => 'Veuillez choisir le niveau',
                    'empty_data' => null,
                    'data' => $this->helper->getLevels(),
                    'multiple' => true,
                    'mapped' => false
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_edit_categories';
    }

}