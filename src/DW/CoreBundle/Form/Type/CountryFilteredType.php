<?php


namespace DW\CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CountryType as BaseCountryType;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CountryFilteredType extends BaseCountryType
{
    private $country;

    /**
     * @param string $country
     */
    public function __construct($country)
    {
        $this->country = $country;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $country = Intl::getRegionBundle()->getCountryName($this->country);

        $resolver->setDefaults(
            array(
                'choices' => array($country => $this->country),
                'required' => true,
                'empty_data' => $this->country,
                'data' => $this->country,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'hidden';
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'country_filtered';
    }
}
