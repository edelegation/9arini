<?php

namespace DW\CoreBundle\Form\Type;

use DW\CoreBundle\Form\DataTransformer\TimeRangeViewTransformer;
use DW\CoreBundle\Model\DateRange;
use DW\CoreBundle\Model\TimeRange;
use DW\CoreBundle\Validator\TimeRangeValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

//todo: Fix SF2.8 depreciated : use 'choices_as_values' => true and flip choices keys values
class TimeRangeType extends AbstractType
{
    protected $timeUnit;
    protected $timeUnitIsDay;
    protected $timesMax;
    protected $timePicker;

    /**
     * @param int  $timeUnit in minute
     * @param int  $timesMax
     * @param bool $timePicker
     */
    public function __construct($timeUnit, $timesMax, $timePicker)
    {
        $this->timeUnit = $timeUnit;
        $this->timeUnitIsDay = ($timeUnit % 1440 == 0) ? true : false;
        $this->timesMax = $timesMax;
        $this->timePicker = $timePicker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->timeUnitIsDay) {
            throw new \Exception("Time ranges are only available for time unit not in day mode");
        }
        $date = new \DateTime();
        $date->setTime(8, 00);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options,$date) {
                $form = $event->getForm();
                $options['start_options']['data'] = (array_key_exists('data',$options['start_options']) && ($options['start_options']['data'] instanceof \DateTime))
                    ? $options['start_options']['data']
                    : $date;

                $options['end_options']['data'] = (array_key_exists('data',$options['end_options']) && ($options['end_options']['data'] instanceof \DateTime))
                    ? $options['end_options']['data']
                    : $date;

                $form
                    ->add(
                        'start',
                        'time',
                        array_merge(
                            array(
                                'label' => 'time_range.start',
                                'property_path' => 'start',
                                'empty_value' => '',
                                'widget' => 'choice',
                                'input' => 'datetime',
                                'model_timezone' => 'UTC',
                                'view_timezone' => 'UTC',
                                'attr' => array(
                                    'data-type' => 'start',
                                ),
                            ),
                            $options['start_options']
                        )
                    )->add(
                        'end',
                        'time',
                        array_merge(
                            array(
                                'label' => 'time_range.end',
                                'property_path' => 'end',
                                'empty_value' => '',
                                'widget' => 'choice',
                                'input' => 'datetime',
                                'model_timezone' => 'UTC',
                                'view_timezone' => 'UTC',
                                'attr' => array(
                                    'data-type' => 'end',
                                ),
                            ),
                            $options['end_options']
                        )
                    );


                //TimePicker
                if ($this->timePicker) {
                    $form
                        ->add(
                            'start_picker',
                            'text',
                            array(
                                'mapped' => false,
                                //'widget' => 'single_text',
                                /** @Ignore */
                                'label' => false,
                                'data' => $options['start_options']['data']->format('H:i')
                            )

                        )
                        ->add(
                            'end_picker',
                            'text',
                            array(
                                'mapped' => false,
                                'read_only' => false,
                                //'widget' => 'single_text',
                                /** @Ignore */
                                'label' => false,
                                'data' => $options['end_options']['data']->format('H:i')
                            )
                        );
                }

                $minHour = 1;
                if (array_key_exists('min_hour',$options) && !is_null($options['min_hour'])) {
                    $minHour = $options['min_hour'];
                }
                if (array_key_exists('max_hour',$options) && !is_null($options['max_hour'])) {
                    $this->timesMax = $options['max_hour'];
                }

                //Times display mode: range or duration
                if ($options['display_mode'] == "duration") {
                    if ($this->timesMax > 1) { //Create times unit choice list limited to timesMax
                        $nbMinutes = null;
                        if (isset($options['start_options']['data']) && isset($options['end_options']['data'])) {
                            $timeRange = new TimeRange(
                                $options['start_options']['data'],
                                $options['end_options']['data']
                            );
                            $nbMinutes = $timeRange->getDuration($this->timeUnit) * $this->timeUnit;
                        }
                        $nbMinutes = 60;

                        /** @var DateRange $dateRange */
                        $form
                            ->add(
                                'nb_minutes',
                                'choice',
                                array(
                                    //from one time unit to timesMax * timeUnit
                                    'choices' => array_combine(
                                        range($minHour, $this->timesMax),
                                        range($this->timeUnit * $minHour, $this->timesMax * $this->timeUnit, $this->timeUnit)
                                    ),
                                    'data' => $nbMinutes * $minHour,
                                    /** @Ignore */
                                    'empty_value' => '',
                                    'attr' => array(
                                        'class' => 'no-scroll no-arrow'
                                    ),
                                    'choices_as_values' => true
                                )
                            );
                    } else {//One time unit. $this->timesMax = 1
                        $form
                            ->add(
                                'nb_minutes',
                                'hidden',
                                array(
                                    'data' => $this->timeUnit
                                )
                            );
                    }
                }
            }
        );

        $builder->addViewTransformer($options['transformer']);
        $builder->addEventSubscriber($options['validator']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Model\TimeRange',
                'end_options' => array(),
                'start_options' => array(),
                'transformer' => null,
                'validator' => null,
                'translation_domain' => 'dw_listing',
                'display_mode' => 'range',
                'min_hour' => '',
                'max_hour' => ''
            )
        );

        $resolver->setAllowedTypes(
            array(
                'transformer' => array('Symfony\Component\Form\DataTransformerInterface', 'null'),
                'validator' => array('Symfony\Component\EventDispatcher\EventSubscriberInterface', 'null'),
            )
        );

        // Those normalizers lazily create the required objects, if none given.
        $resolver->setNormalizer(
            'transformer',
            function (Options $options, $value) {
                if (!$value) {
                    $value = new TimeRangeViewTransformer(new OptionsResolver());
                }

                return $value;
            }
        );

        $resolver->setNormalizer(
            'validator',
            function (Options $options, $value) {
                if (!$value) {
                    $value = new TimeRangeValidator(
                        new OptionsResolver(), array(
                            'required' => $options["required"]
                        )
                    );
                }

                return $value;
            }
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'time_range';
    }
}
