<?php

namespace DW\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class PriceRangeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'min',
                'price',
                array(
                    'label' => 'listing.form.price',
                    'scale' => 0
                )
            )
            ->add(
                'max',
                'price',
                array(
                    /** @Ignore */
                    'label' => false,
                    'scale' => 0
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'translation_domain' => 'dw_listing',
                'data_class' => 'DW\CoreBundle\Model\PriceRange',
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'price_range';
    }
}
