<?php

namespace DW\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekDaysType extends AbstractType
{

    public function __construct()
    {

    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'mapped' => false,
                'choices' => array(
                    'dw.monday' => '1',
                    'dw.tuesday' => '2',
                    'dw.wednesday' => '3',
                    'dw.thursday' => '4',
                    'dw.friday' => '5',
                    'dw.saturday' => '6',
                    'dw.sunday' => '7',
                ),
                'translation_domain' => 'dw',
                'multiple' => true,
                'expanded' => true,
                /** @Ignore */
                'label' => false,
                'data' => array('1', '2', '3', '4', '5', '6', '7'),
                'choices_as_values' => true
            )
        );
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return 'choice';
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'weekdays';
    }
}
