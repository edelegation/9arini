<?php

namespace DW\CoreBundle\Form\Type;

use Buzz\Exception\InvalidArgumentException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Model\ListingSearchRequest;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Doctrine\Common\Collections\Criteria;

class ListingCategoryType extends AbstractType
{

    private $request;
    private $entityManager;
    private $parentsCategoriesIDs;
    /**
     * @var RequestStack
     */
    private $searchRequest;
    private $requestStack;
    private $route;

    /**
     * ListingCategoryType constructor.
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     * @param ListingSearchRequest $searchRequest
     * @param $parentsCategoriesIDs
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        ListingSearchRequest $searchRequest,
        $parentsCategoriesIDs
    )
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->searchRequest = $searchRequest;
        $this->entityManager = $entityManager;
        $this->parentsCategoriesIDs = $parentsCategoriesIDs;
        $this->route = $this->request->attributes->get('_route') == 'dw_listing_search_result';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $searchRequest = isset($options['search_request']) ? $options['search_request'] : $this->searchRequest;

        $listingCategoryRepository = $this->entityManager->getRepository("DWCoreBundle:ListingCategory");
        $categories = $listingCategoryRepository->findBy(['parent' => null]);

        $cats = [];
        foreach ($categories as $category){
            if (in_array($category->getId(), $this->parentsCategoriesIDs)) {
                $key = (string) array_search($category->getId(), $this->parentsCategoriesIDs);
                $cats[$key] = $category;
            }
        }

        $levels = array('level' => $cats['level']->getChildren()->toArray());
        foreach($cats['system']->getChildren()->toArray() as $system) {
            if(count($system->getChildren()->toArray())) {
                $levels[$system->getName()] = $system->getChildren()->toArray();
            } else {
                $levels[] = $system;
            }
        }

        $builder
            ->add(
                'type',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => $cats['type']->getChildren()->toArray(),
                    'data' => $searchRequest->getType()
                        ? $listingCategoryRepository->find($searchRequest->getType())
                        : $searchRequest->getType(),
                    'multiple' => false,
                    'choice_attr' => function ($choice) {
                        if ($choice->getId() == $this->parentsCategoriesIDs['tutoring_subject_type']) {
                            return array('class' => 'tutoring');
                        }
                        return array('class' => 'training');
                    },
                    'empty_value' => !$this->requestStack->getParentRequest() ? '' : false,
                    'empty_data' => null,
                    'required' => true,
                    'constraints' => $this->route ? [] : [new NotNull(['message' => 'Requis'])]

                )
            )
            ->add(
                'subject',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => array(
                        'tutoring_subject' => $this->getSorted($cats['tutoring_subject']->getChildren(),'name', 'ASC')->toArray(),
                        'training_subject' => $this->getSorted($cats['training_subject']->getChildren(),'name', 'ASC')->toArray()
                    ),
                    'empty_value' => !$this->requestStack->getParentRequest() ? '' : 'Toutes les disciplines',
                    'empty_data' => null,
                    'data' => $searchRequest->getSubject()
                            ? $listingCategoryRepository->find($searchRequest->getSubject())
                            : $searchRequest->getSubject(),
                    'multiple' => false,
                    'required' => true,
                    'constraints' => $this->route ? [] : [new NotNull(['message' => 'Requis'])]
                )
            )
            ->add(
                'level',
                EntityType::class,
                array(
                    'class' => "DWCoreBundle:ListingCategory",
                    'choices' => $levels,
                    'empty_value' => 'Tous les niveaux',
                    'empty_data' => null,
                    'block_name' => 'level',
                    'data' => $searchRequest->getLevels()
                        ? $listingCategoryRepository->findBy([
                                'id' => $searchRequest->getLevels()
                            ])
                        : $searchRequest->getLevels(),
                    'multiple' => true,
                    'required' => true
                )
            );

        $builder->addEventListener(FormEvents::POST_SUBMIT,function(FormEvent $event) use ($searchRequest){
            $form = $event->getForm();
            $data = $event->getData();

            $level = $data['level'];
            if (!count($level) && !$this->route) {
                $form->get('level')->addError(new FormError('Requis'));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver
            ->setDefaults(
                array(
                    'class' => 'DW\CoreBundle\Entity\ListingCategory',
                    'required' => false,
                    /** @Ignore */
                    'label' => false
                )
            );

        $resolver->setDefined(
            array(
                'search_request'
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_category';
    }

    public function getSorted($collection, $field, $sort = 'ASC')
    {
        $criteria = Criteria::create()
            ->orderBy(array($field => $sort));

        return $collection->matching($criteria);
    }
}
