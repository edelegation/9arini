<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Model\ListingLocationSearchRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ListingLocationSearchType extends AbstractType
{

    public static $locationCountryError = 'listing.search.form.country.not_blank';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'address',
                'text',
                array(
                    'label' => 'listing.search.form.address'
                )
            )
            ->add('lat', 'hidden')
            ->add('lng', 'hidden')
            ->add('viewport', 'hidden')
            ->add(
                'country',
                'hidden',
                array(
                    'constraints' => array(
                        new NotBlank(
                            array(
                                "message" => self::$locationCountryError
                            )
                        ),
                    ),
                )
            )
            ->add('area', 'hidden')
            ->add('department', 'hidden')
            ->add('city', 'hidden')
            ->add('zip', 'hidden')
            ->add('route', 'hidden')
            ->add('streetNumber', 'hidden')
            ->add('addressType', 'hidden');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
                'data_class' => 'DW\CoreBundle\Model\ListingLocationSearchRequest',
                'translation_domain' => 'dw_listing',
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_location_search';
    }
}
