<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Entity\ListingLocation;
use DW\CoreBundle\Form\Type\EntityHiddenType;
use DW\CoreBundle\Form\Type\ImageType;
use DW\CoreBundle\Form\Type\ListingCategoryType;
use DW\CoreBundle\Form\Type\PriceType;
use DW\UserBundle\Entity\User;
use DW\UserBundle\Entity\UserImage;
use DW\UserBundle\Form\Type\UserImageType;
use DW\UserBundle\Security\LoginManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class ListingNewType
 * Categories are created trough ajax in ListingNewCategoriesType
 *
 * @package DW\CoreBundle\Form\Type\Frontend
 */
class ListingNewType extends AbstractType
{
    public static $tacError = 'listing.form.tac.error';
    public static $credentialError = 'user.form.credential.error';

    private $securityTokenStorage;
    private $securityAuthChecker;
    private $loginManager;
    private $request;
    /** @var User|null */
    private $user = null;
    protected $dispatcher;


    /**
     * @var array uploaded files
     */
    protected $uploaded;

    /**
     * @param TokenStorage             $securityTokenStorage
     * @param AuthorizationChecker     $securityAuthChecker
     * @param LoginManager             $loginManager
     * @param RequestStack             $requestStack
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        TokenStorage $securityTokenStorage,
        AuthorizationChecker $securityAuthChecker,
        LoginManager $loginManager,
        RequestStack $requestStack,
        EventDispatcherInterface $dispatcher
    ) {
        $this->securityTokenStorage = $securityTokenStorage;
        $this->securityAuthChecker = $securityAuthChecker;
        $this->loginManager = $loginManager;
        $this->request = $requestStack->getCurrentRequest();
        if ($this->securityAuthChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->user = $this->securityTokenStorage->getToken()->getUser();
        }
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->user && is_null($this->user->getImage())){
            $builder
                ->add(
                    'image',
                    new UserImageType(true),
                    ['mapped' => false]
                );
        }

        $builder
            ->add(
                'categories',
                ListingCategoryType::class,
                ['mapped' => false]
            )
            ->add(
                'title',
                TextType::class,
                array(
                    'label' => 'listing.form.title'
                )
            )
            ->add(
                'description',
                TextareaType::class,
                array(
                    'label' => 'listing.form.description'
                )
            )
            ->add(
                'slug',
                HiddenType::class
            )
            ->add(
                'price',
                PriceType::class,
                array(
                    'label' => 'listing.form.price',
                    'currency' => 'TND'
                )
            );

        // Default listing location
        $listingLocation = null;
        if ($this->user) {
            if ($this->user->getListings()->count()) {
                /** @var Listing $listing */
                $listing = $this->user->getListings()->first();
                /** @var ListingLocation $location */
                $location = $listing->getLocation();


                $listingLocation = new ListingLocation();
                $listingLocation->setListing($listing);
                $listingLocation->setCountry($location->getCountry());
                $listingLocation->setCity($location->getCity());
                $listingLocation->setRegion($location->getRegion());
                $listingLocation->setAddress($location->getAddress());

            }
        }


        //var_dump($listingLocation);die;
        $builder
            ->add(
                'location',
                new ListingLocationType(),
                array(
                    'data_class' => 'DW\CoreBundle\Entity\ListingLocation',
                    /** @Ignore */
                    'label' => false,
                    'data' => $listingLocation
                )
            );

        $builder
            ->add(
                "tac",
                CheckboxType::class,
                array(
                    'label' => 'listing.form.tac',
                    'mapped' => false,
                    'constraints' => new IsTrue(
                        array(
                            "message" => self::$tacError
                        )
                    ),
                    'attr' => ['single' => true]
                )
            );

        $builder->add(
            'user',
            EntityHiddenType::class,
            array(
                'data' => $this->user,
                'class' => 'DW\UserBundle\Entity\User',
                'data_class' => null
            )
        );

    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\Listing',
                'csrf_token_id' => 'listing_new',
                'translation_domain' => 'dw_listing',
                //'cascade_validation' => true,
                //'validation_groups' => array('Listing'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_new';
    }
}
