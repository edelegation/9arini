<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Event\ListingFormBuilderEvent;
use DW\CoreBundle\Event\ListingFormEvents;
use DW\CoreBundle\Form\DataTransformer\ListingListingCategoriesToListingCategoriesTransformer;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingNewCategoriesType extends AbstractType
{
    protected $dispatcher;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $listing = $builder->getData();

        $builder
            ->add(
                'listingListingCategories',
                'listing_category'
            );

        $builder
            ->get('listingListingCategories')
            ->addModelTransformer(new ListingListingCategoriesToListingCategoriesTransformer($listing));


        //Dispatch LISTING_NEW_CATEGORIES_FORM_BUILD Event. Listener listening this event can add fields and validation
        //Used for example to add fields to categories
        $this->dispatcher->dispatch(
            ListingFormEvents::LISTING_NEW_CATEGORIES_FORM_BUILD,
            new ListingFormBuilderEvent($builder)
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\Listing',
                'csrf_token_id' => 'listing_new_categories',
                'translation_domain' => 'dw_listing',
                'cascade_validation' => false,//To have error on collection item field,
                'validation_groups' => false,//To not have listing validation errors when categories are only edited
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_new_categories';
    }

}
