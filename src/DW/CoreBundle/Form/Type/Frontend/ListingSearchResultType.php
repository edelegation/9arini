<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Form\Type\ListingCategoryType;
use DW\CoreBundle\Form\Type\ListingCharacteristicType;
use DW\CoreBundle\Form\Type\PriceRangeType;
use DW\CoreBundle\Model\ListingSearchRequest;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingSearchResultType extends AbstractType
{

    protected $session;
    protected $entityManager;
    protected $request;
    protected $locale;
    protected $timeUnit;
    protected $timeUnitFlexibility;
    protected $timeUnitIsDay;
    protected $allowSingleDay;
    protected $endDayIncluded;
    protected $daysDisplayMode;
    protected $timesDisplayMode;
    protected $minStartDelay;
    protected $dispatcher;

    /**
     * @param Session                  $session
     * @param EntityManager            $entityManager
     * @param RequestStack             $requestStack
     * @param int                      $timeUnit
     * @param boolean                  $timeUnitFlexibility
     * @param boolean                  $allowSingleDay
     * @param boolean                  $endDayIncluded
     * @param string                   $daysDisplayMode
     * @param string                   $timesDisplayMode
     * @param int                      $minStartDelay
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        Session $session,
        EntityManager $entityManager,
        RequestStack $requestStack,
        $timeUnit,
        $timeUnitFlexibility,
        $allowSingleDay,
        $endDayIncluded,
        $daysDisplayMode,
        $timesDisplayMode,
        $minStartDelay,
        EventDispatcherInterface $dispatcher
    ) {
        $this->session = $session;
        $this->entityManager = $entityManager;
        $this->request = $requestStack->getCurrentRequest();
        $this->locale = $this->request->getLocale();
        $this->timeUnit = $timeUnit;
        $this->timeUnitFlexibility = $timeUnitFlexibility;
        $this->timeUnitIsDay = ($timeUnit % 1440 == 0) ? true : false;
        $this->allowSingleDay = $allowSingleDay;
        $this->endDayIncluded = $endDayIncluded;
        $this->daysDisplayMode = $daysDisplayMode;
        $this->timesDisplayMode = $timesDisplayMode;
        $this->minStartDelay = $minStartDelay;
        $this->dispatcher = $dispatcher;;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ListingSearchRequest $listingSearchRequest */
        $listingSearchRequest = $builder->getData();

        $builder
            ->add(
                'location',
                new ListingLocationSearchType()
            )
            ->add(
                'categories',
                ListingCategoryType::class,
                [
                    'label' => 'listing_search.form.categories',
                    'mapped' => false,
                    'search_request' => $listingSearchRequest
                ]
            )
            ->add(
                'page',
                HiddenType::class
            )
            ->add(
                'price_range',
                new PriceRangeType(),
                [
                    /** @Ignore */
                    'label' => false
                ]
            );

        //CHARACTERISTICS
        $characteristics = $listingSearchRequest->getCharacteristics();
        $builder
            ->add(
                'characteristics',
                ListingCharacteristicType::class,
                [
                    'mapped' => false,
                    'data' => $characteristics
                ]
            )
            ->add(
                'sort_by',
                ChoiceType::class,
                [
                    'choices' => array_flip(ListingSearchRequest::$sortByValues),
                    'empty_value' => 'listing_search.form.sort_by.empty_value',
                    'choices_as_values' => true
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(
            [
                'csrf_protection' => false,
                'data_class' => 'DW\CoreBundle\Model\ListingSearchRequest',
                'translation_domain' => 'dw_listing',
            ]
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_search_result';
    }
}