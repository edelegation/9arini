<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Entity\ListingLocation;
use DW\CoreBundle\Entity\Region;
use DW\CoreBundle\Form\Type\CountryFilteredType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ListingLocationType
 */
class ListingLocationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'region',
                EntityType::class,
                array(
                    'class' => 'DW\CoreBundle\Entity\Region',
                    'empty_data' => null,
                    'empty_value' => '',
                    'label' => 'Region',
                    'translation_domain' => 'dw_listing',
                    'required' => true,
                )
            )
            ->add(
                'city',
                EntityType::class,
                array(
                    'class' => 'DW\CoreBundle\Entity\City',
                    'empty_data' => null,
                    'empty_value' => '',
                    'label' => 'listing.form.location.city',
                    'translation_domain' => 'dw_listing',
                    'required' => true,
                    'choice_attr' => function ($choice) {
                            return array('class' => 'ville ville-region-'.$choice->getRegion()->getId());
                    }
                )
            )
            ->add(
                'address',
                TextType::class,
                array(
                    'label' => 'Adresse',
                    'translation_domain' => 'dw_listing',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Veuillez entrer l\'adresse'
                    ]
                )
            )

            //This field contains geocoding information in JSON format.
            //Its value is transformed to Coordinate entity through data transformer
            ->add(
                'coordinate',
                'geocoding_to_coordinate'
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(
            array(
                'translation_domain' => 'dw_listing',
                'data_class' => 'DW\CoreBundle\Entity\ListingLocation',
                'cascade_validation' => true,
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_location';
    }
}
