<?php

namespace DW\CoreBundle\Form\Type\Frontend;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Event\BookingFormBuilderEvent;
use DW\CoreBundle\Event\BookingFormEvents;
use DW\CoreBundle\Model\Manager\BookingManager;
use DW\UserBundle\Entity\User;
use DW\UserBundle\Security\LoginManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraints\IsTrue;

class BookingNewType extends AbstractType
{
    public static $tacError = 'booking.form.tac.error';
    public static $messageError = 'booking.form.message.error';
    public static $unavailableError = 'booking.new.error.unavailable';
    public static $amountError = 'booking.new.error.amount_invalid {{ min_price }}';
    public static $voucherError = 'booking.new.error.voucher';
    public static $credentialError = 'user.form.credential.error';
    public static $messageDeliveryInvalid = 'booking.new.delivery.error';
    public static $messageDeliveryMaxInvalid = 'booking.new.delivery_max.error';

    private $bookingManager;
    private $loginManager;
    private $securityTokenStorage;
    private $securityAuthChecker;
    private $request;
    private $dispatcher;
    protected $allowSingleDay;
    protected $endDayInclude;
    protected $minStartDelay;
    protected $minStartTimeDelay;
    private $endDayIncluded;

    /**
     * @param BookingManager       $bookingManager
     * @param TokenStorage         $securityTokenStorage
     * @param AuthorizationChecker $securityAuthChecker
     * @param LoginManager             $loginManager
     * @param RequestStack             $requestStack
     * @param EventDispatcherInterface $dispatcher
     * @param bool                     $allowSingleDay
     * @param bool                     $endDayIncluded
     * @param int                      $minStartDelay
     * @param int                      $minStartTimeDelay
     */
    public function __construct(
        BookingManager $bookingManager,
        TokenStorage $securityTokenStorage,
        AuthorizationChecker $securityAuthChecker,
        LoginManager $loginManager,
        RequestStack $requestStack,
        EventDispatcherInterface $dispatcher,
        $allowSingleDay,
        $endDayIncluded,
        $minStartDelay,
        $minStartTimeDelay
    ) {
        $this->bookingManager = $bookingManager;
        $this->securityTokenStorage = $securityTokenStorage;
        $this->securityAuthChecker = $securityAuthChecker;
        $this->loginManager = $loginManager;
        $this->request = $requestStack->getCurrentRequest();
        $this->dispatcher = $dispatcher;
        $this->allowSingleDay = $allowSingleDay;
        $this->endDayIncluded = $endDayIncluded;
        $this->minStartDelay = $minStartDelay;
        $this->minStartTimeDelay = $minStartTimeDelay;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Booking $booking */
        $booking = $builder->getData();

        $builder
            ->add(
                'date_range',
                'date_range',
                array(
                    'mapped' => false,
                    /** @Ignore */
                    'label' => false,
                    'required' => true,
                    'start_options' => array(
                        'label' => 'booking.form.start',
                        'mapped' => true,
                        'data' => $booking->getStart()
                    ),
                    'end_options' => array(
                        'label' => 'booking.form.end',
                        'mapped' => true,
                        'data' => $booking->getEnd()
                    ),
                    'allow_single_day' => $this->allowSingleDay,
                    'end_day_included' => $this->endDayIncluded,
                    'error_bubbling' => false
                )
            )
            ->add(
                'tac',
                'checkbox',
                array(
                    'label' => 'listing.form.tac',
                    'mapped' => false,
                    'constraints' => new IsTrue(
                        array(
                            "message" => self::$tacError
                        )
                    ),
                )
            );

        if (!$this->bookingManager->getTimeUnitIsDay()) {
            //All date and time fields are hidden in this form
            $builder->add(
                'time_range',
                'time_range',
                array(
                    'mapped' => false,
                    'start_options' => array(
                        'mapped' => true,
                        'data' => $booking->getStartTime()
                    ),
                    'end_options' => array(
                        'mapped' => true,
                        'data' => $booking->getEndTime()
                    ),
                    'required' => true,
                    /** @Ignore */
                    'label' => false,
                    'block_name' => 'time_range_hidden',
                )
            );
        }

        if ($this->bookingManager->voucherIsEnabled()) {
            $builder
                ->add(
                    'voucher',
                    'voucher',
                    array(
                        'translation_domain' => 'dw_voucher',
                    )
                );
        }

        if ($this->bookingManager->optionIsEnabled()) {
            $builder
                ->add(
                    'options',
                    'collection',
                    array(
                        'allow_delete' => false,
                        'allow_add' => false,
                        'type' => 'booking_option',
                        'by_reference' => false,
                        'prototype' => false,
                        /** @Ignore */
                        'label' => false,
                        'cascade_validation' => true,//Important to have error on collection item field!
                        'translation_domain' => 'dw_listing_option',
                    )
                );

            //Add new Listing Options eventually not already attached to booking
            $builder->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    /** @var Booking $booking */
                    $booking = $event->getData();

                    $booking = $this->bookingManager->setBookingOptions($booking);

                    $event->setData($booking);
                }
            );
        }

        /**
         * Message type
         */
        $builder
            ->add(
                'message',
                'textarea',
                array(
                    'attr' => array('placeholder' => 'Rédiger votre message de demande à l\'attention de l\'offreur'),
                    'label' => 'booking.form.message',
                    'required' => true
                )
            );

        //Dispatch BOOKING_NEW_FORM_BUILD Event. Listener listening this event can add fields and validation
        //Used for example by some payment provider bundle like mangopay
        $this->dispatcher->dispatch(BookingFormEvents::BOOKING_NEW_FORM_BUILD, new BookingFormBuilderEvent($builder));


        $builder
            ->add(
                'user',
                'entity_hidden',
                array(
                    'data' => $this->securityTokenStorage->getToken()->getUser(),
                    'class' => 'DW\UserBundle\Entity\User',
                    'data_class' => null
            ));

        /**
         * Add errors to the form if any
         *
         * @param FormInterface $form
         * @param               $errors
         */
        $formErrors = function (FormInterface $form, $errors) {
            $keys = array_keys($errors, 'date_range.invalid.min_start');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $now = new \DateTime();
                if ($this->minStartDelay > 0) {
                    $now->add(new \DateInterval('P' . $this->minStartDelay . 'D'));
                }
                $form['date_range']->addError(
                    new FormError(
                        'date_range.invalid.min_start {{ min_start_day }}',
                        'dw',
                        array(
                            '{{ min_start_day }}' => $now->format('d/m/Y'),
                        )
                    )
                );
            }

            $keys = array_keys($errors, 'time_range.invalid.min_start');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $now = new \DateTime();
                if ($this->minStartTimeDelay > 0) {
                    $now->add(new \DateInterval('PT' . $this->minStartTimeDelay . 'H'));
                }
                $form['date_range']->addError(
                    new FormError(
                        'time_range.invalid.min_start {{ min_start_time }}',
                        'dw',
                        array(
                            '{{ min_start_time }}' => $now->format('d/m/Y H:i'),
                        )
                    )
                );
            }

            $keys = array_keys($errors, 'unavailable');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $form['date_range']->addError(
                    new FormError(self::$unavailableError)
                );
            }

            $keys = array_keys($errors, 'amount_invalid');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $form['date_range']->addError(
                    new FormError(
                        self::$amountError,
                        'dw',
                        array(
                            '{{ min_price }}' => $this->bookingManager->minPrice
                        )
                    )
                );
            }

            $keys = array_keys($errors, 'code_voucher_invalid');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $form['codeVoucher']->addError(
                    new FormError(
                        self::$voucherError,
                        'dw_booking',
                        array()
                    )
                );
            }

            $keys = array_keys($errors, 'delivery_invalid');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $form['deliveryAddress']->addError(
                    new FormError(
                        self::$messageDeliveryInvalid,
                        'dw_booking',
                        array()
                    )
                );
            }

            $keys = array_keys($errors, 'delivery_max_invalid');
            if (count($keys)) {
                foreach ($keys as $key) {
                    unset($errors[$key]);
                }
                $form['deliveryAddress']->addError(
                    new FormError(
                        self::$messageDeliveryMaxInvalid,
                        'dw_booking',
                        array()
                    )
                );
            }

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $form['date_range']->addError(
                        new FormError($error)
                    );
                }
            }

        };


        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formErrors) {
                $form = $event->getForm();

                //todo: check if needed and not already made in BookingValidator class
                //Set Booking Amounts or throw Error if booking is invalid
                /** @var Booking $booking */
                $booking = $event->getData();
                $errors = $this->bookingManager->checkBookingAvailabilityAndSetAmounts($booking);

                if (!count($errors)) {
                    $event->setData($booking);
                } else {
                    $formErrors($form, $errors);
                }
            }
        );


        $builder->addEventListener(
            FormEvents::SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();

                $tac = $form->get('tac')->getData();
                if (empty($tac)) {
                    $form['tac']->addError(
                        new FormError(self::$tacError)
                    );
                }

                $message = $form->get('message')->getData();
                if (empty($message)) {
                    $form['message']->addError(
                        new FormError(self::$messageError)
                    );
                }
            }
        );


    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\Booking',
                'intention' => 'booking_new',
                'translation_domain' => 'dw_booking',
                'cascade_validation' => true,
                'validation_groups' => array('new', 'default'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'booking_new';
    }
}
