<?php

namespace DW\CoreBundle\Form\Type;

use DW\CoreBundle\Entity\ListingListingCharacteristic;
use DW\CoreBundle\Repository\ListingCharacteristicValueRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListingListingCharacteristicType extends AbstractType
{
    protected $locale;

    /**
     * @param   $locale
     */
    public function __construct($locale)
    {
        $this->locale = $locale;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $form = $event->getForm();
                /** @var ListingListingCharacteristic $llc */
                $llc = $event->getData();

                $form->add(
                    'listingCharacteristicValue',
                    'entity',
                    array(
                        'query_builder' => function (ListingCharacteristicValueRepository $lcvr) use ($llc) {
                            $lct = $llc->getListingCharacteristic()->getListingCharacteristicType();

                            return $lcvr->getFindAllTranslatedQueryBuilder(
                                $lct
                            );
                        },
                        'empty_value' => 'listing.form.characteristic.choose',
                        'property' => 'name',
                        'class' => 'DW\CoreBundle\Entity\ListingCharacteristicValue',
                        'required' => true
                    )
                );
            }
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\CoreBundle\Entity\ListingListingCharacteristic',
                'translation_domain' => 'dw_listing',
                'required' => false
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'listing_listing_characteristic';
    }
}
