<?php

namespace DW\CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\DateType;


class DateHiddenType extends DateType
{
    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'date_hidden';
    }

    public function getParent()
    {
        return 'hidden';
    }
}
