<?php

namespace DW\CoreBundle\Form\Handler\Frontend;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Model\Manager\ListingManager;
use DW\UserBundle\Form\Handler\RegistrationFormHandler;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handle Listing Form
 *
 */
class ListingFormHandler
{
    protected $request;
    protected $listingManager;


    /**
     * @param RequestStack            $requestStack
     * @param ListingManager          $listingManager
     */
    public function __construct(
        RequestStack $requestStack,
        ListingManager $listingManager
    ) {
        $this->request = $requestStack->getCurrentRequest();
        $this->listingManager = $listingManager;
    }


    /**
     * @return Listing
     */
    public function init()
    {
        $listing = new Listing();
        $listing = $this->addCategories($listing);

        return $listing;
    }

    /**
     * Process form
     *
     * @param Form $form
     *
     * @return Booking|string
     */
    public function process($form)
    {
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $this->request->isMethod('POST') && $form->isValid()) {
            return $this->onSuccess($form);
        }

        return false;
    }

    /**
     * @param Form $form
     * @return boolean
     */
    private function onSuccess(Form $form)
    {
        /** @var Listing $listing */
        $listing = $form->getData();
        $this->listingManager->save($listing);

        return true;
    }

    /**
     * Add selected categories and corresponding fields values from post parameters while listing deposit
     *
     * @param  Listing $listing
     * @return Listing
     */
    public function addCategories(Listing $listing)
    {
        $requestPost = $this->request->request;

        $categories = $requestPost->has('categories')
            ? $requestPost->get('categories')
            : null;

        $categories = $requestPost->has('listing')
            ? $requestPost->get('listing')
            : $categories;

        if(!is_null($categories)){

            if (array_key_exists('categories', $categories)) {
                $categories = $categories['categories'];
            }
            $categories = $this->flattenArray($categories);
            $listing = $this->listingManager->addCategories($listing,$categories);
        }

        return $listing;
    }

    /**
     * @param $categories
     * @return array
     */
    private function flattenArray($categories)
    {
        $result = array();
        if(!is_null($categories)){
            array_walk_recursive($categories, function ($v, $k) use (&$result) {
                $result[] = $v;
            });
        }
        return $result;
    }

}