<?php

namespace DW\CoreBundle\Form\Handler\Dashboard;

use DW\CoreBundle\Entity\Booking;
use DW\MessageBundle\Helper\Helper;
use Symfony\Component\Form\Form;

/**
 * Handle Offerer Dashboard Booking Form
 *
 */
class BookingOffererFormHandler extends BookingFormHandler
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * BookingAskerFormHandler constructor.
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Save Booking.
     *
     * @param Form $form
     *
     * @return int equal to :
     * 1: Success
     * -2: Wrong Booking Status
     * -3: Payin PreAuth error
     * -4: Unknown error
     */
    protected function onSuccess(Form $form)
    {
        $result = -4; //Unknown error

        /** @var Booking $booking */
        $booking = $form->getData();
        $message = $this->helper->cleanMessage($form->get("message")->getData());
        $this->threadManager->addReplyThread($booking, $message, $booking->getListing()->getUser());
        //Accept or refuse
        $type = $this->request->get('type');

        $hasCorrectStartTime = $booking->hasCorrectStartTime(
            $this->minStartDelay,
            $this->minStartTimeDelay,
            $this->bookingManager->getTimeUnitIsDay()
        );

        if ($booking->getStatus() == Booking::STATUS_NEW && $hasCorrectStartTime) {
            if ($type == 'accept') {
                if ($this->bookingManager->accept($booking)) {
                    $result = 1;
                } else {
                    $result = -3;
                }
            } elseif ($type == 'refuse') {
                $this->bookingManager->refuse($booking);
                $result = 1;
            }
        } else {
            $result = -2;
        }

        return $result;
    }
}
