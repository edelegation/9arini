<?php

namespace DW\CoreBundle\Authentication\Handler;

use DW\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;



class LoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var Router
     */
    protected  $router;

    /**
     * @param HttpUtils $httpUtils
     * @param array $options
     */
    public function __construct(Router $router, HttpUtils $httpUtils, array $options)
    {
        $this->router = $router;
        parent::__construct($httpUtils, $options);

    }

    /**
     * {@inheritDoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $key        = '_security.main.target_path';
        $session    = $request->getSession();
        $url        = $this->router->generate('dw_home');

        if ($session->has($key)) {
            $url = $session->get($key);
            $session->remove($key);
        }

        /** @var User $user */
        $user = $token->getUser();
        $cookies = $request->cookies;

        if ($cookies->has('userType')) {
            $userType = $cookies->get('userType');
        } else {
            $userType = 'asker';
            if ($user && $user->getListings()->count()) {
                $userType = 'offerer';
            }
        }

        if ($request->isXmlHttpRequest()) {
            $data = [
                'success' => true,
                'message' => 'login success',
                'referer' => $url,
            ];
            $response = new Response(json_encode($data));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }

        $response = parent::onAuthenticationSuccess($request, $token);
        $session->set('profile', $userType);
        $response->headers->setCookie(new Cookie('userType', $userType, 0, '/', null, false, false));

        return $response;
    }
}
