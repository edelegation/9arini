<?php

namespace DW\CoreBundle\Repository;

use DW\CoreBundle\Entity\ListingCharacteristicType;
use Doctrine\ORM\EntityRepository;

class ListingCharacteristicValueRepository extends EntityRepository
{
    /**
     * @param ListingCharacteristicType $listingCharacteristicType
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindAllTranslatedQueryBuilder(ListingCharacteristicType $listingCharacteristicType)
    {
        $queryBuilder = $this->createQueryBuilder('lcv')
            ->andWhere('lcv.listingCharacteristicType = :lcr')
            ->setParameter('lcr', $listingCharacteristicType);

//        $queryBuilder->getQuery()->useQueryCache(true);
//        $queryBuilder->getQuery()->useResultCache(true, 3600, 'listing_characteristic_values');
        return $queryBuilder;
    }

    /**
     * @param string $locale
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
//    public function findAllTranslated($locale)
//    {
//        return $this->getFindAllTranslatedQueryBuilder($locale)
//            ->getQuery()
//            ->getResult();
//
//    }
}
