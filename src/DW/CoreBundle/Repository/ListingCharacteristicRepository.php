<?php

namespace DW\CoreBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class ListingCharacteristicRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindAllTranslatedQueryBuilder()
    {
        $queryBuilder = $this->createQueryBuilder('lc')
            ->addSelect("lcg, lcty, lcv")
            ->leftJoin('lc.listingCharacteristicGroup', 'lcg')
            ->leftJoin('lc.listingCharacteristicType', 'lcty')
            ->leftJoin('lcty.listingCharacteristicValues', 'lcv')
            ->orderBy('lcg.position', 'ASC')
            ->addOrderBy('lc.position', 'ASC');

//        $queryBuilder->getQuery()->useQueryCache(true);
//        $queryBuilder->getQuery()->useResultCache(true, 3600, 'listing_characteristics');

        return $queryBuilder;
    }

    /**
     *
     * @return ArrayCollection|array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAllTranslated()
    {
        return $this->getFindAllTranslatedQueryBuilder()
            ->getQuery()
            ->getResult();
    }


    /**
     * @param $locale
     * @return \Doctrine\ORM\QueryBuilder
     */
//    public function getFindAllTranslatedWithValuesQueryBuilder($locale)
//    {
//        $queryBuilder = $this->createQueryBuilder('lc')
//            ->addSelect("lct, lctt, lcv, lcvt")
//            ->leftJoin('lc.translations', 'lct')
//            ->leftJoin('lc.listingCharacteristicType', 'lctt')
//            ->leftJoin('lctt.listingCharacteristicValues', 'lcv')
//            ->leftJoin('lcv.translations', 'lcvt')
//            ->andWhere('lct.locale = :locale')
//            ->setParameter('locale', $locale);
//
//        return $queryBuilder;
//    }

    /**
     * @param string $locale
     *
     * @return ArrayCollection|array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
//    public function findAllTranslatedValues($locale)
//    {
//        return $this->getFindAllTranslatedWithValuesQueryBuilder($locale)
//            ->getQuery()
//            ->getResult();
//
//    }
}
