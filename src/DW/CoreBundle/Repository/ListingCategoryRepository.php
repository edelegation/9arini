<?php

namespace DW\CoreBundle\Repository;

use DW\CoreBundle\Entity\ListingCategory;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class ListingCategoryRepository extends NestedTreeRepository
{
    protected $rootAlias;

    /**
     * @param $excludedIDs
     * @return ListingCategory[]|mixed
     */
    public function findCategories($excludedIDs)
    {
        $qb = $this->getNodesHierarchyQueryBuilder();

        $alias = $qb->getRootAliases();
        $this->rootAlias = $alias[0];
        if(is_array($excludedIDs)){
            $qb->andWhere($this->rootAlias . ".root NOT IN (:excludedIDs)")
                ->setParameter('excludedIDs', $excludedIDs);
        }else{
            $qb->andWhere($this->rootAlias . ".root <> :excludedID")
                ->setParameter('excludedID', $excludedIDs);
        }

        $query = $qb->getQuery();
        $query->useResultCache(true, 43200, 'findCategories');

        return $query->execute();
    }

    /**
     * @param array   $ids
     * @return ListingCategory[]|mixed
     */
    public function findCategoriesByIds(array $ids)
    {
        $qb = $this->getNodesHierarchyQueryBuilder();

        $alias = $qb->getRootAliases();
        $this->rootAlias = $alias[0];

        $qb->andWhere($this->rootAlias . ".id IN (:ids)")
            ->setParameter('ids', $ids);


        $query = $qb->getQuery();
        $query->useResultCache(true, 43200, 'findCategoriesByIds');

        return $query->execute();
    }

}
