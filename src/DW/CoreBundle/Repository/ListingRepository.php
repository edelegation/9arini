<?php

namespace DW\CoreBundle\Repository;

use DW\CoreBundle\Entity\Listing;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

class ListingRepository extends EntityRepository
{
    /**
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindQueryBuilder()
    {
        $queryBuilder = $this->_em->createQueryBuilder()
            //Select
            ->select("partial l.{id, price, slug, title, description, averageRating, certified, createdAt, commentCount}")
            ->addSelect("partial llcat.{id, listing, category}")
            ->addSelect("partial ca.{id, name, lft, lvl, rgt, root, cssClass}")
            //->addSelect("partial i.{id, name}")
            ->addSelect("partial u.{id, firstName}")
            //->addSelect("partial ln.{id}")
            ->addSelect("partial ln.{id, address, city, region}")
            ->addSelect("partial co.{id, lat, lng}")
            ->addSelect("partial rg.{id, name, code}")
            ->addSelect("partial cy.{id, name, code}")
            ->addSelect("partial ui.{id, name}")
            ->addSelect("'' AS DUMMY")//To maintain fields on same array level when extra fields are added

            //From
            ->from('DWCoreBundle:Listing', 'l')
            ->leftJoin('l.listingListingCategories', 'llcat')
            ->leftJoin('l.listingListingCategories', 'llcat2')
            ->leftJoin('llcat.category', 'ca')
            ->leftJoin('l.user', 'u')
            ->leftJoin('u.image', 'ui', Query\Expr\Join::WITH, 'ui.position = 1')
            ->leftJoin('l.location', 'ln')
            ->leftJoin('ln.coordinate', 'co')
            ->leftJoin('ln.region', 'rg')
            ->leftJoin('ln.city', 'cy');

//            ->leftJoin('co.country', 'cy');

        $queryBuilder
            ->addGroupBy('l.id');

        return $queryBuilder;
    }

    /**
     * @param string $slug
     * @param bool   $joined
     *
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneBySlug($slug, $joined = true)
    {
        $queryBuilder = $this->createQueryBuilder('l')
            ->where('l.slug = :slug')
            ->setParameter('slug', $slug);

        if ($joined) {
            $queryBuilder
                ->addSelect("u, i")
                ->leftJoin('l.user', 'u')
                ->leftJoin('u.image', 'i');
        }

//          $queryBuilder
//            ->addSelect("llc, lc, lct")
//            ->leftJoin('l.listingListingCharacteristics', 'llc')
//            ->leftJoin('llc.listingCharacteristic', 'lc')
//            ->leftJoin('lc.translations', 'lct')
//            ->leftJoin('lc.listingCharacteristicGroup', 'lcg')
//            ->leftJoin('lc.translations', 'lcgt')
//            ->andWhere('lct.locale = :locale')
//            ->andWhere('lcgt.locale = :locale');

        try {
            $query = $queryBuilder->getQuery();
//            $query->useResultCache(true, 3600, 'findOneBySlug');
//            $query->setFetchMode("DW\CoreBundle\Entity\Listing", "listingListingCharacteristics", ClassMetadata::FETCH_EAGER);
//            $query->setFetchMode("DW\CoreBundle\Entity\ListingListingCharacteristic", "listingCharacteristic", ClassMetadata::FETCH_EAGER);
//            $query->setFetchMode("DW\CoreBundle\Entity\ListingCharacteristic", "ListingCharacteristicGroup", ClassMetadata::FETCH_EAGER);

            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param string $slug
     *
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTranslationsBySlug($slug)
    {
        $listing = $this->findOneBySlug($slug, false);

        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('lt')
            ->from('DWCoreBundle:ListingTranslation', 'lt')
            ->where('lt.translatable = :listing')
            ->setParameter('listing', $listing);
        try {
            return $queryBuilder->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param int    $ownerId
     * @param array  $status
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getFindByOwnerQuery($ownerId, $status)
    {
        $queryBuilder = $this->createQueryBuilder('l')
            ->addSelect("c, llcat, ca, u")
//            ->addSelect("t, i, c, ca, cat, u, rt")
            ->leftJoin('l.user', 'u')
            //->leftJoin('u.reviewsTo', 'rt')
            ->leftJoin('l.listingListingCharacteristics', 'c')
            ->leftJoin('l.listingListingCategories', 'llcat')
            ->leftJoin('llcat.category', 'ca')
            ->where('u.id = :ownerId')
            ->andWhere('l.status IN (:status)')
            //->andWhere('rt.reviewTo = :reviewTo')
            ->setParameter('ownerId', $ownerId)
            ->setParameter('status', $status);

        //->setParameter('reviewTo', $ownerId);

        return $queryBuilder;

    }

    /**
     * @param $ownerId
     * @param $status
     * @return array
     */
    public function findByOwner($ownerId, $status)
    {
        return $this->getFindByOwnerQuery($ownerId, $status)->getQuery()->getResult();
    }


    /**
     * @param $title
     *
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByTitle($title)
    {
        $queryBuilder = $this->createQueryBuilder('l')
            ->addSelect("u, i")
            ->leftJoin('l.user', 'u')
            ->leftJoin('u.images', 'i')
            ->where('l.title = :title')
            ->setParameter('title', $title);
        try {

            $query = $queryBuilder->getQuery();

            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param bool $withUser
     * @param bool $withTranslations
     *
     * @param int  $hydrationMode
     * @return array|null
     */
    public function findAllPublished(
        $withUser = true,
        $withTranslations = false,
        $hydrationMode = AbstractQuery::HYDRATE_OBJECT
    ) {
        $queryBuilder = $this->createQueryBuilder('l')
            ->where('l.status = :listingStatus')
            ->setParameter('listingStatus', Listing::STATUS_PUBLISHED);

        if ($withUser) {
            $queryBuilder
                ->addSelect("u")
                ->leftJoin('l.user', 'u');
        }

        try {
            $query = $queryBuilder->getQuery();

            return $query->getResult($hydrationMode);
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findByHighestRanking($limit)
    {
        $queryBuilder = $this->getFindQueryBuilder();

        //Where
        $queryBuilder
            ->where('l.status = :listingStatus')
            ->setParameter('listingStatus', Listing::STATUS_PUBLISHED)
            ->setMaxResults($limit)
            ->orderBy('l.createdAt', 'DESC');
        try {
            $query = $queryBuilder->getQuery();
            $query->useResultCache(true, 21600, 'findByHighestRanking');

            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getRandomListing($limit)
    {
        $queryBuilder = $this->createQueryBuilder('l')
            ->addSelect('RAND() as HIDDEN rand')
            ->where('l.status = :listingStatus')
            ->andWhere('l.display = :display')
            ->setParameters(array(
                'listingStatus' => Listing::STATUS_PUBLISHED,
                'display' => true
            ))
            ->addOrderBy('rand')
            ->setMaxResults($limit);

            try {
                $query = $queryBuilder->getQuery();
                //$query->useResultCache(true, 21600, 'getRandomListing');

                return $query->getResult();
            } catch (NoResultException $e) {
                return null;
            }
    }
}
