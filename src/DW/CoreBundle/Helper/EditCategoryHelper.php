<?php

namespace DW\CoreBundle\Helper;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Entity\Listing;

class EditCategoryHelper
{
    private $type;
    private $subject;
    private $levels;
    private $parentsCategoriesIDs;
    private $categories;
    private $em;


    public function __construct(EntityManager $em, $parentsCategoriesIDs)
    {
        $this->em = $em;
        $this->levels = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->parentsCategoriesIDs = $parentsCategoriesIDs;
    }

    public function extractCategories(Listing $listing)
    {
        $categories = $this->getCategories($listing);
        foreach ($categories as $category) {
            if(!is_null($category->getParent())) {
                $key = array_search($category->getParent()->getId(), $this->parentsCategoriesIDs);
                if ($key == 'type') {
                    $this->type = $category;
                } elseif ($key == 'tutoring_subject' || $key == 'training_subject') {
                    $this->subject = $category;
                } else {
                    $this->levels->add($category);
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return ArrayCollection
     */
    public function getLevels()
    {
        return $this->levels;
    }

    private function getCategories($listing)
    {
        $listingCatgories = $this->em->getRepository('DWCoreBundle:ListingListingCategory')->findBy([
            'listing' => $listing
        ]);
        foreach ($listingCatgories as $listingCategory) {
            $this->categories->add($listingCategory->getCategory());
        }

        return $this->categories;
    }
}