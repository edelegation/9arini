<?php

namespace DW\CoreBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UserTypeCookieListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();
        $session = $request->getSession();
        $cookies = $request->cookies;
        if ($cookies->has('userType') && $cookies->get('userType') == "offerer") {
            $session->set('profile', 'offerer');
        } elseif ($cookies->has('userType') && $cookies->get('userType') == "asker") {
            $session->set('profile', 'asker');
        }

    }
}