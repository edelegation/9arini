<?php


namespace DW\CoreBundle\Twig;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Helper\GlobalHelper;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Translation\TranslatorInterface;

class CoreExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    protected $translator;
    protected $session;
    protected $globalHelper;
    protected $timeUnit;
    protected $timeUnitIsDay;
    protected $daysDisplayMode;
    protected $timesDisplayMode;
    protected $timeUnitFlexibility;
    protected $timeUnitAllDay;
    protected $timePicker;
    protected $allowSingleDay;
    protected $endDayIncluded;
    protected $listingDefaultStatus;
    protected $listingPricePrecision;
    protected $priceMin;
    protected $priceMax;
    protected $feeAsOfferer;
    protected $feeAsAsker;
    protected $displayMarker;
    protected $bookingExpirationDelay;
    protected $bookingValidationMoment;
    protected $bookingValidationDelay;
    protected $bookingPriceMin;
    protected $vatRate;
    protected $includeVat;
    protected $displayVat;
    protected $listingSearchMinResult;
    protected $listingDuplication;
    protected $minStartDelay;
    protected $minStartTimeDelay;
    protected $currency;

    /**
     *
     * @param TranslatorInterface $translator
     * @param Session             $session
     * @param GlobalHelper        $globalHelper
     * @param array                             $parameters
     *
     */

    public function __construct(
        TranslatorInterface $translator,
        Session $session,
        GlobalHelper $globalHelper,
        array $parameters
    ) {
        //Services
        $this->translator = $translator;
        $this->session = $session;
        $this->globalHelper = $globalHelper;

        $parameters = $parameters['parameters'];

        //Time unit
        $this->timeUnit = $parameters["dw_time_unit"];
        $this->timeUnitIsDay = ($this->timeUnit % 1440 == 0) ? true : false;
        $this->timeUnitAllDay = $parameters["dw_time_unit_allday"];
        $this->timeUnitFlexibility = $parameters["dw_time_unit_flexibility"];
        $this->daysDisplayMode = $parameters["dw_days_display_mode"];
        $this->timesDisplayMode = $parameters["dw_times_display_mode"];
        $this->timePicker = $parameters["dw_time_picker"];

        //Fees
        $this->feeAsOfferer = $parameters["dw_fee_as_offerer"];
        $this->feeAsAsker = $parameters["dw_fee_as_asker"];

        //Status
        $this->listingDefaultStatus = $parameters["dw_listing_availability_status"];

        //Prices
        $this->listingPricePrecision = $parameters["dw_listing_price_precision"];
        $this->priceMin = $parameters["dw_listing_price_min"];
        $this->priceMax = $parameters["dw_listing_price_max"];
        $this->bookingPriceMin = $parameters["dw_booking_price_min"];

        //Map
        $this->displayMarker = $parameters["dw_listing_map_display_marker"];

        $this->listingSearchMinResult = $parameters["dw_listing_search_min_result"];
        $this->listingDuplication = $parameters["dw_listing_duplication"];

        $this->allowSingleDay = $parameters["dw_booking_allow_single_day"];
        $this->endDayIncluded = $parameters["dw_booking_end_day_included"];

        //Delay
        $this->bookingExpirationDelay = $parameters["dw_booking_expiration_delay"] * 60;//Converted to seconds
        $this->bookingValidationMoment = $parameters["dw_booking_validated_moment"];
        $this->bookingValidationDelay = $parameters["dw_booking_validated_delay"];
        $this->minStartDelay = $parameters["dw_booking_min_start_delay"];;
        $this->minStartTimeDelay = $parameters["dw_booking_min_start_time_delay"];

        //VAT
        $this->vatRate = $parameters["dw_vat"];
        $this->includeVat = $parameters["dw_include_vat"];
        $this->displayVat = $parameters["dw_display_vat"];

        $this->currency = $parameters["dw_currency"];
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('repeat', array($this, 'stringRepeatFilter')),
            new \Twig_SimpleFilter('format_seconds', array($this, 'formatSecondsFilter')),
            new \Twig_SimpleFilter('date_from_string', array($this, 'dateFromStringFilter')),
            new \Twig_SimpleFilter('add_time_unit_text', array($this, 'addTimeUnitTextFilter')),
            new \Twig_SimpleFilter('ucwords', 'ucwords'),
            new \Twig_SimpleFilter('format_price', array($this, 'formatPriceFilter'))
        );
    }

    /**
     * @param $input
     * @param $multiplier
     * @return string
     */
    public function stringRepeatFilter($input, $multiplier)
    {
        return str_repeat($input, $multiplier);
    }

    public function dateFromStringFilter($date)
    {
        return \DateTime::createFromFormat('d/m/Y', $date);
    }

    /**
     * Format time from seconds to unit
     *
     * @param int $seconds
     * @param string $format
     *
     * @return string
     */
    public function formatSecondsFilter($seconds, $format = 'dhm')
    {
        $time = $this->globalHelper->secondsToTime($seconds);
        switch ($format) {
            case 'h':
                $result = ($time['d'] * 24) + $time['h'] . "h";
                break;
            default:
                $result = ($time['d'] * 24) + $time['h'] . "h " . $time['m'] . "m";
        }

        return $result;
    }

    /**
     * Add unit time text to duration value
     *
     * @param int    $duration
     * @return string
     */
    public function addTimeUnitTextFilter($duration)
    {
        if ($this->timeUnitIsDay) {
            if ($this->timeUnitAllDay) {
                return $this->translator->transChoice(
                    'time_unit_day',
                    $duration,
                    array('%count%' => $duration),
                    'dw'
                );
            } else {
                return $this->translator->transChoice(
                    'time_unit_night',
                    $duration,
                    array('%count%' => $duration),
                    'dw'
                );
            }
        } else {
            return $this->translator->transChoice(
                'time_unit_hour',
                $duration,
                array('%count%' => $duration),
                'dw'
            );
        }
    }

    /**
     * @param int    $price
     * @param int    $precision
     * @param bool   $convert
     * @return string
     */
    public function formatPriceFilter($price, $precision = null, $convert = true)
    {
        if (is_null($precision)) {
            $precision = $this->listingPricePrecision;
        }

        return $price;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'session_upload_progress_name', function () {
                return ini_get("session.upload_progress.name");
            }
            ),
            new \Twig_SimpleFunction('cancellationPolicies', array($this, 'cancellationPoliciesFunction')),
            new \Twig_SimpleFunction('vatInclusionText', array($this, 'vatInclusionText')),
            new \Twig_SimpleFunction('date_from_string', array($this, 'dateFromStringFilter')),
        );
    }

    /**
     * Display cancelation Policies text rules
     *
     * @return string
     */
    public function cancellationPoliciesFunction()
    {
        $policiesText = $this->translator->trans(
                'listing.cancellation_policy.help',
                array(),
                'dw_listing'
            ) . ":<br/>";

        foreach (Listing::$cancellationPolicyValues as $policyValue => $policyText) {
            /** @Ignore */
            $policyTextTrans = $this->translator->trans($policyText, array(), 'dw_listing');
            /** @Ignore */
            $policyDescTrans = $this->translator->trans(
                Listing::$cancellationPolicyDescriptionsHelp[$policyValue],
                array(),
                'dw_listing'
            );

            $policiesText .= "-" . $policyTextTrans . ":<br/>" . $policyDescTrans . "<br/>";
        }

        return $policiesText;
    }


    /**
     * Display VAT include / exclude text
     *
     * @param bool|null $displayVat Override default app parameter if setted
     * @param bool|null $includeVat Override default app parameter if setted
     *
     * @return string
     */
    public function vatInclusionText($displayVat = null, $includeVat = null)
    {
        if (($this->displayVat && $displayVat === null) || $displayVat === true) {
            if (($this->includeVat && $includeVat === null) || $includeVat === true) {
                return $this->translator->trans(
                    'vat_included',
                    array(),
                    'dw'
                );
            } else {
                return $this->translator->trans(
                    'vat_excluded',
                    array(),
                    'dw'
                );
            }
        }

        return '';
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getGlobals()
    {
        $listing = new \ReflectionClass("DW\CoreBundle\Entity\Listing");
        $listingConstants = $listing->getConstants();

        $listingAvailability = new \ReflectionClass("DW\CoreBundle\Document\ListingAvailability");
        $listingAvailabilityConstants = $listingAvailability->getConstants();

        $userImage = new \ReflectionClass("DW\UserBundle\Entity\UserImage");
        $userImageConstants = $userImage->getConstants();

        $booking = new \ReflectionClass("DW\CoreBundle\Entity\Booking");
        $bookingConstants = $booking->getConstants();

        $order = new \ReflectionClass("DW\PaymentBundle\Entity\Orders");
        $orderConstants = $order->getConstants();


        //CSS class by status
        $bookingStatusClass = array(
            Booking::STATUS_DRAFT => 'btn-yellow',
            Booking::STATUS_NEW => 'btn-yellow',
            Booking::STATUS_ACCEPTED => 'btn-polo-blue',
            Booking::STATUS_PAYED => 'btn-algae-green',
            Booking::STATUS_EXPIRED => 'btn-nomad',
            Booking::STATUS_REFUSED => 'btn-flamingo',
            Booking::STATUS_CANCELED_ASKER => 'btn-salmon',
//            Booking::STATUS_CANCELED_OFFERER => 'btn-salmon',
            Booking::STATUS_PAYMENT_REFUSED => 'btn-fuzzy-brown'
        );

        $bookingBankWire = new \ReflectionClass("DW\CoreBundle\Entity\BookingBankWire");
        $bookingBankWireConstants = $bookingBankWire->getConstants();

        $bookingPayinRefund = new \ReflectionClass("DW\CoreBundle\Entity\BookingPayinRefund");
        $bookingPayinRefundConstants = $bookingPayinRefund->getConstants();

        return array(
            'ListingConstants' => $listingConstants,
            'ListingAvailabilityConstants' => $listingAvailabilityConstants,
            'UserImageConstants' => $userImageConstants,
            'BookingConstants' => $bookingConstants,
            'OrderConstants' => $orderConstants,
            'BookingBankWireConstants' => $bookingBankWireConstants,
            'BookingPayinRefundConstants' => $bookingPayinRefundConstants,
            'bookingStatusClass' => $bookingStatusClass,
            'timeUnit' => $this->timeUnit,
            'timeUnitIsDay' => $this->timeUnitIsDay,
            'timeUnitAllDay' => $this->timeUnitAllDay,
            'timePicker' => $this->timePicker,
            'daysDisplayMode' => $this->daysDisplayMode,
            'timesDisplayMode' => $this->timesDisplayMode,
            'timeUnitFlexibility' => $this->timeUnitFlexibility,
            'allowSingleDay' => $this->allowSingleDay,
            'endDayIncluded' => $this->endDayIncluded,
            'listingDefaultStatus' => $this->listingDefaultStatus,
            'listingPricePrecision' => $this->listingPricePrecision,
            'priceMin' => $this->priceMin,
            'priceMax' => $this->priceMax,
            'feeAsOfferer' => $this->feeAsOfferer,
            'feeAsAsker' => $this->feeAsAsker,
            'displayMarker' => $this->displayMarker,
            'bookingExpirationDelay' => $this->bookingExpirationDelay,
            'bookingValidationMoment' => $this->bookingValidationMoment,
            'bookingValidationDelay' => $this->bookingValidationDelay,
            'bookingPriceMin' => $this->bookingPriceMin,
            'vatRate' => $this->vatRate,
            'includeVat' => $this->includeVat,
            'displayVat' => $this->displayVat,
            'listingSearchMinResult' => $this->listingSearchMinResult,
            'listingDuplication' => $this->listingDuplication,
            'minStartDelay' => $this->minStartDelay,
            'minStartTimeDelay' => $this->minStartTimeDelay,
            'currency' => $this->currency
        );
    }


    /**
     * @inheritdoc
     *
     * @return string
     */
    public function getName()
    {
        return 'core_extension';
    }
}
