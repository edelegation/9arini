<?php

namespace DW\CoreBundle\Twig;

use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Entity\Listing;
use Symfony\Component\DependencyInjection\Container;

/**
 * CategoryExtension will render the name of subject category
 */
class CategoryExtension extends \Twig_Extension
{
    protected $manager;
    private $listingID;
    /**
     * @var Container
     */
    private $container;

    public function __construct(EntityManager $manager,Container $container)
    {
        $this->manager = $manager;
        $this->container = $container;
    }

    /**
     * @inheritdoc
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('subjectName', array($this, 'getSubjectName')),
        );
    }

    /**
     * @param $listingID
     */
    public function getSubjectName($listingID)
    {
        $this->listingID = $listingID;
        $lcs = $this->getListingCategories();
        $subjects = [
            $this->container->getParameter('listing_parent_categories')['tutoring_subject'],
            $this->container->getParameter('listing_parent_categories')['training_subject']
        ];

        if(count($lcs) > 0) {
            foreach ($lcs as $lc){
                $c = $lc->getCategory();
                if($c->getParent() && in_array($c->getParent()->getId(),$subjects)){
                    return $c->getName();
                }
            }
        }
    }

    public function getListingCategories()
    {
        return $this->getListing()->getListingListingCategories();
    }

    public function getListing(){
        return $this->manager->getRepository('DWCoreBundle:Listing')->find($this->listingID);
    }


    /** @inheritdoc */
    public function getName()
    {
        return 'category_extension';
    }
}