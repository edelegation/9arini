<?php

namespace DW\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DWCoreBundle
 *
 */
class DWCoreBundle extends Bundle
{
}
