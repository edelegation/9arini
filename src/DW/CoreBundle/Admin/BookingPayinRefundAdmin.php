<?php

namespace DW\CoreBundle\Admin;

use DW\CoreBundle\Entity\BookingPayinRefund;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BookingPayinRefundAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';
    protected $baseRoutePattern = 'booking-payin-refund';
    protected $timeUnit;
    protected $timeUnitIsDay;

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    public function setTimeUnit($timeUnit)
    {
        $this->timeUnit = $timeUnit;
        $this->timeUnitIsDay = ($timeUnit % 1440 == 0) ? true : false;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.booking_payin_refund.title')
            ->add(
                'user',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.asker.label'
                )
            )
            ->add(
                'booking',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.label',
                )
            )
            ->add(
                'amount',
                'price',
                array(
                    'scale' => 2,
                    'disabled' => true,
                    'label' => 'admin.booking_payin_refund.amount.label',
                    'include_vat' => true
                )
            )
            /*->add(
                'status',
                'choice',
                array(
                    'disabled' => true,
                    'choices' => array_flip(BookingPayinRefund::$statusValues),
                    'empty_value' => 'admin.booking_bank_wire.booking.status.label',
                    'label' => 'admin.booking_bank_wire.booking.status.label',
                    'translation_domain' => 'dw_booking',
                    'choices_as_values' => true
                )
            )*/
            ->add(
                'validated',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.validated.label',
                    'disabled' => false,
                )
            )
            ->add(
                'payedAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking_payin_refund.payed_at.label',
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.created_at.label',
                )
            )
            ->add(
                'updatedAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.updated_at.label',
                )
            )
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            /*->add(
                'status',
                'doctrine_orm_string',
                array(),
                'choice',
                array(
                    'choices' => array_flip(BookingPayinRefund::$statusValues),
                    'label' => 'admin.booking_bank_wire.booking.status.label',
                    'translation_domain' => 'dw_booking',
                    'choices_as_values' => true
                )
            )*/
            ->add(
                'validated',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.validated.label'
                )
            )
            ->add(
                'booking.id',
                null,
                array('label' => 'admin.booking_bank_wire.booking_id.label')
            )
            ->add(
                'user.email',
                null,
                array('label' => 'admin.booking.offerer.label')
            )
            ->add(
                'createdAt',
                'doctrine_orm_callback',
                array(
                    'label' => 'admin.booking.created_at.label',
                    'callback' => function ($queryBuilder, $alias, $field, $value) {
                        /** @var \DateTime $date */
                        $date = $value['value'];
                        if (!$date) {
                            return false;
                        }

                        $queryBuilder
                            ->andWhere("DATE_FORMAT($alias.createdAt,'%Y-%m-%d') = :createdAt")
                            ->setParameter('createdAt', $date->format('Y-m-d'));

                        return true;
                    },
                    'field_type' => 'sonata_type_date_picker',
                    'field_options' => array('format' => 'dd/MM/yyyy'),
                ),
                null
            );
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add(
                'booking',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.booking.label'
                )
            )
            /*->add(
                'statusText',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.booking.status.label',
                    'template' => 'DWSonataAdminBundle::list_field_value_translated.html.twig',
                    'data_trans' => 'dw_booking'
                )
            )*/
            ->add(
                'validated',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.validated.label',
                    'editable' => true,
                    'template' => 'DWCoreBundle:Admin\BookingPayinRefund:validated.html.twig'
                )
            )
            ->add(
                'user',
                null,
                array(
                    'label' => 'admin.booking.offerer.label',
                )
            )
            ->add(
                'booking.listing',
                null,
                array(
                    'label' => 'admin.listing.label'
                )
            )
            ->add(
                'booking.start',
                'date',
                array(
                    'label' => 'admin.booking.start.label',
                    'format' => 'd/m/Y'
                )
            )
            ->add(
                'booking.end',
                'date',
                array(
                    'label' => 'admin.booking.end.label',
                    'format' => 'd/m/Y'
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'label' => 'admin.booking.created_at.label',
                    'format' => 'd/m/Y'
                )
            )
            ->add(
                'booking.amountToPayByAskerDecimal',
                null,
                array(
                    'label' => 'admin.booking.amount.label'
                )
            )
            ->add(
                'amountDecimal',
                null,
                array(
                    'label' => 'admin.booking_payin_refund.amount.label'
                )
            )
            ->add(
                'payedAt',
                null,
                array(
                    'label' => 'admin.booking_payin_refund.payed_at.label',
                    'format' => 'd/m/Y'
                )
            );


    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions["delete"]);

        return $actions;
    }

    public function getExportFields()
    {
        $fields = array(
            'Id' => 'id',
            'Booking' => 'booking',
            'Status' => 'statusText',
            'User' => 'user',
            'Booking Listing' => 'booking.listing',
            'Booking Start' => 'booking.start',
            'Booking End' => 'booking.end',
            'Created At' => 'createdAt',
            'Booking Amount Pay By Asker' => 'booking.amountToPayByAskerDecimal',
            'Amount' => 'amountDecimal',
            'Payed At' => 'payedAt'
        );

        return $fields;
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $dataSourceIt = $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
        $dataSourceIt->setDateTimeFormat('d M Y');

        return $dataSourceIt;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->add('validate', $this->getRouterIdParameter().'/validate');
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'DWCoreBundle:Admin\BookingPayinRefund:list.html.twig';
                break;

            case 'base_list_field':
                return 'DWCoreBundle:Admin\BookingPayinRefund:base_list_field.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
