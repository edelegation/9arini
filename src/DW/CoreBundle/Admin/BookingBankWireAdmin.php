<?php

namespace DW\CoreBundle\Admin;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\BookingBankWire;
use DW\CoreBundle\Model\Manager\BookingBankWireManager;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BookingBankWireAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';
    protected $baseRoutePattern = 'booking-bank-wire';
    protected $timeUnit;
    protected $timeUnitIsDay;
    /** @var  BookingBankWireManager $bookingBankWireManager */
    protected $bookingBankWireManager;
    protected $bundles;

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    public function setTimeUnit($timeUnit)
    {
        $this->timeUnit = $timeUnit;
        $this->timeUnitIsDay = ($timeUnit % 1440 == 0) ? true : false;
    }

    public function setBookingBankWireManager(BookingBankWireManager $bookingBankWireManager)
    {
        $this->bookingBankWireManager = $bookingBankWireManager;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var BookingBankWire $bookingBankWire */
        $bookingBankWire = $this->getSubject();

        $formMapper
            ->with('admin.booking_bank_wire.title')
            ->add(
                'user',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.offerer.label'
                )
            )
            ->add(
                'booking',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.label',
                )
            )->add(
                'booking.status',
                'choice',
                array(
                    'disabled' => true,
                    'choices' => array_flip(Booking::$statusValues),
                    'empty_value' => 'admin.booking.status.label',
                    'label' => 'admin.booking_bank_wire.booking.status.label',
                    'translation_domain' => 'dw_booking',
                    'choices_as_values' => true
                )
            );

        $formMapper
            ->add(
                'status',
                'choice',
                array(
                    'choices' => array_flip(BookingBankWire::$statusValues),
                    'empty_value' => 'admin.booking.status.label',
                    'label' => 'admin.booking.status.label',
                    'translation_domain' => 'dw_booking',
                    'help' => 'admin.booking_bank_wire.status.help',
                    'choices_as_values' => true
                )
            )
            ->add(
                'payedAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking_bank_wire.payed_at.label',
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.created_at.label',
                )
            )
            ->add(
                'updatedAt',
                null,
                array(
                    'disabled' => true,
                    'label' => 'admin.booking.updated_at.label',
                )
            )
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add(
                'status',
                'doctrine_orm_string',
                array(),
                'choice',
                array(
                    'choices' => array_flip(BookingBankWire::$statusValues),
                    'label' => 'admin.booking.status.label',
                    'translation_domain' => 'dw_booking',
                    'choices_as_values' => true
                )
            )
            ->add(
                'booking.id',
                null,
                array('label' => 'admin.booking_bank_wire.booking_id.label')
            )
            ->add(
                'user.email',
                null,
                array('label' => 'admin.booking.offerer.label')
            )
            ->add(
                'createdAt',
                'doctrine_orm_callback',
                array(
                    'label' => 'admin.booking.created_at.label',
                    'callback' => function ($queryBuilder, $alias, $field, $value) {
                        /** @var \DateTime $date */
                        $date = $value['value'];
                        if (!$date) {
                            return false;
                        }

                        $queryBuilder
                            ->andWhere("DATE_FORMAT($alias.createdAt,'%Y-%m-%d') = :createdAt")
                            ->setParameter('createdAt', $date->format('Y-m-d'));

                        return true;
                    },
                    'field_type' => 'sonata_type_date_picker',
                    'field_options' => array('format' => 'dd/MM/yyyy'),
                ),
                null
            );
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add(
                'statusText',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.status.label',
                    'template' => 'DWSonataAdminBundle::list_field_value_translated.html.twig',
                    'data_trans' => 'dw_booking'
                )
            )
            ->add(
                'booking',
                null,
                array(
                    'label' => 'admin.booking_bank_wire.booking.label'
                )
            )
            ->add(
                'booking.statusText',
                null,
                array(
                    'label' => 'admin.booking.status.label',
                    'template' => 'DWSonataAdminBundle::list_field_value_translated.html.twig',
                    'data_trans' => 'dw_booking'
                )
            )
            ->add(
                'user',
                null,
                array(
                    'label' => 'admin.booking.offerer.label',
                )
            )
            ->add(
                'booking.listing',
                null,
                array(
                    'label' => 'admin.listing.label'
                )
            )
            ->add(
                'booking.start',
                'date',
                array(
                    'label' => 'admin.booking.start.label',
                    'format' => 'd/m/Y'
                )
            )
            ->add(
                'booking.end',
                'date',
                array(
                    'label' => 'admin.booking.end.label',
                    'format' => 'd/m/Y'
                )
            )
            ->add(
                'booking.amountToPayByAskerDecimal',
                null,
                array(
                    'label' => 'admin.booking.amount.label'
                )
            )
            ->add(
                'amountDecimal',
                null,
                array(
                    /** @Ignore */
                    'label' =>
                        (
                        'admin.booking_bank_wire.amount.label'
                        )
                )
            );

        $listMapper->add(
            '_action',
            'actions',
            array(
                'actions' => array(
                    'edit' => array(),
//                    'do_bank_wire' => array(
//                        'template' => 'DWMangoPayBundle::Admin/list_action_do_bank_wire.html.twig'
//                    )
                )
            )
        );
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions["delete"]);

        return $actions;
    }

    public function getExportFields()
    {
        $fields = array(
            'Id' => 'id',
            'Status' => 'statusText',
            'Booking' => 'booking',
            'Booking Status' => 'booking.statusText',
            'User' => 'user',
            'Booking Listing' => 'booking.listing',
            'Booking Start' => 'booking.start',
            'Booking End' => 'booking.end',
            'Booking Amount Pay To Offerer' => 'booking.amountToPayToOffererDecimal',
            'Amount' => 'amountDecimal',
        );

        return $fields;
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $dataSourceIt = $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
        $dataSourceIt->setDateTimeFormat('d M Y');

        return $dataSourceIt;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}
