<?php

namespace DW\CoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ListingCategoryAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';
    protected $baseRoutePattern = 'listing-category';

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'root, lft'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.listing_category.title')
            ->add(
                'name',
                null,
                array('label' => 'admin.listing_category.name.label')
            )
            ->add(
                'parent',
                null,
                array(
                    'label' => 'admin.listing_category.parent.label'
                )
            )
            ->add(
                'cssClass',
                null,
                array('label' => 'admin.listing_category.css_class.label')
            )
            ->add(
                "display",
                'choice',
                array(
                    'label' => 'admin.listing_category.display.label',
                    'required' => true,
                    'choices' => array(
                        0 => 'Non',
                        1 => 'Oui'
                    ),
                    'multiple' => false,
                    'expanded' => true
                )
            );

        $formMapper
            ->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'name',
                null,
                array('label' => 'admin.listing_category.name.label')
            )
            ->add(
                'parent',
                null,
                array('label' => 'admin.listing_category.parent.label')
            )
            ->add(
                'cssClass',
                null,
                array('label' => 'admin.listing_category.css_class.label')
            )
            /*->add(
                'display',
                ChoiceType::class,
                array(
                    'label' => 'admin.listing_category.display.label',
                    'choices' => array(
                        1 => 'label_type_yes', // or 'True'
                        2 => 'label_type_no' // or 'False'
                    )
                )
            )*/;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add(
                'name',
                null,
                array(
                    'label' => 'admin.listing_category.name.label',
                )
            )
            ->addIdentifier(
                'parent',
                null,
                array('label' => 'admin.listing_category.parent.label')
            )
            ->add(
                'cssClass',
                null,
                array(
                    'label' => 'admin.listing_category.css_class.label',
                )
            )
            ->add(
                "display","boolean",
                array(
                    'label' => 'admin.listing_category.display.label',
                )
            );


        $listMapper->add(
            '_action',
            'actions',
            array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                )
            )
        );
    }

    public function getExportFields()
    {
        return array(
            'Id' => 'id',
            'name' => 'name',
            'category' => 'parent'
        );
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $dataSourceIt = $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
        $dataSourceIt->setDateTimeFormat('d M Y');

        return $dataSourceIt;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions["delete"]);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->remove('create');
        //$collection->remove('delete');
    }
}
