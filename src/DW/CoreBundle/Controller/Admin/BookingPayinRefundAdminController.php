<?php

namespace DW\CoreBundle\Controller\Admin;

use DW\CoreBundle\Entity\BookingPayinRefund;
use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyPath;

class BookingPayinRefundAdminController extends BaseController
{

    public function validateAction(Request $request,$id)
    {
        /** @var BookingPayinRefund $object */
        $object = $this->admin->getSubject();
        $field    = $request->get('field');
        $code     = $request->get('code');
        $objectId = $request->get('objectId');
        $value    = $request->get('value');
        $context  = $request->get('context');

        // alter should be done by using a post method
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 'KO', 'message' => 'Expected a XmlHttpRequest request header'));
        }

        if ($request->getMethod() != 'POST') {
            return new JsonResponse(array('status' => 'KO', 'message' => 'Expected a POST Request'));
        }

        $rootObject = $object;

        if (!$object) {
            return new JsonResponse(array('status' => 'KO', 'message' => 'Object does not exist'));
        }

        // check user permission
        if (false === $this->admin->isGranted('EDIT', $object)) {
            return new JsonResponse(array('status' => 'KO', 'message' => 'Invalid permissions'));
        }

        if ($context == 'list') {
            $fieldDescription = $this->admin->getListFieldDescription($field);
        } else {
            return new JsonResponse(array('status' => 'KO', 'message' => 'Invalid context'));
        }

        if (!$fieldDescription) {
            return new JsonResponse(array('status' => 'KO', 'message' => 'The field does not exist'));
        }

        if (!$fieldDescription->getOption('editable')) {
            return new JsonResponse(array('status' => 'KO', 'message' => 'The field cannot be edit, editable option must be set to true'));
        }


        $object->setValidated((bool) $value);
        $object->setStatus($value);

        $this->admin->update($object);
        $twig = $this->get('twig');
        // render the widget
        // todo : fix this, the twig environment variable is not set inside the extension ...
        $extension = $twig->getExtension('sonata_admin');
        $extension->initRuntime($twig);

        $content = $extension->renderListElement($twig, $rootObject, $fieldDescription);

        return new JsonResponse(array('status' => 'OK', 'content' => $content));
    }
}