<?php

namespace DW\CoreBundle\Controller\Frontend;

use DW\CoreBundle\Entity\Listing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Listing Availability controller.
 *
 * @Route("/listing_availabilities")
 */
class ListingAvailabilityController extends Controller
{

    /**
     * Lists ListingAvailability Documents
     *
     * @Route("/{listing_id}/{start}/{end}",
     *      name="dw_listing_availabilities",
     *      requirements={
     *          "listing_id" = "\d+",
     *          "start"= "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",
     *          "end"= "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$",
     *          "_format"="json"
     *      },
     *      defaults={"_format": "json"}
     * )
     * @Security("is_granted('view', listing)")
     * @ParamConverter("listing", class="DWCoreBundle:Listing", options={"id" = "listing_id"})
     *
     * @Method("GET")
     *
     * @param  Request $request
     * @param  Listing $listing
     * @param  string  $start format yyyy-mm-dd
     * @param  string  $end   format yyyy-mm-dd
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, Listing $listing, $start, $end)
    {
        $start = new \DateTime($start);
        $end = new \DateTime($end);

        $availabilities = $this->get("dw.listing_availability.manager")->getAvailabilitiesByListingAndDateRange(
            $listing->getId(),
            $start,
            $end,
            "calendar"
        );

        //Convert and format prices
        array_walk(
            $availabilities,
            function (&$el, $key) {
                $el["title"] = $this->get('dw.twig.core_extension')->formatPriceFilter(
                    $el["title"],
                    0
                );
            }
        );

        return new JsonResponse($availabilities);
    }
}
