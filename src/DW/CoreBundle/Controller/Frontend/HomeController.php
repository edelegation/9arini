<?php

namespace DW\CoreBundle\Controller\Frontend;

use Doctrine\Common\Collections\ArrayCollection;
use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Repository\ListingRepository;
use DW\UserBundle\Entity\UserImage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HomeController
 *
 */
class HomeController extends Controller
{
    /**
     * @Route("/", name="dw_home")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var ListingRepository $listingRepository */
        $listingRepository = $this->getDoctrine()->getRepository('DWCoreBundle:Listing');
        $listings = $listingRepository->getRandomListing($this->container->getParameter('dw.listing_slider_home_limit'));
        $ids = $this->container->getParameter('listing_parent_categories');
        $categories = $this->getDoctrine()->getRepository('DWCoreBundle:ListingCategory')->findCategoriesByIds([
            $ids['tutoring_subject'],
            $ids['training_subject']
        ]);

        $listingsCollection = new ArrayCollection($listings);
        $nbResults = $listingsCollection->count();
        $resultsIterator = $listingsCollection->getIterator();
        $markers = $this->getMarkers($request, $listingsCollection, $resultsIterator);

        return $this->render(
            'DWCoreBundle:Frontend\Home:index.html.twig',
            array(
                'results' => $resultsIterator,
                'nb_results' => $nbResults,
                'markers' => $markers,
                'listings' => $listings,
                'categories' => $categories
            )
        );
    }

    /**
     * Get Markers
     *
     * @param  Request        $request
     * @param  Paginator      $results
     * @param  \ArrayIterator $resultsIterator
     * @return array
     */
    protected function getMarkers(Request $request, $results, $resultsIterator)
    {
        //We get listings id of current page to change their marker aspect on the map
        /*$resultsInPage = array();
        foreach ($resultsIterator as $i => $result) {
            $resultsInPage[] = $result[0]['id'];
        }*/

        //We need to display all listings (without pagination) of the current search on the map
//        $results->getQuery()->setFirstResult(null);
//        $results->getQuery()->setMaxResults(null);
        $nbResults = $results->count();

        $imagePath = UserImage::IMAGE_FOLDER;
        $liipCacheManager = $this->get('liip_imagine.cache.manager');
        $markers = array();

        foreach ($results->getIterator() as $i => $listing) {
            $imageName = count($listing->getUser()->getImage()) ? $listing->getUser()->getImage()->getName() : UserImage::IMAGE_DEFAULT;

            $image = $liipCacheManager->getBrowserPath($imagePath . $imageName, 'user_medium', array());

            $price = $listing->getPrice();

            $category = count($listing->getListingListingCategories()) ?
                $listing->getListingListingCategories()[0]->getCategory()->getName() : '';

            $icon = count($listing->getListingListingCategories()) ?
                $listing->getListingListingCategories()[0]->getCategory()->getCssClass() : '';

            $rating1 = $rating2 = $rating3 = $rating4 = $rating5 = 'empty';
            if ($listing->getAverageRating()) {
                $_rating1 = ($listing->getAverageRating() >= 0.25) ? 'half' : 'empty';
                $rating1 = ($listing->getAverageRating() >= 0.75) ? '' : $_rating1;
                $_rating2 = ($listing->getAverageRating() >= 1.25) ? 'half' : 'empty';
                $rating2 = ($listing->getAverageRating() >= 1.75) ? '' : $_rating2;
                $_rating3 = ($listing->getAverageRating() >= 2.25) ? 'half' : 'empty';
                $rating3 = ($listing->getAverageRating() >= 2.75) ? '' : $_rating3;
                $_rating4 = ($listing->getAverageRating() >= 3.25) ? 'half' : 'empty';
                $rating4 = ($listing->getAverageRating() >= 3.75) ? '' : $_rating4;
                $_rating5 = ($listing->getAverageRating() >= 4.25) ? 'half' : 'empty';
                $rating5 = ($listing->getAverageRating() >= 4.75) ? '' : $_rating5;
            }

            $markers[] = array(
                'id' => $listing->getId(),
                'lat' => $listing->getLocation()->getCoordinate()->getLat(),
                'lng' => $listing->getLocation()->getCoordinate()->getLng(),
                'address' => $listing->getLocation()->getCity()->getName(),
                'title' => $listing->getTitle(),
                'category' => $category,
                'commentCount' => $listing->getCommentCount(),
                'image' => $image,
                // TODO adding subject icon from BO
                'icon' => $icon,
                'rating1' => $rating1,
                'rating2' => $rating2,
                'rating3' => $rating3,
                'rating4' => $rating4,
                'rating5' => $rating5,
                'price' => $price,
                'certified' => $listing->getCertified() ? 'certified' : 'hidden',
                'url' => $url = $this->generateUrl(
                    'dw_listing_show',
                    array('slug' => $listing->getSlug())
                ),
                'zindex' => $i,
                'opacity' => 0.4,

            );
        }

        return $markers;
    }
}
