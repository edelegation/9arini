<?php

namespace DW\CoreBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class ListingFavouriteController extends ListingSearchController
{

    /**
     * Favourites Listings result.
     *
     * @Route("/listing/favourite", name="dw_listing_favourite")
     * @Method("GET")
     *
     * @param  Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexFavouriteAction(Request $request)
    {
        $markers = array();
        $resultsIterator = new \ArrayIterator();
        $nbResults = 0;

        $listingSearchRequest = $this->get('dw.listing_search_request');
        $form = $this->createSearchResultForm($listingSearchRequest);

        $form->handleRequest($request);

        // handle the form for pagination
        if ($form->isSubmitted() && $form->isValid()) {
            $listingSearchRequest = $form->getData();
        }

        $favourites = explode(',', $request->cookies->get('favourite'));
        if (count($favourites) > 0) {
            $results = $this->get("dw.listing_search.manager")->getListingsByIds(
                $favourites,
                $listingSearchRequest->getPage()
            );
            $nbResults = $results->count();
            $resultsIterator = $results->getIterator();
            $markers = $this->getMarkers($request, $results, $resultsIterator);
        }

        return $this->render(
            '@DWCore/Frontend/ListingResult/result.html.twig',
            array(
                'results' => $resultsIterator,
                'nb_results' => $nbResults,
                'markers' => $markers,
                'listing_search_request' => $listingSearchRequest,
                'pagination' => array(
                    'page' => $listingSearchRequest->getPage(),
                    'pages_count' => ceil($nbResults / $listingSearchRequest->getMaxPerPage()),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                ),
                'form' => $form->createView()
            )
        );

    }

}
