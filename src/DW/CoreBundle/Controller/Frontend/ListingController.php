<?php

namespace DW\CoreBundle\Controller\Frontend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Entity\ListingLocation;
use DW\UserBundle\Entity\User;
use DW\UserBundle\Entity\UserImage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Listing controller.
 *
 * @Route("/listing")
 */
class ListingController extends Controller
{
    /**
     * Creates a new Listing entity.
     *
     * @Route("/new", name="dw_listing_new")
     * @Method({"GET", "POST"})
     *
     * @param  Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $listingHandler = $this->get('dw.form.handler.listing');
        $listing = new Listing();
        $form = $this->createCreateForm($listing);
        $listingHandler->addCategories($listing);
        $success = $listingHandler->process($form);

        // GET LAST COORDINATE LISTING
        /** @var User $user */
        $user = $this->getUser();
        /** @var Listing $listing */
        $firstListing = $user->getListings()->first();
        /** @var ListingLocation $location */
        $location = $firstListing ? $firstListing->getLocation() : null;

        if ($success) {

            if ($file = $request->files->get('listing')['image']['file']) {
                $imageCrop = $request->request->get('listing')['image']['crop_img'];
                $userImage = new UserImage();
                $userImage->setUser($this->getUser());
                $userImage->setFile($file);
                $userImage->setCropImg($imageCrop);
                $userImage->upload();

                $em = $this->getDoctrine()->getManager();
                $em->persist($userImage);
                $em->flush();

            }

            $url = $this->generateUrl(
                'dw_dashboard_listing_edit_presentation',
                array('id' => $listing->getId())
            );

            $this->container->get('session')->getFlashBag()->add(
                'success',
                $this->container->get('translator')->trans('listing.new.success', array(), 'dw_listing')
            );

            $this->get('session')->getFlashBag()->add(
                'warning',
                $this->container->get('translator')->trans('listing.flash.validate', array(), 'dw_listing')

            );

            return $this->redirect($url);
        }

        return $this->render(
            'DWCoreBundle:Frontend/Listing:new.html.twig',
            array(
                'listing' => $listing,
                'location' => $location,
                'form' => $form->createView(),
            )
        );

    }

    /**
     * Creates a form to create a Listing entity.
     *
     * @param Listing $listing The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Listing $listing)
    {
        $form = $this->get('form.factory')->createNamed(
            'listing',
            'listing_new',
            $listing,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_listing_new'),
            )
        );

        return $form;
    }


    /**
     * Finds and displays a Listing entity.
     *
     * @Route("/{slug}/show", name="dw_listing_show", requirements={
     *      "slug" = "[a-z0-9-]+$"
     * })
     * @Method("GET")
     * @Security("is_granted('view', listing)")
     * @ParamConverter("listing", class="DW\CoreBundle\Entity\Listing", options={"repository_method" = "findOneBySlug"})
     *
     * @param Request $request
     * @param Listing $listing
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, Listing $listing)
    {
        $page = $request->query->get('page',1);
        $maxReviewPerPage = $this->container->getParameter('dw.review_max_per_page');
        $reviews = $this->container->get('dw.review.manager')->getListingReviews($listing,$page);
        $nbResults = $reviews->count();

        // Levels
        $parentsLevels = [];
        foreach($listing->getListingListingCategories() as $llc) {
            if($llc->getCategory()->getLvl() == 2){
                $parentsLevels[$llc->getCategory()->getParent()->getName()][] = $llc->getCategory()->getName();
            }elseif($llc->getCategory()->getParent()->getId() == 5) {
                $parentsLevels[] = $llc->getCategory()->getName();
            }
        }

        return $this->render(
            'DWCoreBundle:Frontend/Listing:show.html.twig',
            array(
                'listing' => $listing,
                'reviews' => $reviews,
                'nb_results' => $nbResults,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($nbResults / $maxReviewPerPage),
                    'route' => $request->get('_route'),
                    'route_params' => $request->get('_route_params')
                ),
                'parents_levels' => $parentsLevels
            )
        );
    }

}
