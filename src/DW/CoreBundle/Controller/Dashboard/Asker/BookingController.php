<?php

namespace DW\CoreBundle\Controller\Dashboard\Asker;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\Transaction;
use DW\MessageBundle\Event\MessageEvent;
use DW\MessageBundle\Event\MessageEvents;
use DW\PaymentBundle\Entity\Orders;
use DW\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Booking Dashboard controller.
 *
 * @Route("/asker/booking")
 */
class BookingController extends Controller
{

    /**
     * Lists all booking entities.
     *
     * @Route("/{page}", name="dw_dashboard_booking_asker", defaults={"page" = 1})
     * @Method("GET")
     *
     * @param  Request $request
     * @param  int     $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page)
    {
        $filterForm = $this->createBookingFilterForm();
        $filterForm->handleRequest($request);

        $status = $request->query->get('status');
        $bookingManager = $this->get('dw.booking.manager');
        $bookings = $bookingManager->findByAsker(
            $this->getUser()->getId(),
            $page,
            array($status)
        );

        return $this->render(
            'DWCoreBundle:Dashboard/Booking:index.html.twig',
            array(
                'bookings' => $bookings,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($bookings->count() / $bookingManager->maxPerPage),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                ),
                'filterForm' => $filterForm->createView(),
                'nav_active' => 'bookings'
            )
        );

    }


    /**
     * Finds and displays a Booking entity.
     *
     * @Route("/{id}/show", name="dw_dashboard_booking_show_asker", requirements={
     *      "id" = "\d+",
     * })
     * @Method({"GET", "POST"})
     * @Security("is_granted('view_as_asker', booking)")
     * @ParamConverter("booking", class="DW\CoreBundle\Entity\Booking")
     *
     * @param Request $request
     * @param Booking $booking
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, Booking $booking)
    {
        $threadObj = $booking->getThread();
        /** @var Form $form */
        $form = $this->container->get('fos_message.reply_form.factory')->create($threadObj);
        $paramArr = $request->get($form->getName());
        $request->request->set($form->getName(), $paramArr);

        $formHandler = $this->container->get('fos_message.reply_form.handler');

        $form->handleRequest($request);

        if ('POST' == $request->getMethod() && $form->isValid() ) {

            $this->get('dw_message.helper')->cleanFormMessage($form->getData());

            $message = $formHandler->processValidForm($form);

            $selfUrl = $this->container->get('router')->generate(
                'dw_dashboard_booking_show_asker',
                array('id' => $booking->getId())
            );

            $recipients = $threadObj->getOtherParticipants($this->getUser());
            $recipient = (count($recipients) > 0) ? $recipients[0] : $this->getUser();
            $this->container->get('dw_user.mailer.twig_swift')
                ->sendNotificationForNewMessageToUser($recipient, $threadObj);

            $this->container->get('dw_user.mailer.twig_swift')
                ->sendNotificationMessageToAdmin($booking, $threadObj);

            return new RedirectResponse($selfUrl);
        }

        // Levels
        $parentsLevels = [];
        foreach($booking->getListing()->getListingListingCategories() as $llc) {
            if($llc->getCategory()->getLvl() == 2){
                $parentsLevels[$llc->getCategory()->getParent()->getName()][] = $llc->getCategory()->getName();
            }elseif($llc->getCategory()->getParent()->getId() == 5) {
                $parentsLevels[] = $llc->getCategory()->getName();
            }
        }

        return $this->render(
            'DWCoreBundle:Dashboard/Booking:show.html.twig',
            array(
                'booking' => $booking,
                'form' => $form->createView(),
                'other_user_rating' => $booking->getListing()->getUser()->getAverageOffererRating(),
                'amount_total' => $booking->getAmountToPayByAskerDecimal(),
                'vat_inclusion_text' => $this->get('dw.twig.core_extension')
                    ->vatInclusionText(true, true),
                'navactive' => 'bookings',
                'parents_levels' => $parentsLevels
            )
        );
    }


    /**
     * Edit a Booking entity. (Cancel)
     *
     * @Route("/{id}/edit/{type}", name="dw_dashboard_booking_edit_asker", requirements={
     *      "id" = "\d+",
     *      "type" = "cancel",
     * })
     * @Method({"GET", "POST"})
     * @Security("is_granted('edit_as_asker', booking)")
     * @ParamConverter("booking", class="DW\CoreBundle\Entity\Booking")
     *
     * @param Request $request
     * @param Booking $booking
     * @param string  $type The edition type (cancel)
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Booking $booking, $type)
    {
        $bookingHandler = $this->get('dw.form.handler.booking.asker.dashboard');
        $bookingRefundManger = $this->get('dw.booking_payin_refund.manager');
        $form = $this->createEditForm($booking, $type);

        $success = $bookingHandler->process($form);

        $translator = $this->container->get('translator');
        $session = $this->container->get('session');
        if ($success == 1) {
            $url = $this->generateUrl(
                'dw_dashboard_booking_edit_asker',
                array(
                    'id' => $booking->getId(),
                    'type' => $type
                )
            );

            $session->getFlashBag()->add(
                'success',
                $translator->trans('booking.edit.success', array(), 'dw_booking')
            );
            if($request->isXmlHttpRequest()){
                return new JsonResponse(['success' => true]);
            }
            return $this->redirect($url);
        }

        return $this->render(
            'DWCoreBundle:Dashboard/Booking:edit.html.twig',
            array(
                'booking' => $booking,
                'type' => $type,
                'form' => $form->createView(),
                'other_user' => $booking->getListing()->getUser(),
                'other_user_rating' => $booking->getListing()->getUser()->getAverageOffererRating(),
                'amount_total' => $bookingRefundManger->getAmountDecimalToRefundOrRefundedToAsker($booking),
                'vat_inclusion_text' => $this->get('dw.twig.core_extension')
                    ->vatInclusionText(true, true)
            )
        );
    }

    /**
     * Creates a form to edit a Booking entity.
     *
     * @param Booking $booking The entity
     * @param string  $type    The edition type (accept or refuse)
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Booking $booking, $type)
    {
        $form = $this->get('form.factory')->createNamed(
            'booking',
            'booking_edit',
            $booking,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl(
                    'dw_dashboard_booking_edit_asker',
                    array(
                        'id' => $booking->getId(),
                        'type' => $type,
                    )
                ),
            )
        );

        return $form;
    }

    /**
     * Creates a form to filter bookings
     *
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createBookingFilterForm()
    {
        $form = $this->get('form.factory')->createNamed(
            '',
            'booking_status_filter',
            null,
            array(
                'action' => $this->generateUrl(
                    'dw_dashboard_booking_asker',
                    array('page' => 1)
                ),
                'method' => 'GET',
            )
        );

        return $form;
    }

    /**
     *
     * @Route("/{id}/show-voucher", name="dw_dashboard_booking_show_voucher", requirements={
     *      "id" = "\d+"
     * })
     * @Method("GET")
     *
     * @param Request $request
     * @param Booking $booking
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showVoucherAction(Request $request, Booking $booking)
    {
        return $this->render(
            'DWCoreBundle:Dashboard/Booking:show_voucher.html.twig',
            array(
                'booking' => $booking
            )
        );
    }

}
