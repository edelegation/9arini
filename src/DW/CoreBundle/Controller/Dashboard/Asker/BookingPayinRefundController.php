<?php

namespace DW\CoreBundle\Controller\Dashboard\Asker;

use DW\CoreBundle\Entity\BookingPayinRefund;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Booking Payin Refund Dashboard controller.
 *
 * @Route("/asker/booking-payin-refund")
 */
class BookingPayinRefundController extends Controller
{

    /**
     * Lists all booking payin refund.
     *
     * @Route("/{page}", name="dw_dashboard_booking_payin_refund_asker", defaults={"page" = 1})
     * @Method("GET")
     *
     * @param  Request $request
     * @param  int     $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page)
    {
        $bookingPayinRefundManager = $this->get('dw.booking_payin_refund.manager');
        $bookingPayinRefunds = $bookingPayinRefundManager->findByAsker(
            $this->getUser()->getId(),
            $page,
            array(BookingPayinRefund::STATUS_TO_DO,BookingPayinRefund::STATUS_PAYED)
        );

        return $this->render(
            'DWCoreBundle:Dashboard/BookingPayinRefund:index.html.twig',
            array(
                'booking_payin_refunds' => $bookingPayinRefunds,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($bookingPayinRefunds->count() / $bookingPayinRefundManager->maxPerPage),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                )
            )
        );
    }


    /**
     * Show booking Payin Refund bill.
     *
     * @Route("/{id}/show-bill", name="dw_dashboard_booking_payin_refund_show_bill_asker", requirements={"id" = "\d+"})
     * @Method("GET")
     *
     * @param  Request $request
     * @param  int     $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showBillAction(Request $request, $id)
    {
        $bookingPayinRefundManager = $this->get('dw.booking_payin_refund.manager');
        $bookingPayinRefund = $bookingPayinRefundManager->findOneByAsker(
            $id,
            $this->getUser()->getId(),
            array(BookingPayinRefund::STATUS_PAYED)
        );

        if (!$bookingPayinRefund) {
            throw $this->createNotFoundException('Bill not found');
        }

        return $this->render(
            'DWCoreBundle:Dashboard/BookingPayinRefund:show_bill.html.twig',
            array(
                'booking_payin_refund' => $bookingPayinRefund,
            )
        );
    }


}
