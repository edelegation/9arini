<?php

namespace DW\CoreBundle\Controller\Dashboard\Asker;

use DW\CoreBundle\Entity\Booking;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Booking Payin Dashboard controller.
 *
 * @Route("/asker/booking-payin")
 */
class BookingPayinController extends Controller
{

    /**
     * Lists all booking payin.
     *
     * @Route("/{page}", name="dw_dashboard_booking_payin_asker", defaults={"page" = 1})
     * @Method("GET")
     *
     * @param  Request $request
     * @param  int     $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page)
    {
        $bookingManager = $this->get('dw.booking.manager');
        $bookings = $bookingManager->findPayedByAsker(
            $this->getUser()->getId(),
            $page,
            array(Booking::STATUS_PAYED, Booking::STATUS_CANCELED_ASKER)
        );

        return $this->render(
            'DWCoreBundle:Dashboard/BookingPayin:index.html.twig',
            array(
                'bookings' => $bookings,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($bookings->count() / $bookingManager->maxPerPage),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                )
            )
        );
    }


    /**
     * Show booking Bank Wire bill.
     *
     * @Route("/{id}/show-bill", name="dw_dashboard_booking_payin_show_bill_asker", requirements={"id" = "\d+"})
     * @Method("GET")
     *
     * @param  Request $request
     * @param  int     $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showBillAction(Request $request, $id)
    {
        $bookingManager = $this->get('dw.booking.manager');
        $booking = $bookingManager->findOneByAsker(
            $id,
            $this->getUser()->getId(),
            array(Booking::STATUS_PAYED, Booking::STATUS_CANCELED_ASKER)
        );

        if (!$booking) {
            throw $this->createNotFoundException('Bill not found');
        }

        return $this->render(
            'DWCoreBundle:Dashboard/BookingPayin:show_bill.html.twig',
            array(
                'booking' => $booking,
            )
        );
    }


}
