<?php

namespace DW\CoreBundle\Controller\Dashboard\Offerer;

use DW\CoreBundle\Entity\Listing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Listing Dashboard controller.
 *
 * @Route("/listing")
 */
class ListingPresentationController extends Controller
{
    /**
     * Edits Listing presentation.
     *
     * @Route("/{id}/edit_presentation", name="dw_dashboard_listing_edit_presentation", requirements={"id" = "\d+"})
     * @Security("is_granted('edit', listing)")
     * @ParamConverter("listing", class="DWCoreBundle:Listing")
     *
     * @Method({"GET", "PUT", "POST"})
     *
     * @param Request $request
     * @param         $listing
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editPresentationAction(Request $request, Listing $listing)
    {
        $translator = $this->get('translator');
        $editForm = $this->createEditPresentationForm($listing);
        $editForm->handleRequest($request);

        $selfUrl = $this->generateUrl(
            'dw_dashboard_listing_edit_presentation',
            array(
                'id' => $listing->getId()
            )
        );
        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->get("dw.listing.manager")->save($listing);

            $this->get('session')->getFlashBag()->add(
                'success',
                $translator->trans('listing.edit.success', array(), 'dw_listing')

            );

            $this->get('session')->getFlashBag()->add(
                'warning',
                $translator->trans('listing.flash.validate', array(), 'dw_listing')

            );

            return $this->redirect($selfUrl);
        }

        return $this->render(
            'DWCoreBundle:Dashboard/Listing:edit_presentation.html.twig',
            array(
                'listing' => $listing,
                'form' => $editForm->createView()
            )
        );

    }

    /**
     * Creates a form to edit a Listing entity.
     *
     * @param Listing $listing The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditPresentationForm(Listing $listing)
    {
        $form = $this->get('form.factory')->createNamed(
            'listing',
            'listing_edit_description',
            $listing,
            array(
                'action' => $this->generateUrl(
                    'dw_dashboard_listing_edit_presentation',
                    array('id' => $listing->getId())
                ),
                'method' => 'POST',
            )
        );

        return $form;
    }

}
