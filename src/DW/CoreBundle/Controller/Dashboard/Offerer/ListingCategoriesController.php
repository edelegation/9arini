<?php

namespace DW\CoreBundle\Controller\Dashboard\Offerer;

use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Model\ListingEditCategoriesRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Listing Dashboard category controller.
 *
 * @Route("/listing")
 */
class ListingCategoriesController extends Controller
{
    /**
     * @param Listing $listing
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCategoriesForm(Listing $listing)
    {
        $form = $this->get('form.factory')->createNamed(
            'categories',
            'listing_edit_categories',
            $listing,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl(
                    'dw_dashboard_listing_edit_categories',
                    array('id' => $listing->getId())
                )
            )
        );

        return $form;
    }

    /**
     * Edit Listing categories.
     *
     * @Route("/{id}/edit_categories", name="dw_dashboard_listing_edit_categories", requirements={"id" = "\d+"})
     * @Security("is_granted('edit', listing)")
     * @ParamConverter("listing", class="DWCoreBundle:Listing")
     *
     * @Method({"POST", "GET"})
     *
     * @param Request $request
     * @param         $listing
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editCategoriesAction(Request $request, Listing $listing)
    {

        $form = $this->createCategoriesForm($listing);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listingHandler = $this->get('dw.form.handler.listing');
            $listingHandler->addCategories($listing);

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('listing.edit.success', array(), 'dw_listing')
            );

            $selfUrl = $this->generateUrl(
                'dw_dashboard_listing_edit_categories',
                array('id' => $listing->getId())
            );

            return $this->redirect($selfUrl);
        }

        return $this->render(
            'DWCoreBundle:Dashboard/Listing:edit_categories.html.twig',
            array(
                'listing' => $listing,
                'form' => $form->createView()
            )
        );
    }

}
