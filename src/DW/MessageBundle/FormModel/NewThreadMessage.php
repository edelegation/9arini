<?php

namespace DW\MessageBundle\FormModel;

use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\Listing;
use FOS\MessageBundle\FormModel\AbstractMessage;
use FOS\MessageBundle\Model\ParticipantInterface;

class NewThreadMessage extends AbstractMessage
{

    /**
     * The user who receives the message
     *
     * @var ParticipantInterface
     */
    protected $recipient;

    /**
     * The thread subject
     *
     * @var string
     */
    protected $subject;

    /**
     * The thread listing
     *
     * @var string
     */
    protected $listing;

    /**
     * The thread booking
     *
     * @var string
     */
    protected $booking;

    /**
     * @return Listing
     */
    public function getListing()
    {
        return $this->listing;
    }

    /**
     * @param  Listing $listing
     * @return null
     */
    public function setListing(Listing $listing = null)
    {
        $this->listing = $listing;
    }

    /**
     * @return Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @param  Booking $booking
     * @return null
     */
    public function setBooking(Booking $booking = null)
    {
        $this->booking = $booking;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param  string
     * @return null
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return ParticipantInterface
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param  ParticipantInterface
     * @return null
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }
}
