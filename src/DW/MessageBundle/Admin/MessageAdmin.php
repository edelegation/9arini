<?php

namespace DW\MessageBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;


class MessageAdmin extends Admin
{
    protected $translationDomain = 'SonataAdminBundle';
    protected $baseRoutePattern = 'message';

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'sender',
                null,
                array(
                    'read_only' => true,
                    'disabled' => true,
                ),
                array()
            )
            ->add(
                'createdAt',
                null,
                array(
                    'read_only' => true,
                    'disabled' => true,
                ),
                array()
            )
            ->add('body', null, array(), array())
            ->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}
