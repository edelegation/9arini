<?php

namespace DW\MessageBundle\Controller\Dashboard;

use Doctrine\ORM\Tools\Pagination\Paginator;
use DW\CoreBundle\Entity\Listing;
use DW\MessageBundle\Entity\Message;
use DW\MessageBundle\Repository\MessageRepository;
use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\MessageBundle\Model\ThreadInterface;
use FOS\MessageBundle\Provider\ProviderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Message controller.
 *
 * @Route("/message")
 */
class MessageController extends Controller
{

    /**
     * lists the available messages
     *
     * @Method("GET")
     * @Route("/{page}", name="dw_dashboard_message", requirements={"page" = "\d+"}, defaults={"page" = 1})
     *
     * @param Request $request
     * @param Integer $page
     * @return RedirectResponse
     */
    public function indexAction(Request $request, $page)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof ParticipantInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $userType = $request->getSession()->get('profile', 'asker');
        $threadManager = $this->container->get('dw_message.thread_manager');
        $threads = $threadManager->getListingInboxThreads($user, $userType, $page);

        return $this->render(
            'DWMessageBundle:Dashboard/Message:inbox.html.twig',
            array(
                'threads' => $threads,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($threads->count() / $threadManager->maxPerPage),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                ),
            )
        );

    }

    /**
     * Creates a new message thread.
     *
     * @Route("/{listingId}/new", name="dw_dashboard_message_new", requirements={"listingId" = "\d+"})
     * @param Request $request
     * @param         $listingId
     * @return RedirectResponse
     */
    public function newThreadAction(Request $request, $listingId)
    {
        $em = $this->container->get('doctrine')->getManager();

        /** @var Form $form */
        $form = $this->container->get('fos_message.new_thread_form.factory')->create();

        /** @var Listing $listing */
        $listing = $em->getRepository('DWCoreBundle:Listing')->find($listingId);

        /** @var \DW\MessageBundle\Entity\Thread $thread */
        $thread = $form->getData();
        $thread->setListing($listing);
        $thread->setSubject($listing->getTitle());
        $thread->setRecipient($listing->getUser());
        $form->setData($thread);

        $formHandler = $this->container->get('fos_message.new_thread_form.handler');
        /** @var Message $message */
        if ($message = $formHandler->process($form)) {
            $this->container->get('dw_user.mailer.twig_swift')
                ->sendNotificationForNewMessageToUser($listing->getUser(), $message->getThread());

            return new RedirectResponse(
                $this->container->get('router')
                    ->generate(
                        'dw_dashboard_message_thread_view',
                        array(
                            'threadId' => $message->getThread()->getId()
                        )
                    )
            );
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->get('dw.helper.global')->addFormErrorMessagesToFlashBag(
                $form,
                $this->get('session')->getFlashBag()
            );

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->container->get('templating')->renderResponse(
            'DWMessageBundle:Dashboard/Message:newThread.html.twig',
            array(
                'form' => $form->createView(),
                'thread' => $form->getData(),
                'listing' => $listing
            )
        );
    }

    /**
     * Displays a thread, also allows to reply to it.
     * @Route("/conversation/{threadId}", name="dw_dashboard_message_thread_view", requirements={"threadId" = "\d+"})
     *
     * @param Request $request
     * @param         $threadId
     * @return RedirectResponse
     */
    public function threadAction(Request $request, $threadId)
    {
        /* @var $threadObj \DW\MessageBundle\Entity\Thread */
        $threadObj = $this->getProvider()->getThread($threadId);
        /** @var Form $form */
        $form = $this->container->get('fos_message.reply_form.factory')->create($threadObj);
        $paramArr = $request->get($form->getName());
        $request->request->set($form->getName(), $paramArr);

        $formHandler = $this->container->get('fos_message.reply_form.handler');

        $selfUrl = $this->container->get('router')->generate(
            'dw_dashboard_message_thread_view',
            array('threadId' => $threadObj->getId())
        );

        if ($formHandler->process($form)) {
            $recipients = $threadObj->getOtherParticipants($this->getUser());
            $recipient = (count($recipients) > 0) ? $recipients[0] : $this->getUser();
            $this->container->get('dw_user.mailer.twig_swift')
                ->sendNotificationForNewMessageToUser($recipient, $threadObj);

            return new RedirectResponse($selfUrl);
        }

        return $this->container->get('templating')->renderResponse(
            'DWMessageBundle:Dashboard/Message:thread.html.twig',
            array(
                'form' => $form->createView(),
                'thread' => $threadObj
            )
        );
    }

    /**
     * Deletes a thread
     * @Route("/delete/{threadId}", name="dw_dashboard_message_thread_delete", requirements={"threadId" = "\d+"})
     *
     * @param string $threadId the thread id
     *
     * @return RedirectResponse
     */
    public function deleteAction($threadId)
    {
        /** @var ThreadInterface $thread */
        $thread = $this->getProvider()->getThread($threadId);
        $this->container->get('fos_message.deleter')->markAsDeleted($thread);
        $this->container->get('fos_message.thread_manager')->saveThread($thread);

        return new RedirectResponse(
            $this->container->get('router')->generate('dw_dashboard_message')
        );
    }

    /**
     * Gets the provider service
     *
     * @return ProviderInterface
     */
    protected function getProvider()
    {
        return $this->container->get('fos_message.provider');
    }

    /**
     * Get number of unread messages for user
     *
     * @param Request $request
     *
     * @Route("/get-nb-unread-messages", name="dw_dashboard_message_nb_unread")
     *
     * @return Response
     */
    public function nbUnReadMessagesAction(Request $request)
    {
        $noty = $this->get('dw.noty_service');
        if ($request->isXmlHttpRequest()) {
            $user = $this->getUser();
            $notif = $noty->getNotifications($user);
//            $em = $this->container->get('doctrine')->getManager();
//            /** @var MessageRepository $repo */
//            $repo = $em->getRepository('DWMessageBundle:Message');
//            $nbMessages = $repo->getNbUnreadMessage($user, true);
//
//            $response['asker'] = ($nbMessages[0]['asker']) ? $nbMessages[0]['asker'] : 0;
//            $response['offerer'] = $nbMessages[0]['offerer'] ? $nbMessages[0]['offerer'] : 0;
//            $response['total'] = $response['asker'] + $response['offerer'];

        }

        return new Response(json_encode($notif));
    }

}
