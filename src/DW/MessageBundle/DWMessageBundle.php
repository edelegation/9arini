<?php

namespace DW\MessageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * FOS Message bundle (extended)
 *
 */
class DWMessageBundle extends Bundle
{

    public function getParent()
    {
        return 'FOSMessageBundle';
    }

}
