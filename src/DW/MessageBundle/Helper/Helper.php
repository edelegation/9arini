<?php

namespace DW\MessageBundle\Helper;

use FOS\MessageBundle\FormModel\ReplyMessage;
use FOS\MessageBundle\Model\MessageInterface;

class Helper
{
    public function cleanFormMessage($message)
    {
        if(is_object($message)) {
            /** @var MessageInterface|ReplyMessage $message */
            $bodyMessage = $this->cleanMessage($message->getBody());
            $message->setBody($bodyMessage);
        }
    }

    /**
     * @param $bodyMessage
     * @return mixed
     */
    public function cleanMessage($bodyMessage)
    {
        // remove email
        $bodyMessage = preg_replace('/([a-zA-Z0-9_\-\.]+)(([\-\.\[\]\(\{\}\) ]*)@([\-\.\[\]\(\{\}\) ]*)|([\-\.\[\]\(\{\}\) ]*)at([\-\.\[\]\(\{\}\) ]*))((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\- ]+\.)+))([a-zA-Z ]{2,4}|[0-9]{1,3})(\]?)/', '[supprimé]', $bodyMessage);

        // remove phone
        /* $message = preg_replace('/([0-9]{3}[\- .]?[0-9]+)/','[supprimé]',$message); */
        $bodyMessage = preg_replace('/([0-9_\-\.\[\{\(\)\}\] ]{3})([0-9_\-\.\[\{\(\)\}\] ]+)/', '[supprimé]', $bodyMessage);
        return $bodyMessage;
    }
}