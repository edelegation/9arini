<?php

namespace DW\MessageBundle\SMS;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiSMS
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityManager
     */
    private $manager;

    private $apiParams;

    /**
     * SmsMessage constructor.
     * @param RequestStack $requestStack
     * @param EntityManager $manager
     * @param $apiParams
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $manager,
        $apiParams
    )
    {
        $this->requestStack = $requestStack;
        $this->manager = $manager;
        $this->apiParams = $apiParams;
    }

    /**
     * @param int $mobile
     * @param string $sms
     * @param $date
     * @param null $sender
     * @return mixed $response
     */
    public function sendSMS($mobile, $sms, $date, $sender = null)
    {
        $url = $this->init($mobile, $sms, $date, $sender);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        //dump($response); die;
        return $response;

    }


    /**
     * @param $mobile
     * @param $sms
     * @param $sender
     * @param $date
     * @return string
     */
    private function init($mobile, $sms, $date, $sender)
    {
        $url = $this->apiParams['url'];
        $url .= "&key=" . $this->apiParams['key'];
        $url .= "&mobile=216" . $mobile;
        $url .= "&sms=" . $sms;

        if (is_null($sender)) {
            $url .= "&sender=" . $this->apiParams['sender'];
        } else {
            $url .= "&sender=" . $sender;
        }

        $url .= "&date=" . $date->format('d/m/Y');
        $url .= "&heure=" . $date->format('H:i:s');

        // Encode
        $url = str_replace(" ","%20",$url);

        return $url;
    }
}