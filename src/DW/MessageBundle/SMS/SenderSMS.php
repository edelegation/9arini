<?php

namespace DW\MessageBundle\SMS;


use Doctrine\ORM\EntityManagerInterface;
use DW\MessageBundle\Entity\Sms;
use DW\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SenderSMS
{
    /**
     * @var ApiSMS
     */
    private $apiSMS;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    private $messageId;

    /**
     * SenderSMS constructor.
     * @param ApiSMS $apiSMS
     * @param UserManagerInterface $userManager
     * @param EntityManagerInterface $manager
     * @param LoggerInterface $logger
     * @param TranslatorInterface $translator
     */
    public function __construct(
        ApiSMS $apiSMS,
        UserManagerInterface $userManager,
        EntityManagerInterface $manager,
        LoggerInterface $logger,
        TranslatorInterface $translator
    )
    {
        $this->apiSMS = $apiSMS;
        $this->userManager = $userManager;
        $this->manager = $manager;
        $this->logger = $logger;
        $this->translator = $translator;
    }

    public function sendCodeConfirmationPhone(User $user, $flush = false)
    {
        if ($user->getAttemptConfirmationPhone() < 2) {

            $code = mt_rand(100000, 999999);
            $msg = "Votre code de confirmation numéro est : " . $code;

            $date =  new \Datetime();

            $response = $this->apiSMS->sendSMS($user->getPhone(), $msg, $date, 'TunSMS Test');

            if ( $this->checkResponse($response) ) {

                // Save Data SMS
                $sms = new Sms();
                $sms->setMessage($msg);
                $sms->setMessageId($this->messageId);
                $sms->setSentAt($date);

                $this->manager->persist($sms);
                $this->manager->flush();

                // Update Data User

                $user->setPhoneVerified(false);
                $user->setConfirmationTokenPhone($code);
                $user->setAttemptConfirmationPhone($user->getAttemptConfirmationPhone() + 1);

                if ($flush) {
                    $this->userManager->updateUser($user);
                }

                return 'success';
            };

            return 'error';
        }

        return 'warning';
    }

    public function getMessageResultConfirmationPhone($result)
    {
        switch ($result) {
            case 'success':
                $notice     = ['type' => 'success', 'msg' => $this->translator->trans('user.edit.contact.confirm_phone_success', array(), 'dw_user')];
                break;
            case 'warning':
                $notice     = ['type' => 'warning', 'msg' => $this->translator->trans('user.edit.contact.confirm_phone_warning', array(), 'dw_user')];
                break;
            case 'exist':
                $notice     = ['type' => 'error', 'msg' => $this->translator->trans('user.edit.contact.confirm_phone_exist', array(), 'dw_user')];
                break;
            default:
                $notice     = ['type' => 'error', 'msg' => $this->translator->trans('user.edit.contact.confirm_phone_error', array(), 'dw_user')];
                break;
        }

        return $notice;
    }

    private function checkResponse($xml)
    {
        libxml_use_internal_errors(true);

        $objectSimpleXml = simplexml_load_string($xml);

        $errors = libxml_get_errors();

        if ($objectSimpleXml) {

            $this->logger->info('SMS status Code ' . $objectSimpleXml->status->status_code);

            if($objectSimpleXml->status->status_code == 200) {

                $this->messageId = $objectSimpleXml->status->message_id;

                return true;
            }
        } else {

            $this->logger->error('Errors are '.var_export($errors, true));
            $this->logger->error('invalid XML');
        }

        return false;
    }
}