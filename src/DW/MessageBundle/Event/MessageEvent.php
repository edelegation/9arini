<?php

namespace DW\MessageBundle\Event;

use FOS\MessageBundle\FormModel\ReplyMessage;
use FOS\MessageBundle\Model\MessageInterface;
use Symfony\Component\EventDispatcher\Event;

class MessageEvent extends Event
{
    /**
     * @var MessageInterface|ReplyMessage
     */
    private $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * @return MessageInterface|ReplyMessage
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param MessageInterface|ReplyMessage $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
