<?php

namespace DW\MessageBundle\Event;

/**
 * Declares all events thrown in the MessageBundle.
 */
final class MessageEvents
{
    /**
     * The PRE_SEND event occurs before a message has been sent
     * The event is an instance of DW\MessageBundle\Event\MessageEvent.
     *
     * @var string
     */
    const PRE_SEND = 'fos_message.pre_send';
}
