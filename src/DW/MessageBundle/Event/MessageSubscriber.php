<?php

namespace DW\MessageBundle\Event;

use DW\MessageBundle\Helper\Helper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MessageSubscriber implements EventSubscriberInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * MessageSubscriber constructor.
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    public function onPreMessage(MessageEvent $event)
    {
        $message        = $event->getMessage();
        $bodyMessage    = $this->helper->cleanMessage($message->getBody());

        $message->setBody($bodyMessage);
        $event->setMessage($message);
    }


    public static function getSubscribedEvents()
    {
        return array(
            MessageEvents::PRE_SEND => array('onPreMessage', 1)
        );
    }

}