<?php

namespace DW\ConfigBundle\DataFixtures\ORM;

use DW\ConfigBundle\Entity\Parameter;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadParameterData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var  ContainerInterface container */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $parameters = $this->container->getParameter('dw_config.parameters_allowed');
        if (is_array($parameters) && count($parameters)) {
            foreach ($parameters as $parameterName) {
                $parameter = new Parameter();
                $parameter->setName($parameterName);
                $parameter->setValue(null);
                $manager->persist($parameter);
            }

            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }

}
