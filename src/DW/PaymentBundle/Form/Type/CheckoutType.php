<?php

namespace DW\PaymentBundle\Form\Type;

use DW\PaymentBundle\Entity\Orders;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CheckoutType extends AbstractType
{
    /**
     * @var
     */
    private $pointID;
    /**
     * @var
     */
    private $remoteID;
    /**
     * @var
     */
    private $testMode;
    /**
     * @var
     */
    private $successURL;
    /**
     * @var
     */
    private $failureURL;
    /**
     * @var
     */
    private $culture;

    /**
     * @var Container $container
     */
    private $container;


    private $amoutType;


    /**
     * CheckoutType constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->pointID = $this->container->getParameter('runpay_sys_point_id');
        $this->remoteID = $this->container->getParameter('runpay_sys_invoice_remote_id');
        $this->testMode = $this->container->getParameter('runpay_sys_test_mode');
        $this->successURL = $this->container->get('router')->generate('clictopay_success',[],0);
        $this->failureURL = $this->container->get('router')->generate('clictopay_failure',[],0);
        $this->culture = $this->container->getParameter('runpay_sys_culture');
        $this->amoutType = $this->container->getParameter('runpay_sys_amount_type');

    }


    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Orders $order */
        $order = $builder->getData();

        $builder
            ->add(
                'SYS_POINT_ID',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->pointID
                )
            )->add(
                'SYS_INVOICE_REMOTE_ID',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->remoteID
                )
            )->add(
                'SYS_TEST_MODE',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->testMode
                )
            )->add(
                'SYS_SUCCESS_URL',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->successURL
                )
            )->add(
                'SYS_FAILURE_URL',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->failureURL
                )
            )->add(
                'SYS_CULTURE',
                'hidden',
                array(
                    'mapped' => false,
                    'data' => $this->culture
                )
            )->add(
                'SYS_INVOICE_AMOUNT_TYPE',
                'hidden',
                ['data' => $this->amoutType,]
            );

    }

    public function getName()
    {
        return null;
    }
}
