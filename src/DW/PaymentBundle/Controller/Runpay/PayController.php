<?php

namespace DW\PaymentBundle\Controller\Runpay;

use DW\CoreBundle\Entity\Booking;
use DW\PaymentBundle\Entity\Orders;
use DW\PaymentBundle\Form\Type\CheckoutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PayController extends Controller
{

    /**
     * @Route("/checkout", name="runpay_checkout")
     * @Method({"GET"})
     */
    public function checkoutAction()
    {
        $form = $this->createCheckoutForm();
        $form->add('SYS_MESSAGE','hidden',array('data' => 'Payement du cours | 9arini.tn'));
        $form->add('SYS_PRODUCT','hidden',array('data' => 'Payement du cours ddddd | 9arini.tn'));
        $form->add('SYS_INVOICE_AMOUNT_VALUE','hidden',array('data' => 500));
        return $this->render('DWPaymentBundle:Runpay:checkout.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @param Orders $order
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createCheckoutForm($order = null)
    {
        $container = $this->get('service_container');
        $form = $this->get('form.factory')->createNamed(
            null,
            new CheckoutType($container),
            $order,
            array(
                'method' => 'POST',
                'action' => $this->container->getParameter('runpay_payment_url')
            )
        );

        return $form;
    }
}
