<?php

namespace DW\PaymentBundle\Controller\Runpay;

use DW\CoreBundle\Entity\Booking;
use DW\PaymentBundle\Entity\Orders;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{

    /**
     * @Route("/process/{api_key}/{tid}", name="runpay_api_process")
     * @Method({"GET"})
     * return json object with all data of order
     */
    public function processAction()
    {
        $apiManager = $this->get('dw.api_manager');
        $json = array('DATA' => ['ERROR' => 'API KEY NOT AUTHORIZED']);
        if($apiManager->checkApiKey()){
            $order = $apiManager->getOrderObject();
            $json = array('DATA' => ['ERROR' => 'TRANSACTION NOT FOUND']);
            if($order){
                $json = array('DATA' => ['ERROR' => 'TRANSACTION ALREADY PAID']);
                if(!$order->getStatus()){
                    /** @var Booking $booking */
                    $booking = $order->getBooking();
                    $json = array(
                        'DATA' => [
                            'TRANSACTION_ID' => $apiManager->getTransactionId(),
                            'SYS_INVOICE_AMOUNT_VALUE' => $booking->getAmountTotal(),
                            'SYS_INVOICE_AMOUNT_TYPE' => 'TND',
                            'SYS_PRODUCT' => [
                                'ASKER' => $booking->getUser()->getFullName(),
                                'OFFERER' => $order->getBooking()->getListing()->getUser()->getFullName(),
                                'LISTING_TITLE' => $booking->getListing()->getTitle(),
                                'LISTING_CERTIFIED' => $booking->getListing()->getCertified(),
                            ],
                            'SYS_MESSAGE' => '9ARINI PAYEMENT',
                            'SYS_PAYMENT_STATE' => $order->getStatus(),
                            'TOKEN' => $order->getToken()
                        ]
                    );
                }
            }
        }

        return new JsonResponse($json);
    }

    /**
     * @Route("/confirm/{api_key}/{tid}/{token}", name="runpay_api_confirm")
     * @Method({"GET"})
     * return json object with status of order
     */
    public function confirmAction()
    {
        $apiManager = $this->get('dw.api_manager');
        $json = array('DATA' => ['ERROR' => 'API KEY NOT AUTHORIZED']);
        if($apiManager->checkApiKey()){
            $order = $apiManager->getOrderObject();
            $json = array('DATA' => ['ERROR' => 'TRANSACTION NOT FOUND']);
            if($order){
                $json = array('DATA' => ['ERROR' => 'TOKEN NOT VALID']);
                if($apiManager->checkToken()){
                    $json = array('DATA' => ['ERROR' => 'TRANSACTION ALREADY PAID']);
                    if(!$order->getStatus()){
                        $apiManager->processPayment($order, Orders::METHOD_RUNPAY);

                        $json = [
                            'DATA' => [
                                'SUCCESS' => 'OK',
                                'PAYMENT_ID' => $order->getId(),
                            ]
                        ];

                        //$json = array('DATA' => ['SUCCESS' => 'OK']);
                    }
                }
            }
        }

        return new JsonResponse($json);
    }
}
