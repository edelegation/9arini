<?php

namespace DW\PaymentBundle\Controller\Order;

use DW\CoreBundle\Entity\Booking;
use DW\PaymentBundle\Entity\Orders;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends Controller
{
    /**
     * Popup show methods payment
     *
     * @Route("/methods-payment/{id}", name="dw_methods_payment", requirements={
     *      "id" = "\d+"
     * })
     * @Method({"GET", "POST"})
     * @Security("is_granted('edit_as_asker', booking)")
     * @ParamConverter("booking", class="DW\CoreBundle\Entity\Booking")
     *
     * @param Booking $booking
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function methodsPaymentAction(Booking $booking)
    {
        $manager = $this->get('dw.api_manager');
        $order = $manager->getOrder($booking);

        return $this->render('DWPaymentBundle:Order:methods_payment.html.twig', array(
            'order' => $order
        ));
    }

    /**
     * @Route("/payment-clictopay/{id}", name="dw_payment_clictopay", requirements={
     *      "id" = "\d+"
     * })
     */
    public function paymentClictopayAction(Request $request, Orders $order)
    {
        $url = $this->container->getParameter('clictopay_url');
        $affilie = $this->container->getParameter('clictopay_affilie');

        $clictopay = $order->getClictopay();
        $session = $request->getSession();
        $session->set('clictopay', [
            'reference' => $clictopay->getReference(),
            'customer' => $clictopay->getCustomer(),
            'booking_id' => $order->getBooking()->getId()
        ]);

        return $this->render("DWPaymentBundle:Order:payment_clictopay.html.twig", array(
            'clictopay' => $order->getClictopay(),
            'url'       => $url,
            'affilie'   => $affilie
        ));
    }
}
