<?php

namespace DW\PaymentBundle\Controller\Clictopay;

use DW\CoreBundle\Entity\Booking;
use DW\PaymentBundle\Entity\Clictopay;
use DW\PaymentBundle\Entity\Orders;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    /**
     * @Route("/notification", name="clictopay_notification")
     */
    public function indexAction(Request $request)
    {
        $query = $request->query->all();
        $reference = $query['Reference'];
        $action = $query['Action'];

        if (empty($reference) || empty($action)) {
            $content = "";
        }

        $em = $this->getDoctrine()->getManager();
        $clictopay = $em->getRepository("DWPaymentBundle:Clictopay")->findOneBy([
            "reference" => $reference
        ]);
        $content = "";
        if($clictopay && !empty($clictopay->getTotal())) {
            switch (strtoupper($action)) {

                case "DETAIL":

                    $amount = sprintf('%.3f', $clictopay->getTotal());

                    $content = "Reference=$reference&Action=$action&Reponse=$amount";
                    break;

                case "ACCORD":

                    //$test041 = true;
                    //$response = 'NO';
                    //if(!$test041){
                        // Save Param (data returned from clictopay in GET method)
                        $clictopay->setParam(array('GET' => $query));
                        $em->persist($clictopay);
                        $em->flush();

                        // Process Payment
                        $order      = $clictopay->getOrder();
                        $manager    = $this->get('dw.api_manager');
                        $manager->processPayment($order,Orders::METHOD_CLICTOPAY);
                        $response = 'OK';
                    //}


                    $content = "Reference=$reference&Action=$action&Reponse=$response";
                    break;

                case "ERREUR":
                    $order = $clictopay->getOrder();
                    $em->remove($order);
                    $em->flush();
                    $content = "Reference=$reference&Action=$action&Reponse=OK";
                    break;
                case "REFUS":
                    $order = $clictopay->getOrder();
                    $em->remove($order);
                    $em->flush();
                    $content = "Reference=$reference&Action=$action&Reponse=OK";
                    break;
                case "ANNULATION":
                    $order = $clictopay->getOrder();
                    $em->remove($order);
                    $em->flush();
                    $content = "Reference=$reference&Action=$action&Reponse=OK";
                    break;
            }
        }

        $this->get('monolog.logger.clictopay')->info($clictopay->getClientIP() . " " . $content);

        $response = new Response($content);
        return $response;
    }

    /**
     * @Route("/success", name="clictopay_success")
     *
     */
    public function successAction(Request $request)
    {
        $params     = $request->request->all();
        $session    = $this->get('session');
        $em         = $this->getDoctrine()->getManager();

        if (in_array('Reference', $params) && in_array('sid', $params)) {

            $reference  = $params['Reference'];
            $customer   = $params['sid'];

            $clictopay  = $em->getRepository('DWPaymentBundle:Clictopay')->findOneBy([
                'reference' => $reference,
                'customer' => $customer
            ]);

            if($clictopay){

                /** @var Clictopay $clictopay */

                // Merge Param (data returned from clictopay in POST method) with current data GET
                $clictopay->setParam(array_merge(array('POST' => $params)));
                $em->persist($clictopay);
                $em->flush();

                if ($session->has('clictopay')) {
                    // Clear Session Clictopay
                    $session->remove('clictopay');
                }

                /** @var Orders $order */
                $order      = $clictopay->getOrder();

                return  $this->render("DWPaymentBundle:Clictopay/Notification:success.html.twig", [
                    'order' => $order
                ]);
            }

        } elseif ($session->has('clictopay')) {

            /** @var Clictopay $clictopay */

            $params     = $session->get('clictopay');
            $reference  = $params['reference'];
            $customer   = $params['customer'];

            $clictopay  = $em->getRepository('DWPaymentBundle:Clictopay')->findOneBy([
                'reference' => $reference,
                'customer' => $customer
            ]);

            // Clear Session Clictopay
            $session->remove('clictopay');

            /** @var Orders $order */
            $order      = $clictopay->getOrder();

            return  $this->render("DWPaymentBundle:Clictopay/Notification:success.html.twig", [
                'order' => $order
            ]);
        }

        $booking = $em->getRepository('DWCoreBundle:Booking')->find(1717852351);
        $order   = $booking->getOrder();

        return  $this->render("DWPaymentBundle:Clictopay/Notification:success.html.twig", [
            'order' => $order
        ]);
    }

    /**
     * @Route("/failure", name="clictopay_failure")
     *
     */
    public function failureAction()
    {
        $session = $this->get('session');
        $bookingID = false;
        if ($session->has('clictopay')) {

            $params     = $session->get('clictopay');
            $reference  = $params['reference'];
            $customer   = $params['customer'];
            $bookingID  = $params['booking_id'];

            $em = $this->getDoctrine()->getManager();

            $clictopay = $em->getRepository('DWPaymentBundle:Clictopay')->findOneBy([
                'reference' => $reference,
                'customer' => $customer
            ]);

            if($clictopay) {

                // Remove Order
                $order = $clictopay->getOrder();
                $em->remove($order);
                $em->flush();

                // Clear Session Clictopay
                $session->remove('clictopay');
            }

            $session->getFlashBag()->add(
                'error',
                'Échec de payement'
            );

        }

        return $this->render("DWPaymentBundle:Clictopay/Notification:failure.html.twig", [
            'booking_id' => $bookingID
        ]);
    }
}
