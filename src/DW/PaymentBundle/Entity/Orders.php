<?php

namespace DW\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity
 */
class Orders
{
    const METHOD_CLICTOPAY  = 'CLICTOPAY';
    const METHOD_RUNPAY     = 'RUNPAY';
    const STATUS_CREATED     = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=255, nullable=true)
     */
    private $method;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float")
     */
    private $total;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="purchased_at", type="datetime", nullable=true)
     */
    private $purchasedAt;

    /**
     * @ORM\ManyToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="orders", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="DW\CoreBundle\Entity\Booking", inversedBy="order")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $booking;

    /**
     * @ORM\Column(name="token", type="string", nullable=false, length=40)
     */
    private $token;

    /**
     * @ORM\Column(name="transaction_id", type="integer")
     */
    private $transactionID;

    /**
     * @var Orders
     *
     * @ORM\OneToOne(targetEntity="DW\PaymentBundle\Entity\Clictopay", mappedBy="order", cascade={"persist", "remove"})
     */
    private $clictopay;

    /**
     * Orders constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set method
     *
     * @param string $method
     *
     * @return Orders
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Orders
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Orders
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \DW\UserBundle\Entity\User $user
     *
     * @return Orders
     */
    public function setUser(\DW\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DW\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set booking
     *
     * @param \DW\CoreBundle\Entity\Booking $booking
     *
     * @return Orders
     */
    public function setBooking(\DW\CoreBundle\Entity\Booking $booking)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return \DW\CoreBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * @return \DateTime
     */
    public function getPurchasedAt()
    {
        return $this->purchasedAt;
    }

    /**
     * @param \DateTime $purchasedAt
     */
    public function setPurchasedAt($purchasedAt)
    {
        $this->purchasedAt = $purchasedAt;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getTransactionID()
    {
        return $this->transactionID;
    }

    /**
     * @param int $transactionID
     */
    public function setTransactionID($transactionID)
    {
        $this->transactionID = $transactionID;
    }

    /**
     * Set clictopay
     *
     * @param \DW\PaymentBundle\Entity\Clictopay $clictopay
     *
     * @return Orders
     */
    public function setClictopay(\DW\PaymentBundle\Entity\Clictopay $clictopay = null)
    {
        $this->clictopay = $clictopay;

        return $this;
    }

    /**
     * Get clictopay
     *
     * @return \DW\PaymentBundle\Entity\Clictopay
     */
    public function getClictopay()
    {
        return $this->clictopay;
    }
}
