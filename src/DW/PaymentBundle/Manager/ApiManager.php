<?php

namespace DW\PaymentBundle\Manager;

use DW\CoreBundle\Entity\Booking;
use Doctrine\ORM\EntityManager;
use DW\CoreBundle\Mailer\TwigSwiftMailer;
use DW\PaymentBundle\Entity\Clictopay;
use DW\PaymentBundle\Entity\Orders;
use Symfony\Component\HttpFoundation\RequestStack;

class ApiManager
{
    private $em;
    private $requestStack;
    private $request;
    private $routeParams;
    private $apiKey;
    private $apiAuth;
    private $digitTransaction;

    const API_KEY           = "api_key";
    const TRANSACTION_ID    = "tid";
    const TOKEN             = "token";
    /**
     * @var TwigSwiftMailer
     */
    private $mailer;


    /**
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     * @param TwigSwiftMailer $mailer
     * @param $apiKey
     * @param $apiAuth
     * @param $digitTransaction
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        TwigSwiftMailer $mailer,
        $apiKey, $apiAuth, $digitTransaction)
    {

        $this->requestStack     = $requestStack;
        $this->em               = $entityManager;
        $this->request          = $this->requestStack->getCurrentRequest();
        $this->routeParams      = $this->request->attributes->get('_route_params');
        $this->apiKey           = $apiKey;
        $this->apiAuth          = $apiAuth;
        $this->digitTransaction = $digitTransaction;
        $this->mailer           = $mailer;
    }


    public function getKey(){
        $key = array_key_exists(ApiManager::API_KEY,$this->routeParams)
            ? $this->routeParams[ApiManager::API_KEY]
            : null;
        return $key;
    }

    public function getTransactionId(){
        $transactionID = array_key_exists(ApiManager::TRANSACTION_ID,$this->routeParams)
            ? $this->routeParams[ApiManager::TRANSACTION_ID]
            : null;
        return $transactionID;
    }

    public function getToken(){
        $token = array_key_exists(ApiManager::TOKEN,$this->routeParams)
            ? $this->routeParams[ApiManager::TOKEN]
            : null;
        return $token;
    }

    public function getData(){

        return $this->routeParams;
    }

    public function checkApiKey()
    {
        return ($this->getKey() == $this->apiKey) && ($this->apiAuth == true) ;
    }

    public function checkToken()
    {
        return ($this->getToken() == $this->getOrderObject()->getToken());
    }

    /**
     * @return Orders
     */
    public function getOrderObject()
    {
        $criteria = ['transactionID' => $this->getTransactionId(), 'token' => $this->getToken()];
        if(is_null($criteria['token'])) { unset($criteria['token']); }
        return $this->em->getRepository('DWPaymentBundle:Orders')->findOneBy($criteria);
    }

    /**
     * @param $order
     */
    public function processPayment($order,$method)
    {
        /** @var Orders $order */
        $order->setStatus(true);
        $order->setPurchasedAt(new \DateTime());
        $order->setMethod($method);

        $this->persistAndFlush($order);

        $this->updateBooking($order->getBooking());

        //Mail offerer
        $this->mailer->sendPaymentSuccessToOfferer($order->getBooking());
        //Mail asker
        $this->mailer->sendPaymentSuccessToAsker($order->getBooking());

        //Mail admin
        $this->mailer->sendMessageToAdminPayment($order->getBooking());
    }

    /**
     * @return string
     */
    public function generateTransactionID()
    {
        $code = "";
        for ($i=1; $i<= $this->digitTransaction; $i++) {
            $code .= mt_rand(0,9);
        }
        return (string) $code;
    }

    private function updateBooking(Booking $booking)
    {
        $booking->setStatus(Booking::STATUS_PAYED);
        $booking->setPayedBookingAt(new \DateTime());
        $booking->setValidated(false);

        $this->persistAndFlush($booking);
    }

    private function persistAndFlush($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function getOrder(Booking $booking)
    {
        if(!$booking->getOrder() ) {
            $user       = $booking->getUser();
            $total      = sprintf('%.3f',  (float) $booking->getAmountTotalDecimal());

            /* Create new Order */
            $order = new Orders();
            $order->setUser($user);
            $order->setBooking($booking);
            $order->setTotal($total);
            $order->setTransactionID($this->generateTransactionID());
            $order->setStatus(false);
            $order->setToken(sha1($user->getUsername().$order->getTransactionID()));

            $this->persistAndFlush($order);

            /* Generate Unique Reference */
            $reference  = substr(md5(microtime()),rand(0,26),5) .$order->getId() . $user->getId();

            /* Create new Clictopay */
            $clictopay = new Clictopay();
            $clictopay->setTotal($total);
            $clictopay->setReference($reference);
            $clictopay->setCustomer($user->getId());
            $clictopay->setOrder($order);

            $this->persistAndFlush($clictopay);

            return $order;
        }else{
            if(!$booking->getOrder()->getStatus()) {
                return $booking->getOrder();
            }
        }
    }
}
