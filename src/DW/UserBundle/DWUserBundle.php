<?php

namespace DW\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DWUserBundle extends Bundle
{

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'SonataUserBundle';
    }

}
