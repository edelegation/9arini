<?php

namespace DW\UserBundle\Entity;

use DW\UserBundle\Entity\Cin;
use DW\CoreBundle\Entity\Booking;
use DW\CoreBundle\Entity\BookingBankWire;
use DW\CoreBundle\Entity\BookingPayinRefund;
use DW\CoreBundle\Entity\Listing;
use DW\MessageBundle\Entity\Message;
use DW\PaymentBundle\Entity\Clictopay;
use DW\ReviewBundle\Entity\Review;
use DW\UserBundle\Model\ListingAlertInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Entity\User as BaseUser;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 *
 * @ORM\Entity(repositoryClass="DW\UserBundle\Repository\UserRepository")
 *
 * @UniqueEntity(
 *      fields={"email"},
 *      groups={"DWRegistration", "DWProfile", "DWProfileContact", "default"},
 *      message="dw_user.email.already_used"
 * )
 *
 * @UniqueEntity(
 *      fields={"username"},
 *      groups={"DWRegistration", "DWProfile", "DWProfileContact", "default"},
 *      message="dw_user.email.already_used"
 * )
 *
 * @ORM\Table(name="`user`",indexes={
 *    @ORM\Index(name="slug_u_idx", columns={"slug"}),
 *    @ORM\Index(name="enabled_idx", columns={"enabled"}),
 *    @ORM\Index(name="email_idx", columns={"email"})
 *  })
 *
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements ParticipantInterface
{
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Sluggable\Sluggable;

    /* VERIFIED VALUES */
    const VERIFIED_DEFAULT = 0;
    const VERIFIED_PHONE = 1;
    const VERIFIED_CIN = 2;
    const VERIFIED_DIPLOMA = 3;
    const VERIFIED_CV = 4;

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="DW\CoreBundle\Model\CustomIdGenerator")
     *
     * @var integer
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\Email(message="dw_user.email.invalid", strict=true, groups={"DWRegistration", "DWProfile", "DWProfileContact"})
     *
     * @Assert\NotBlank(message="dw_user.email.blank", groups={"DWRegistration", "DWProfile", "DWProfileContact"})
     *
     * @Assert\Length(
     *     min=3,
     *     max="255",
     *     minMessage="dw_user.username.short",
     *     maxMessage="dw_user.username.long",
     *     groups={"DWRegistration", "DWProfile", "DWProfileContact"}
     * )
     */
    protected $email;

    /**
     * @ORM\Column(name="last_name", type="string", length=100)
     *
     * @Assert\NotBlank(message="dw_user.last_name.blank", groups={
     *  "DWRegistration", "DWProfile"
     * })
     *
     * @Assert\Length(
     *     min=3,
     *     max="100",
     *     minMessage="dw_user.last_name.short",
     *     maxMessage="dw_user.last_name.long",
     *     groups={"DWRegistration", "DWProfile"}
     * )
     */
    protected $lastName;

    /**
     * @ORM\Column(name="first_name", type="string", length=100)
     *
     * @Assert\NotBlank(message="dw_user.first_name.blank", groups={
     *  "DWRegistration", "DWProfile"
     * })
     *
     * @Assert\Length(
     *     min=3,
     *     max="100",
     *     minMessage="dw_user.first_name.short",
     *     maxMessage="dw_user.first_name.long",
     *     groups={"DWRegistration", "DWProfile"}
     * )
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_prefix", type="string", length=6, nullable=true)
     */
//    protected $phonePrefix = '+216';

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=16, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_second", type="string", length=16, nullable=true)
     */
    protected $phoneSecond;

    /**
     * @var \DateTime $birthday
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    protected $birthday;


    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=3, nullable=true)
     */
    protected $nationality = "TN";

    /**
     * @var string
     *
     * @ORM\Column(name="country_of_residence", type="string", length=3, nullable=true)
     *
     * @Assert\NotBlank(message="dw_user.country_of_residence.blank", groups={
     *  "DWRegistration"
     * })
     */
    protected $countryOfResidence = "TN";

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=50, nullable=true)
     */
    protected $profession;

    /**
     * @Assert\NotBlank(message="assert.not_blank", groups={"DWProfile"})
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="name_pay", type="string", length=50, nullable=true)
     *
     * @Assert\NotBlank(message="dw_user.name_pay", groups={
     *  "DWProfilePayment"
     * })
     */
    protected $namePay;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=100, nullable=true)
     *
     * @Assert\NotBlank(message="dw_user.bank_name.blank", groups={
     *  "DWProfilePayment"
     * })
     */
    protected $bankName;

    /**
     * @var string
     *
     * @ORM\Column(name="agency_name", type="string", length=100, nullable=true)
     *
     * @Assert\NotBlank(message="dw_user.agency_name.blank", groups={
     *  "DWProfilePayment"
     * })
     */
    protected $agencyName;

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=45, nullable=true)
     *
     * @Assert\NotBlank(message="dw_user.iban.blank", groups={
     *  "DWProfilePayment"
     * })
     *
     */
    protected $iban;

    /**
     * @ORM\Column(name="annual_income", type="decimal", precision=10, scale=2, nullable=true)
     *
     * @var integer
     */
    protected $annualIncome;

    /**
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "{{ limit }}dw_user.password.short",
     * )
     *
     * @var string
     */
    protected $plainPassword;

    /**
     *
     * @ORM\Column(name="phone_verified", type="boolean", nullable=true)
     *
     * @var boolean
     */
    protected $phoneVerified;

    /**
     *
     * @ORM\Column(name="email_verified", type="boolean", nullable=true)
     *
     * @var boolean
     */
    protected $emailVerified;

    /**
     *
     * @ORM\Column(name="id_card_verified", type="boolean", nullable=true)
     *
     * @var boolean
     */
    protected $idCardVerified = false;

    /**
     *
     * @ORM\Column(name="diploma_verified", type="boolean")
     *
     * @var boolean
     */
    protected $diplomaVerified = false;

    /**
     *
     * @ORM\Column(name="cv_verified", type="boolean")
     *
     * @var boolean
     */
    protected $cvVerified = false;

    /**
     *
     * @ORM\Column(name="nb_bookings_offerer", type="smallint", nullable=true)
     *
     * @var int
     */
    protected $nbBookingsOfferer;

    /**
     *
     * @ORM\Column(name="nb_bookings_asker", type="smallint", nullable=true)
     *
     * @var int
     */
    protected $nbBookingsAsker;

    /**
     *
     * @ORM\Column(name="fee_as_asker", type="smallint", nullable=true)
     *
     * @var integer Percent
     */
    protected $feeAsAsker;

    /**
     * @ORM\Column(name="fee_as_offerer", type="smallint", nullable=true)
     *
     * @var integer Percent
     */
    protected $feeAsOfferer;

    /**
     * @ORM\Column(name="average_rating_as_asker", type="smallint", nullable=true)
     *
     * @var integer
     */
    protected $averageAskerRating;


    /**
     * @ORM\Column(name="average_rating_as_offerer", type="smallint", nullable=true)
     *
     * @var integer
     */
    protected $averageOffererRating;

    /**
     * @ORM\Column(name="mother_tongue", type="string", length=5, nullable=true)
     *
     *
     * @var string
     */
    protected $motherTongue;

    /**
     * @ORM\Column(name="answer_delay", type="integer", nullable=true)
     *
     * @var integer
     */
    protected $answerDelay;

    /**
     * @ORM\OneToMany(targetEntity="DW\MessageBundle\Entity\Message", mappedBy="sender", cascade={"remove"}, orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="DW\ReviewBundle\Entity\Review", mappedBy="reviewBy", cascade={"remove"}, orphanRemoval=true)
     */
    private $reviewsBy;

    /**
     * @ORM\OneToMany(targetEntity="DW\ReviewBundle\Entity\Review", mappedBy="reviewTo", cascade={"remove"}, orphanRemoval=true)
     */
    private $reviewsTo;

    /**
     * @ORM\OneToOne(targetEntity="DW\UserBundle\Entity\UserFacebook", mappedBy="user", cascade={"remove"}, orphanRemoval=true)
     */
    private $userFacebook;

    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\Listing", mappedBy="user", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var Listing[]
     */
    private $listings;

    /**
     * @ORM\OneToMany(targetEntity="DW\UserBundle\Entity\UserAddress", mappedBy="user", cascade={"persist", "remove"})
     *
     * @var UserAddress[]
     */
    private $addresses;

    /**
     *
     * @ORM\OneToOne(targetEntity="UserImage", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position" = "asc"})
     *
     * @var UserImage
     */
    protected $image;

    /**
     * @ORM\OneToMany(targetEntity="UserLanguage", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @var UserLanguage[]
     */
    protected $languages;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\Booking", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var Booking[]
     */
    protected $bookings;


    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\BookingBankWire", mappedBy="user", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var BookingBankWire[]
     */
    private $bookingBankWires;

    /**
     * @ORM\OneToMany(targetEntity="DW\CoreBundle\Entity\BookingPayinRefund", mappedBy="user", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var BookingPayinRefund[]
     */
    private $bookingPayinRefunds;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\UserBundle\Model\ListingAlertInterface", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $listingAlerts;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\UserBundle\Entity\Certification", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var Certification[]
     */
    protected $certifications;

    /**
     *
     * @ORM\OneToOne(targetEntity="DW\UserBundle\Entity\CurriculumVitae", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var CurriculumVitae
     */
    protected $cv;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\PaymentBundle\Entity\Orders", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" = "desc"})
     *
     * @var Clictopay[]
     */
    protected $orders;

    /**
     *
     * @ORM\OneToMany(targetEntity="DW\UserBundle\Entity\Cin", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     */
    protected $cins;

    /**
     * @Assert\Count(max="2", groups={"DWProfileContact"})
     * @Assert\All(
     *     @Assert\File(
     *     maxSize="3M",
     *     maxSizeMessage = "Les fichiers de cin ne doit pas dépasser 3Mb.",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg", "application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Les fichiers de cin doivent être au format JPG, GIF, PNG ou PDF.",
     *     groups={"DWProfileContact"}
     *     )
     * )
     *
     * @var UploadedFile[]
     */
    protected $cinFiles;

    /**
     * @ORM\Column(name="verified", type="integer", nullable=false)
     *
     * @var integer
     */
    protected $verified = self::VERIFIED_DEFAULT;

    /**
     * Random integer sent to the user phone in order to verify it
     * @ORM\Column(name="confirmation_token_phone", type="integer", nullable=true)
     *
     * @var string
     */
    protected $confirmationTokenPhone;

    /**
     * Count of attempt confirmation phone number
     * @ORM\Column(name="attempt_confirmation_phone", type="integer", nullable=true)
     *
     * @var string
     */
    protected $attemptConfirmationPhone = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listings = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->reviewsBy = new ArrayCollection();
        $this->reviewsTo = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->bookingBankWires = new ArrayCollection();
        $this->bookingPayinRefunds = new ArrayCollection();
        $this->listingAlerts = new ArrayCollection();
        $this->certifications =  new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->cins = new ArrayCollection();
        $this->attemptConfirmationPhone = 0;
        parent::__construct();
    }

    public function getSluggableFields()
    {
        return ['firstName', 'id'];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getName()
    {
        return $this->firstName . " " . ucfirst(substr($this->lastName, 0, 1) . ".");
    }

    public function setEmail($email)
    {
        $email = is_null($email) ? '' : $email;
        parent::setEmail($email);
        $this->setUsername($email);
    }


    /**
     * Set phoneVerified
     *
     * @param boolean $phoneVerified
     * @return User
     */
    public function setPhoneVerified($phoneVerified)
    {
        $this->phoneVerified = $phoneVerified;

        return $this;
    }

    /**
     * Get phoneVerified
     *
     * @return boolean
     */
    public function getPhoneVerified()
    {
        return $this->phoneVerified;
    }

    /**
     * Set emailVerified
     *
     * @param boolean $emailVerified
     * @return User
     */
    public function setEmailVerified($emailVerified)
    {
        $this->emailVerified = $emailVerified;

        return $this;
    }

    /**
     * Get emailVerified
     *
     * @return boolean
     */
    public function getEmailVerified()
    {
        return $this->emailVerified;
    }

    /**
     * Set idCardVerified
     *
     * @param boolean $idCardVerified
     * @return User
     */
    public function setIdCardVerified($idCardVerified)
    {
        $this->idCardVerified = $idCardVerified;

        return $this;
    }

    /**
     * Get idCardVerified
     *
     * @return boolean
     */
    public function getIdCardVerified()
    {
        return $this->idCardVerified;
    }

    /**
     * @return bool
     */
    public function isDiplomaVerified()
    {
        return $this->diplomaVerified;
    }

    /**
     * @param bool $diplomaVerified
     */
    public function setDiplomaVerified($diplomaVerified)
    {
        $this->diplomaVerified = $diplomaVerified;
    }

    /**
     * @return bool
     */
    public function isCvVerified()
    {
        return $this->cvVerified;
    }

    /**
     * @param bool $cvVerified
     */
    public function setCvVerified($cvVerified)
    {
        $this->cvVerified = $cvVerified;
    }

    /**
     * Set nbBookingsOfferer
     *
     * @param int $nbBookingsOfferer
     * @return User
     */
    public function setNbBookingsOfferer($nbBookingsOfferer)
    {
        $this->nbBookingsOfferer = $nbBookingsOfferer;

        return $this;
    }

    /**
     * Get nbBookingsOfferer
     *
     * @return int
     */
    public function getNbBookingsOfferer()
    {
        return $this->nbBookingsOfferer;
    }

    /**
     * @return int
     */
    public function getNbBookingsAsker()
    {
        return $this->nbBookingsAsker;
    }

    /**
     * @param int $nbBookingsAsker
     */
    public function setNbBookingsAsker($nbBookingsAsker)
    {
        $this->nbBookingsAsker = $nbBookingsAsker;
    }

    /**
     * @return int
     */
    public function getFeeAsAsker()
    {
        return $this->feeAsAsker;
    }

    /**
     * @param int $feeAsAsker
     */
    public function setFeeAsAsker($feeAsAsker)
    {
        $this->feeAsAsker = $feeAsAsker;
    }

    /**
     * @return int
     */
    public function getFeeAsOfferer()
    {
        return $this->feeAsOfferer;
    }

    /**
     * @param int $feeAsOfferer
     */
    public function setFeeAsOfferer($feeAsOfferer)
    {
        $this->feeAsOfferer = $feeAsOfferer;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * Set description
     *
     * @param  string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getNamePay()
    {
        return $this->namePay;
    }

    /**
     * @param string $namePay
     */
    public function setNamePay($namePay)
    {
        $this->namePay = $namePay;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getAgencyName()
    {
        return $this->agencyName;
    }

    /**
     * @param string $agencyName
     */
    public function setAgencyName($agencyName)
    {
        $this->agencyName = $agencyName;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return int
     */
    public function getAnnualIncome()
    {
        return $this->annualIncome;
    }

    /**
     * @param int $annualIncome
     */
    public function setAnnualIncome($annualIncome)
    {
        $this->annualIncome = $annualIncome;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phoneSecond
     */
    public function setPhoneSecond($phoneSecond)
    {
        $this->phoneSecond = $phoneSecond;
    }

    /**
     * @return string
     */
    public function getPhoneSecond()
    {
        return $this->phoneSecond;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    /*public function getPhonePrefix()
    {
        return $this->phonePrefix;
    }*/

    /**
     * @param string $phonePrefix
     */
    /*public function setPhonePrefix($phonePrefix)
    {
        $this->phonePrefix = $phonePrefix;
    }*/


    /**
     * @return string
     */
    public function getCountryOfResidence()
    {
        return $this->countryOfResidence;
    }

    /**
     * @param string $countryOfResidence
     */
    public function setCountryOfResidence($countryOfResidence)
    {
        $this->countryOfResidence = $countryOfResidence;
    }


    /**
     * Set averageAskerRating
     *
     * @param  integer $averageAskerRating
     * @return Listing
     */
    public function setAverageAskerRating($averageAskerRating)
    {
        $this->averageAskerRating = $averageAskerRating;

        return $this;
    }

    /**
     * Get averageAskerRating
     *
     * @return integer
     */
    public function getAverageAskerRating()
    {
        return $this->averageAskerRating;
    }


    /**
     * Set averageOffererRating
     *
     * @param  integer $averageOffererRating
     * @return Listing
     */
    public function setAverageOffererRating($averageOffererRating)
    {
        $this->averageOffererRating = $averageOffererRating;

        return $this;
    }

    /**
     * Get averageOffererRating
     *
     * @return integer
     */
    public function getAverageOffererRating()
    {
        return $this->averageOffererRating;
    }

    /**
     * Set answerDelay
     *
     * @param  integer $answerDelay
     * @return Listing
     */
    public function setAnswerDelay($answerDelay)
    {
        $this->answerDelay = $answerDelay;

        return $this;
    }

    /**
     * Get answerDelay
     *
     * @return integer
     */
    public function getAnswerDelay()
    {
        return $this->answerDelay;
    }

    /**
     * @return string
     */
    public function getMotherTongue()
    {
        return $this->motherTongue;
    }

    /**
     * @param string $motherTongue
     */
    public function setMotherTongue($motherTongue)
    {
        $this->motherTongue = $motherTongue;
    }

    /**
     * @return mixed
     */
    public function getUserFacebook()
    {
        return $this->userFacebook;
    }

    /**
     * @param userFacebook $userFacebook
     */
    public function setUserFacebook($userFacebook)
    {
        $userFacebook->setUser($this);
        $this->userFacebook = $userFacebook;

    }


    public function getFullName()
    {
        return implode(' ', array_filter(array($this->getFirstName(), $this->getLastName())));
    }

    public function __toString()
    {
        return $this->getFullName();
    }


    /**
     * Add listings
     *
     * @param  Listing $listing
     * @return User
     */
    public function addListing(Listing $listing)
    {
        $this->listings[] = $listing;

        return $this;
    }

    /**
     * Remove listings
     *
     * @param Listing $listing
     */
    public function removeListing(Listing $listing)
    {
        $this->listings->removeElement($listing);
    }

    /**
     * Get listings
     *
     * @return Listing[]|\Doctrine\Common\Collections\Collection
     */
    public function getListings()
    {
        return $this->listings;
    }

    /**
     * Add language
     *
     * @param \DW\UserBundle\Entity\UserLanguage $language
     *
     * @return Listing
     */
    public function addLanguage(UserLanguage $language)
    {
        $language->setUser($this);
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \DW\UserBundle\Entity\UserLanguage $language
     */
    public function removeLanguage(UserLanguage $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection|UserLanguage[]
     */
    public function getLanguages()
    {
        return $this->languages;
    }


    /**
     * @return mixed
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * @param ArrayCollection|Booking[] $bookings
     */
    public function setBookings(ArrayCollection $bookings)
    {
        foreach ($bookings as $booking) {
            $booking->setUser($this);
        }

        $this->bookings = $bookings;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param ArrayCollection|Message[] $messages
     */
    public function setMessages(ArrayCollection $messages)
    {
        foreach ($messages as $message) {
            $message->setSender($this);
        }

        $this->messages = $messages;
    }


    /**
     * @return mixed
     */
    public function getReviewsBy()
    {
        return $this->reviewsBy;
    }

    /**
     * @param ArrayCollection|Message[] $reviewsBy
     */
    public function setReviewsBy(ArrayCollection $reviewsBy)
    {
        foreach ($reviewsBy as $review) {
            $review->setReviewBy($this);
        }

        $this->reviewsBy = $reviewsBy;
    }

    /**
     * @return mixed
     */
    public function getReviewsTo()
    {
        return $this->reviewsTo;
    }

    /**
     * @param ArrayCollection|Review[] $reviewsTo
     */
    public function setReviewsTo(ArrayCollection $reviewsTo)
    {
        foreach ($reviewsTo as $review) {
            $review->setReviewTo($this);
        }

        $this->reviewsTo = $reviewsTo;
    }


    /**
     * Add Address
     *
     * @param  UserAddress $address
     * @return User
     */
    public function addAddress(UserAddress $address)
    {
        $address->setUser($this);
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove Address
     *
     * @param UserAddress $address
     */
    public function removeAddress(UserAddress $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }


    /**
     * @return BookingBankWire[]
     */
    public function getBookingBankWires()
    {
        return $this->bookingBankWires;
    }

    /**
     * @param ArrayCollection|BookingBankWire[] $bookingBankWires
     */
    public function setBookingBankWires(ArrayCollection $bookingBankWires)
    {
        foreach ($bookingBankWires as $bookingBankWire) {
            $bookingBankWire->setUser($this);
        }

        $this->bookingBankWires = $bookingBankWires;
    }


    /**
     * @return BookingPayinRefund[]
     */
    public function getBookingPayinRefunds()
    {
        return $this->bookingPayinRefunds;
    }

    /**
     * @param ArrayCollection|BookingPayinRefund[] $bookingPayinRefunds
     */
    public function setBookingPayinRefunds(ArrayCollection $bookingPayinRefunds)
    {
        foreach ($bookingPayinRefunds as $bookingPayinRefund) {
            $bookingPayinRefund->setUser($this);
        }

        $this->bookingPayinRefunds = $bookingPayinRefunds;
    }

    /**
     * Add ListingAlert
     *
     * @param  ListingAlertInterface $listingAlert
     * @return User
     */
    public function addListingAlert($listingAlert)
    {
        $listingAlert->setUser($this);
        $this->listingAlerts[] = $listingAlert;

        return $this;
    }

    /**
     * Remove ListingAlert
     *
     * @param ListingAlertInterface $listingAlert
     */
    public function removeListingAlert($listingAlert)
    {
        $this->listingAlerts->removeElement($listingAlert);
    }

    /**
     * Get ListingAlerts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListingAlerts()
    {
        return $this->listingAlerts;
    }

    /**
     * @param ArrayCollection $listingAlerts
     * @return $this
     */
    public function setListingAlerts(ArrayCollection $listingAlerts)
    {
        foreach ($listingAlerts as $listingAlert) {
            $listingAlert->setUser($this);
        }

        $this->listingAlerts = $listingAlerts;
    }


    /**
     * @param bool $strict
     *
     * @return array
     */
    public function getCompletionInformations($strict = true)
    {
        return array(
            "description" => (
                ($strict && $this->getDescription()) ||
                (!$strict && strlen($this->getDescription()) > 250)
            ) ? 1 : 0,
            "image" => $this->getImage() ? 1 : 0,
        );
    }

    /**
     * Add message
     *
     * @param \DW\MessageBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\DW\MessageBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \DW\MessageBundle\Entity\Message $message
     */
    public function removeMessage(\DW\MessageBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Add reviewsBy
     *
     * @param \DW\ReviewBundle\Entity\Review $reviewsBy
     *
     * @return User
     */
    public function addReviewsBy(\DW\ReviewBundle\Entity\Review $reviewsBy)
    {
        $this->reviewsBy[] = $reviewsBy;

        return $this;
    }

    /**
     * Remove reviewsBy
     *
     * @param \DW\ReviewBundle\Entity\Review $reviewsBy
     */
    public function removeReviewsBy(\DW\ReviewBundle\Entity\Review $reviewsBy)
    {
        $this->reviewsBy->removeElement($reviewsBy);
    }

    /**
     * Add reviewsTo
     *
     * @param \DW\ReviewBundle\Entity\Review $reviewsTo
     *
     * @return User
     */
    public function addReviewsTo(\DW\ReviewBundle\Entity\Review $reviewsTo)
    {
        $this->reviewsTo[] = $reviewsTo;

        return $this;
    }

    /**
     * Remove reviewsTo
     *
     * @param \DW\ReviewBundle\Entity\Review $reviewsTo
     */
    public function removeReviewsTo(\DW\ReviewBundle\Entity\Review $reviewsTo)
    {
        $this->reviewsTo->removeElement($reviewsTo);
    }

    /**
     * Add booking
     *
     * @param \DW\CoreBundle\Entity\Booking $booking
     *
     * @return User
     */
    public function addBooking(\DW\CoreBundle\Entity\Booking $booking)
    {
        $this->bookings[] = $booking;

        return $this;
    }

    /**
     * Remove booking
     *
     * @param \DW\CoreBundle\Entity\Booking $booking
     */
    public function removeBooking(\DW\CoreBundle\Entity\Booking $booking)
    {
        $this->bookings->removeElement($booking);
    }

    /**
     * Add bookingBankWire
     *
     * @param \DW\CoreBundle\Entity\BookingBankWire $bookingBankWire
     *
     * @return User
     */
    public function addBookingBankWire(\DW\CoreBundle\Entity\BookingBankWire $bookingBankWire)
    {
        $this->bookingBankWires[] = $bookingBankWire;

        return $this;
    }

    /**
     * Remove bookingBankWire
     *
     * @param \DW\CoreBundle\Entity\BookingBankWire $bookingBankWire
     */
    public function removeBookingBankWire(\DW\CoreBundle\Entity\BookingBankWire $bookingBankWire)
    {
        $this->bookingBankWires->removeElement($bookingBankWire);
    }

    /**
     * Add bookingPayinRefund
     *
     * @param \DW\CoreBundle\Entity\BookingPayinRefund $bookingPayinRefund
     *
     * @return User
     */
    public function addBookingPayinRefund(\DW\CoreBundle\Entity\BookingPayinRefund $bookingPayinRefund)
    {
        $this->bookingPayinRefunds[] = $bookingPayinRefund;

        return $this;
    }

    /**
     * Remove bookingPayinRefund
     *
     * @param \DW\CoreBundle\Entity\BookingPayinRefund $bookingPayinRefund
     */
    public function removeBookingPayinRefund(\DW\CoreBundle\Entity\BookingPayinRefund $bookingPayinRefund)
    {
        $this->bookingPayinRefunds->removeElement($bookingPayinRefund);
    }

    /**
     * Add certification
     *
     * @param \DW\UserBundle\Entity\Certification $certification
     *
     * @return User
     */
    public function addCertification(\DW\UserBundle\Entity\Certification $certification)
    {
        $this->certifications[] = $certification;

        return $this;
    }

    /**
     * Remove certification
     *
     * @param \DW\UserBundle\Entity\Certification $certification
     */
    public function removeCertification(\DW\UserBundle\Entity\Certification $certification)
    {
        $this->certifications->removeElement($certification);
    }

    /**
     * Get certifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCertifications()
    {
        return $this->certifications;
    }

    /**
     * Add cv
     *
     * @param \DW\UserBundle\Entity\CurriculumVitae $cv
     *
     * @return User
     */
    public function setCv(\DW\UserBundle\Entity\CurriculumVitae $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return \DW\UserBundle\Entity\CurriculumVitae
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Add order
     *
     * @param \DW\PaymentBundle\Entity\Orders $order
     *
     * @return User
     */
    public function addOrder(\DW\PaymentBundle\Entity\Orders $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \DW\PaymentBundle\Entity\Orders $order
     */
    public function removeOrder(\DW\PaymentBundle\Entity\Orders $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set image
     *
     * @param \DW\UserBundle\Entity\UserImage $image
     *
     * @return User
     */
    public function setImage(\DW\UserBundle\Entity\UserImage $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \DW\UserBundle\Entity\UserImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add cin
     *
     * @param \DW\UserBundle\Entity\Cin $cin
     *
     * @return User
     */
    public function addCin(\DW\UserBundle\Entity\Cin $cin)
    {
        $this->cins[] = $cin;

        return $this;
    }

    /**
     * Remove cin
     *
     * @param \DW\UserBundle\Entity\Cin $cin
     */
    public function removeCin(\DW\UserBundle\Entity\Cin $cin)
    {
        $this->cins->removeElement($cin);
    }

    /**
     * Get cins
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCins()
    {
        return $this->cins;
    }

    /**
     * @return UploadedFile[]
     */
    public function getCinFiles()
    {
        return $this->cinFiles;
    }

    /**
     * @param UploadedFile[] $cinFiles
     */
    public function setCinFiles($cinFiles)
    {
        $this->cinFiles = $cinFiles;
    }

    /**
     * @return int
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * @param int $verified
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;
    }

    public function getFieldsVerified()
    {
        $verified = [];
        $verified['form.label_phone'] = $this->phoneVerified ? true : false;
        $verified['form.label_cin'] = $this->idCardVerified ? true : false;
        $verified['form.label_diploma'] = $this->diplomaVerified ? true : false;
        $verified['form.label_cv'] = $this->cvVerified ? true : false;

        return $verified;

    }

    /**
     * Get diplomaVerified
     *
     * @return boolean
     */
    public function getDiplomaVerified()
    {
        return $this->diplomaVerified;
    }

    /**
     * Get cvVerified
     *
     * @return boolean
     */
    public function getCvVerified()
    {
        return $this->cvVerified;
    }

    /**
     * Set confirmationTokenPhone
     *
     * @param integer $confirmationTokenPhone
     *
     * @return User
     */
    public function setConfirmationTokenPhone($confirmationTokenPhone)
    {
        $this->confirmationTokenPhone = $confirmationTokenPhone;

        return $this;
    }

    /**
     * Get confirmationTokenPhone
     *
     * @return integer
     */
    public function getConfirmationTokenPhone()
    {
        return $this->confirmationTokenPhone;
    }

    /**
     * Set attemptConfirmationPhone
     *
     * @param integer $attemptConfirmationPhone
     *
     * @return User
     */
    public function setAttemptConfirmationPhone($attemptConfirmationPhone)
    {
        $this->attemptConfirmationPhone = $attemptConfirmationPhone;

        return $this;
    }

    /**
     * Get attemptConfirmationPhone
     *
     * @return integer
     */
    public function getAttemptConfirmationPhone()
    {
        return $this->attemptConfirmationPhone;
    }
}
