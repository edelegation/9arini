<?php

namespace DW\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="cins")
 * @ORM\HasLifecycleCallbacks
 */
class Cin {

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="cert_name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="cins", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var User
     */
    private $user;

    /**
     * @var UploadedFile $cinFile
     */
    private $cinFile;

    /**
     * Cin constructor.
     */
    public function __construct(UploadedFile $file)
    {
        $this->createdAt = new \DateTime();
        $this->setCinFile($file);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Cin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Cin
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \DW\UserBundle\Entity\User $user
     *
     * @return Cin
     */
    public function setUser(\DW\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getCinFile()
    {
        return $this->cinFile;
    }

    /**
     * @param UploadedFile $cinFile
     */
    public function setCinFile($cinFile)
    {
        $this->cinFile = $cinFile;
    }

    /**
     * Get user
     *
     * @return \DW\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getAbsolutePath()
    {
        return null === $this->name
            ? null
            : $this->getUploadRootDir().'/'.$this->name;
    }

    public function getWebPath()
    {
        return null === $this->name
            ? null
            : $this->getUploadDir().'/'.$this->name;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/users/cins';
    }


    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getCinFile()) {
            return;
        }

        $this->getCinFile()->move($this->getUploadRootDir(), $this->getName());
        $this->cinFile = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }
}
