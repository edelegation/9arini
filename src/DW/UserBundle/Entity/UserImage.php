<?php

namespace DW\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DW\CoreBundle\Entity\Listing;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserImage
 *
 * @ORM\Table(name="user_image", indexes={
 *    @ORM\Index(name="position_u_idx", columns={"position"})
 *  })
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserImage
{

    const IMAGE_DEFAULT = "default-user.png";
    const IMAGE_FOLDER = "/uploads/users/images/";
    const IMAGE_CROP_FOLDER = "/uploads/users/images/crop/";

    private static $validMimesType = ['image/gif','image/jpg','image/png'];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=255, name="name", nullable=false)
     *
     * @var string $name
     */
    protected $name;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="smallint", nullable=false)
     */
    private $position = 1;

    /**
     * @ORM\OneToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="image")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @Assert\File(
     *     maxSize = "3M",
     *     mimeTypes = {"image/gif", "image/jpg", "image/jpeg", "image/png"},
     *     mimeTypesMessage = "L'image de avatar doivent être au format JPG, GIF ou PNG.",
     *     maxSizeMessage = "L'image de avatar ne doit pas dépasser 3Mb.",
     * )
     * @var $file
     */
    private $file;

    /**
     *
     * @ORM\Column(name="crop_img", type="string", length=255, nullable=true)
     *
     * @var string $cropImg
     */
    private $cropImg;

    /**
     * @Assert\IsTrue(message="Image recadrée invalide")
     */
    public function isCropImgValid()
    {

        if (!empty($this->cropImg) && ($this->cropImg != $this->name)) {
            return $this->hasValidBase64Image();
        }

        return true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets name.
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position
     *
     * @param  boolean $position
     * @return UserImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set user
     *
     * @param  User $user
     * @return UserImage
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Get listing
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getAbsolutePath($crop = false)
    {
        return null === $this->name
            ? null
            : $this->getUploadRootDir($crop) . $this->name;
    }

    public function getWebPath()
    {
        if (!is_null($this->name)) {
            return $this->cropImg
                ? self::IMAGE_CROP_FOLDER . $this->name
                : self::IMAGE_FOLDER . $this->name;
        }
        return null;
    }

    protected function getUploadRootDir($crop = false)
    {
        if ($crop) {
            return __DIR__.'/../../../../web'. self::IMAGE_CROP_FOLDER;
        }
        return __DIR__.'/../../../../web'. self::IMAGE_FOLDER;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (!is_dir($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir(), 0777,true);
        }

        if (!is_dir($this->getUploadRootDir(true))) {
            mkdir($this->getUploadRootDir(true), 0777,true);
        }

        $fileName = $this->getFileName();

        # Remove previous image
        $this->removeUpload();

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $fileName
        );


        if (!empty($this->cropImg) && $this->hasValidBase64Image() && ($this->cropImg != $this->name)) {


            #Remove Previous Image Crop
            $this->removeUpload(true);

            file_put_contents(
                $this->getUploadRootDir(true) . $fileName,
                base64_decode($this->getBase64Image())
            );

            $this->setCropImg($fileName);
            $this->setName($fileName);

            $this->checkMimeFilePutContent();
        } else {
            # Remove previous image crop
            $this->removeUpload(true);
            $this->setCropImg(null);
        }

        $this->setName($fileName);

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     * @param bool $crop
     */
    public function removeUpload($crop = false)
    {
        if ($file = $this->getAbsolutePath($crop)) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    /**
     * @return string
     */
    public function getCropImg()
    {
        return $this->cropImg;
    }

    /**
     * @param string $cropImg
     */
    public function setCropImg($cropImg)
    {
        $this->cropImg = $cropImg;
    }

    private function hasValidMimeBase64Image() {

        foreach (self::$validMimesType as $mimeType) {
            if (strpos($this->cropImg, $mimeType)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    private function getBase64Image() {
        $data = explode(',', substr( $this->cropImg , 5 ) , 2);

        if(count($data) != 2) {
            return false;
        }

        return $data[1];
    }

    /**
     * @return bool
     */
    private function hasValidBase64Image() {

        if (!$this->hasValidMimeBase64Image()) {
            return false;
        }

        $base64 = $this->getBase64Image();

        if (!$base64) {
            return false;
        }

        $img = imagecreatefromstring(base64_decode($base64));

        if (!$img) {
            return false;
        }

        $size = getimagesizefromstring(base64_decode($base64));

        if (!$size
            || $size[0] == 0
            || $size[1] == 0
            || !$size['mime']
            || !in_array($size['mime'], self::$validMimesType)
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        $fileName = str_replace($this->getUser()->getId(), time(), $this->getUser()->getSlug()) . "." . $this->getFile()->guessClientExtension();

        if (!$this->getUser()->getListings()->isEmpty()) {

            /** @var Listing $firstListing */
            $firstListing = $this->getUser()->getListings()->get(0);
            $fileName = str_replace($firstListing->getId(), $fileName, $firstListing->getSlug());

        }

        return $fileName;
    }

    private function checkMimeFilePutContent()
    {
        # checking mime type of file put content
        $mimeCropImg = mime_content_type($this->getAbsolutePath(true));

        if (!in_array($mimeCropImg, self::$validMimesType)) {
            unlink($this->getAbsolutePath(true));
            $this->setCropImg(null);
        }
    }
}
