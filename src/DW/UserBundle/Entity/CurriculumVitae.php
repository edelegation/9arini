<?php

namespace DW\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="curriculum_vitae")
 * @ORM\HasLifecycleCallbacks
 */
class CurriculumVitae{

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="cv_name", type="string", length=255, nullable=true)
     */
    private $cv;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="cvs", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var User
     */
    private $user;

    /**
     * @Assert\File(
     *     maxSize="3M",
     *     maxSizeMessage = "Le curriculum vitae ne doit pas dépasser 3Mb.",
     *     mimeTypes={"application/pdf", "application/msword"},
     *     mimeTypesMessage = "Le curriculum vitae doivent être au format PDF, DOC ou DOCX."
     * )
     */
    private $path;

    /**
     * @var string
     */
    private $tmp;

    /**
     * Certification constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CurriculumVitae
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \DW\UserBundle\Entity\User $user
     *
     * @return CurriculumVitae
     */
    public function setUser(\DW\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DW\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @param UploadedFile $path
     */
    public function setPath(UploadedFile $path)
    {
        $this->path = $path;
    }
    /**
     * @return UploadedFile
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set cvName
     *
     * @param string $cv
     *
     * @return CurriculumVitae
     */
    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return string
     */
    public function getCv()
    {
        return $this->cv;
    }


    /**
     * @return string
     */
    public function getTmp()
    {
        return $this->tmp;
    }

    /**
     * @param string $tmp
     */
    public function setTmp($tmp)
    {
        $this->tmp = $tmp;
    }

    public function getAbsolutePath()
    {
        return null === $this->cv
            ? null
            : $this->getUploadRootDir().'/'.$this->cv;
    }

    public function getWebPath()
    {
        return null === $this->cv
            ? null
            : $this->getUploadDir().'/'.$this->cv;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/users/cv';
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null === $this->getPath()) {
            return;
        }

        if($oldCv = $this->user->getCv()) {
            $this->setTmp($oldCv->getCv());
        }

        $this->cv = 'cv-'
            . $this->user->getId()
            . '-' .substr(md5($this->getPath()->getClientOriginalName()),0,7)
            . '.' .$this->getPath()->guessClientExtension();

    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getPath()) {
            return;
        }

        if (!is_dir($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir(), 0777, true);
        }

        $this->getPath()->move($this->getUploadRootDir(), $this->cv);

        // check if we have an old cin
        if (isset($this->tmp)) {
            // delete the old cin
            unlink($this->getUploadRootDir().'/'.$this->tmp);
            // clear the tmp
            $this->tmp = null;
        }

        $this->path = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
