<?php

namespace DW\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="certification")
 * @ORM\HasLifecycleCallbacks
 */
class Certification {

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(name="cert_name", type="string", length=255, nullable=true)
     */
    private $cert;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="DW\UserBundle\Entity\User", inversedBy="certifications", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @var User
     */
    private $user;

    /**
     * @Assert\File(
     *     maxSize="3M",
     *     maxSizeMessage = "L'image de certification ne doit pas dépasser 3Mb.",
     *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"},
     *     mimeTypesMessage = "L'image de certification doivent être au format JPG, GIF ou PNG."
     * )
     * */
    private $path;

    /**
     * Certification constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Certification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Certification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \DW\UserBundle\Entity\User $user
     *
     * @return Certification
     */
    public function setUser(\DW\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DW\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @param UploadedFile $path
     */
    public function setPath(UploadedFile $path)
    {
        $this->path = $path;
    }

    /**
     * @return UploadedFile
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * Set certName
     *
     * @param string $cert
     *
     * @return Certification
     */
    public function setCert($cert)
    {
        $this->cert = $cert;

        return $this;
    }

    /**
     * Get cert
     *
     * @return string
     */
    public function getCert()
    {
        return $this->cert;
    }


    public function getAbsolutePath()
    {
        return null === $this->cert
            ? null
            : $this->getUploadRootDir().'/'.$this->cert;
    }

    public function getWebPath()
    {
        return null === $this->cert
            ? null
            : $this->getUploadDir().'/'.$this->cert;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/users/cert';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null === $this->getPath()) {
            return;
        }

        $this->cert = 'cert-'
            . $this->user->getId()
            . '-' .md5($this->getPath()->getClientOriginalName())
            . '.' .$this->getPath()->guessClientExtension();

    }


    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getPath()) {
            return;
        }

        if (!is_dir($this->getUploadRootDir())) {
            mkdir($this->getUploadRootDir(), 0777,true);
        }

        $this->getPath()->move($this->getUploadRootDir(),$this->cert);
        $this->path = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}
