<?php

namespace DW\UserBundle\Event;

use Doctrine\ORM\EntityManagerInterface;
use DW\MessageBundle\SMS\SenderSMS;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var SenderSMS
     */
    private $senderSMS;

    public function __construct(EntityManagerInterface $manager, SenderSMS $senderSMS)
    {
        $this->manager = $manager;
        $this->senderSMS = $senderSMS;
    }

    public function onRegister(UserEvent $event, $eventName, EventDispatcherInterface $dispatcher)
    {
        //$user = $event->getUser();
    }

    public function onPhoneUpdate(UserEvent $event, $eventName, EventDispatcherInterface $dispatcher)
    {
        $uow = $this->manager->getUnitOfWork();
        $user = $event->getUser();
        $oldDataUser = $uow->getOriginalEntityData($user);

        if ( $user->getPhone() != $oldDataUser['phone'] ) {
            $string = $this->senderSMS->sendCodeConfirmationPhone($user);
            $result = $this->senderSMS->getMessageResultConfirmationPhone($string);
            $event->setResult($result);
        }
    }


    public static function getSubscribedEvents()
    {
        return array(
            //UserEvents::USER_REGISTER => array('onRegister', 1),
            //UserEvents::USER_PROFILE_UPDATE => array('onPhoneUpdate', 1),
        );
    }

}