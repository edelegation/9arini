<?php

namespace DW\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class CurriculumVitaeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path',FileType::class, [
                'label' => 'form.cv.upload',
                'required' => false,
                'translation_domain' => 'dw_user',
                'constraints' => [
                    new File([
                        'maxSize' => '3M',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/msword'
                        ],
                        'maxSizeMessage' => 'Le curriculum vitae ne doit pas dépasser 3Mb.',
                        'mimeTypesMessage' => 'Le curriculum vitae doivent être au format PDF, DOC ou DOCX.',
                    ])
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DW\UserBundle\Entity\CurriculumVitae',
            'mapped' => false,
            'required' => false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dw_userbundle_curriculumvitae';
    }
}
