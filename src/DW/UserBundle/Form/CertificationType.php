<?php

namespace DW\UserBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use DW\UserBundle\Entity\CurriculumVitae;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class CertificationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'form.certification.title',
                'required' => false
            ])
            ->add('path', FileType::class, [
                'label' => 'form.certification.upload',
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '3M',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/pjpeg'
                        ],
                        'maxSizeMessage' => 'L\'image de certification ne doit pas dépasser 3Mb.',
                        'mimeTypesMessage' => 'L\'image de certification doivent être au format JPG, GIF ou PNG.',
                    ])
                ]
            ])
            ->add('cv', CurriculumVitaeType::class,[
                //'mapped' => false,
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DW\UserBundle\Entity\Certification'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dw_userbundle_certification';
    }
}
