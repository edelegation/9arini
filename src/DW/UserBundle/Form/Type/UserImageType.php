<?php

namespace DW\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;


class UserImageType extends AbstractType
{
    private $requiredImage = false;

    public function __construct($requiredImage = false)
    {
        $this->requiredImage = $requiredImage;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $constraintsImage = [
            new File([
                'maxSize' => '3M',
                'mimeTypes' => [
                    'image/png',
                    'image/jpeg',
                    'image/pjpeg',
                    'image/gif'
                ],
                'maxSizeMessage' => 'L\'image de avatar ne doit pas dépasser 3Mb.',
                'mimeTypesMessage' => 'L\'image de avatar doivent être au format JPG, GIF ou PNG.',
            ]),
            $this->requiredImage
                ? new NotBlank(['message' => 'Requis'])
                : '',
        ];

        $builder
            ->add(
                'file',
                'file',
                array(
                    'label' => 'form.user.avatar.upload',
                    'required' => false,
                    'constraints' => array_filter($constraintsImage)
                )
            )
            ->add('crop_img',HiddenType::class,[
                'required' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\UserBundle\Entity\UserImage',
                'csrf_token_id' => 'user_image',
                'translation_domain' => 'dw_user',
                'cascade_validation' => true,
                /** @Ignore */
                'label' => false
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_image';
    }

}
