<?php

namespace DW\UserBundle\Form\Type;

use DW\CoreBundle\Form\Type\LanguageFilteredType;
use DW\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileAboutMeFormType extends AbstractType
{
    private $class;
    private $request;
    private $locale;
    /**
     * @var array uploaded files
     */
    protected $uploaded;

    /**
     * @param string       $class The User class name
     * @param RequestStack $requestStack
     */
    public function __construct($class, RequestStack $requestStack)
    {
        $this->class = $class;
        $this->request = $requestStack->getCurrentRequest();
        $this->locale = $this->request->getLocale();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData();

        $builder->add(
            'description',
            'textarea', [
                'label' => 'form.user.description',
                'attr'  => [
                    'placeholder' => 'form.user.placeholder.description'
                ]
            ]
        );

        $builder
            ->add(
                'image',
                new UserImageType(),
                [
                    'required' => false
                ]
            )
            ->add(
                'language',
                'language',
                array(
                    'mapped' => false,
                    'label' => 'dw.language',
                    'preferred_choices' => array("ar", "fr", "en"),
                    'empty_value' => 'user.about.language.select',
                    'required' => false
                )
            )
            ->add(
                'languages',
                'collection',
                array(
                    'allow_delete' => true,
                    'allow_add' => true,
                    'by_reference' => false,
                    'type' => new UserLanguageType(),
                    'label' => false
                )
            )
            /*->add(
                'motherTongue',
                'language',
                array(
                    'label' => 'dw.motherTongue',
                    'preferred_choices' => array("en", "fr", "es", "de", "it", "ar", "zh", "ru"),
                    'data' => $user->getMotherTongue() ? $user->getMotherTongue() : $this->locale
                )
            )*/
            ->add(
                'lastName',
                'text',
                array(
                    'label' => 'form.user.last_name',
                )
            )
            ->add(
                'firstName',
                'text',
                array(
                    'label' => 'form.user.first_name'
                )
            )
            ->add(
                'birthday',
                'birthday',
                array(
                    'label' => 'form.user.birthday',
//                    'format' => 'dd MMMM yyyy',
                    'widget' => "choice",
                    'years' => range(date('Y') - 18, date('Y') - 100),
                    'required' => true
                )
            )
            ->add(
                'nationality',
                'country',
                array(
                    'label' => 'form.user.nationality',
                    'required' => false,
                    'preferred_choices' => array("GB", "FR", "ES", "DE", "IT", "CH", "US", "RU"),
                )
            )
            ->add(
                'countryOfResidence',
                'country',
                array(
                    'label' => 'form.user.countryOfResidence',
                    'required' => true,
                    'preferred_choices' => array("GB", "FR", "ES", "DE", "IT", "CH", "US", "RU"),
                )
            )
            ->add(
                'profession',
                'text',
                array(
                    'label' => 'form.user.profession',
                    'required' => false
                )
            );

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $data = $event->getData();
                $data = $data ?: array();
                if (array_key_exists('uploaded', $data["image"])) {
                    // capture uploaded files and store them for onSubmit event
                    $this->uploaded = $data["image"]['uploaded'];
                }
            }
        );


        $builder->addEventListener(
            FormEvents::SUBMIT,
            function (FormEvent $event) {
                $data = $event->getData();
                /** @var User $user */
                $user = $data;

                /*if ($this->uploaded) {
                    $nbImages = $user->getImages()->count();
                    //Add new images
                    $imagesUploadedArray = explode(",", trim($this->uploaded, ","));
                    foreach ($imagesUploadedArray as $i => $image) {
                        $userImage = new UserImage();
                        $userImage->setuser($user);
                        $userImage->setName($image);
                        $userImage->setPosition($nbImages + $i + 1);
                        $user->addImage($userImage);
                    }

                    $event->setData($user);
                }*/
            }
        );
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => $this->class,
                'csrf_token_id' => 'profile',
                'translation_domain' => 'dw_user',
                'cascade_validation' => true,
                //'validation_groups' => array('DWProfile'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_profile_about_me';
    }

}
