<?php

namespace DW\UserBundle\Form\Type;

use DW\UserBundle\Form\CinType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotNull;

class ProfileContactFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                'email',
                array(
                    'label' => 'form.user.email'
                )
            )
            /*->add(
                'phone_prefix',
                'text',
                array(
                    'label' => 'form.user.phone_prefix',
                    'required' => false,
                    'data' => '+33'
                )
            )*/
            ->add(
                'phone',
                'text',
                array(
                    'label' => 'form.user.phone',
                    'required' => true
                )
            )
            ->add(
                'phone_second',
                'text',
                array(
                    'label' => 'form.user.phone_second',
                    'required' => false
                )
            )
            ->add(
                'plainPassword',
                'repeated',
                array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'dw_user'),
                    'first_options' => array(
                        'label' => 'form.password',
                        'required' => true
                    ),
                    'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'required' => true
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                    'required' => false
                )
            )
            ->add(
                'addresses',
                'collection',
                array(
                    'type' => new UserAddressFormType(),
                    /** @Ignore */
                    'label' => false,
                    'required' => false
                )
            )
            ->add(
                'cinFiles',
                FileType::class,
                [
                    'label' => 'form.user.cin.upload',
                    'required' => false,
                    'multiple' => true
                ]
            );


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\UserBundle\Entity\User',
                'csrf_token_id' => 'profile',
                'translation_domain' => 'dw_user',
                'cascade_validation' => true,
                'validation_groups' => array('DWProfileContact'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_profile_contact';
    }
}
