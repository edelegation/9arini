<?php

namespace DW\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RegistrationFormType
 *
 */
class RegistrationFormType extends AbstractType
{
    private $class;

    /**
     * @param string $class
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'lastName',
                null,
                array('label' => 'form.last_name')
            )
            ->add(
                'firstName',
                null,
                array('label' => 'form.first_name')
            )
            ->add(
                'email',
                'email',
                array('label' => 'form.email')
            )
//            ->add(
//                'birthday',
//                'birthday',
//                array(
//                    'label' => 'form.user.birthday',
//                    'widget' => "choice",
//                    'years' => range(date('Y') - 18, date('Y') - 100),
//                    'required' => true
//                )
//            )
//            ->add(
//                'countryOfResidence',
//                'country',
//                array(
//                    'label' => 'form.user.countryOfResidence',
//                    'required' => true,
//                    'preferred_choices' => array("GB", "FR", "ES", "DE", "IT", "CH", "US", "RU"),
//                )
//            )
            ->add(
                'plainPassword',
                'repeated',
                array(
                    'type' => 'password',
                    'options' => array('translation_domain' => 'dw_user'),
                    'first_options' => array(
                        'label' => 'form.password',
                        'required' => true
                    ),
                    'second_options' => array(
                        'label' => 'form.password_confirmation',
                        'required' => true
                    ),
                    'invalid_message' => 'fos_user.password.mismatch',
                    'required' => true
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => $this->class,
                'csrf_token_id' => 'user_registration',
                'translation_domain' => 'dw_user',
                'validation_groups' => array('DWRegistration'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_registration';
    }
}
