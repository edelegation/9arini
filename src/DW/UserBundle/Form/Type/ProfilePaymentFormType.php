<?php

namespace DW\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfilePaymentFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'namePay',
                'text',
                array(
                    'label' => 'form.user.name_pay',
                    'required' => true,
                )
            )
            ->add(
                'bankName',
                null,
                array(
                    'label' => 'form.user.bank_name',
                    'required' => true
                )
            )
            ->add(
                'agencyName',
                null,
                array(
                    'label' => 'form.user.agency_name',
                    'required' => true
                )
            )
            ->add(
                'iban',
                'text',
                array(
                    'label' => 'form.user.rip_iban',
                    'required' => true
                )
            );


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\UserBundle\Entity\User',
                'csrf_token_id' => 'DWProfilePayment',
                'translation_domain' => 'dw_user',
                'cascade_validation' => true,
                'validation_groups' => array('DWProfilePayment'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user_profile_payment';
    }
}
