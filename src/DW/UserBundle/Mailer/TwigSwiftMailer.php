<?php

namespace DW\UserBundle\Mailer;

use DW\CoreBundle\Entity\Booking;
use FOS\MessageBundle\Model\ThreadInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class TwigSwiftMailer implements MailerInterface
{
    protected $mailer;
    protected $router;
    protected $twig;
    protected $requestStack;
    protected $parameters;
    protected $fromName;
    protected $fromEmail;

    /**
     * @param \Swift_Mailer         $mailer
     * @param UrlGeneratorInterface $router
     * @param \Twig_Environment     $twig
     * @param RequestStack          $requestStack
     * @param array                 $parameters
     */
    public function __construct(
        \Swift_Mailer $mailer,
        UrlGeneratorInterface $router,
        \Twig_Environment $twig,
        RequestStack $requestStack,
        array $parameters
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->parameters = $parameters;
        $this->fromName = $parameters['from_name'];
        $this->fromEmail = $parameters['from_email'];
    }

    /**
     * @param UserInterface $user
     */
    public function sendAccountCreatedMessageToUser(UserInterface $user)
    {
        $template = $this->parameters['templates']['account_created_user'];

        $context = array(
            'user' => $user,
            'dw_site_name' => $this->parameters['site_name']
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param UserInterface $user
     */
    public function sendResettingEmailMessageToUser(UserInterface $user)
    {
        $template = $this->parameters['templates']['forgot_password_user'];
        $password_reset_link = $this->router->generate(
            'dw_user_resetting_reset',
            array('token' => $user->getConfirmationToken()),
            true
        );
        $context = array(
            'user' => $user,
            'password_reset_link' => $password_reset_link
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }

    /**
     * @param ThreadInterface $thread
     */
    public function sendNotificationMessageToAdmin(Booking $booking, ThreadInterface $thread)
    {
        $template = $this->parameters['templates']['new_message_admin'];
        $threadUrl = $this->router->generate(
            'admin_dw_message_thread_edit',
            array('id' => $thread->getId()),
            true
        );

        $context = array(
            'thread_url' => $threadUrl,
            'booking' => $booking
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $this->fromEmail);
    }


    /**
     * @param UserInterface   $user
     * @param ThreadInterface $thread
     */
    public function sendNotificationForNewMessageToUser(UserInterface $user, ThreadInterface $thread)
    {
        $template = $this->parameters['templates']['new_message_user'];
        $threadUrl = $this->router->generate(
            'dw_dashboard_message_thread_view',
            array('threadId' => $thread->getId()),
            true
        );

        $context = array(
            'user' => $user,
            'thread_url' => $threadUrl
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param UserInterface $user
     */
    public function sendAccountCreationConfirmationMessageToUser(UserInterface $user)
    {
        $template = $this->parameters['templates']['account_creation_confirmation_user'];
        $url = $this->router->generate(
            'dw_user_register_confirmation',
            array('token' => $user->getConfirmationToken()),
            true
        );
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url
        );

        $this->sendMessage($template, $context, $this->fromEmail, $this->fromName, $user->getEmail());
    }


    /**
     * @param string $templateName
     * @param array $context
     * @param string $fromEmail
     * @param string $fromName
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $fromName, $toEmail)
    {
        /** @var \Twig_Template $template */
        $template = $this->twig->loadTemplate($templateName);
        $context = $this->twig->mergeGlobals($context);

        $subject = $template->renderBlock('subject', $context);
        $context["message"] = $template->renderBlock('message', $context);

        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = $template->renderBlock('body_html', $context);

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom([$fromEmail => $fromName])
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message
                ->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);

        $this->mailer->getTransport()->stop();
    }
}
