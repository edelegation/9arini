<?php


namespace DW\UserBundle\Controller\Dashboard;

use DW\UserBundle\Entity\Certification;
use DW\UserBundle\Entity\CurriculumVitae;
use DW\UserBundle\Entity\User;
use DW\UserBundle\Entity\UserAddress;
use DW\UserBundle\Event\UserEvent;
use DW\UserBundle\Event\UserEvents;
use DW\UserBundle\Form\CertificationType;
use DW\UserBundle\Form\Type\ProfileContactFormType;
use DW\UserBundle\Form\Type\ProfilePaymentFormType;
use DW\UserBundle\Form\Type\ProfileSwitchFormType;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ProfileController
 *
 * @Route("/user")
 */
class ProfileController extends Controller
{

    /**
     * Edit user profile
     *
     * @Route("/edit-about-me", name="dw_user_dashboard_profile_edit_about_me")
     * @Method({"GET", "POST"})
     *
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAboutMeAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createEditAboutMeForm($user);
        $form->handleRequest($request);
        //dump($form->getData());die;
        if ($form->isSubmitted() && $form->isValid()) {
            if($form->getData()->getImage()){
                if($user->getImage()->getUser() === null){
                    $user->getImage()->setUser($user);
                }
                $form->getData()->getImage()->upload();
            }

            $this->get("dw_user.user_manager")->updateUser($user);
            $this->container->get('session')->getFlashBag()->add(
                'success',
                $this->container->get('translator')->trans('user.edit.about_me.success', array(), 'dw_user')
            );

            $url = $this->generateUrl('dw_user_dashboard_profile_edit_about_me');

            return new RedirectResponse($url);
        }

        return $this->container->get('templating')->renderResponse(
            'DWUserBundle:Dashboard/Profile:edit_about_me.html.twig',
            array(
                'form' => $form->createView(),
                'user' => $user
            )
        );

    }


    /**
     * Creates a form to edit a user entity.
     *
     * @param mixed $user
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditAboutMeForm($user)
    {
        $form = $this->get('form.factory')->createNamed(
            'user',
            'user_profile_about_me',
            $user,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_user_dashboard_profile_edit_about_me'),
            )
        );

        return $form;
    }


    /**
     * Edit user profile
     *
     * @Route("/edit-payment", name="dw_user_dashboard_profile_edit_payment")
     * @Method({"GET", "POST"})
     *
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPaymentAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createEditPaymentForm($user);
        $success = $this->get('dw_user.form.handler.edit_payment')->process($form);

        $session = $this->container->get('session');
        $translator = $this->container->get('translator');

        if ($success > 0) {
            $session->getFlashBag()->add(
                'success',
                $translator->trans('user.edit.payment.success', array(), 'dw_user')
            );

            return $this->redirect(
                $this->generateUrl(
                    'dw_user_dashboard_profile_edit_payment'
                )
            );
        } elseif ($success < 0) {
            $session->getFlashBag()->add(
                'error',
                $translator->trans('user.edit.payment.error', array(), 'dw_user')
            );
        }

        return $this->render(
            'DWUserBundle:Dashboard/Profile:edit_payment.html.twig',
            array(
                'form' => $form->createView(),
                'user' => $user
            )
        );
    }

    /**
     * Creates a form to edit a user entity.
     *
     * @param mixed $user
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditPaymentForm($user)
    {
        $form = $this->get('form.factory')->createNamed(
            'user',
            new ProfilePaymentFormType(),
            $user,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_user_dashboard_profile_edit_payment'),
            )
        );

        return $form;
    }


    /**
     * Edit user profile
     *
     * @Route("/edit-contact", name="dw_user_dashboard_profile_edit_contact")
     * @Method({"GET", "POST"})
     *
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editContactAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createEditContactForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $session = $this->get('session');

            //Eventually change user profile data before update it
            $event = new UserEvent($user);
            $this->get('event_dispatcher')->dispatch(UserEvents::USER_PROFILE_UPDATE, $event);
            $user = $event->getUser();
//            $result = $event->getResult();

            $this->get("dw_user.user_manager")->updateUser($user);

            $session->getFlashBag()->add('success', $this->get('translator')->trans('user.edit.contact.success', array(), 'dw_user'));

//            if(is_array($result) && !empty($result)) {
//                $session->getFlashBag()->add($result['type'], $result['msg']);
//            }

            return $this->redirect($this->generateUrl('dw_user_dashboard_profile_edit_contact'));
        }

        return $this->render(
            'DWUserBundle:Dashboard/Profile:edit_contact.html.twig',
            array(
                'form' => $form->createView(),
                'user' => $user
            )
        );
    }

    /**
     * Creates a form to edit a user entity.
     *
     * @param mixed $user
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditContactForm($user)
    {
        $addresses = $user->getAddresses();
        if (count($addresses) == 0) {
            $user->addAddress(new UserAddress());
        }
        $form = $this->get('form.factory')->createNamed(
            'user',
            new ProfileContactFormType(),
            $user,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_user_dashboard_profile_edit_contact'),
            )
        );


        return $form;
    }

    /**
     * Edit user profile
     *
     * @Route("/certification", name="dw_user_dashboard_profile_certification")
     * @Method({"GET", "POST"})
     *
     * @param $request Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function certificationAction(Request $request)
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $certification = new Certification();

        $form = $this->createCertificationForm($certification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            if(null !== $certification->getTitle()) {
                $certification->setUser($user);
                $em->persist($certification);

            }

            $cvFile = $request->files->get($form->getName())['cv']['path'];

            if(null !== $cvFile) {
                $cv = new CurriculumVitae();
                if(null !== $user->getCv()) {
                    $cv = $user->getCv();
                }
                $cv->setUser($user);
                $cv->setPath($cvFile);
                $em->persist($cv);
            }

            $em->flush();

            $this->container->get('session')->getFlashBag()->add(
                'success',
                $this->container->get('translator')->trans('user.edit.about_me.success', array(), 'dw_user')
            );

            $url = $this->generateUrl('dw_user_dashboard_profile_certification');

            return new RedirectResponse($url);
        }

        return $this->render('DWUserBundle:Dashboard/Profile:certification.html.twig',[
            'form' => $form->createView(),
            'user' => $user
        ]);

    }

    /**
     * Creates a form to certification user
     *
     * @param mixed $certification
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCertificationForm($certification)
    {
        $form = $this->get('form.factory')->createNamed(
            'certification',
            new CertificationType(),
            $certification,
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_user_dashboard_profile_certification'),
            )
        );

        return $form;
    }

    /**
     * Switch profile
     *
     * @Route("/profile-switch", name="dw_user_dashboard_profile_switch")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileSwitchAction(Request $request)
    {
        $session = $request->getSession();
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->createProfileSwitchForm($request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $session->set('profile', $data['profile']);

            $response = $this->redirect($this->generateUrl('dw_dashboard_message'));

            $response->headers->setCookie(new Cookie('userType', $data['profile'], 0, '/', null, false, false));

            return $response;
        }

        $type = $request->getSession()->get('profile', 'asker');

        $em = $this->container->get('doctrine')->getManager();
        $nbMessages = $em->getRepository('DWMessageBundle:Message')->getNbUnreadMessage($user, $type);

        return $this->render(
            'DWUserBundle:Dashboard/Profile:profile_switch.html.twig',
            array(
                'form' => $form->createView(),
                'nbMessages' => $nbMessages,
                'type' => $type,
            )
        );
    }

    /**
     * Creates a form to switch a user profile.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createProfileSwitchForm(Request $request)
    {
        $session = $request->getSession();
        if ($session->has('profile')) {
            $selectedProfile = $session->get('profile');
        } else {
            $user = $this->getUser();
            $selectedProfile = ($user && $user->getListings()->count()) ? 'offerer' : 'asker';
            $session->set('profile', $selectedProfile);
        }

        $form = $this->get('form.factory')->createNamed(
            'profileSwitch',
            new ProfileSwitchFormType(),
            array('profile' => $selectedProfile),
            array(
                'method' => 'POST',
                'action' => $this->generateUrl('dw_user_dashboard_profile_switch'),
                'attr' => array('class' => 'form-switchers')
            )
        );

        return $form;
    }

    /**
     * Delete certification profile
     *
     * @Route("/certification/{id}", name="dw_user_dashboard_profile_certification_delete")
     * @Method({"GET", "POST"})
     *
     */
    public function deleteCertificationAction(Certification $certification)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($certification);
        $em->flush();

        return $this->redirectToRoute('dw_user_dashboard_profile_certification');
    }

    /**
     * Delete curriculum profile
     *
     * @Route("/curriculum/{id}", name="dw_user_dashboard_profile_curriculum_delete")
     * @Method({"GET", "POST"})
     *
     */
    public function deleteCurriculumAction(CurriculumVitae $curriculumVitae)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($curriculumVitae);
        $em->flush();

        return $this->redirectToRoute('dw_user_dashboard_profile_certification');

    }

    /**
     *
     * @Route("/ajax-confirm-phone", name="dw_user_dashboard_profile_ajax_confirm_phone")
     * @Method({"POST"})
     *
     */
    public function ajaxConfirmPhoneAction(Request $request)
    {
        $code = $request->request->has('code')
            ? $request->request->get('code')
            : null;

        /** @var User $user */
        $user = $this->getUser();
        $referer = false;

        $notice = ['type' => 'error', 'msg' => $this->get('translator')->trans('user.edit.contact.confirmed_phone_error', array(), 'dw_user')];
        if( $code === $user->getConfirmationTokenPhone()) {

            $user->setPhoneVerified(true);
            $user->setConfirmationTokenPhone(null);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $session = $this->get('session');

            if ($session->has('redirect_path')) {
                $referer = $session->get('redirect_path');
                $session->remove('redirect_path');
            }

            $notice = ['type' => 'success', 'msg' => $this->get('translator')->trans('user.edit.contact.confirmed_phone_success', array(), 'dw_user'), 'referer' => $referer];
        }

        return new JsonResponse($notice);

    }

    /**
     *
     * @Route("/ajax-resend-code", name="dw_user_dashboard_profile_ajax_resend_code")
     * @Method({"GET", "POST"})
     *
     */
    public function ajaxResendCodeAction()
    {
        /** @var User $user */
        $user       = $this->getUser();

        $senderSMS  = $this->get('dw_message.sender_sms');
        $result     = $senderSMS->sendCodeConfirmationPhone($user, true);
        $notice     = $senderSMS->getMessageResultConfirmationPhone($result);

        if ($notice['type'] == 'error') {
            $notice['msg'] = $this->get('translator')->trans('user.edit.contact.confirm_phone_error_ajax', array(), 'dw_user');
        }

        return new JsonResponse($notice);

    }

}
