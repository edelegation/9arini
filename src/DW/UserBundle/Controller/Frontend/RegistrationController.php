<?php


namespace DW\UserBundle\Controller\Frontend;

use DW\UserBundle\Entity\User;
use DW\UserBundle\Form\Handler\RegistrationFormHandler;
use DW\UserBundle\Form\Type\LoginFormType;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class RegistrationController
 *
 */
class RegistrationController extends ContainerAware
{

    /**
     * Register user
     *
     * @Route("/register", name="dw_user_register")
     *
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction()
    {
        $session = $this->container->get('session');
        $router = $this->container->get('router');
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            if (!$session->has('profile')) {
                $session->set('profile', 'asker');
            }
            $url = $router->generate('dw_home');

            return new RedirectResponse($url);
        } else {
            $form = $this->container->get('fos_user.registration.form');
            /** @var RegistrationFormHandler $formHandler */
            $formHandler            = $this->container->get('fos_user.registration.form.handler');
            $confirmationEnabled    = $this->container->getParameter('fos_user.registration.confirmation.enabled');
            $request                = $this->container->get('request');
            $templating             = $this->container->get('templating');
            $process                = $formHandler->process($confirmationEnabled);
            if ($process) {
                $user = $form->getData();

                // Return content after success register by call ajax
                if($request->isXmlHttpRequest()){

                    $referer = $router->generate('dw_home');

                    $content = $confirmationEnabled
                        ? $templating->render('DWUserBundle:Frontend/Common:_checkEmail.html.twig',['user' => $user])
                        : $templating->render('DWUserBundle:Frontend/Common:_confirmed.html.twig',['user' => $user]);

                    $session->getFlashBag()->add('success',$content);

                    return new JsonResponse([
                        'referer' => $referer,
                        'success' => true
                    ]);
                }

                $session->getFlashBag()->add(
                    'success',
                    $this->container->get('translator')->trans('user.register.success', array(), 'dw_user')
                );

                $url = $router->generate('dw_user_register_confirmed');

                if ($confirmationEnabled) {
                    $session->set('dw_user_send_confirmation_email/email', $user->getEmail());
                    $url = $router->generate('dw_user_registration_check_email');
                }

                return new RedirectResponse($url);
            }

            // Return form with errors by call ajax
            if($request->isXmlHttpRequest() && $request->isMethod('POST')){

                return  new Response(
                    $templating->render('DWUserBundle:Frontend/Common:_register.html.twig',
                        array('form_registration' => $form->createView())
                    )
                );
            }

            $formLogin = $this->createLoginForm();

            return $templating->renderResponse('DWUserBundle:Frontend/Registration:register.html.twig',
                array('form_login' => $formLogin->createView(), 'form_register' => $form->createView())
            );
        }

    }


    /**
     *  Tell the user to check his email provider
     *
     * @Route("/check-email", name="dw_user_registration_check_email")
     * @Method("GET")
     *
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function checkEmailAction()
    {
        $email = $this->container->get('session')->get('dw_user_send_confirmation_email/email');
        $this->container->get('session')->remove('dw_user_send_confirmation_email/email');
        $user = $this->container->get('dw_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->container->get('templating')->renderResponse(
            'DWUserBundle:Frontend/Registration:checkEmail.html.twig',
            array(
                'user' => $user,
            )
        );
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     *
     * @Route("/register-confirmation/{token}", name="dw_user_register_confirmation")
     * @Method("GET")
     *
     * @param string $token
     *
     *
     * @return null|RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws NotFoundHttpException
     */
    public function confirmAction($token)
    {
        /** @var User $user */
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setLastLogin(new \DateTime());
        $user->setEmailVerified(true);

        /** @var RegistrationFormHandler $formHandler */
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $formHandler->handleRegistration($user);

        $response = new RedirectResponse($this->container->get('router')->generate('dw_user_register_confirmed'));

        return $response;
    }


    /**
     * Tell the user his account is now confirmed
     *
     * @Route("/register-confirmed", name="dw_user_register_confirmed")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws AccessDeniedException
     */
    public function confirmedAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->container->get('templating')->renderResponse(
            'DWUserBundle:Frontend/Registration:confirmed.html.twig',
            array(
                'user' => $user,
            )
        );
    }

    /**
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createLoginForm()
    {
        $form = $this->container->get('form.factory')->createNamed(
            '',
            new LoginFormType(),
            null,
            array(
                'method' => 'POST',
                'action' => $this->container->get('router')->generate('dw_user_login_check'),
            )
        );

        return $form;
    }
}
