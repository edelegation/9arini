<?php


namespace DW\UserBundle\Controller\Frontend;

use DW\UserBundle\Form\Type\LoginFormType;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class SecurityController
 *
 */
class SecurityController extends BaseController
{
    /**
     *
     * @Route("/login", name="dw_user_login")
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        $session = $this->container->get('session');
        $request = $this->container->get('request');
        $form = $this->createLoginForm();
        $formRegister = $this->container->get('fos_user.registration.form');
        $templating = $this->container->get('templating');
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {

            if (!$session->has('profile')) {
                $session->set('profile', 'asker');
            }

            $url = $request->get('redirect_to')
                ? $request->get('redirect_to')
                : $this->container->get('router')->generate('dw_home');

            $response = new RedirectResponse($url);
        } else {
            //$form->handleRequest($request);

            // get the error if any (works with forward and redirect -- see below)
            if ($request->attributes->has(Security::ACCESS_DENIED_ERROR)) {
                $error = $request->attributes->get(Security::ACCESS_DENIED_ERROR);
            } elseif (null !== $session && $session->has(Security::ACCESS_DENIED_ERROR)) {
                $error = $session->get(Security::ACCESS_DENIED_ERROR);
                $session->remove(Security::ACCESS_DENIED_ERROR);
            } elseif ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
                $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
            } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
                $error = $session->get(Security::AUTHENTICATION_ERROR);
                $session->remove(Security::AUTHENTICATION_ERROR);
            } else {
                $error = '';
            }

//            $translator = $this->container->get('translator');
            if ($error) {
                // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
                $error = $error->getMessage();
//                $session->getFlashBag()->add(
//                    'error',
//                    /** @Ignore */
//                    $translator->trans($error, array(), 'dw_user')
//                );
            }
            $response = $templating->renderResponse(
                '@DWUser/Frontend/Security/login.html.twig',
                array(
                    'form_login' => $form->createView(),
                    'form_register' =>$formRegister->createView()
                )
            );
        }

        if($request->isXmlHttpRequest()){
            $response = $templating->render(
                '@DWUser/Frontend/Common/_login.html.twig',
                ['form_login' => $form->createView(), 'error' => $error]
            );

            return new Response($response);
        }


        return $response;
    }

    /**
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createLoginForm()
    {
        $form = $this->container->get('form.factory')->createNamed(
            '',
            new LoginFormType(),
            null,
            array(
                'method' => 'POST',
                'action' => $this->container->get('router')->generate('dw_user_login_check'),
            )
        );

        return $form;
    }


    /**
     * Login check
     *
     * @Route("/login-check", name="dw_user_login_check")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function checkAction()
    {
        throw new \RuntimeException(
            'You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.'
        );
    }

    /**
     * Logout user
     *
     * @Route("/logout", name="dw_user_logout")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     *
     * @Route("/login-register-popup", name="dw_user_login_register_popup")
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginRegisterPopupAction()
    {
        $formLogin = $this->createLoginForm();
        $formRegister = $this->container->get('fos_user.registration.form');

        $response = $this->container->get('templating')->renderResponse(
            '@DWUser/Frontend/Common/_login_register_popup.html.twig',
            array(
                'form_login' => $formLogin->createView(),
                'form_registration' => $formRegister->createView()
            )
        );
        return $response;
    }
}
