<?php

namespace DW\UserBundle\Controller\Frontend;

use DW\CoreBundle\Entity\Listing;
use DW\CoreBundle\Repository\ListingRepository;
use DW\UserBundle\Entity\User;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Controller managing the user profile
 *
 * @Route("/user")
 */
class ProfileController extends ContainerAware
{
    /**
     * Show user profile
     *
     * @Route("/{id}/show", name="dw_user_profile_show", requirements={
     *      "id" = "\d+"
     * })
     * @Method("GET")
     * @ParamConverter("user", class="DWUserBundle:User")
     *
     * @param  Request $request
     * @param  User    $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function showAction(Request $request, User $user,$page = 1)
    {
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $em = $this->container->get('doctrine')->getManager();
        /** @var ListingRepository $listingRepository */
        $listingRepository = $em->getRepository('DWCoreBundle:Listing');
        $userListings = $listingRepository->findByOwner(
            $user->getId(),
            array(Listing::STATUS_PUBLISHED)
        );

        $reviewManager = $this->container->get('dw.review.manager');
        $reviewsReceived = $reviewManager->getUserReviews(null, $user, 'received');

        return $this->container->get('templating')->renderResponse(
            'DWUserBundle:Frontend/Profile:show.html.twig',
            array(
                'user' => $user,
                'user_listings' => $userListings,
                'reviews_received' => $reviewsReceived,
                'pagination' => array(
                    'page' => $page,
                    'pages_count' => ceil($reviewsReceived->count() / 10),
                    'route' => $request->get('_route'),
                    'route_params' => $request->query->all()
                ),
            )
        );
    }


}
