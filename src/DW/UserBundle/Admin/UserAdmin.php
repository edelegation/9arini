<?php

namespace DW\UserBundle\Admin;

use Doctrine\ORM\Query\Expr;
use DW\UserBundle\Form\Type\UserAddressFormType;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;

class UserAdmin extends SonataUserAdmin
{
    protected $baseRoutePattern = 'user';
    protected $translationDomain = 'SonataUserBundle';
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'createdAt'
    );

//    protected $perPageOptions = array(5, 15, 25, 50, 100, 150, 200);


    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /* @var $subject \DW\UserBundle\Entity\User */
        $subject = $this->getSubject();

        $formMapper
            ->with('Profile-1')
            ->add(
                'enabled',
                null,
                array(
                    'required' => false,
                )
            )
            ->add(
                'id',
                null,
                array(
                    'required' => true,
                    'disabled' => true
                )
            )
            ->add(
                'firstName',
                null,
                array(
                    'required' => true,
                    'disabled' => true
                )
            )
            ->add(
                'lastName',
                null,
                array(
                    'required' => true,
                    'disabled' => true
                )
            )
            ->add(
                'email',
                null,
                array(
                    'required' => true,
                    'disabled' => true
                )
            )
            ->add(
                'plainPassword',
                'text',
                array(
                    'required' => (!$subject || is_null($subject->getId()))
                )
            )
            /*->add(
                'motherTongue',
                'language',
                array(
                    'required' => true,
                )
            )*/
            ->end();

        $formMapper->with('Profile-2')
            ->add(
                'description',
                'textarea',array(
                    'required' => false,
            )
            )
            ->add(
                'birthday',
                'birthday',
                array(
                    'format' => 'dd - MMMM - yyyy',
                    'years' => range(date('Y') - 18, date('Y') - 80),
                    'disabled' => true
                )
            )
            ->add(
                'phone',
                null,
                array(
                    'required' => false,
                )
            )
            ->add(
                'phoneSecond',
                null,
                array(
                    'required' => false,
                )
            )
            ->add(
                'addresses',
                'collection',
                array(
                    'type' => new UserAddressFormType(),
                    'entry_options' => array('label' => false),
                    'label' => false,
                    'required' => false
                )
            )
            ->add(
                'nationality',
                'country',
                array(
                    'disabled' => true
                )
            )
            ->add(
                'profession',
                null,
                array(
                    'required' => false
                )
            )
            ->add(
                'namePay',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'bankName',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'agencyName',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'iban',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'annualIncome',
                null,
                array(
                    'required' => false
                )
            )
            ->add(
                'feeAsAsker',//Percent
                'integer',
                array(
                    'attr' => array(
                        'min' => 0,
                        'max' => 100
                    ),
                    'required' => false
                )
            )
            ->add(
                'feeAsOfferer', //Percent
                'integer',
                array(
                    'attr' => array(
                        'min' => 0,
                        'max' => 100
                    ),
                    'required' => false
                )
            )
            ->add(
                'phoneVerified',
                null,
                array(
                    'required' => false
                )
            )
            ->add(
                'emailVerified',
                null,
                array(
                    'required' => false
                )
            )
            ->add(
                'idCardVerified',
                null,
                array(
                    'required' => false
                )
            )->add(
                'diplomaVerified',
                null,
                array(
                    'required' => false
                )
            )->add(
                'cvVerified',
                null,
                array(
                    'required' => false
                )
            )
            ->add(
                'nbBookingsOfferer',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'nbBookingsAsker',
                null,
                array(
                    'required' => false,
                    'disabled' => true
                )
            )
            ->add(
                'createdAt',
                null,
                array(
                    'disabled' => true,
                )
            )
            ->end();

    }

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('show.user.general', array('collapsed' => true) )
                ->add('username')
                ->add('email')
            ->end()

            ->with('show.user.profile')
                ->add('image', null, array(
                    'template' => 'DWUserBundle:Admin:image.html.twig'
                ))
                ->add('firstname')
                ->add('lastname')
                ->add('phone')
                ->add('birthday')
                ->add('cins', null, array(
                    'template' => 'DWUserBundle:Admin:list_cins.html.twig'
                ))
                ->add('cv', null, array(
                    'template' => 'DWUserBundle:Admin:cv.html.twig'
                ))
                ->add('certifications', null, array(
                    'template' => 'DWUserBundle:Admin:list_certifications.html.twig'
                ))
                ->add('languages', null, array(
                    'template' => 'DWUserBundle:Admin:list_languages.html.twig'
                ))
                ->add('nationality', null, array(
                    'template' => 'DWUserBundle:Admin:nationality.html.twig'
                ))

                ->add('addresses', null, array(
                    'template' => 'DWUserBundle:Admin:list_addresses.html.twig'
                ))
                ->add('description')

            ->end()

            ->with('show.user.bank')
                ->add('namePay')
                ->add('bankName')
                ->add('agencyName')
                ->add('iban')
            ->end();


    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier(
                'id',
                null,
                array()
            );

        $listMapper
            ->addIdentifier('fullname')
//            ->add('email')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))

            ->add('feeAsAsker', null, array('editable' => true))
            ->add('feeAsOfferer', null, array('editable' => true))
            ->add('verified', 'boolean',[
                    'label' => 'form.label_verified',
                    'template' => 'DWUserBundle:Admin:verified.html.twig'
                ]
            )

            ->add('listings', null, array('associated_property' => 'getTitle'))
            /*->add('phoneVerified', null, [
                'label' => 'form.label_phoneVerified'
            ])
            ->add('diplomaVerified', null, [
                'label' => 'form.label_diplomaVerified'
            ])
            ->add('idCardVerified', null, [
                'label' => 'form.label_idCardVerified'
            ])
            ->add('cvVerified', null, [
                'label' => 'form.label_cvVerified'
            ])*/
            ->add('userFacebook', null, array('associated_property' => 'getFacebookId'))
            ->add(
                'createdAt',
                null,
                array(
                    'format' => "d/m/Y H:i",
                )
            );

        if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add(
                    'impersonating',
                    'string',
                    array('template' => 'DWSonataAdminBundle::impersonating.html.twig')
                );
        }

        $listMapper->add(
            '_action',
            'actions',
            array(
                'actions' => array(
                    'edit' => array(),
                    'list_user_listings' => array(
                        'template' => 'DWSonataAdminBundle::list_action_list_user_listings.html.twig'
                    )
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add(
                'fullname',
                'doctrine_orm_callback',
                array(
                    'callback' => array($this, 'getFullNameFilter'),
                    'field_type' => 'text',
                    'operator_type' => 'hidden',
                    'operator_options' => array()
                )
            )
            ->add('locked')
            ->add('email')
            ->add('groups');
    }

    public function getFullNameFilter($queryBuilder, $alias, $field, $value)
    {
        if (!$value['value']) {
            return false;
        }

        $exp = new Expr();
        $queryBuilder
            ->andWhere(
                $exp->orX(
                    $exp->like($alias . '.firstName', $exp->literal('%' . $value['value'] . '%')),
                    $exp->like($alias . '.lastName', $exp->literal('%' . $value['value'] . '%')),
                    $exp->like(
                        $exp->concat(
                            $alias . '.firstName',
                            $exp->concat($exp->literal(' '), $alias . '.lastName')
                        ),
                        $exp->literal('%' . $value['value'] . '%')
                    )
                )
            );

        return true;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions["delete"]);

        $label = $this->getConfigurationPool()->getContainer()->get('translator')->trans(
            'action_reset_fees',
            array(),
            'SonataAdminBundle'
        );

        $actions['reset_fees'] = array(
            /** @Ignore */
            'label' => $label,
            'ask_confirmation' => true
        );

        return $actions;
    }

    public function getExportFields()
    {
        $fields = array(
            'Id' => 'id',
            'First name' => 'firstName',
            'Last name' => 'lastName',
            'Email' => 'email',
            'Enabled' => 'enabled',
            'Locked' => 'locked',
            'Created At' => 'createdAt'
        );

        return $fields;
    }

    public function getDataSourceIterator()
    {
        $datagrid = $this->getDatagrid();
        $datagrid->buildPager();

        $dataSourceIt = $this->getModelManager()->getDataSourceIterator($datagrid, $this->getExportFields());
        $dataSourceIt->setDateTimeFormat('d M Y');

        return $dataSourceIt;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }
}
