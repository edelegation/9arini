<?php

namespace DW\UserBundle\Security;

use DW\UserBundle\Model\UserManager;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * OAuthUserProvider
 */
class OAuthUserProvider extends BaseClass
{

    protected $dwUserManager;

    /**
     * Constructor.
     *
     * @param UserManager $dwUserManager
     */
    public function __construct(UserManager $dwUserManager)
    {

        $this->dwUserManager = $dwUserManager;
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $user = $this->dwUserManager->findUserBy(array('username' => $username));

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $user = $this->dwUserManager->checkAndCreateOrUpdateUserByOAuth($response);

        if (!$user instanceof UserInterface) {
            //loadUserByOAuthUserResponse() must return a UserInterface.
            throw new AuthenticationServiceException('Error while logging.');
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === 'DW\\UserBundle\\Entity\\User';
    }
}
