<?php

namespace DW\UserBundle\Listener;

use DW\UserBundle\Entity\User;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ProfileCompleteListener
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * List of routes
     */
    private static $routes = ['dw_listing_new','dw_booking_new'];


    /**
     * Constructor
     *
     * @param TokenStorage $tokenStorage
     * @param Router $router
     * @param Session $session
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TokenStorage $tokenStorage,
        Router $router,
        Session $session,
        TranslatorInterface $translator
    )
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->translator = $translator;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->tokenStorage->getToken()) {
            $this->user = $this->tokenStorage->getToken()->getUser();
            $path = $event->getRequest()->getPathInfo();
            if (is_object($this->user)
                && in_array($event->getRequest()->attributes->get('_route'), self::$routes)
                && HttpKernel::MASTER_REQUEST == $event->getRequestType()
                && !$this->hasCompleteProfile()
            ) {
                $this->session->set('redirect_path', $path);
                $this->session->getFlashBag()->add(
                    'warning',
                    $this->getMessageFlash()
                );
                $event->setResponse(new RedirectResponse($this->router->generate('dw_user_dashboard_profile_edit_contact')));
            }
        }
    }

    /**
     * @return bool Check complete profile user
     */
    private function hasCompleteProfile()
    {
        if($this->hasEmail() && $this->hasPhone()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool Check image user exist
     */
    private function hasImage()
    {
        if ($this->user->getImage() && is_null($this->user->getImage()->getName())) {
            return true;
        }
        return false;
    }

    /**
     * @return bool Check phone user exist
     */
    private function hasPhone()
    {
        return $this->user->getPhone() ? true : false;
    }

    /**
     * @return bool Check email user exist
     */
    private function hasEmail()
    {
        return $this->user->getEmail() ? true : false;
    }

    private function getMessageFlash()
    {
        if (!$this->hasEmail() && !$this->hasPhone()) {
            return $this->translator->trans('user.profile.missing_email_and_phone', array(), 'dw_user');
        } elseif (!$this->hasEmail()) {
            return $this->translator->trans('user.profile.missing_email', array(), 'dw_user');
        } elseif (!$this->hasPhone()) {
            return $this->translator->trans('user.profile.missing_phone', array(), 'dw_user');
        }
    }
}