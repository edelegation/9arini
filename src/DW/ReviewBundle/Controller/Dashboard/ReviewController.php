<?php

namespace DW\ReviewBundle\Controller\Dashboard;

use DW\CoreBundle\Entity\Booking;
use DW\ReviewBundle\Entity\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Review controller.
 *
 * @Route("/review")
 */
class ReviewController extends Controller
{
    /**
     * Creates a new rating for a booking.
     *
     * @Route("/new/{booking_id}", name="dw_dashboard_review_new")
     *
     * @Method({"GET", "POST"})
     * @ParamConverter("booking", class="DW\CoreBundle\Entity\Booking",
     *          options={"id" = "booking_id"})
     * @Security("is_granted('add', booking)")
     *
     * @param  Request $request
     * @param  Booking $booking
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Booking $booking)
    {
        $user = $this->getUser();
        $formHandler = $this->get('dw.form.handler.review');
        $translator = $this->get('translator');

        //Reviews form handling
        $review = $formHandler->create($booking, $user);
        if (!$review) {
            throw new AccessDeniedException('Review already added for this booking by user');
        }
        $form = $this->createCreateForm($review);
        $submitted = $formHandler->process($form);
        if ($submitted !== false) {
            $this->get('session')->getFlashBag()->add(
                'success',
                $translator->trans('review.new.success', array(), 'dw_review')
            );

            return $this->redirect($this->generateUrl('dw_dashboard_reviews_made'));
        }

        return $this->render(
            'DWReviewBundle:Dashboard/Review:new.html.twig',
            array(
                'form' => $form->createView(),
                'booking' => $booking,
                'reviewTo' => $review->getReviewTo()
            )
        );
    }

    /**
     * Creates a form to create a review entity.
     *
     * @param review $review The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Review $review)
    {
        $form = $this->get('form.factory')->createNamed(
            '',
            'review_new',
            $review
        );

        return $form;
    }


    /**
     * List of reviews made by the user
     *
     * @Route("/reviews-made", name="dw_dashboard_reviews_made")
     *
     * @Method({"GET"})
     *
     * @param  Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function madeReviewsAction(Request $request)
    {
        $user = $this->getUser();
        $userType = $request->getSession()->get('profile', 'asker');

        $reviewManager = $this->get('dw.review.manager');
        $madeReviews = $reviewManager->getUserReviews($userType, $user, 'made');
        $unreviewedBookings = $reviewManager->getUnreviewedBookings($userType, $user);
        return $this->render(
            'DWReviewBundle:Dashboard/Review:index.html.twig',
            array(
                'reviews' => $madeReviews,
                'unreviewed_bookings' => $unreviewedBookings,
                'reviews_type' => 'made',
                'user_type' => $userType
            )
        );
    }

    /**
     * List of reviews received to the user user
     *
     * @Route("/reviews-received", name="dw_dashboard_reviews_received")
     *
     * @Method({"GET"})
     *
     * @param  Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function receivedReviewsAction(Request $request)
    {
        $user = $this->getUser();
        $userType = $request->getSession()->get('profile', 'asker');
        $reviewManager = $this->get('dw.review.manager');

        $receivedReviews = $reviewManager->getUserReviews($userType, $user, 'received');
        $unreviewedBookings = $reviewManager->getUnreviewedBookings($userType, $user);
        return $this->render(
            'DWReviewBundle:Dashboard/Review:index.html.twig',
            array(
                'reviews' => $receivedReviews,
                'unreviewed_bookings' => $unreviewedBookings,
                'reviews_type' => 'received',
                'user_type' => $userType
            )
        );
    }
}
