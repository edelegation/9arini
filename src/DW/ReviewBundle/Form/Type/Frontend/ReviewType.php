<?php

namespace DW\ReviewBundle\Form\Type\Frontend;

use DW\ReviewBundle\Entity\Review;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'rating',
                'star_rating',
                array(
                    /** @Ignore */
                    'choices' =>
                        array(
                            "1" => 1,
                            "2" => 2,
                            "3" => 3,
                            "4" => 4,
                            "5" => 5
                        ),
                    'expanded' => true,
                    'multiple' => false,
                    'label' => 'review.form.rating.label',
                    'required' => true,
                    'choices_as_values' => true
                )
            )
            ->add(
                'comment',
                'textarea',
                array(
                    'label' => 'review.form.comment.label',
                    'required' => true,
                )
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'DW\ReviewBundle\Entity\Review',
                'translation_domain' => 'dw_review',
                'cascade_validation' => true,
                'validation_groups' => array('DWReview'),
            )
        );
    }

    /**
     * BC
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'review_new';
    }
}
