<?php

namespace DW\ReviewBundle\Entity;

use DW\ReviewBundle\Model\BaseReview;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Review
 *
 * @ORM\Entity(repositoryClass="DW\ReviewBundle\Repository\ReviewRepository")
 * @ORM\Table(name="review")
 * @UniqueEntity(
 *     fields={"booking", "reviewBy"},
 *     errorPath="booking",
 *      message="entity.review.unique_allowed"
 * )
 * @ORM\EntityListeners({"DW\ReviewBundle\Entity\Listener\ReviewListener" })
 */
class Review extends BaseReview
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer
     */
    private $id;

    public function __construct()
    {

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        $booking = $this->getBooking();

        return $this->getReviewBy()->getName() . " (" . $booking->getListing() . ":" . $booking->getStart()->format(
            'd-m-Y'
        ) . ")";
    }
}
