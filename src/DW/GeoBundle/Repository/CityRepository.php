<?php

namespace DW\GeoBundle\Repository;

use DW\GeoBundle\Entity\City;
use DW\GeoBundle\Entity\Department;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * CityRepository
 *
 */
class CityRepository extends EntityRepository
{
    /**
     * @param string     $name
     * @param Department $department
     * @return City|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByNameAndDepartment($name, $department)
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->addSelect("cg")
            ->leftJoin('c.geocoding', 'cg')
            ->where('c.name = :name')
            ->andWhere('c.department = :department')
            ->setParameter('name', $name)
            ->setParameter('department', $department);

        try {
            return $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    /**
     * @return array|null
     */
    public function findAllCities()
    {
        $queryBuilder = $this->createQueryBuilder('ci')
            ->addSelect("c, cig")
            ->leftJoin('ci.country', 'c')
            ->leftJoin('ci.geocoding', 'cig')
            ->orderBy('ci.name');
        try {
            $query = $queryBuilder->getQuery();

            return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return null;
        }
    }
}
