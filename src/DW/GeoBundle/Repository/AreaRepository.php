<?php

namespace DW\GeoBundle\Repository;

use DW\GeoBundle\Entity\Area;
use DW\GeoBundle\Entity\Country;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class AreaRepository extends EntityRepository
{
    /**
     * @param string  $name
     * @param Country $country
     * @return Area|null
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByNameAndCountry($name, $country)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->addSelect("ag")
            ->leftJoin('a.geocoding', 'ag')
            ->where('a.name = :name')
            ->andWhere('a.country = :country')
            ->setParameter('name', $name)
            ->setParameter('country', $country);
        try {
            return $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return array|null
     */
    public function findAllAreas()
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->addSelect("c, ag")
            ->leftJoin('a.country', 'c')
            ->leftJoin('a.geocoding', 'ag')
            ->orderBy('a.name');
        try {
            $query = $queryBuilder->getQuery();

            return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        } catch (NoResultException $e) {
            return null;
        }
    }

}
