<?php

namespace DW\GeoBundle\Controller;

use DW\CoreBundle\Entity\Listing;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Listing controller.
 *
 * @Route("/geocoding")
 */
class GeocodingController extends Controller
{
    /**
     * Add new geocoding informations to existing geographical entities
     *
     * @Route("/{slug}/create", name="dw_geo_create", requirements={
     *      "slug" = "[a-z0-9-]+$"
     * })
     * @Method("POST")
     * @ParamConverter("listing", class="DW\CoreBundle\Entity\Listing", options={"repository_method" = "findOneBySlug"})
     *
     * @param Request $request
     * @param Listing $listing
     *
     * @return Response
     */
    public function createAction(Request $request, Listing $listing)
    {
        $type = $request->request->get('type');
        $geocoding = $request->request->get('geocoding');

        $this->get('dw_geo.geocoding.manager')->createGeocoding($listing, $type, $geocoding);

        return new Response('true');
    }
}
