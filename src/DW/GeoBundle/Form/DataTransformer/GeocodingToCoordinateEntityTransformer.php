<?php

namespace DW\GeoBundle\Form\DataTransformer;

use DW\GeoBundle\Entity\Area;
use DW\GeoBundle\Entity\City;
use DW\GeoBundle\Entity\Coordinate;
use DW\GeoBundle\Entity\Country;
use DW\GeoBundle\Entity\Department;
use DW\GeoBundle\Geocoder\Provider\GoogleMapsProvider;
use Doctrine\Common\Persistence\ObjectManager;
use Geocoder\Exception\NoResult;
use Ivory\HttpAdapter\CurlHttpAdapter;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class GeocodingToCoordinateEntityTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;
    private $googlePlaceAPIKey;

    /**
     * @var bool
     */
    const DEBUG = false;

    /**
     * @param ObjectManager $om
     * @param string        $googlePlaceAPIKey
     */
    public function __construct(ObjectManager $om, $googlePlaceAPIKey = null)
    {
        $this->om = $om;
        $this->googlePlaceAPIKey = $googlePlaceAPIKey ? $googlePlaceAPIKey : null;
    }

    /**
     * Transforms an object (coordinate) to a string (number).
     *
     * @param  Coordinate|null $coordinate
     * @return string
     */
    public function transform($coordinate)
    {
        if (null === $coordinate) {
            return "";
        }

        return "";
    }

    /**
     * Transforms a JSON string (geocoding) to an object (coordinate).
     *
     * @param  string $geocodingI18n json Geocoding in multi languages
     * @return Coordinate|null
     * @throws TransformationFailedException if object (coordinate) is not found.
     */
    public function reverseTransform($geocodingI18n)
    {
        if (!$geocodingI18n) {
            throw new TransformationFailedException();
        }
        $this->debug("reverseTransform > geocodingI18n :\n" . print_r($geocodingI18n, 1));

        // wrap numbers
        $geocodingI18n = preg_replace('/:\s*(\-?\d+(\.\d+)?([e|E][\-|\+]\d+)?)/', ': "$1"', $geocodingI18n);
        $geocodingI18n = json_decode($geocodingI18n);

        $this->debug("reverseTransform > geocodingI18n JSON decoded :\n" . print_r($geocodingI18n, 1));

        if (!$geocodingI18n->{'fr'}) {
            $this->debug("reverseTransform > Error: No geocodingI18n locale.\n");
            throw new TransformationFailedException();
        }

        $lat = $geocodingI18n->lat;
        $lng = $geocodingI18n->lng;

        //Geocoding server side
        $geocodingI18nServer = $this->getGeocodingServer(
            $geocodingI18n->{'fr'}->country_short,
            $lat,
            $lng
        );

        //Success? The server data are used.
        if ($geocodingI18nServer) {
            $geocodingI18n = $geocodingI18nServer;
            $this->debug("reverseTransform > geocodingI18n server done :\n" . print_r($geocodingI18n, 1));
        }

        //Current Locale Geocoding
        $geocoding = $this->getGeocoding($geocodingI18n);
        if (!$geocoding) {
            $this->debug("reverseTransform > Error: No geocoding.");
            throw new TransformationFailedException();
        }

        //JSON and DB lat and lng must be compared with the same precision:
        //ex : 48.869782|2.3508079000000635 (JSON) != 48.8697820|2.3508079 (DB)
        $coordinate = $this->om->getRepository('DWGeoBundle:Coordinate')->findOneBy(
            array(
                'lat' => number_format($lat, 7, '.', ''),
                'lng' => number_format($lng, 7, '.', ''),
            )
        );

        $this->debug("reverseTransform > LatLng:\n" . $lat . "--" . $lng);
//        die();

        if (null === $coordinate) {
            try {
                $route = (isset($geocoding->route) && $geocoding->route) ? $geocoding->route : 'noroute';
                $streetNumber = (isset($geocoding->street_number) && $geocoding->street_number) ?
                    $geocoding->street_number : 'nostreetnumber';
                $zip = (isset($geocoding->postal_code) && $geocoding->postal_code) ? $geocoding->postal_code :
                    (isset($geocoding->administrative_area_level_2_short) && $geocoding->administrative_area_level_2_short ?
                        $geocoding->administrative_area_level_2_short : '');

                //Coordinate
                $coordinate = new Coordinate();
                $coordinate->setLat($lat);
                $coordinate->setLng($lng);
                $coordinate->setRoute($route);
                $coordinate->setStreetNumber($streetNumber);
                $coordinate->setZip($zip);
                $coordinate->setAddress($geocodingI18n->formatted_address);
                $geographicalEntities = $this->getGeographicalEntities($geocodingI18n);
                $coordinate->setCountry($geographicalEntities["country"]);
                $coordinate->setArea($geographicalEntities["area"]);
                $coordinate->setDepartment($geographicalEntities["department"]);
                $coordinate->setCity($geographicalEntities["city"]);

            } catch (TransformationFailedException $e) {
                throw new TransformationFailedException();
            }

        }

        return $coordinate;
    }

    /**
     *
     * Get geocoding server datas
     *
     * @param $region
     * @param $lat
     * @param $lng
     * @return bool|mixed
     */
    private function getGeocodingServer($region, $lat, $lng)
    {
        //Server geocoding. If we can we use it instead of client geocoding
        try {
            $geocodingsServer = $geocodingI18nServer = array();
            //Geocoding for each locale

                $geocoder = new GoogleMapsProvider(
                    new CurlHttpAdapter(), 'fr', $region, true, $this->googlePlaceAPIKey
                );
                $geocoder->limit(1);
                $geocodingsServer['fr'] = $geocoder->reverse($lat, $lng);


            //Move current locale to end of array. The current locale is the reference.
            $localeGeocoding = $geocodingsServer['fr'];
            unset($geocodingsServer['fr']);
            $geocodingsServer['fr'] = $localeGeocoding;

            $this->debug("getGeocodingServer > geocodingsServer:\n" . print_r($geocodingsServer, 1));

            //Merge them
            foreach ($geocodingsServer as $geocodingServer) {
                $geocodingI18nServer = array_merge($geocodingI18nServer, $geocodingServer);
            }

        } catch (NoResult $e) {
            $this->debug("getGeocodingServer NoResult Error:\n" . $e->getMessage());
            throw new TransformationFailedException();
        }

        //Replace the client geocoding by the server one
        if ($geocodingI18nServer) {
            return json_decode(json_encode($geocodingI18nServer));
        } else {
            return false;
        }
    }

    /**
     * Get and/or set Geographical Entities from geocoding information
     *
     * @param object $geocodingI18n
     *
     * @return array
     */
    private function getGeographicalEntities($geocodingI18n)
    {
        //Translations
        $countries = $areas = $departments = $cities = array();
            $geoLocale = $geocodingI18n->{'fr'};
            //Country
            $countries['fr'] = $geoLocale->country;
            //Area
            $areas['fr'] =
                (isset($geoLocale->administrative_area_level_1) && $geoLocale->administrative_area_level_1) ?
                    $geoLocale->administrative_area_level_1 :
                    (
                    (isset($geoLocale->administrative_area_level_2) && $geoLocale->administrative_area_level_2) ?
                        $geoLocale->administrative_area_level_2 : 'noarea'
                    );
            //Department
            $departments['fr'] = (
                isset($geoLocale->administrative_area_level_2) && $geoLocale->administrative_area_level_2) ?
                $geoLocale->administrative_area_level_2 : 'nodepartment';

            //City
            $cities['fr'] = (
                isset($geoLocale->locality) && $geoLocale->locality) ? $geoLocale->locality :
                (
                (isset($geoLocale->postal_town) && $geoLocale->postal_town) ? $geoLocale->postal_town : 'nozip'
                );


        $country = $this->getOrCreateCountry(
            $geocodingI18n->{'fr'}->country_short,
            $countries
        );

        $area = $this->getOrCreateArea(
            $areas,
            $country
        );
        $department = $this->getOrCreateDepartment(
            $departments,
            $area
        );

        $city = $this->getOrCreateCity(
            $cities,
            $department
        );

        return array(
            "country" => $country,
            "area" => $area,
            "department" => $department,
            "city" => $city,
        );

    }

    /**
     * Verify if a geocoding is valid and return geocoding for current locale
     *
     * @param  string $geocodingI18n (json)
     * @return bool|object
     */
    private function getGeocoding($geocodingI18n)
    {
        //Geocoding in current locale
        if (!isset($geocodingI18n->{'fr'})) {
            return false;
        }
        $geocoding = $geocodingI18n->{'fr'};
        //die(print_r($geocoding, 1));
        if (!isset($geocodingI18n->lat) || !isset($geocodingI18n->lng) ||
            !isset($geocoding->country_short) || !isset($geocodingI18n->formatted_address) ||
            (
                !isset($geocoding->administrative_area_level_1) && !isset($geocoding->administrative_area_level_2)
            )
        ) {
            return false;
        }

        return $geocoding;
    }

    /**
     * Get or create and translate country
     *
     * @param  string $code
     * @param  array  $names
     * @return Country
     */
    private function getOrCreateCountry($code, $names)
    {
        $country = $this->om->getRepository('DWGeoBundle:Country')->findOneBy(
            array(
                'code' => $code
            )
        );

        if (null === $country) {
            $country = new Country();
            $country->setCode($code);
            foreach ($names as $lang => $name) {
                $country->setName($name);
            }

            $this->om->persist($country);
        }

        return $country;
    }

    /**
     * Get or create and translate area
     *
     * @param  array   $names
     * @param  Country $country
     * @return Area|mixed|null
     */
    private function getOrCreateArea($names, $country)
    {
        $area = null;
        if ($country->getId()) {
            $area = $this->om->getRepository('DWGeoBundle:Area')->findOneByNameAndCountry(
                $names['fr'],
                $country
            );
        }

        if (null === $area) {
            $area = new Area();
            $area->setCountry($country);
            foreach ($names as $lang => $name) {
                $area->setName($name);
            }

            $this->om->persist($area);
        }

        return $area;
    }

    /**
     * Get or create and translate department
     *
     * @param  array $names
     * @param  Area  $area
     * @return mixed
     */
    private function getOrCreateDepartment($names, $area)
    {
        $department = null;
        if ($area->getId()) {
            $department = $this->om->getRepository('DWGeoBundle:Department')->findOneByNameAndArea(
                $names['fr'],
                $area
            );
        }

        if (null === $department) {
            $department = new Department();
            $department->setCountry($area->getCountry());
            $department->setArea($area);
            foreach ($names as $lang => $name) {
                $department->setName($name);
            }

            $this->om->persist($department);
        }

        return $department;
    }

    /**
     * Get or create and translate city
     *
     * @param  array      $names
     * @param  Department $department
     * @return City
     */
    private function getOrCreateCity($names, $department)
    {
        $city = null;
        if ($department->getId()) {
            $city = $this->om->getRepository('DWGeoBundle:City')->findOneByNameAndDepartment(
                $names['fr'],
                $department
            );
        }

        if (null === $city) {
            $city = new City();
            $city->setCountry($department->getCountry());
            $city->setArea($department->getArea());
            $city->setDepartment($department);
            foreach ($names as $lang => $name) {
                $city->setName($name);
            }
            $this->om->persist($city);
        }

        return $city;
    }

    /**
     * @param $message
     */
    private function debug($message)
    {
        if (self::DEBUG) {
            echo nl2br($message) . "<br>";
        }
    }
}
