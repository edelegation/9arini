<?php

namespace DW\SonataAdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DWSonataAdminBundle extends Bundle
{

    public function getParent()
    {
        return 'SonataAdminBundle';
    }

}
